﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" TEXT NOT NULL CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY,
    "ProductVersion" TEXT NOT NULL
);

BEGIN TRANSACTION;

CREATE TABLE "sgr_datacategoryitem" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_datacategoryitem" PRIMARY KEY,

    -- 字典分类标识
    "m_categorytypecode" TEXT NOT NULL,

    -- 字典项名称
    "m_dcitemname" TEXT NOT NULL,

    -- 字典项值
    "m_dcitemvalue" TEXT NOT NULL,

    -- 备注
    "m_remarks" TEXT NULL,

    -- 是否可编辑
    "m_iseditable" INTEGER NOT NULL,

    -- 排序号
    "m_ordernumber" INTEGER NOT NULL,

    -- 组织机构状态
    "m_state" INTEGER NOT NULL,

    -- 上级节点Id
    "sgr_parentid" INTEGER NOT NULL,

    -- 树节点层次目录
    "sgr_nodepath" TEXT NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL
);

CREATE TABLE "sgr_department" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_department" PRIMARY KEY,

    -- 部门编码
    "m_code" TEXT NOT NULL,

    -- 部门名称
    "m_name" TEXT NOT NULL,

    -- 组织排序号
    "m_ordernumber" INTEGER NOT NULL,

    -- 负责人
    "m_leader" TEXT NULL,

    -- 联系电话
    "m_phone" TEXT NULL,

    -- 联系邮箱
    "m_email" TEXT NULL,

    -- 描述
    "m_remarks" TEXT NULL,

    -- 部门状态
    "m_state" INTEGER NOT NULL,

    -- 上级节点Id
    "sgr_parentid" INTEGER NOT NULL,

    -- 树节点层次目录
    "sgr_nodepath" TEXT NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL
);

CREATE TABLE "sgr_duty" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_duty" PRIMARY KEY,

    -- 职务编码
    "m_code" TEXT NOT NULL,

    -- 职务名称
    "m_name" TEXT NOT NULL,

    -- 排序号
    "m_ordernumber" INTEGER NOT NULL,

    -- 职务备注
    "m_remarks" TEXT NULL,

    -- 职务状态
    "m_state" INTEGER NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL
);

CREATE TABLE "sgr_loglogin" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_loglogin" PRIMARY KEY,

    -- 登录账号
    "m_loginname" TEXT NOT NULL,

    -- 用户姓名
    "m_username" TEXT NULL,

    -- 登录时间
    "m_logintime" TEXT NOT NULL,

    -- 登录Ip地址
    "m_ipaddress" TEXT NULL,

    -- 登录地点
    "m_location" TEXT NULL,

    -- 登录途径
    "m_loginway" TEXT NULL,

    "m_loginprovider" TEXT NULL,

    "m_providerkey" TEXT NULL,

    "m_providerdisplayname" TEXT NULL,

    -- 客户端浏览器
    "m_clientbrowser" TEXT NULL,

    -- 客户端系统
    "m_clientos" TEXT NULL,

    -- 登录状态
    "m_status" INTEGER NOT NULL,

    -- 登录描述
    "m_remark" TEXT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL
);

CREATE TABLE "sgr_logoperate" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_logoperate" PRIMARY KEY,

    -- 登录账号
    "m_loginname" TEXT NOT NULL,

    -- 用户姓名
    "m_username" TEXT NOT NULL,

    -- 登录Ip地址
    "m_ipaddress" TEXT NULL,

    -- 登录地点
    "m_location" TEXT NULL,

    -- 客户端浏览器
    "m_clientbrowser" TEXT NULL,

    -- 客户端系统
    "m_clientos" TEXT NULL,

    -- 登录途径
    "m_operateway" TEXT NULL,

    -- 请求说明
    "m_requestdescription" TEXT NOT NULL,

    -- 请求地址
    "m_requesturl" TEXT NOT NULL,

    -- 请求方法
    "m_httpmethod" TEXT NOT NULL,

    -- 请求参数
    "m_requestparam" TEXT NOT NULL,

    -- 请求时间
    "m_requesttime" TEXT NOT NULL,

    -- 请求耗时
    "m_requestduration" INTEGER NULL,

    -- 请求结果
    "m_status" INTEGER NOT NULL,

    -- 请求结果描述
    "m_remark" TEXT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL
);

CREATE TABLE "sgr_organization" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_organization" PRIMARY KEY,

    -- 组织机构名称
    "m_name" TEXT NOT NULL,

    -- 人员规模编码
    "m_staffsizecode" TEXT NOT NULL,

    -- 描述
    "m_remarks" TEXT NULL,

    -- Logo地址
    "m_logourl" TEXT NULL,

    -- 组织机构编码
    "m_code" TEXT NOT NULL,

    -- 组织机构类型编码
    "m_orgtypecode" TEXT NOT NULL,

    -- 所属行政区划编码
    "m_areacode" TEXT NOT NULL,

    -- 机构负责人
    "m_leader" TEXT NULL,

    -- 联系电话
    "m_phone" TEXT NULL,

    -- 联系邮箱
    "m_email" TEXT NULL,

    -- 所在地址
    "m_address" TEXT NULL,

    -- 营业执照路径
    "m_businesslicensepath" TEXT NULL,

    -- 组织机构认证状态
    "m_confirmed" INTEGER NOT NULL,

    -- 组织机构排序号
    "m_ordernumber" INTEGER NOT NULL,

    -- 组织机构状态
    "m_state" INTEGER NOT NULL,

    -- 上级节点Id
    "sgr_parentid" INTEGER NOT NULL,

    -- 树节点层次目录
    "sgr_nodepath" TEXT NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 扩展对象/实体
    "sgr_extensiondata" TEXT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL,

    -- 删除的用户ID
    "sgr_deleteruserid" INTEGER NOT NULL,

    -- 删除时间
    "sgr_deletiontime" TEXT NULL,

    -- 是否已经被软删除
    "sgr_isdeleted" INTEGER NOT NULL
);

CREATE TABLE "sgr_role" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_role" PRIMARY KEY,

    -- 角色编码
    "m_code" TEXT NOT NULL,

    -- 角色名称
    "m_rolename" TEXT NOT NULL,

    -- 排序号
    "m_ordernumber" INTEGER NOT NULL,

    -- 备注
    "m_remarks" TEXT NULL,

    -- 状态
    "m_state" INTEGER NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL
);

CREATE TABLE "sgr_user" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_user" PRIMARY KEY,

    -- 登录名称
    "m_loginname" TEXT NOT NULL,

    -- 登录密码
    "m_loginpassword" TEXT NOT NULL,

    -- 首次登录时间
    "m_firstlogintime" TEXT NULL,

    -- 最近一次登录时间
    "m_lastlogintime" TEXT NULL,

    -- 登录成功次数
    "m_loginsuccesscount" INTEGER NOT NULL,

    -- 连续登录失败次数
    "m_failedloginattempts" INTEGER NOT NULL,

    -- 禁止登录的截止时间
    "m_cannotloginuntilutc" TEXT NULL,

    -- 用户姓名
    "m_username" TEXT NULL,

    -- 用户绑定的手机号码
    "m_userphone" TEXT NULL,

    -- 用户邮箱地址
    "m_useremail" TEXT NULL,

    -- 用户QQ号码
    "m_qq" TEXT NULL,

    -- 用户微信号码
    "m_wechat" TEXT NULL,

    "m_issuperadmin" INTEGER NOT NULL,

    -- 状态
    "m_state" INTEGER NOT NULL,

    -- 所属部门Id
    "m_departmentid" INTEGER NULL,

    -- 所属部门名称
    "m_departmentname" TEXT NOT NULL,

    -- 行版本
    "sgr_rowversion" INTEGER NOT NULL,

    -- 所在组织ID
    "sgr_orgid" INTEGER NOT NULL,

    -- 创建的用户ID
    "sgr_creatoruserid" INTEGER NOT NULL,

    -- 创建时间
    "sgr_creationtime" TEXT NOT NULL,

    -- 修改的用户ID
    "sgr_lastmodifieruserid" INTEGER NOT NULL,

    -- 修改时间
    "sgr_lastmodificationtime" TEXT NULL,

    -- 删除的用户ID
    "sgr_deleteruserid" INTEGER NOT NULL,

    -- 删除时间
    "sgr_deletiontime" TEXT NULL,

    -- 是否已经被软删除
    "sgr_isdeleted" INTEGER NOT NULL
);

CREATE TABLE "sgr_user_refreshtoken" (
    -- 主键
    "sgr_id" INTEGER NOT NULL CONSTRAINT "PK_sgr_user_refreshtoken" PRIMARY KEY,

    -- 令牌内容
    "m_token" TEXT NOT NULL,

    -- 失效时间
    "m_expires" TEXT NOT NULL,

    -- 用户标识
    "m_userid" INTEGER NOT NULL
);

CREATE TABLE "sgr_role_resource" (
    -- 主键
    "sgr_id" TEXT NOT NULL CONSTRAINT "PK_sgr_role_resource" PRIMARY KEY,

    -- 资源标识
    "m_resourcecode" TEXT NOT NULL,

    -- 资源类型
    "m_resourcetype" INTEGER NOT NULL,

    -- 角色标识
    "m_roleid" INTEGER NOT NULL,
    CONSTRAINT "FK_sgr_role_resource_sgr_role_m_roleid" FOREIGN KEY ("m_roleid") REFERENCES "sgr_role" ("sgr_id") ON DELETE CASCADE
);

CREATE TABLE "sgr_user_duty" (
    -- 主键
    "sgr_id" TEXT NOT NULL CONSTRAINT "PK_sgr_user_duty" PRIMARY KEY,

    -- 岗位标识
    "m_dutyid" INTEGER NOT NULL,

    -- 用户标识
    "m_userid" INTEGER NOT NULL,
    CONSTRAINT "FK_sgr_user_duty_sgr_user_m_userid" FOREIGN KEY ("m_userid") REFERENCES "sgr_user" ("sgr_id") ON DELETE CASCADE
);

CREATE TABLE "sgr_user_role" (
    -- 主键
    "sgr_id" TEXT NOT NULL CONSTRAINT "PK_sgr_user_role" PRIMARY KEY,

    -- 角色标识
    "m_roleid" INTEGER NOT NULL,

    -- 用户标识
    "m_userid" INTEGER NOT NULL,
    CONSTRAINT "FK_sgr_user_role_sgr_user_m_userid" FOREIGN KEY ("m_userid") REFERENCES "sgr_user" ("sgr_id") ON DELETE CASCADE
);

CREATE INDEX "IX_sgr_datacategoryitem_m_categorytypecode" ON "sgr_datacategoryitem" ("m_categorytypecode");

CREATE INDEX "IX_sgr_datacategoryitem_sgr_nodepath" ON "sgr_datacategoryitem" ("sgr_nodepath");

CREATE INDEX "IX_sgr_datacategoryitem_sgr_parentid" ON "sgr_datacategoryitem" ("sgr_parentid");

CREATE INDEX "IX_sgr_department_sgr_nodepath" ON "sgr_department" ("sgr_nodepath");

CREATE INDEX "IX_sgr_department_sgr_orgid" ON "sgr_department" ("sgr_orgid");

CREATE INDEX "IX_sgr_department_sgr_parentid" ON "sgr_department" ("sgr_parentid");

CREATE INDEX "IX_sgr_duty_sgr_orgid" ON "sgr_duty" ("sgr_orgid");

CREATE INDEX "IX_sgr_loglogin_sgr_orgid" ON "sgr_loglogin" ("sgr_orgid");

CREATE INDEX "IX_sgr_logoperate_sgr_orgid" ON "sgr_logoperate" ("sgr_orgid");

CREATE INDEX "IX_sgr_organization_m_code" ON "sgr_organization" ("m_code");

CREATE INDEX "IX_sgr_organization_sgr_nodepath" ON "sgr_organization" ("sgr_nodepath");

CREATE INDEX "IX_sgr_organization_sgr_parentid" ON "sgr_organization" ("sgr_parentid");

CREATE INDEX "IX_sgr_role_sgr_orgid" ON "sgr_role" ("sgr_orgid");

CREATE INDEX "IX_sgr_role_resource_m_roleid" ON "sgr_role_resource" ("m_roleid");

CREATE INDEX "IX_sgr_user_m_departmentid" ON "sgr_user" ("m_departmentid");

CREATE INDEX "IX_sgr_user_m_loginname" ON "sgr_user" ("m_loginname");

CREATE INDEX "IX_sgr_user_sgr_orgid" ON "sgr_user" ("sgr_orgid");

CREATE INDEX "IX_sgr_user_duty_m_userid" ON "sgr_user_duty" ("m_userid");

CREATE INDEX "IX_sgr_user_refreshtoken_m_token" ON "sgr_user_refreshtoken" ("m_token");

CREATE INDEX "IX_sgr_user_refreshtoken_m_userid" ON "sgr_user_refreshtoken" ("m_userid");

CREATE INDEX "IX_sgr_user_role_m_userid" ON "sgr_user_role" ("m_userid");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20240326160946_Sgr0327', '8.0.3');

COMMIT;

