﻿CREATE TABLE `gen_id_generator` (
  `id` varchar(128)  NOT NULL DEFAULT '',
  `max_id` bigint(20) NOT NULL DEFAULT '1',
  `step` int(11) NOT NULL,
  `description` varchar(256)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

insert into gen_id_generator(id, max_id, step, description) values('sgr-segment', 1, 2000, 'Common Segment Mode Get Id')；