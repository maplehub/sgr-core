# 审计日志

## 功能描述

审计日志是后台管理系统中至关重要的功能模块，用于记录和跟踪系统中的各种操作事件。通过审计日志，管理员能够监控系统活动、追踪异常行为、排查问题以及满足合规性要求。

**1. 记录操作事件**

系统自动记录所有用户的操作行为，包括但不限于：

- 用户登录与登出：记录登录时间、IP地址、登录设备等信息。 【该部分功能与Sgr.Identity集成在一起，此处不做过多说明】
- 数据访问：记录用户访问的数据类型、时间和详情
- 数据修改：记录用户对数据的增、删、改操作，包括修改前后的数据内容。

**2. 记录内容**

- 日志ID：唯一标识一条日志记录。
- 操作用户：执行操作的用户账号和姓名。
- 请求地址
- 请求方法
- 请求说明
- 请求参数
- 请求时间
- 请求耗时（毫秒）
- 请求结果(成功，失败)
- 请求Ip地址
- 请求地点
- 请求客户端浏览器
- 请求客户端系统
- 请求途径（平台、小程序、APP）

**3. 审计日志查询**

提供多种查询条件，允许管理员根据需要筛选日志记录，包括：

- 时间范围：按天、周、月或自定义时间段查询日志。
- 用户：按用户名或用户ID筛选日志。
- 请求类型：筛选特定类型的操作。
- 请求模块：筛选特定模块的操作日志。
- 请求结果：筛选成功或失败的日志记录。

## 使用方法

**1. 注册并配置审计日志相关服务**

```
var builder = WebApplication.CreateBuilder(args);
...
// 是否记录完整日志，一般默认False即可。完整日志在基础日志的基础上增加了请求参数、请求来源、客户端操作系统、客户端浏览器四项信息。
bool isAuditFull = false; 
builder.Services.AddSgrAuditLog(applicationOptions.IsAuditFull);
```

**2. 启用审计日志相关处理管道**

```
var builder = WebApplication.CreateBuilder(args);

...

//启用并配置审计日志拦截器
Action<IAuditLogFilterOptions>? AuditLogFilterOptionsConfigure;	// 审计日志拦截器选项
app.UseSgrAuditLogFilters(applicationOptions.AuditLogFilterOptionsConfigure);

//启用并配置审计日志中间件
bool EnableAuditLogMiddleware = false;	//是否启用审计日志中间件
Action<IAuditLogMiddlewareOptions>? AuditLogMiddlewareOptionsConfigure;	//审计日志中间件参数配置
if (EnableAuditLogMiddleware)
    app.UseSgrAuditLog(AuditLogMiddlewareOptionsConfigure);

```

**3. 使用审计日志**

方案一：程序中不启用审计日志中间件，仅在需要添加审计日志的方法或类前添加审计日志拦截器即可：

- Action方法前使用： [AuditLogActionFilter("创建数据字典项")]
- Page类前使用：     [AuditLogPageFilter("数据字典管理")]

```
[Route("api/v1/sgr/[controller]")]
[ApiController]
public class DataCategoryController : ControllerBase
{
    /// <summary>
    /// 创建数据字典项
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost]
    [AuditLogActionFilter("创建数据字典项")]
    public async Task<bool> CreateAsync([FromBody] CreateCategpryItemCommand command)
    {
		...
    }
}
```

方案二：程序中不启用审计日志中间件，对所有请求进行全面审计，仅在个别特殊请求方法或类前添加禁用审计标记即可。

- Action方法前使用： [UnAuditLogActionFilter("创建数据字典项")]
- Page类前使用：     [UnAuditLogPageFilter("数据字典管理")]

