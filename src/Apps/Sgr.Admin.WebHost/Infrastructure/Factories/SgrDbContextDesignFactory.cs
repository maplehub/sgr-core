﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Sgr.Data;
using Sgr.EntityFrameworkCore;
using Sgr.MediatR;
using SkiaSharp;
using System.IO;

namespace Sgr.Admin.WebHost.Infrastructure.Factories
{
    public class SgrDbContextDesignFactory : IDesignTimeDbContextFactory<SgrDbContext>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public SgrDbContext CreateDbContext(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();

            var config = new ConfigurationBuilder()
               .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
               .AddJsonFile("appsettings.json")
               .AddEnvironmentVariables()
               .Build();

            string dbType = config.GetRequiredString("Sgr:DataConnectionStrings:DatabaseType").ToUpper();
            string connectionString = config.GetRequiredString("Sgr:DataConnectionStrings:ConnectionString");

            if (dbType == "SQLITE")
            {
                if (string.IsNullOrEmpty(connectionString))
                    connectionString = DefaultDataConnectionStringProvider.GetDefaultSqliteDatabaseConnectionString();

                var optionsBuilder = new DbContextOptionsBuilder<SgrDbContext>();
                optionsBuilder.UseSqlite(connectionString, (builder) =>
                {
                    builder.MigrationsAssembly("Sgr.Admin.WebHost");
                });

                return new SgrDbContext(optionsBuilder.Options, new NoMediator());
            }
            else
            {
                var optionsBuilder = new DbContextOptionsBuilder<SgrDbContext>();
                optionsBuilder.UseMySql(
                    connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    (builder) =>
                    {
                        builder.MigrationsAssembly("Sgr.Admin.WebHost");
                    });

                return new SgrDbContext(optionsBuilder.Options, new NoMediator());
            }

            //if (dbType == "SQLITE")
            //{
            //    if (string.IsNullOrEmpty(connectionString))
            //        connectionString = DefaultDataConnectionStringProvider.GetDefaultSqliteDatabaseConnectionString();

            //    var dbContextFactory = new SgrSqliteDbContextFactory();
            //    return dbContextFactory.CreateSqrDbContext(connectionString, new NoMediator(), (builder) =>
            //    {
            //        builder.MigrationsAssembly("Sgr.Admin.WebHost");
            //        builder.MigrationsAssembly("Sgr.Data.EntityFrameworkCore.Sqlite");
            //    });
            //}
            //else
            //{
            //    var dbContextFactory = new SgrMysqlDbContextFactory();
            //    return dbContextFactory.CreateSqrDbContext(connectionString, new NoMediator(), (builder) =>
            //    {
            //        builder.MigrationsAssembly("Sgr.Admin.WebHost");
            //        builder.MigrationsAssembly("Sgr.Data.EntityFrameworkCore.Mysql");
            //    });
            //}
        }
    }
}