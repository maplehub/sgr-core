using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Sgr.Admin.WebHost.Extensions;
using Sgr.Data;
using Sgr.MediatR.Behaviors;
using Sgr.Utilities;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Sgr.Admin.WebHost
{
    /// <summary>
    ///
    /// </summary>
    public class Program
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            //设置日志
            builder.Logging.ClearProviders();
            //builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
            builder.Host.UseNLogWeb();

            builder.Services.AddDbContexts(builder.Configuration);
            builder.Services.AddSgrWeb(builder.Configuration, builder.Environment);

            builder.Services.AddMediatR(cfg =>
            {
                cfg.RegisterServicesFromAssemblyContaining(typeof(Program));
#if DEBUG
                cfg.AddOpenBehavior(typeof(LoggingBehavior<,>));
#endif
                cfg.AddOpenBehavior(typeof(ValidatorBehavior<,>));
                cfg.AddOpenBehavior(typeof(TransactionBehavior<,>));
            });

            builder.Services.AddControllers().AddJsonOptions(options => { JsonHelper.UpdateJsonSerializerOptions(options.JsonSerializerOptions); });
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSgrAuthentication(builder.Configuration, true);

            var xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.AllDirectories);
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("sgr", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "SGR API",
                    Description = "An ASP.NET Core Web API for SGR",
                    TermsOfService = new Uri("https://github.com/fengqinhua/sgr"),
                    Contact = new OpenApiContact { Name = "Mapleleaf", Email = "fengqinhua2016@163.com" },
                    License = new OpenApiLicense { Name = "MIT License", Url = new Uri("https://gitee.com/maplehub/sgr-core/blob/master/LICENSE") }
                });

                options.AddSgrAuthenticationSupport();

                foreach (var xmlFile in xmlFiles)
                {
                    if (Path.GetFileName(xmlFile).StartsWith("Sgr")) // 根据需要过滤
                        options.IncludeXmlComments(xmlFile);
                }
            });

            var app = builder.Build();

            //// Configure the HTTP request pipeline.
            //if (!app.Environment.IsDevelopment())
            //{
            //    app.UseExceptionHandler("/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}

            app.UseSgrWeb();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger(options =>
                {
                    options.RouteTemplate = "docs/{documentName}/swagger.json";
                });

                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("sgr/swagger.json", "SGR API");
                    options.RoutePrefix = "docs";
                });
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseStaticFiles();
            app.MapControllers();

            //当处于开发者模式下，程序自动执行数据库初始化逻辑，确保数据库相关表和数据存在
            if (app.Environment.IsDevelopment())
            {
                //var factory = new SgrMysqlDbContextFactory();
                //var db = factory.CreateSqrDbContext("server=127.0.0.1;port=3306;database=sgr;uid=root;pwd=1qaz@WSX;Pooling=true;Min Pool Size=0;Max Pool Size=100;", new NoMediator(), b => { });
                //db.Database.CanConnect();

                //using (var scope = app.Services.CreateScope())
                //{
                //    var sgrDbContext = scope.ServiceProvider.GetRequiredService<SgrDbContext>();
                //    sgrDbContext.Database.CanConnect();
                //}

                using (var scope = app.Services.CreateScope())
                {
                    IDatabaseSeed databaseSeed = scope.ServiceProvider.GetRequiredService<IDatabaseSeed>();
                    await databaseSeed.SeedAsync();
                }
            }

            await app.RunAsync();
        }
    }
}