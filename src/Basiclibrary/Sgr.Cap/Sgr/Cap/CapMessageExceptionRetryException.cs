﻿/**************************************************************
 *
 * 唯一标识：62480e73-9068-41d4-8919-ece9bd5dafb0
 * 命名空间：Sgr.Cap
 * 创建时间：2024/7/22 21:23:53
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Cap
{
    public class CapMessageExceptionRetryException : BusinessException
    {
        /// <summary>
        /// CAP消息被标记为已处理后，再处理业务逻辑时发生的异常
        /// <para>抛出该异常后，CAP重试时，将忽略消息被标记为已处理的状态</para>
        /// </summary>
        /// <param name="innerException">引发当前异常的异常信息.</param>
        public CapMessageExceptionRetryException(Exception innerException)
            : base(nameof(CapMessageExceptionRetryException), innerException)
        {
        }
    }
}