﻿using Microsoft.EntityFrameworkCore;
using Sgr.Application.Queries;
using Sgr.Application.ViewModels;
using Sgr.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Application
{
    public class EfCoreReadDbContext : IReadDbContext
    {
        private readonly SgrSlaveDbContext _context;
        private bool _disposed;

        public EfCoreReadDbContext(SgrSlaveDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        #region 查询构建

        public IQueryable<T> Query<T>() where T : class
        {
            ThrowIfDisposed();
            return _context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> QueryWithNavigation<T>(params string[] navigationPropertyPath) where T : class
        {
            ThrowIfDisposed();
            var query = _context.Set<T>().AsNoTracking();
            return navigationPropertyPath.Aggregate(query, (current, path) => current.Include(path));
        }

        #endregion 查询构建

        #region 基础查询

        public Task<T?> FirstOrDefaultAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.FirstOrDefaultAsync(cancellationToken);
        }

        public Task<T?> SingleOrDefaultAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.SingleOrDefaultAsync(cancellationToken);
        }

        public async Task<IEnumerable<T>> ToListAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return await query.ToListAsync(cancellationToken);
        }

        #endregion 基础查询

        #region 高级查询

        public Task<PagedResponse<T>> PaginateAsync<T>(IQueryable<T> query, int skip, int take, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();

            return query.ToPagedListBySkipAsync(skip, take);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<PagedResponse<T>> PagedListByPageSizeAsync<T>(
            IQueryable<T> query,
             int pageIndex,
             int pageSize,
            CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();

            return query.ToPagedListByPageSizeAsync(pageIndex, pageSize);
        }

        #endregion 高级查询

        #region 聚合查询

        public Task<int> CountAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.CountAsync(cancellationToken);
        }

        public Task<long> LongCountAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.LongCountAsync(cancellationToken);
        }

        public Task<bool> AnyAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.AnyAsync(cancellationToken);
        }

        public Task<TResult?> MaxAsync<T, TResult>(IQueryable<T> query, Expression<Func<T, TResult?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.MaxAsync(selector, cancellationToken);
        }

        public Task<TResult?> MinAsync<T, TResult>(IQueryable<T> query, Expression<Func<T, TResult?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.MinAsync(selector, cancellationToken);
        }

        public Task<decimal?> AverageAsync<T>(IQueryable<T> query, Expression<Func<T, decimal?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            ThrowIfDisposed();
            return query.AverageAsync(selector, cancellationToken);
        }

        #endregion 聚合查询

        #region IDispose

        public void Dispose()
        {
            if (!_disposed)
            {
                _context?.Dispose();
                _disposed = true;
            }
        }

        public async ValueTask DisposeAsync()
        {
            if (!_disposed)
            {
                await _context.DisposeAsync().ConfigureAwait(false);
                _disposed = true;
            }
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        #endregion IDispose
    }
}