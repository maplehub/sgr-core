﻿/**************************************************************
 * 
 * 唯一标识：f7fcbf23-3679-4391-a32b-045713f2acf1
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/18 20:07:05
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.EntityFrameworkCore
{
    public class SgrSlaveDbContext : DbContext
    {
        public SgrSlaveDbContext(DbContextOptions<SgrSlaveDbContext> options) 
            : base(options)
        {
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
#if DEBUG
            Console.WriteLine("SgrSlaveDbContext OnModelCreating *******************************************");
#endif

            base.OnModelCreating(modelBuilder);

            var types = EntityFrameworkTypeRegistrar.Instance.GetAllTypeRegistrar<SgrDbContext>();

            foreach (var type in types)
            {
                if (Activator.CreateInstance(type) is IEntityFrameworkTypeProvider typeProvider)
                {
                    typeProvider.RegisterEntities(modelBuilder);
                    typeProvider.RegisterEntityConfigurations(modelBuilder);
                }
            }

        }
    }
}
