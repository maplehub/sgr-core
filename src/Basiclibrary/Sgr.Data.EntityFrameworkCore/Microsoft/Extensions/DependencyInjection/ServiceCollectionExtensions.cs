﻿/**************************************************************
 *
 * 唯一标识：a090197a-e0b5-409b-bc64-fe8154bd115f
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2024/6/18 20:50:46
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.Application;
using Sgr.Application.Queries;
using Sgr.Domain.Uow;
using Sgr.EntityFrameworkCore;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSgrEntityFrameworkCore(this IServiceCollection services)
        {
            services.AddSgrData();

            services.TryAddScoped<IUnitOfWork>(sp => { return sp.GetRequiredService<SgrDbContext>(); });

            //注入查询服务的IReadDbContext
            if (services.Any(f => f.ServiceType == typeof(IReadDbContext)))
                services.Replace(ServiceDescriptor.Scoped<IReadDbContext, EfCoreReadDbContext>());
            else
                services.AddScoped<IReadDbContext, EfCoreReadDbContext>();

            return services;
        }
    }
}