﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.EntityFrameworkCore;
using Sgr.Trackers.BackGroundTasks;
using Sgr.Trackers.Domain;
using Sgr.Trackers.Infrastructure;
using Sgr.Trackers.Infrastructure.Repositories;
using Sgr.Trackers.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    ///
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加跟踪器
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddTrackers(this IServiceCollection services)
        {
            services.TryAddScoped<IMessageTrackerLogRepository, MessageTrackerLogRepository>();
            services.TryAddScoped<IMessageTracker, DefaultMessageTracker>();

            services.TryAddScoped<CleanupIneffectiveMessageTrackerBackgroundTask>();

            EntityFrameworkTypeRegistrar.Instance.Register<SgrDbContext, MessageTrackerLogEntityFrameworkTypeProvider>();

            return services;
        }
    }
}