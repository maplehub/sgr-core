﻿/**************************************************************
 *
 * 唯一标识：081f2388-43c8-4bac-bd5d-964477db9ee7
 * 命名空间：Sgr.Trackers.BackGroundTasks
 * 创建时间：2024/7/21 11:14:02
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Logging;
using Sgr.BackGroundTasks;
using Sgr.Trackers.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Trackers.BackGroundTasks
{
    /// <summary>
    /// 清理失效的MessageTracker记录
    /// </summary>
    public class CleanupIneffectiveMessageTrackerBackgroundTask : IBackGroundTask
    {
        private readonly ILogger<CleanupIneffectiveMessageTrackerBackgroundTask> _logger;
        private readonly IMessageTrackerLogRepository _messageTrackerLogRepository;

        public CleanupIneffectiveMessageTrackerBackgroundTask(IMessageTrackerLogRepository messageTrackerLogRepository,
            ILogger<CleanupIneffectiveMessageTrackerBackgroundTask> logger)
        {
            _messageTrackerLogRepository = messageTrackerLogRepository;
            _logger = logger;
        }

        public async Task ExecuteAsync()
        {
            _logger.LogInformation($"CleanupIneffectiveMessageTrackerBackgroundTask Execute at {DateTimeOffset.Now}");

            await _messageTrackerLogRepository.CleanupIneffectiveAsync();
            await _messageTrackerLogRepository.UnitOfWork.SaveEntitiesAsync();

            _logger.LogInformation($"CleanupIneffectiveMessageTrackerBackgroundTask Executed.");
        }
    }
}