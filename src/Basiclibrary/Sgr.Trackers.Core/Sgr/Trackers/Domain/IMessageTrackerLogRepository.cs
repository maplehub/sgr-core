﻿/**************************************************************
 *
 * 唯一标识：8ef3fab8-99ea-4046-88fb-439f619230a5
 * 命名空间：Sgr.Trackers.Domain
 * 创建时间：2024/7/17 18:29:45
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers.Domain
{
    public interface IMessageTrackerLogRepository : IBaseRepositoryOfTEntityAndTPrimaryKey<MessageTrackerLog, long>
    {
        /// <summary>
        /// 检查Code是否存在
        /// </summary>
        /// <param name="code"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<bool> ExistCodeAsync(string code,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// 清理已失效的记录
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task CleanupIneffectiveAsync(CancellationToken token = default);
    }
}