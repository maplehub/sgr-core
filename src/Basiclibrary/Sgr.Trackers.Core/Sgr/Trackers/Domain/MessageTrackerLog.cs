﻿/**************************************************************
 *
 * 唯一标识：730ed6eb-db90-448a-8da1-baad98849e7e
 * 命名空间：Sgr.Trackers.Domain
 * 创建时间：2024/7/17 18:21:09
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Domain.Entities;
using System;

namespace Sgr.Trackers.Domain
{
    public class MessageTrackerLog : Entity<long>, IAggregateRoot, IOptimisticLock
    {
        public MessageTrackerLog()
        {
        }

        /// <summary>
        /// 标识
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 失效时间
        /// </summary>
        public DateTimeOffset ExpireTime { get; set; }

        #region IOptimisticLock (乐观锁)

        /// <summary>
        /// 行版本
        /// </summary>
        public long RowVersion { get; set; }

        #endregion IOptimisticLock (乐观锁)
    }
}