﻿/**************************************************************
 *
 * 唯一标识：7e98055b-c9fc-494f-a6ef-305411dc8e2e
 * 命名空间：Sgr.Trackers
 * 创建时间：2024/7/17 18:15:35
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Trackers.Domain;
using Sgr.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers.Services
{
    public class DefaultMessageTracker : IMessageTracker
    {
        private readonly IMessageTrackerLogRepository _messageTrackerLogRepository;

        public DefaultMessageTracker(IMessageTrackerLogRepository messageTrackerLogRepository)
        {
            this._messageTrackerLogRepository = messageTrackerLogRepository;
        }

        /// <summary>
        /// 判断消息是否已处理
        /// </summary>
        /// <param name="msgId">消息标识</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> HasProcessedAsync(string msgId, CancellationToken token = default)
        {
            return await _messageTrackerLogRepository.ExistCodeAsync(msgId, token);
        }

        /// <summary>
        /// 将指定消息标识标记为已执行
        /// </summary>
        /// <param name="msgId">消息标识</param>
        /// <param name="effectiveSecond">有效期，单位秒（默认7天）</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task MarkAsProcessedAsync(string msgId, int effectiveSecond = 604800, CancellationToken token = default)
        {
            await _messageTrackerLogRepository.InsertAsync(new MessageTrackerLog()
            {
                Code = msgId,
                ExpireTime = DateTimeOffset.Now.AddSeconds(effectiveSecond)
            }, token);

            await _messageTrackerLogRepository.UnitOfWork.SaveEntitiesAsync(token);
        }
    }
}