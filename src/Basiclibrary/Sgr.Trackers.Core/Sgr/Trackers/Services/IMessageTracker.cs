﻿/**************************************************************
 *
 * 唯一标识：616535b8-53be-44a2-9189-5d136b1ab7a7
 * 命名空间：Sgr.Trackers
 * 创建时间：2024/7/17 17:30:05
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers.Services
{
    /// <summary>
    /// 基于数据库事务实现接口幂等处理
    /// </summary>
    public interface IMessageTracker
    {
        /// <summary>
        /// 判断消息是否已处理
        /// </summary>
        /// <param name="msgId">消息标识</param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> HasProcessedAsync(string msgId, CancellationToken token = default);

        /// <summary>
        /// 将指定消息标识标记为已执行
        /// </summary>
        /// <param name="msgId">消息标识</param>
        /// <param name="effectiveSecond">有效期，单位秒（默认7天）</param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task MarkAsProcessedAsync(string msgId, int effectiveSecond = 604800, CancellationToken token = default);
    }
}