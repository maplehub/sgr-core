﻿/**************************************************************
 *
 * 唯一标识：a810b80a-0b2a-40d3-8a72-91d4ae2c6016
 * 命名空间：Sgr.Trackers.Infrastructure.Repositories
 * 创建时间：2024/7/17 18:36:44
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.EntityFrameworkCore;
using Sgr.Domain.Entities.Auditing;
using Sgr.Domain.Repositories;
using Sgr.Domain.Uow;
using Sgr.EntityFrameworkCore;
using Sgr.Exceptions;
using Sgr.Generator;
using Sgr.Trackers.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers.Infrastructure.Repositories
{
    public class MessageTrackerLogRepository
        : EfCoreRepositoryOfTEntityAndTPrimaryKey<MessageTrackerLog, long>, IMessageTrackerLogRepository
    {
        private readonly SgrDbContext _context;
        private readonly INumberIdGenerator _numberIdGenerator;

        public MessageTrackerLogRepository(SgrDbContext context, INumberIdGenerator numberIdGenerator)
        {
            _context = context ?? throw new BusinessException("SgrDbContext Is Null");
            _numberIdGenerator = numberIdGenerator;
        }

        public override IUnitOfWork UnitOfWork => _context;

        protected override void CheckAndSetId(MessageTrackerLog entity)
        {
            if (entity.Id == 0)
                entity.Id = _numberIdGenerator.GenerateUniqueId();
        }

        protected override IAuditedOperator? GetAuditedOperator()
        {
            return null;
        }

        protected override DbContext GetDbContext()
        {
            return _context;
        }

        /// <summary>
        /// 检查Code是否存在
        /// </summary>
        /// <param name="code"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> ExistCodeAsync(string code,
            CancellationToken cancellationToken = default)
        {
            return await _context.Set<MessageTrackerLog>().AnyAsync(f => f.Code == code);
        }

        /// <summary>
        /// 清理已失效的记录
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task CleanupIneffectiveAsync(CancellationToken token = default)
        {
            await _context.Set<MessageTrackerLog>().Where(f => f.ExpireTime < DateTimeOffset.Now).ExecuteDeleteAsync(token);
        }
    }
}