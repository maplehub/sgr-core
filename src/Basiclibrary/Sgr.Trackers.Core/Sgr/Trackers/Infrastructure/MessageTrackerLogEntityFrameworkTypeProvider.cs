﻿/**************************************************************
 *
 * 唯一标识：fe0ad9bb-525b-4b3e-b12d-c6ef5427466d
 * 命名空间：Sgr.Trackers.Infrastructure
 * 创建时间：2024/7/17 18:40:05
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.EntityFrameworkCore;
using Sgr.EntityFrameworkCore;
using Sgr.Trackers.Domain;
using Sgr.Trackers.Infrastructure.EntityConfigurations;

namespace Sgr.Trackers.Infrastructure
{
    public class MessageTrackerLogEntityFrameworkTypeProvider : IEntityFrameworkTypeProvider
    {
        public void RegisterEntities(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MessageTrackerLog>();
        }

        public void RegisterEntityConfigurations(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MessageTrackerLogConfiguration());
        }
    }
}