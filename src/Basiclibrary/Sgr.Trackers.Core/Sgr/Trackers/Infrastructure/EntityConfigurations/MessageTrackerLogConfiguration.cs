﻿/**************************************************************
 *
 * 唯一标识：a425ea33-f3e2-4d4e-8c86-90b2b5c5ebc9
 * 命名空间：Sgr.Trackers.Infrastructure.EntityConfigurations
 * 创建时间：2024/7/17 18:32:27
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sgr.EntityFrameworkCore.EntityConfigurations;
using Sgr.Trackers.Domain;
using System.Reflection.Emit;
using System.Reflection.Metadata;

namespace Sgr.Trackers.Infrastructure.EntityConfigurations
{
    internal class MessageTrackerLogConfiguration : EntityTypeConfigurationBase<MessageTrackerLog, long>
    {
        public override void Configure(EntityTypeBuilder<MessageTrackerLog> builder)
        {
            builder.ToTable("sgr_message_trackerlog");

            base.Configure(builder);

            builder.PropertyAndHasColumnName(b => b.Code, GetColumnNameCase())
                .HasMaxLength(200)
                .IsRequired()
                .HasComment("标识");

            builder.PropertyAndHasColumnName(b => b.ExpireTime, GetColumnNameCase())
                .HasComment("创建时间");

            //唯一索引
            builder.HasIndex(b => b.Code).IsUnique();
        }
    }
}