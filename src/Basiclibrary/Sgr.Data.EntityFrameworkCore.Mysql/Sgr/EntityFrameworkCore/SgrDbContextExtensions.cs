﻿/**************************************************************
 *
 * 唯一标识：1857788f-352b-4adc-9f95-4fa1fa062bed
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/18 20:45:39
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Sgr.Data;
using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;

namespace Sgr.EntityFrameworkCore
{
    public static class SgrDbContextExtensions
    {
        public static IServiceCollection UseMySql(
            this IServiceCollection services,
            string name,
            Action<MySqlDbContextOptionsBuilder> mySQLOptionsAction,
            ILoggerFactory? loggerFactory = null)
        {
            services.AddDbContext<SgrDbContext>((serviceProvider, dbContextBuilder) =>
            {
                IDataConnectionStringResolve resolve = serviceProvider.GetRequiredService<IDataConnectionStringResolve>();
                string connectionString = resolve.GetMainDatabaseConnectionString(name);

                dbContextBuilder.UseMySql(connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    optionsBuilder =>
                    {
                        optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                        mySQLOptionsAction?.Invoke(optionsBuilder);
                    });

                if (loggerFactory != null)
                    dbContextBuilder.UseLoggerFactory(loggerFactory);
            });

            services.AddDbContext<SgrSlaveDbContext>((serviceProvider, dbContextBuilder) =>
            {
                IDataConnectionStringResolve resolve = serviceProvider.GetRequiredService<IDataConnectionStringResolve>();
                string connectionString = resolve.GetSlaveDatabaseConnectionString(name);

                dbContextBuilder.UseMySql(connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    optionsBuilder =>
                    {
                        optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                        mySQLOptionsAction?.Invoke(optionsBuilder);
                    });

                if (loggerFactory != null)
                    dbContextBuilder.UseLoggerFactory(loggerFactory);
            });

            return services;
            //services.AddDbContext<SgrDbContext>(options =>
            //{
            //    //var serverVersion = ServerVersion.Parse(configuration.GetRequiredString("ConnectionStrings:DatabaseVersion"));
            //    //options.UseMySql(configuration.GetRequiredString("ConnectionStrings:ConnectionString"), serverVersion, (sqlOptions) =>
            //    //{
            //    //    sqlOptions
            //    //           .MinBatchSize(1)
            //    //           .MaxBatchSize(1000);
            //    //    //sqlOptions.CharSet(CharSet.Utf8);
            //    //    //sqlOptions.MigrationsAssembly(typeof(Program).Assembly.FullName);
            //    //    //// Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency
            //    //    //sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            //    //});
            //});
        }
    }
}