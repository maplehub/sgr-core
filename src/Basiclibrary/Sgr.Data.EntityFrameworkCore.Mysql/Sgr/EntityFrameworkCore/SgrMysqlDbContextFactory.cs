﻿/**************************************************************
 * 
 * 唯一标识：c1015622-d6fd-4df7-b4c1-ad6b7c09374e
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/19 9:41:11
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Sgr.MediatR;
using System;

namespace Sgr.EntityFrameworkCore
{

    public class SgrMysqlDbContextFactory
    {
        public virtual SgrDbContext CreateSqrDbContext(string connectionString, IMediator mediator, Action<MySqlDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrDbContext>(connectionString, optionsAction);

            var dbContext = new SgrDbContext(optionsBuilder.Options, mediator);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }

        public virtual SgrSlaveDbContext CreateSgrSlaveDbContext(string connectionString, Action<MySqlDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrSlaveDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrSlaveDbContext>(connectionString, optionsAction);

            var dbContext = new SgrSlaveDbContext(optionsBuilder.Options);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }


        protected virtual DbContextOptionsBuilder<TContext> getDbContextOptionsBuilder<TContext>(string connectionString, Action<MySqlDbContextOptionsBuilder> optionsAction) where TContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

#if DEBUG
            Console.WriteLine("DB Connection String :" + connectionString);
#endif
            optionsBuilder.UseMySql(connectionString,
                ServerVersion.AutoDetect(connectionString),
                builder => optionsAction?.Invoke(builder)); //b => b.MigrationsAssembly("Sgr.Admin.WebHost"));
            return optionsBuilder;
        }
    }
}
