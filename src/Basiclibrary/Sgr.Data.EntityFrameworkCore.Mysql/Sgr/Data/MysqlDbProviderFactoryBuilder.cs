﻿/**************************************************************
 * 
 * 唯一标识：b83ca179-08a9-483c-a3bd-260ac7094a1b
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/21 16:27:49
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Data.DatabaseTranslators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data
{
    public class MysqlDbProviderFactoryBuilder : IDbProviderFactoryBuilder
    {
        public DatabaseType DatabaseType => DatabaseType.MySql;

        public DbProviderFactory GetDbProviderFactory()
        {
            return MySqlConnector.MySqlConnectorFactory.Instance;
        }
    }
}
