﻿/**************************************************************
 * 
 * 唯一标识：216b177b-0694-4ab3-8cbe-03bd534c86e1
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2024/6/20 19:27:17
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.Data;
using Sgr.Data.DatabaseTranslators;
using Sgr.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSgrData(this IServiceCollection services)
        {
            //数据源相关
            services.TryAddTransient<IDatabaseSeed, DefaultDatabaseSeed>();

            services.TryAddScoped<IDataConnectionStringResolve, DefalutDataConnectionStringResolve>();
            services.TryAddSingleton<IDataConnectionStringManager, DefaultDataConnectionStringManager>();
            services.TryAddSingleton<IDataConnectionStringProvider, DefaultDataConnectionStringProvider>();

            services.TryAddTransient<IDatabaseTranslatorFactory, DefaultDatabaseTranslatorFactory>();

            return services;
        }
    }
}
