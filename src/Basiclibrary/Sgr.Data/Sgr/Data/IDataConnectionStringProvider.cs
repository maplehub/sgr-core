﻿/**************************************************************
 * 
 * 唯一标识：cb928041-7050-4ed2-8b32-f6dd20c7975c
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/17 21:05:33
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Data
{
    /// <summary>
    /// 数据源提供者
    /// </summary>
    public interface IDataConnectionStringProvider
    {
        /// <summary>
        /// 读取数据源
        /// </summary>
        /// <returns></returns>
        IEnumerable<DataConnectionString> LoadDataConnectionStrings();
    }
}
