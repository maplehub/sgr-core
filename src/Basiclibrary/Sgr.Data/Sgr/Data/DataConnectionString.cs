﻿/**************************************************************
 * 
 * 唯一标识：6e22d710-db4b-48ab-b2ba-e533750d2946
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/17 20:51:38
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Data
{
    /// <summary>
    /// 数据源
    /// </summary>
    public class DataConnectionString
    {
        /// <summary>
        /// 数据源
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dbProvider"></param>
        /// <param name="connectionString"></param>
        /// <param name="slaves"></param>
        public DataConnectionString(string name, DatabaseType dbProvider,string connectionString, string[] slaves)
        {
            Name = name;
            DbProvider = dbProvider;
            ConnectionString = connectionString;
            Slaves = slaves ?? new string[] { };
        }

        /// <summary>
        /// 数据库连接字符串名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public DatabaseType DbProvider { get; private set; }
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public string ConnectionString { get; private set; }
        /// <summary>
        /// 对于的从库连接字符串
        /// </summary>
        public string[] Slaves { get; private set; }
    }
}
