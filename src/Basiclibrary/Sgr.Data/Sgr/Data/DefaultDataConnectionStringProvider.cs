﻿/**************************************************************
 * 
 * 唯一标识：f03febf8-474e-43e7-a054-d9caffc84165
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/17 21:29:18
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.Configuration;
using Sgr.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Text;

namespace Sgr.Data
{
    public class DefaultDataConnectionStringProvider : IDataConnectionStringProvider
    {
        private readonly IConfiguration configuration;

        public DefaultDataConnectionStringProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IEnumerable<DataConnectionString> LoadDataConnectionStrings()
        {
            //string dbType = configuration.GetRequiredString("Sgr:DataConnectionStrings:DatabaseType").ToUpper();
            string connectionString = configuration.GetRequiredString("Sgr:DataConnectionStrings:ConnectionString");
            string databaseType = (configuration.GetValue<string>("Sgr:DataConnectionStrings:DatabaseType") ?? "").ToUpper();
            string slaves = configuration.GetValue<string>("Sgr:DataConnectionStrings:Slaves") ?? "";
             
            //SQLITE MYSQL
            DatabaseType dbProvider = DatabaseType.SQLite;
            switch (databaseType)
            {
                case "SQLITE":
                    dbProvider = DatabaseType.SQLite;
                    break;
                case "MYSQL":
                    dbProvider = DatabaseType.MySql;
                    break;
                case "ORACLE":
                    dbProvider = DatabaseType.Oracle;
                    break;
                case "DB2":
                    dbProvider = DatabaseType.DB2;
                    break;
                case "SQLSERVER":
                    dbProvider = DatabaseType.SqlServer;
                    break;
                case "POSTGRESQL":
                    dbProvider = DatabaseType.PostgreSql;
                    break;
                default:
                    break;
            }

            if (dbProvider ==  DatabaseType.SQLite)
            {
                if (string.IsNullOrEmpty(connectionString))
                    connectionString = GetDefaultSqliteDatabaseConnectionString();
                slaves = "";
            }

            yield return new DataConnectionString(Constant.DEFAULT_DATABASE_SOURCE_NAME, dbProvider, connectionString, slaves.Length == 0 ? new string[] { }: slaves.Split(','));
        }


        public static string GetDefaultSqliteDatabaseConnectionString()
        {
            return $"Data Source={GetDefaultSqliteDatabaseFilePath()};Mode=ReadWriteCreate;Cache=Shared";
        }

        public static string GetDefaultSqliteDatabaseFilePath()
        {
            //获取并确保存在数据库文件所在目录
            string dbFileDir = System.IO.Path.Combine(LocalFileHelper.GetApplicationDirectory(), "database");
            if (!System.IO.Directory.Exists(dbFileDir))
                System.IO.Directory.CreateDirectory(dbFileDir);

            //获取数据库文件路径
            return System.IO.Path.Combine(dbFileDir, "sgr.db");
        }
    }
}
