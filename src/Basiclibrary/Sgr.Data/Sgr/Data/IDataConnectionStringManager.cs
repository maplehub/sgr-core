﻿/**************************************************************
 * 
 * 唯一标识：79b5c1f7-cd80-402b-9674-be93771c350b
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/17 20:58:34
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Sgr.Data
{
    /// <summary>
    /// 数据源管理器
    /// </summary>
    public interface IDataConnectionStringManager
    {
        /// <summary>
        /// 重新加载数据源信息
        /// </summary>
        void Reload();

        /// <summary>
        /// 获取数据源信息
        /// </summary>
        /// <param name="dataConnectionStringName"></param>
        /// <returns></returns>
        DataConnectionString GetDataConnectionString(string dataConnectionStringName);
    }
}
