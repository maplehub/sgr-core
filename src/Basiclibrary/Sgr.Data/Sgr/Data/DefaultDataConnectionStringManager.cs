﻿/**************************************************************
 * 
 * 唯一标识：71f12da3-ca29-4a55-b8a6-e3ed6f4c164d
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/17 21:08:00
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;

namespace Sgr.Data
{
    public class DefaultDataConnectionStringManager : IDataConnectionStringManager
    {
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        private readonly Dictionary<string, DataConnectionString> _databaseSources = new Dictionary<string, DataConnectionString>(StringComparer.OrdinalIgnoreCase);
        
        private readonly IEnumerable<IDataConnectionStringProvider> _dataConnectionStringProviders;

        public DefaultDataConnectionStringManager(IEnumerable<IDataConnectionStringProvider> dataConnectionStringProviders)
        {
            _dataConnectionStringProviders = dataConnectionStringProviders;
            this.Reload();
        }

        public void Reload()
        {
            _lock.EnterWriteLock();
            try
            {
                _databaseSources.Clear();

                foreach (var item in this._dataConnectionStringProviders)
                {
                    var sources = item.LoadDataConnectionStrings();
                    foreach (var s in sources)
                    {
                        if (_databaseSources.ContainsKey(s.Name))
                            _databaseSources[s.Name] = s;
                        else
                            _databaseSources.Add(s.Name, s);
                    }
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public DataConnectionString GetDataConnectionString(string dataConnectionStringName)
        {
            _lock.EnterReadLock();
            try
            {
                if (!this._databaseSources.TryGetValue(dataConnectionStringName, out DataConnectionString? databaseSource) || databaseSource == null)
                    throw new BusinessException($"未设置[{dataConnectionStringName}]的数据连接字符串");

                return databaseSource;
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }
    }
}
