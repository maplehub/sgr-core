﻿/**************************************************************
 * 
 * 唯一标识：43644661-33e1-4692-8237-7979f0b2d0f9
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:16:44
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class OracleTranslator : DatabaseTranslatorBase
    {
        public OracleTranslator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.Oracle; } }

        public override char Connector => ':';
    }
}
