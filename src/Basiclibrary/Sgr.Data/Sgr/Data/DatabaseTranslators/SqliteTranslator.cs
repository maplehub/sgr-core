﻿/**************************************************************
 * 
 * 唯一标识：f1fe4896-9cc7-4aab-b59b-a44dc69a6e39
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:09:30
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class SqliteTranslator: DatabaseTranslatorBase
    {
        public SqliteTranslator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.SQLite; } }

        public override char Connector => '@';
    }
}
