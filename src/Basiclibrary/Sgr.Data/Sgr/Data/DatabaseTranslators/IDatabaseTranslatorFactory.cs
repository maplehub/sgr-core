﻿/**************************************************************
 * 
 * 唯一标识：ad6f720f-5ebe-4a0a-9295-53ec800f92e2
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:24:59
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public interface IDatabaseTranslatorFactory
    {
        IDatabaseTranslator CreateTranslator(DatabaseType dataBaseType);
    }
}
