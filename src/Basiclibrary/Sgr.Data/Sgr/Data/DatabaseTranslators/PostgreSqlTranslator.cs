﻿/**************************************************************
 * 
 * 唯一标识：72e924e3-d22b-489f-90a8-e52c1c78d92b
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:17:01
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class PostgreSqlTranslator : DatabaseTranslatorBase
    {
        public PostgreSqlTranslator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.PostgreSql; } }

        public override char Connector => ':';
    }
}