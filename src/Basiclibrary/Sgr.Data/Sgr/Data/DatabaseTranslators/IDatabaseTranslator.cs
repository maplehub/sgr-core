﻿/**************************************************************
 * 
 * 唯一标识：83298601-270e-4d61-a055-aadcdbe827c0
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/20 20:04:52
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public interface IDatabaseTranslator
    {
        DatabaseType DataBaseType { get; }

        /// <summary> 
        /// 获取参数连接符
        /// </summary>
        /// <returns></returns>
        char Connector { get; }

        /// <summary>
        /// 引用开始
        /// </summary>
        string OpenQuote { get; }

        /// <summary>
        /// 引用结束
        /// </summary>
        string CloseQuote { get; }

        /// <summary>
        /// 创建数据库连接
        /// </summary>
        /// <returns></returns>
        DbConnection? CreateConnection();
    }
}
