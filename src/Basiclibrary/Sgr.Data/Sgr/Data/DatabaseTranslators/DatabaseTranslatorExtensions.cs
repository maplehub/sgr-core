﻿/**************************************************************
 * 
 * 唯一标识：650add8f-fac8-4b6d-b3a8-0b0ef894642e
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/21 8:53:34
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public static class DatabaseTranslatorExtensions
    {
        /// <summary>
        /// 包装表名称或字段
        /// </summary>
        /// <param name="dbTranslator"></param>
        /// <param name="tableOrColumnName"></param>
        /// <returns></returns>
        public static string Quote(this IDatabaseTranslator dbTranslator, string tableOrColumnName)
        {
            return string.Format("{0}{2}{1}", dbTranslator.OpenQuote, dbTranslator.CloseQuote, tableOrColumnName);
        }
        /// <summary>
        /// 包装参数
        /// </summary>
        /// <param name="dbTranslator"></param>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        public static string QuoteParameter(this IDatabaseTranslator dbTranslator, string parameterName)
        {
            return string.Format("{0}{1}", dbTranslator.Connector, parameterName);
        }

    }
}
