﻿/**************************************************************
 * 
 * 唯一标识：0287253c-8f70-465a-b38e-a8d9697cbad9
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/21 16:12:28
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public interface IDbProviderFactoryBuilder
    {
        DatabaseType DatabaseType { get; }
        DbProviderFactory GetDbProviderFactory();
    }
}
