﻿/**************************************************************
 * 
 * 唯一标识：e08b4628-3fa7-4862-a0b1-dab3e15b9a0a
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:07:39
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public abstract class DatabaseTranslatorBase : IDatabaseTranslator
    {
        private readonly DbProviderFactory _dbProviderFactory;

        public DatabaseTranslatorBase(DbProviderFactory dbProviderFactory)
        {
            _dbProviderFactory = dbProviderFactory;
        }

        public abstract DatabaseType DataBaseType { get; }

        public abstract char Connector { get; }

        public virtual string OpenQuote { get { return string.Empty; } }

        public virtual string CloseQuote { get { return string.Empty; } }

        public virtual DbConnection? CreateConnection()
        {
            return _dbProviderFactory.CreateConnection();
        }
    }
}
