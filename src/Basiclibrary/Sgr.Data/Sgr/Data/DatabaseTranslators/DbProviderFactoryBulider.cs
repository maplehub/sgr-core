﻿///**************************************************************
// * 
// * 唯一标识：6738fbc6-c286-4488-ad45-06d6edb10199
// * 命名空间：Sgr.Data
// * 创建时间：2024/6/20 19:13:48
// * 机器名称：DESKTOP-HJ4OAG9
// * 创建者：CocoYuan
// * 电子邮箱：fengqinhua2016@163.com
// * 描述：
// * 
// *  参考; https://www.cnblogs.com/darjuan/archive/2012/07/11/DbProviderFactories.html
// * 
// **************************************************************/

//using System;
//using System.Collections.Generic;
//using System.Data.Common;
//using System.Text;

//namespace Sgr.Data.DatabaseTranslators
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class DbProviderFactoryBulider
//    {
//        private static Dictionary<DatabaseType, string> providerInvariantNames = new Dictionary<DatabaseType, string>();
//        private static Dictionary<DatabaseType, DbProviderFactory> providerFactoies = new Dictionary<DatabaseType, DbProviderFactory>(10);

//        static DbProviderFactoryBulider()
//        {
//            providerInvariantNames.Add(DatabaseType.SqlServer, "System.Data.SqlClient");
//            providerInvariantNames.Add(DatabaseType.Oracle, "Oracle.DataAccess.Client");
//            providerInvariantNames.Add(DatabaseType.MySql, "MySql.Data.MySqlClient");
//            providerInvariantNames.Add(DatabaseType.SQLite, "System.Data.SQLite");
//            providerInvariantNames.Add(DatabaseType.PostgreSql, "Npgsql");
//            providerInvariantNames.Add(DatabaseType.DB2, "IBM.Data.DB2.iSeries");
//        }

//        public static string GetProviderInvariantName(DatabaseType providerType)
//        {
//            return providerInvariantNames[providerType];
//        }

//        public static DbProviderFactory GetDbProviderFactory(DatabaseType providerType)
//        {
//            if (!providerFactoies.ContainsKey(providerType))
//                providerFactoies.TryAdd(providerType, ImportDbProviderFactory(providerType));

//            return providerFactoies[providerType];
//        }

//        private static DbProviderFactory ImportDbProviderFactory(DatabaseType providerType)
//        {
//            string providerName = providerInvariantNames[providerType];
//            return DbProviderFactories.GetFactory(providerName);

//            //DbProviderFactory factory;
//            //try
//            //{
//            //    factory = DbProviderFactories.GetFactory(providerName);
//            //}
//            //catch (ArgumentException e)
//            //{
//            //    factory = null;
//            //}
//            //return factory;
//        }
//    }
//}
