﻿/**************************************************************
 * 
 * 唯一标识：3b9be92f-b681-414b-bee6-27ec6692fdc5
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:15:23
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class SqlServerTranslator : DatabaseTranslatorBase
    {
        public SqlServerTranslator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.SqlServer; } }

        public override char Connector => '@';

        public override string OpenQuote => "[";

        public override string CloseQuote => "]";
    }
}