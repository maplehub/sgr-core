﻿/**************************************************************
 * 
 * 唯一标识：345398b9-9a48-442f-8a59-20353bf83302
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:17:15
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class DB2Translator : DatabaseTranslatorBase
    {
        public DB2Translator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.DB2; } }

        public override char Connector => '@';
    }
}