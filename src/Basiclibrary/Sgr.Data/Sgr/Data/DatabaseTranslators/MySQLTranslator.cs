﻿/**************************************************************
 * 
 * 唯一标识：bff08cf6-4969-4371-90e5-5d17d6c30ad4
 * 命名空间：Sgr.Data.DatabaseTranslators
 * 创建时间：2024/6/20 20:14:28
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data.DatabaseTranslators
{
    public class MySQLTranslator : DatabaseTranslatorBase
    {
        public MySQLTranslator(DbProviderFactory dbProviderFactory) : base(dbProviderFactory) { }

        public override DatabaseType DataBaseType { get { return DatabaseType.MySql; } }

        public override char Connector => '@';
    }
}