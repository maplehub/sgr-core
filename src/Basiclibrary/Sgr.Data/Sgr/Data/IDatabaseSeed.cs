﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data
{
    public interface IDatabaseSeed
    {
        Task SeedAsync();
    }
}
