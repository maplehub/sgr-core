﻿/**************************************************************
 * 
 * 唯一标识：721f472d-0b5e-4211-8ba6-c46002299109
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/20 19:14:43
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Data
{
    public enum DatabaseType
    {
        SqlServer,
        MySql,
        SQLite,
        Oracle,
        PostgreSql,
        DB2
    }
}
