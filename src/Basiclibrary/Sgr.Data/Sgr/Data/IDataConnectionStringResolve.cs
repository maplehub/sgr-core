﻿/**************************************************************
 * 
 * 唯一标识：7e821436-20ed-4f93-85fe-cb956f618cdb
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/18 23:14:03
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Data
{
    public interface IDataConnectionStringResolve
    {
        /// <summary>
        /// 始终连接主库
        /// </summary>
        bool AlwaysMainDatabase { get; }

        /// <summary>
        /// 获取主库连接字符串
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        string GetMainDatabaseConnectionString(string name);

        /// <summary>
        /// 获取从库连接字符串
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        string GetSlaveDatabaseConnectionString(string name);
    }
}
