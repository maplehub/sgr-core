﻿/**************************************************************
 * 
 * 唯一标识：ce04f742-b32b-42ab-b340-955cc85447d7
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/18 23:15:50
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Utilities;
using System;
using System.Collections.Concurrent;
using System.Text.Json;

namespace Sgr.Data
{
    public class DefalutDataConnectionStringResolve : IDataConnectionStringResolve
    {
        private static readonly ConcurrentDictionary<string, int> SlaveIndexCache = new ConcurrentDictionary<string, int>();

        private readonly IDataConnectionStringManager _dataConnectionStringManager;

        public DefalutDataConnectionStringResolve(IDataConnectionStringManager dataConnectionStringManager)
        {
            _dataConnectionStringManager = dataConnectionStringManager;
        }

        public virtual bool AlwaysMainDatabase => false;

        public virtual string GetMainDatabaseConnectionString(string name)
        {
            return GetDataConnectionString(name).ConnectionString;
        }

        public virtual string GetSlaveDatabaseConnectionString(string name)
        {
            DataConnectionString dataConnectionString = GetDataConnectionString(name);

            string connectionString = dataConnectionString.ConnectionString;
            if (!AlwaysMainDatabase && dataConnectionString.Slaves != null && dataConnectionString.Slaves.Length > 0)
            {
                if (dataConnectionString.Slaves.Length == 1)
                    connectionString = dataConnectionString.Slaves[0];
                else
                {
                    int index = GetSlaveIndex(name, dataConnectionString);
                    connectionString = dataConnectionString.Slaves[index];
                }
            }

            return connectionString;

        }

        protected virtual int GetSlaveIndex(string name, DataConnectionString dataConnectionString)
        {
            //此处随机选取
            //int index = DateTime.Now.Millisecond % dataConnectionString.Slaves.Length;
            //int index = RandomHelper.GetRandom(0, dataConnectionString.Slaves.Length - 1)

            //顺序选取
            int length = dataConnectionString.Slaves.Length - 1;
            int index = SlaveIndexCache.AddOrUpdate(name, 0, (_, oldValue) =>
            {
                if (oldValue == length)
                    return 0;
                else
                    return oldValue + 1;
            });
            return index;
        }

        protected virtual DataConnectionString GetDataConnectionString(string name)
        {
            return _dataConnectionStringManager.GetDataConnectionString(name);
        }
    }
}
