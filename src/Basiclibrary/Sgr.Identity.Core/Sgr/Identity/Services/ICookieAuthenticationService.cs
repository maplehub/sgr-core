﻿using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Sgr.Identity.Services
{
    /// <summary>
    /// 基于Cookie的认证服务
    /// </summary>
    public interface ICookieAuthenticationService
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account">用户信息</param>
        /// <param name="isPersistent">是否保留此标记表示的 Cookie 的值</param>
        /// <returns></returns>
        Task SignInAsync(Account account, bool isPersistent);

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account"></param>
        /// <param name="isPersistent"></param>
        /// <param name="additionalClaims"></param>
        /// <returns></returns>
        Task SignInWithClaimsAsync(Account account, bool isPersistent, IEnumerable<Claim> additionalClaims);

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account"></param>
        /// <param name="authenticationProperties"></param>
        /// <param name="additionalClaims"></param>
        /// <returns></returns>
        Task SignInWithClaimsAsync(Account account, AuthenticationProperties? authenticationProperties, IEnumerable<Claim> additionalClaims);

        /// <summary>
        /// 更新登录状态
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Task RefreshSignInAsync(Account account);

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        Task SignOutAsync();
    }
}