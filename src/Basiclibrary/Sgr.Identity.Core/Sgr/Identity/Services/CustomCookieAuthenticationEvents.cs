﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Sgr.Utilities;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sgr.Identity.Services
{
    public class CustomCookieAuthenticationEvents : CookieAuthenticationEvents
    {
        private readonly HttpCookieOptions _httpCookieOptions;

        public CustomCookieAuthenticationEvents(HttpCookieOptions httpCookieOptions)
        {
            _httpCookieOptions = httpCookieOptions;
        }

        public override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
        {
            //IsAjax则返回401
            var xreq = context.Request.Headers.ContainsKey("x-requested-with");
            if (xreq && context.Request.Headers["x-requested-with"] == "XMLHttpRequest")
            {
                var payload = "{\"serviceerror\": { \"code\": 401,\"message\": \"未登录!\" } }";
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = 401;
                context.Response.WriteAsync(payload);
            }
            else
            {
                context.Response.Redirect(_httpCookieOptions.LoginPath + "?ReturnUrl=" + HttpUtility.UrlEncode(context.Request.Path + context.Request.QueryString));
            }

            return base.RedirectToLogin(context);
        }

        public override Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            //https://learn.microsoft.com/zh-cn/aspnet/core/security/authentication/cookie?view=aspnetcore-9.0

            //var userPrincipal = context.Principal;

            //// Look for the LastChanged claim.
            //var jti = (from c in userPrincipal?.Claims
            //           where c.Type == Constant.CLAIM_JTI
            //           select c.Value).FirstOrDefault();

            //if (balaba...根据令牌的唯一标识jti来判断用户状态是否发生变化，如若变化则退出登录){
            //    context.RejectPrincipal();

            //    await context.HttpContext.SignOutAsync(
            //        CookieAuthenticationDefaults.AuthenticationScheme);
            //}

            return base.ValidatePrincipal(context);
        }
    }
}