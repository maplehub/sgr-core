﻿namespace Sgr.Identity
{
    public class UserToken
    {
        /// <summary>
        /// 令牌类型
        /// </summary>
        public string TokenType { get; set; } = "Bearer";

        /// <summary>
        /// 访问令牌文本内容。注意令牌有过期时间，发现令牌过期后，需要使用RefrashToken重新申请令牌
        /// </summary>
        public string? AccessToken { get; set; }
    }
}