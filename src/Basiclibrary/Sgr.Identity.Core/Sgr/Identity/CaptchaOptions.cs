﻿using System;

namespace Sgr.Identity
{
    public class CaptchaOptions
    {
        /// <summary>
        /// 验证码是否为算术公式
        /// </summary>
        public bool CaptchaIsArithmetic { get; set; }

        /// <summary>
        /// 创建缺省配置项
        /// </summary>
        /// <returns></returns>
        public static CaptchaOptions CreateDefault()
        {
            return new CaptchaOptions()
            {
                CaptchaIsArithmetic = true
            };
        }
    }
}