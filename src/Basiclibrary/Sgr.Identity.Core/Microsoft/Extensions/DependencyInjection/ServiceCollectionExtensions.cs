﻿/**************************************************************
 *
 * 唯一标识：a99a76f5-f40b-4edd-8169-7181108691ad
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2023/8/21 11:39:17
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Sgr.Identity;
using Sgr.Identity.Services;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 扩展
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加 SGR 认证核心服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="useJwt"></param>
        /// <param name="useCookie"></param>
        /// <returns></returns>
        public static IServiceCollection AddSgrAuthentication(this IServiceCollection services,
            IConfiguration configuration,
            bool useJwt = true,
            bool useCookie = false)
        {
            // 注册配置
            CaptchaOptions captchaOptions = configuration.GetSection("Sgr:Captcha").Get<CaptchaOptions>() ?? CaptchaOptions.CreateDefault();
            JwtOptions jwtOptions = configuration.GetSection("Sgr:Identity:JWT").Get<JwtOptions>() ?? JwtOptions.CreateDefault();
            HttpCookieOptions cookieOptions = configuration.GetSection("Sgr:Identity:Cookie").Get<HttpCookieOptions>() ?? HttpCookieOptions.CreateDefault();

            services.TryAddSingleton(captchaOptions);
            services.TryAddSingleton(jwtOptions);
            services.TryAddSingleton(cookieOptions);

            // 根据配置添加认证
            if (useJwt)
                services.AddSgrJwtAuthentication(jwtOptions);

            if (useCookie)
                services.AddSgrCookieAuthentication(cookieOptions);

            return services;
        }

        /// <summary>
        /// 添加 JWT 认证
        /// </summary>
        private static AuthenticationBuilder AddSgrJwtAuthentication(
            this IServiceCollection services,
            JwtOptions jwtOptions)
        {
            return services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(ConfigureJwtBearerOptions(jwtOptions));
        }

        /// <summary>
        /// 配置 JWT Bearer 选项
        /// </summary>
        private static Action<JwtBearerOptions> ConfigureJwtBearerOptions(JwtOptions jwtOptions)
        {
            /***
             *
             * 解决net8中部分自定义Claim被映射成ClaimTypes的解决办法
             * https://zhuanlan.zhihu.com/p/684663526
             * https://github.com/dotnet/aspnetcore/issues/52075
             * https://learn.microsoft.com/zh-cn/dotnet/core/compatibility/aspnet-core/8.0/securitytoken-events
             */
            return options =>
            {
                options.MapInboundClaims = false;
                options.TokenValidationParameters = CreateTokenValidationParameters(jwtOptions);
                options.Events = CreateJwtBearerEvents();
            };
        }

        /// <summary>
        /// 创建 Token 验证参数
        /// </summary>
        private static TokenValidationParameters CreateTokenValidationParameters(JwtOptions jwtOptions)
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,                                                  //是否验证发行商
                ValidateAudience = true,                                                //是否验证受众者
                ValidateLifetime = true,                                                //是否验证失效时间
                ValidateIssuerSigningKey = true,                                        //是否验证签名键
                ValidIssuer = jwtOptions.Issuer,
                ValidAudience = jwtOptions.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key)),
                ClockSkew = TimeSpan.FromSeconds(jwtOptions.ClockSkewSeconds),           //过期时间容错值，解决服务器端时间不同步问题（秒）
                RequireExpirationTime = true
            };
        }

        /// <summary>
        /// 创建 JWT Bearer 事件
        /// </summary>
        private static JwtBearerEvents CreateJwtBearerEvents()
        {
            return new JwtBearerEvents
            {
                OnChallenge = HandleUnauthorizedChallenge,
                OnTokenValidated = ValidateToken
            };
        }

        /// <summary>
        /// 处理未授权响应
        /// </summary>
        private static Task HandleUnauthorizedChallenge(JwtBearerChallengeContext context)
        {
            //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
            context.HandleResponse();

            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            context.Response.ContentType = "application/json";

            var response = new { title = "Unauthorized", status = 401 };
            return context.Response.WriteAsJsonAsync(response);
        }

        /// <summary>
        /// 验证 Token
        /// </summary>
        private static async Task ValidateToken(TokenValidatedContext context)
        {
            var tokenBlacklistService = context.HttpContext.RequestServices
                .GetRequiredService<ITokenBlacklistService>();

            var jti = context.Principal?.Claims?
                .FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Jti)?.Value;

            //如果Token中不包含jti，则认为是无效Token
            //如果jti在黑名单中，则认为是无效Token
            if (string.IsNullOrEmpty(jti) || await tokenBlacklistService.IsBlacklistedAsync(jti))
            {
                context.Fail("Invalid or blacklisted token");
            }
        }

        /// <summary>
        /// 添加 Cookie 认证
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cookieOpt"></param>
        /// <returns></returns>
        private static AuthenticationBuilder AddSgrCookieAuthentication(this IServiceCollection services,
            HttpCookieOptions cookieOpt)
        {
            services.TryAddScoped<CustomCookieAuthenticationEvents>();
            /**
             * 参考： https://learn.microsoft.com/zh-cn/aspnet/core/security/authentication/cookie?view=aspnetcore-9.0
             */

            return services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    //设置存储用户登录信息（用户Token信息）的Cookie名称
                    options.Cookie.Name = cookieOpt.Name;
                    //设置存储用户登录信息（用户Token信息）的Cookie，无法通过客户端浏览器脚本(如JavaScript等)访问到
                    options.Cookie.HttpOnly = cookieOpt.HttpOnly;
                    // 过期时间
                    options.ExpireTimeSpan = TimeSpan.FromSeconds(cookieOpt.ExpireSeconds);
                    //是否在过期时间过半的时候，自动延期
                    options.SlidingExpiration = cookieOpt.SlidingExpiration;
                    //认证失败，会自动跳转到这个地址
                    options.LoginPath = cookieOpt.LoginPath;
                    //注销路径
                    if (!string.IsNullOrEmpty(cookieOpt.LogoutPath))
                        options.LogoutPath = cookieOpt.LogoutPath;
                    //访问拒绝页路径
                    if (!string.IsNullOrEmpty(cookieOpt.AccessDeniedPath))
                        options.AccessDeniedPath = cookieOpt.AccessDeniedPath;
                    //
                    if (!string.IsNullOrEmpty(cookieOpt.ReturnUrlParameter))
                        options.ReturnUrlParameter = cookieOpt.ReturnUrlParameter;

                    //// Secure 属性，确保通过 HTTPS 发送
                    options.Cookie.SecurePolicy = HttpCookieOptions.ToCookieSecurePolicy(cookieOpt.SecurePolicy);

                    //SameSite 策略
                    options.Cookie.SameSite = HttpCookieOptions.ToSameSiteMode(cookieOpt.SameSite);

                    //自定义事件处理程序
                    options.EventsType = typeof(CustomCookieAuthenticationEvents);
                    //options.Events = new CookieAuthenticationEvents
                    //{
                    //    OnRedirectToLogin = context =>
                    //    {
                    //    }
                    //};
                });
        }
    }
}