﻿/**************************************************************
 * 
 * 唯一标识：fdd21bff-f3b1-4308-939b-cd76cf65941d
 * 命名空间：Sgr.Generator.Snowflake
 * 创建时间：2024/6/21 14:49:43
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Generator.Snowflake
{
    public interface IWorkIdRegister
    {
        /// <summary>
        /// 注册 WorkId
        /// </summary>
        /// <param name="workId"></param>
        /// <param name="maxWorkId"></param>
        /// <returns></returns>
        int Register(int workId, int maxWorkId);
    }
}
