﻿/**************************************************************
 * 
 * 唯一标识：a1d823e8-e9e8-4206-9c28-fcd373841dc8
 * 命名空间：Sgr.Generator.Snowflake
 * 创建时间：2024/6/21 14:51:32
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Generator.Snowflake
{
    public class DefaultWorkIdRegister : IWorkIdRegister
    {
        public int Register(int workId, int maxWorkId)
        {
            if (workId < 0 && workId >= maxWorkId)
                throw new BusinessException($"Wordk超出合理范围 [ 0 <= WorkId < {maxWorkId} ]");

            return workId;
        }
    }
}
