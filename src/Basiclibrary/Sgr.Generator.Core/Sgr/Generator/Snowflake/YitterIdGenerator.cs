﻿/**************************************************************
 * 
 * 唯一标识：32264b06-0498-4143-b0a3-85965b2d1a4e
 * 命名空间：Sgr.Generator.Snowflake
 * 创建时间：2024/6/21 14:34:03
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yitter.IdGenerator;

namespace Sgr.Generator.Snowflake
{
    public class YitterIdGenerator : INumberIdGenerator
    {
        private readonly IIdGenerator _idGenerator;

        public YitterIdGenerator(IWorkIdRegister workIdRegister,
            IOptions<IdGeneratorOptions> option)
        {
            IdGeneratorOptions options = option.Value;

            if(options == null)
                options = new IdGeneratorOptions ();

            if (options.WorkerIdBitLength < 6)
                options.WorkerIdBitLength = 6;

            workIdRegister.Register(options.WorkerId, 63);

            _idGenerator = new DefaultIdGenerator(options);
        }


        public long GenerateUniqueId()
        {
            return _idGenerator.NewLong();
        }
    }
}
