﻿/**************************************************************
 * 
 * 唯一标识：44a8113a-98a9-4546-abe0-da1c1b023393
 * 命名空间：Sgr.Generator
 * 创建时间：2024/6/20 22:32:45
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Generator
{
    public interface ISegmentProvider
    {
        /// <summary>
        /// 获取待插入的Segment集合 Key=Id Value=Description
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetSegments();
    }
}
