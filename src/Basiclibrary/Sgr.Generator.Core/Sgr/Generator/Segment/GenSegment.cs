﻿/**************************************************************
 * 
 * 唯一标识：0ac2074a-fa91-48d2-addf-9c8a7ed85af0
 * 命名空间：Sgr.Generator.Segment
 * 创建时间：2024/6/20 11:04:14
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Generator.Segment
{
    public class GenSegment
    {
        private AtomicLong _value = new AtomicLong(0);
        private GenSegmentBuffer _buffer;

        public GenSegment(GenSegmentBuffer buffer)
        {
            _buffer = buffer;
        }

        public AtomicLong GetValue()
        {
            return _value;
        }

        public void SetValue(AtomicLong value)
        {
              _value = value;
        }



        public long Max { get; set; } = 0;

        public int Step { get; set; }

        public GenSegmentBuffer GetBuffer()
        {
            return _buffer;
        }

        public long GetIdle()
        {
            return this.Max - this.GetValue().Get();
        }
    }
}
