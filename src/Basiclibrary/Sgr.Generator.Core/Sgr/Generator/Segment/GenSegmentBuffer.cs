﻿/**************************************************************
 * 
 * 唯一标识：3a2173d2-fa2a-4cc4-b840-9d33e85c0a5a
 * 命名空间：Sgr.Generator.Segment
 * 创建时间：2024/6/20 11:10:53
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Generator.Segment
{
    public class GenSegmentBuffer
    {
        /// <summary>
        /// 标识
        /// </summary>
        private string _key = "";
        /// <summary>
        /// 双Buffer
        /// </summary>
        private GenSegment[] _segments;
        /// <summary>
        /// 当前的使用的 GenSegment 的index
        /// </summary>
        private volatile int _currentPos;
        /// <summary>
        /// 下一个 GenSegment 是否处于可切换状态
        /// </summary>
        private volatile bool _nextReady;
        /// <summary>
        /// 是否已初始化完成
        /// </summary>
        private volatile bool _initOk;
        /// <summary>
        /// 线程是否在运行中
        /// </summary>
        private AtomicBoolean _threadRunning;

        private ReaderWriterLockSlim _lockSlim;

        private volatile int _step;
        private volatile int _minStep;
        private long _updateTimestamp;

        public GenSegmentBuffer(string key)
        {
            _key = key;
            _segments = new GenSegment[] { new GenSegment(this),new GenSegment(this) };
            _currentPos = 0;
            _nextReady = false;
            _initOk = false;
            _threadRunning = new AtomicBoolean(false);

            _updateTimestamp = 0;
            _lockSlim = new  ReaderWriterLockSlim(); 
        }

        /// <summary>
        /// 标识
        /// </summary>
        public string Key { get => _key; }
        /// <summary>
        /// 是否已初始化完成
        /// </summary>
        public bool InitOk { get => _initOk; set => _initOk = value; }
        /// <summary>
        /// 下一个 GenSegment 是否处于可切换状态
        /// </summary>
        public bool NextReady { get => _nextReady; set => _nextReady = value; }
        /// <summary>
        /// 线程是否在运行中
        /// </summary>
        public AtomicBoolean GetThreadRunning()
        {
            return _threadRunning;
        }


        public int Step { get => _step; set => _step = value; }
        public int MinStep { get => _minStep; set => _minStep = value; }
        public long UpdateTimestamp { get => _updateTimestamp; set => _updateTimestamp = value; }

        /// <summary>
        /// 双Buffer
        /// </summary>
        public GenSegment[] Segments { get => _segments;}
        /// <summary>
        /// 读写锁
        /// </summary>
        public ReaderWriterLockSlim Lock { get => _lockSlim;  }

        /// <summary>
        /// 获取当前的使用的 GenSegment
        /// </summary>
        /// <returns></returns>
        public GenSegment GetCurrentGenSegment()
        {
            return Segments[_currentPos];
        }

        /// <summary>
        /// 获取当前的使用的 GenSegment 的index
        /// </summary>
        /// <returns></returns>
        public int GetCurrentPos()
        {
            return _currentPos;
        }

        /// <summary>
        /// 获取下一个 GenSegment 的index
        /// </summary>
        /// <returns></returns>
        public int NextPos()
        {
            return (_currentPos + 1) % 2;
        }

        /// <summary>
        /// 切换当前的使用的 GenSegment 的index
        /// </summary>
        public void SwitchPos()
        {
            _currentPos = NextPos();
        }




    }
}
