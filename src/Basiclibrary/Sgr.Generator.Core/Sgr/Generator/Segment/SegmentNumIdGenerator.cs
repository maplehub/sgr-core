﻿/**************************************************************
 * 
 * 唯一标识：dab05173-83b5-4919-b2b2-2932da3082a4
 * 命名空间：Sgr.Generator.Segment
 * 创建时间：2024/6/20 9:57:06
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.Logging;
using Sgr.Exceptions;
using Sgr.Generator.Segment.Entities;
using Sgr.Generator.Segment.Storage;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Generator.Segment
{
    public class SegmentNumIdGenerator : ISegmentNumIdGenerator
    {
        private bool _disposed;
        private volatile bool initOK = false;
        private ConcurrentDictionary<string, GenSegmentBuffer> cache = new ConcurrentDictionary<string, GenSegmentBuffer>();

        //最大步长不超过100,0000
        private static int MAX_STEP = 1000000;
        //一个Segment维持时间为15分钟
        private static long SEGMENT_DURATION = 15 * 60 * 1000L;

        private readonly ILogger<SegmentNumIdGenerator> _logger;
        private readonly ISegmentEntityDataStorage _genSegmentDataStorage;
        private readonly CancellationTokenSource _cts;

        private System.Threading.SemaphoreSlim slimlock = new SemaphoreSlim(1, 1);

        public SegmentNumIdGenerator(
            ISegmentEntityDataStorage genSegmentDataStorage,
            ILogger<SegmentNumIdGenerator> logger)
        {
            _genSegmentDataStorage = genSegmentDataStorage;
            _cts = new CancellationTokenSource();
            _logger = logger;
        }

        public async Task<bool> InitAsync()
        {
            _logger.LogInformation("SegmentNumIdGenerator Init ...");

            await updateCacheFromDb();

            initOK = true;

            //PS：暂时放弃定期更新Cache
            //updateCacheFromDbAtEveryMinute();

            return initOK;
        }


        public async Task<long> GetUniqueIdAsync(string key = "")
        {
            if (!initOK)
                throw new BusinessException("SegmentNumIdGenerator未完成初始化...");

            if (!cache.TryGetValue(key, out GenSegmentBuffer? buffer) || buffer == null)
                throw new BusinessException($"SegmentNumIdGenerator中不存在Key : {key}");

            //完成号段初始化
            if (!buffer.InitOk)
            {
                await slimlock.WaitAsync();
                try
                {
                    if (!buffer.InitOk)
                    {
                        try
                        {
                            await updateSegmentFromDb(key, buffer.GetCurrentGenSegment());

                            _logger.LogError($"Init buffer. Update KEY {key} With GenSegmentBuffer {buffer.GetCurrentPos} from db");

                            buffer.InitOk = true;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, $"Init GenSegmentBuffer {buffer.GetCurrentPos} exception");
                        }
                    }
                }
                finally
                {
                    slimlock.Release();
                }

                //System.Threading.Monitor.Enter(_locker);
                //try
                //{
                //    if (!buffer.InitOk)
                //    {
                //        try
                //        {
                //            await updateSegmentFromDb(key, buffer.GetCurrentGenSegment());

                //            _logger.LogError($"Init buffer. Update KEY {key} With GenSegmentBuffer {buffer.GetCurrentPos} from db");

                //            buffer.InitOk = true;
                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.LogError(ex, $"Init GenSegmentBuffer {buffer.GetCurrentPos} exception");
                //        }
                //    }
                //}
                //finally
                //{
                //    System.Threading.Monitor.Exit(_locker);
                //}

            }

            return getIdFromSegmentBuffer(buffer);
        }

        public long getIdFromSegmentBuffer(GenSegmentBuffer buffer)
        {
            long result;

            while (true)
            {
                buffer.Lock.EnterReadLock();
                try
                {
                    GenSegment segment = buffer.GetCurrentGenSegment();
  
                    if (!buffer.NextReady && (segment.GetIdle() < 0.9 * segment.Step) && buffer.GetThreadRunning().CompareAndSet(false,true))
                    {
                        //如果存在以下情况，则启动一个后台线程，用于加载一个新的号段
                        // 1. 下一组号段未准备好
                        // 2. 当前号段使用超过10%
                        // 3. 运行后台线程加载新号段的标识

                        Task.Run(async () =>
                        {
                            GenSegment next = buffer.Segments[buffer.NextPos()];

                            bool updateOk = false;

                            try
                            {
                                await updateSegmentFromDb(buffer.Key, next);

                                updateOk = true;

                                _logger.LogInformation($"update segment {buffer.Key} from db ");
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, $"updateSegmentFromDb exception, key = {buffer.Key}");
                            }
                            finally
                            {
                                if (updateOk)
                                {
                                    buffer.Lock.EnterWriteLock();

                                    buffer.NextReady = true;
                                    buffer.GetThreadRunning().Set(false);

                                    buffer.Lock.ExitWriteLock();
                                }
                                else
                                {
                                    buffer.GetThreadRunning().Set(false);
                                }
                            }
                        });
                    }
                     
                    long value = segment.GetValue().PreIncrement();
                    if (value < segment.Max)
                    {
                        result = value;
                        break;
                    }
                }
                finally
                {
                    buffer.Lock.ExitReadLock();
                }

                waitAndSleep(buffer);


                buffer.Lock.EnterWriteLock();
                try
                {
                    GenSegment segment = buffer.GetCurrentGenSegment();

                    long value = segment.GetValue().PreIncrement();
                    if (value < segment.Max)
                    {
                        result = value;
                        break;
                    }

                    if (buffer.NextReady)
                    {
                        buffer.SwitchPos();
                        buffer.NextReady = false;
                    }
                    else
                        throw new BusinessException($"Both two segments in {buffer.Key} are not ready!");
                }
                finally
                {
                    buffer.Lock.ExitWriteLock();
                }
            }

            return result;

        }


        private void waitAndSleep(GenSegmentBuffer buffer)
        {
            int roll = 0;
            while (buffer.GetThreadRunning().Get())
            {
                roll += 1;

                if(roll > 10000)
                {
                    try
                    {
                        Thread.Sleep(50);
                        break;
                    }
                    catch(Exception ex)
                    {
                        _logger.LogError(ex, "waitAndSleep");
                        break;
                    }
                }
            }
        }

        public async Task updateSegmentFromDb(string key, GenSegment segment)
        {
            GenSegmentBuffer buffer = segment.GetBuffer();

            SegmentEntity segmentEntity;
            if (!buffer.InitOk)
            {
                //如果未完成初始化,则执行初始化
                segmentEntity = await _genSegmentDataStorage.UpdateMaxIdAndGetGenSegmentAsync(key);

                buffer.Step = segmentEntity.Step;
                buffer.MinStep = segmentEntity.Step;
            }
            else
            {
                if (buffer.UpdateTimestamp == 0)
                {
                    //首次默认使用Step更新
                    segmentEntity = await _genSegmentDataStorage.UpdateMaxIdAndGetGenSegmentAsync(key);

                    buffer.UpdateTimestamp = CurrentTimeMillis();
                    buffer.Step = segmentEntity.Step;
                    buffer.MinStep = segmentEntity.Step;
                }
                else
                {
                    //非首次根据两次间的时间间隔来计算step，最后更新
                    long duration = CurrentTimeMillis() - buffer.UpdateTimestamp;
                    int nextStep = buffer.Step;
                    if (duration < SEGMENT_DURATION)
                    {
                        //如果获取一分段ID的时间间隔超过十五分钟，那么则步长加倍
                        if (nextStep * 2 > MAX_STEP)
                        {
                            //do nothing
                        }
                        else
                        {
                            nextStep = nextStep * 2;
                        }
                    }
                    else if (duration < SEGMENT_DURATION * 2)
                    {
                        //do nothing with nextStep
                    }
                    else
                    {
                        nextStep = (nextStep / 2 >= buffer.MinStep) ? (nextStep / 2) : nextStep;
                    }

                    _logger.LogInformation($"SegmentNumIdGenerator KEY [{key}], step [{buffer.Step}], duration [{((double)duration / (1000 * 60))}] mins , NextStep : [ {nextStep} ]");

                    segmentEntity = await _genSegmentDataStorage.UpdateMaxIdAndGetGenSegmentAsync(key, nextStep);

                    buffer.UpdateTimestamp = CurrentTimeMillis();
                    buffer.Step = nextStep;
                    buffer.MinStep = segmentEntity.Step;
                }
            }


            // must set value before set max
            long value = segmentEntity.MaxId - buffer.Step;

            segment.GetValue().Set(value);
            segment.Max = segmentEntity.MaxId;
            segment.Step = buffer.Step;
        }


        private async Task updateCacheFromDb()
        {
            _logger.LogInformation("SegmentNumIdGenerator updateCacheFromDb ");

            try
            {
                var dbTags = await _genSegmentDataStorage.GetIdsAsync();
                if (dbTags == null || dbTags.Count() == 0)
                    return;

                IEnumerable<string> cacheTags = cache.Keys;
                HashSet<string> insertTagsSet = new HashSet<string>(dbTags);
                HashSet<string> removeTagsSet = new HashSet<string>(cacheTags);

                //发现新的tags并写入缓存
                foreach (var item in cacheTags)
                {
                    if (insertTagsSet.Contains(item))
                        insertTagsSet.Remove(item);
                }

                foreach (var item in insertTagsSet)
                {
                    GenSegmentBuffer genSegment = new GenSegmentBuffer(item);

                    GenSegment segment = genSegment.GetCurrentGenSegment();
                    segment.SetValue(new Threading.AtomicLong(0));
                    segment.Max = 0;
                    segment.Step = 0;

                    cache.TryAdd(item, genSegment);
                }

                //已失效的删除
                foreach (var item in cacheTags)
                {
                    if (removeTagsSet.Contains(item))
                        removeTagsSet.Remove(item);
                }

                foreach (var item in removeTagsSet)
                {
                    cache.TryRemove(item, out _);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "update cache from db exception");
            }
        }

        private void updateCacheFromDbAtEveryMinute()
        {
            //每隔一分钟执行一次updateCacheFromDb操作
            Task.Run(async () =>
            {
                while (!_cts.IsCancellationRequested)
                {
                    await Task.Delay(60000, _cts.Token);

                    if (_disposed)
                        break;

                    await updateCacheFromDb();
                }
            }, _cts.Token);
        }

        private long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Constant.Jan1st1970).TotalMilliseconds;
        }

        #region IDisposable

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;

            _cts?.Cancel();
            _cts?.Dispose();
        }

        #endregion

    }
}
