﻿/**************************************************************
 * 
 * 唯一标识：a4841b0a-5905-4285-bca2-c13300436994
 * 命名空间：Sgr.Generator.Segment.Dal
 * 创建时间：2024/6/17 15:01:08
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Generator.Segment.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace Sgr.Generator.Segment.Storage
{
    public interface ISegmentEntityDataStorage
    {
        /// <summary>
        /// 插入标识
        /// </summary>
        /// <param name="key"></param>
        /// <param name="s_step"></param>
        /// <param name="s_description"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<bool> InsertAsync(string key, int s_step, string s_description, CancellationToken cancellationToken = default);
        /// <summary>
        /// 获取所有标识集合
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetIdsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// 获取所有的基于标识的Id分段信息
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<IEnumerable<SegmentEntity>> GetAllAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// 更新指定标识的MaxId并返回标识ID
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SegmentEntity> UpdateMaxIdAndGetGenSegmentAsync(string key,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// 使用自定义步长更新指定标识的MaxId并返回标识ID
        /// </summary>
        /// <param name="key"></param>
        /// <param name="step"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SegmentEntity> UpdateMaxIdAndGetGenSegmentAsync(string key,
            int step,
            CancellationToken cancellationToken = default);
    }
}
