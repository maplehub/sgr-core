﻿/**************************************************************
 * 
 * 唯一标识：1459a54c-bc46-4589-8154-7e64f50889cd
 * 命名空间：Sgr.Generator.Segment.Dal.Impl
 * 创建时间：2024/6/17 15:08:26
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Dapper;
using Sgr.Data;
using Sgr.Data.DatabaseTranslators;
using Sgr.Exceptions;
using Sgr.Generator.Segment.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Generator.Segment.Storage
{
    public class SegmentEntityDataStorage : ISegmentEntityDataStorage
    {
        private readonly DataConnectionString _dataConnectionString;
        private readonly IDatabaseTranslator _translator;

 
        public SegmentEntityDataStorage(
            IDataConnectionStringManager dataConnectionStringManager,
            IDatabaseTranslatorFactory databaseTranslatorFactory)
        {
            _dataConnectionString = dataConnectionStringManager.GetDataConnectionString(Constant.DEFAULT_DATABASE_SOURCE_NAME);
            _translator = databaseTranslatorFactory.CreateTranslator(_dataConnectionString.DbProvider);
        }


        private DbConnection GetDbConnection()
        {
           DbConnection dbConnection = _translator.CreateConnection() ?? throw new BusinessException("创建 DbConnection 失败!");
            dbConnection.ConnectionString = _dataConnectionString.ConnectionString;
            return dbConnection;
        }


        public virtual async Task<bool> InsertAsync(string key, int s_step, string s_description, CancellationToken cancellationToken = default)
        {
            using (DbConnection connection = GetDbConnection())
            {
                await connection.OpenAsync();

                string sql = $"INSERT INTO gen_id_generator (id,max_id, step, description) Values ({_translator.QuoteParameter("id")},{_translator.QuoteParameter("max_id")},{_translator.QuoteParameter("step")},{_translator.QuoteParameter("description")});";

                return await connection.ExecuteAsync(sql, new
                {
                    id = key,
                    max_id =1, 
                    step = s_step, 
                    description = s_description
                }) > 0;
            }
        }

        public virtual async Task<IEnumerable<SegmentEntity>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            using (DbConnection connection = GetDbConnection())
            {
                await connection.OpenAsync();

                string sql = "SELECT id AS Id, max_id AS MaxId, step AS Step, description AS Description FROM gen_id_generator";

                return await connection.QueryAsync<SegmentEntity>(sql);
            }
        }

        public virtual async Task<IEnumerable<string>> GetIdsAsync(CancellationToken cancellationToken = default)
        {
            using (DbConnection connection = GetDbConnection())
            {
                await connection.OpenAsync();

                string sql = "SELECT id as Id FROM gen_id_generator";

                return await connection.QueryAsync<string>(sql);
            }
        }

        public async Task<SegmentEntity> UpdateMaxIdAndGetGenSegmentAsync(string key, CancellationToken cancellationToken = default)
        {
            using (DbConnection connection = GetDbConnection())
            {
                await connection.OpenAsync();

                using (var transaction = await connection.BeginTransactionAsync())
                {
                    string updateSql = $"UPDATE gen_id_generator SET max_id = max_id + step WHERE id = {_translator.QuoteParameter("id")}";
                    string querySql = $"SELECT id AS Id, max_id AS MaxId, step AS Step, description AS Description FROM gen_id_generator WHERE id = {_translator.QuoteParameter("id")}";

                    if (await connection.ExecuteAsync(updateSql, new { id = key }, transaction) == 0)
                        throw new BusinessException($"更新号段失败失败! Key ={key}");

                    SegmentEntity result = await connection.QueryFirstAsync<SegmentEntity>(querySql, new { id = key }, transaction);

                    transaction.Commit();

                    return result;
                }
            }
        }

        public async Task<SegmentEntity> UpdateMaxIdAndGetGenSegmentAsync(string key, int step, CancellationToken cancellationToken = default)
        {
            using (DbConnection connection = GetDbConnection())
            {
                await connection.OpenAsync();

                using (var transaction = await connection.BeginTransactionAsync())
                {
                    string querySql = $"SELECT id AS Id, max_id AS MaxId, step AS Step, description AS Description FROM gen_id_generator WHERE id = {_translator.QuoteParameter("id")}";
                    string updateSql = $"UPDATE gen_id_generator SET max_id ={_translator.QuoteParameter("max_id")} WHERE id = {_translator.QuoteParameter("id")}";

                    SegmentEntity segmentEntity = await connection.QueryFirstAsync<SegmentEntity>(querySql, new { id = key }, transaction);
                    segmentEntity.MaxId = segmentEntity.MaxId + step;

                    if (await connection.ExecuteAsync(updateSql, new { id = key, max_id = segmentEntity.MaxId }, transaction) == 0)
                        throw new BusinessException($"更新号段失败失败! Key = {key} ; Step = {step}");

                    transaction.Commit();

                    return segmentEntity;
                }
            }
        }
    }
}
