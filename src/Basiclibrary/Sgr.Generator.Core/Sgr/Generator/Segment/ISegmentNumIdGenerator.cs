﻿/**************************************************************
 * 
 * 唯一标识：9e93f3e4-d7a8-448c-8253-fe22374c4b56
 * 命名空间：Sgr.Generator
 * 创建时间：2024/6/20 9:56:35
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Threading.Tasks;

namespace Sgr.Generator
{
    /// <summary>
    /// 基于数据库号段实现Id生成
    /// </summary>
    public interface ISegmentNumIdGenerator: IDisposable
    {
        /// <summary>
        /// 初始化
        /// </summary>
        Task<bool> InitAsync();
        /// <summary>
        /// 生成不重复的长整型唯一标识
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<long> GetUniqueIdAsync(string key = "");
    }
}
