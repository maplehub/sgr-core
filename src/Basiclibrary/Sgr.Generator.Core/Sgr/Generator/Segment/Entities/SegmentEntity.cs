﻿/**************************************************************
 * 
 * 唯一标识：cbe39b13-a175-4bae-a204-1e45bcf319b4
 * 命名空间：Sgr.Generator.Segment.Entities
 * 创建时间：2024/6/17 14:50:10
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Generator.Segment.Entities
{
    /// <summary>
    /// 基于标识的Id分段信息
    /// </summary>
    public class SegmentEntity: IEntity<string>
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string Id { get; set; } = "";
        /// <summary>
        /// 最大流水ID
        /// </summary>
        public long MaxId { get; set; } = 1;
        /// <summary>
        /// 步长
        /// </summary>
        public int Step { get; set; }

        public string Description { get; set; } = "";

        public bool IsTransient()
        {
            return string.IsNullOrEmpty(Id);
        }
    }
}
