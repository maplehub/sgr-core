﻿/**************************************************************
 * 
 * 唯一标识：a741b6b1-9ee6-4f46-85f4-b813b7979af2
 * 命名空间：Sgr.Generator.Segment.Sead
 * 创建时间：2024/6/20 22:18:58
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.Logging;
using Sgr.Data;
using Sgr.Data.DatabaseTranslators;
using Sgr.Generator.Segment.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Generator.Segment.Sead
{
    public class SegmentDataBaseInitialize : IDataBaseInitialize
    {
        private readonly ISegmentEntityDataStorage _segmentEntityDataStorage;
        private readonly IEnumerable<ISegmentProvider> _segmentProviders;
        private readonly ILogger<SegmentDataBaseInitialize> _logger;

        public SegmentDataBaseInitialize(
            ISegmentEntityDataStorage segmentEntityDataStorage,
            IEnumerable<ISegmentProvider> segmentProviders,
            ILogger<SegmentDataBaseInitialize> logger)
        {
            _logger = logger;

            _segmentProviders = segmentProviders;
            _segmentEntityDataStorage = segmentEntityDataStorage;
        }


        public int Order => 102;

        public async Task Initialize()
        {
            var keys = await _segmentEntityDataStorage.GetIdsAsync();

            foreach (var provider in _segmentProviders)
            {
                var dic = provider.GetSegments();

                foreach (var item in dic)
                {
                    if (!keys.Contains(item.Key))
                    {
                        _logger.LogInformation($"Insert Segment : {item.Key}  ...");
                        if (!await _segmentEntityDataStorage.InsertAsync(item.Key, 2000, item.Value))
                            _logger.LogError($"Insert Segment : {item.Key} Fails !");
                    }
                }
            }
        }
    }
}
