﻿/**************************************************************
 * 
 * 唯一标识：e923503a-32db-4fef-a09e-abb51fe4f95e
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2024/6/21 11:08:20
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.Data.DatabaseTranslators;
using Sgr.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sgr.Generator.Segment.Sead;
using Sgr.Generator.Segment.Storage;
using Sgr.Generator.Segment;
using Sgr.Generator;
using Yitter.IdGenerator;
using Sgr.Generator.Snowflake;
using Microsoft.Extensions.Logging;
using Sgr.Utilities;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSgrGenerator(this IServiceCollection services)
        {
            //Segment
            services.TryAddTransient<IDataBaseInitialize, SegmentDataBaseInitialize>();
            services.TryAddSingleton<ISegmentEntityDataStorage, SegmentEntityDataStorage>();

            //services.TryAddSingleton<ISegmentNumIdGenerator, SegmentNumIdGenerator>();
            if (!services.Any(f => f.ServiceType == typeof(ISegmentNumIdGenerator)))
            {
                services.AddSingleton<ISegmentNumIdGenerator>(sp =>
                {
                    ISegmentEntityDataStorage genSegmentDataStorage = sp.GetRequiredService<ISegmentEntityDataStorage>();
                    ILogger<SegmentNumIdGenerator> logger = sp.GetRequiredService<ILogger<SegmentNumIdGenerator>>();

                    ISegmentNumIdGenerator segmentNumIdGenerator = new SegmentNumIdGenerator(genSegmentDataStorage, logger);
                    AsyncHelper.RunSync(() => segmentNumIdGenerator.InitAsync());
                    //segmentNumIdGenerator.InitAsync().ConfigureAwait(false).GetAwaiter().GetResult();

                    return segmentNumIdGenerator;
                });
            }


            //Snowflake
            services.TryAddTransient<IWorkIdRegister, DefaultWorkIdRegister>();
            services.AddOptions<IdGeneratorOptions>()
                .BindConfiguration("ThirdParty:Yitter_IdGenerator");

            if (services.Any(f => f.ServiceType == typeof(INumberIdGenerator)))
                services.Replace(ServiceDescriptor.Singleton<INumberIdGenerator, YitterIdGenerator>());
            else
                services.AddSingleton<INumberIdGenerator, YitterIdGenerator>();

            return services;
        }
    }
}
