﻿/**************************************************************
 * 
 * 唯一标识：4d64b13d-1cd7-45fb-8dc3-09baa186e9fa
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/21 16:32:41
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Data.DatabaseTranslators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data
{
    internal class SqlServerDbProviderFactoryBuilder : IDbProviderFactoryBuilder
    {
        public DatabaseType DatabaseType => DatabaseType.SqlServer;

        public DbProviderFactory GetDbProviderFactory()
        {
            return Microsoft.Data.SqlClient.SqlClientFactory.Instance;

        }
    }
}