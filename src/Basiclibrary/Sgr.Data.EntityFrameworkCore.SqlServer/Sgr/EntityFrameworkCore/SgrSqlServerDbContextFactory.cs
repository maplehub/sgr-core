﻿/**************************************************************
 * 
 * 唯一标识：f479b412-c95d-44d2-9be3-c9d0fa1053f0
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/19 11:25:11
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.EntityFrameworkCore
{
    public class SgrSqlServerDbContextFactory
    {
        public virtual SgrDbContext CreateSqrDbContext(string connectionString, IMediator mediator, Action<SqlServerDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrDbContext>(connectionString, optionsAction);

            var dbContext = new SgrDbContext(optionsBuilder.Options, mediator);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }

        public virtual SgrSlaveDbContext CreateSgrSlaveDbContext(string connectionString, Action<SqlServerDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrSlaveDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrSlaveDbContext>(connectionString, optionsAction);

            var dbContext = new SgrSlaveDbContext(optionsBuilder.Options);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }


        protected virtual DbContextOptionsBuilder<TContext> getDbContextOptionsBuilder<TContext>(string connectionString, Action<SqlServerDbContextOptionsBuilder> optionsAction) where TContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

#if DEBUG
            Console.WriteLine("DB Connection String :" + connectionString);
#endif
            optionsBuilder.UseSqlServer(connectionString,
                builder => optionsAction?.Invoke(builder)); //b => b.MigrationsAssembly("Sgr.Admin.WebHost"));
            return optionsBuilder;
        }
    }
}
