﻿/**************************************************************
 *
 * 唯一标识：aec34dd2-875a-4045-82a5-6a9dd8ac7b9a
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/19 11:16:15
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Sgr.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.EntityFrameworkCore
{
    public static class SgrDbContextExtensions
    {
        public static IServiceCollection UseSqlServer(
            this IServiceCollection services,
            string name,
            Action<SqlServerDbContextOptionsBuilder> sqlServerDbContextOptionsBuilder,
            ILoggerFactory? loggerFactory = null)
        {
            services.AddDbContext<SgrDbContext>((serviceProvider, dbContextBuilder) =>
            {
                IDataConnectionStringResolve resolve = serviceProvider.GetRequiredService<IDataConnectionStringResolve>();
                string connectionString = resolve.GetMainDatabaseConnectionString(name);

                dbContextBuilder.UseSqlServer(connectionString,
                    optionsBuilder =>
                    {
                        optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                        sqlServerDbContextOptionsBuilder?.Invoke(optionsBuilder);
                    });

                if (loggerFactory != null)
                    dbContextBuilder.UseLoggerFactory(loggerFactory);
            });

            services.AddDbContext<SgrSlaveDbContext>((serviceProvider, dbContextBuilder) =>
            {
                IDataConnectionStringResolve resolve = serviceProvider.GetRequiredService<IDataConnectionStringResolve>();
                string connectionString = resolve.GetSlaveDatabaseConnectionString(name);

                dbContextBuilder.UseSqlServer(connectionString,
                    optionsBuilder =>
                    {
                        optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                        sqlServerDbContextOptionsBuilder?.Invoke(optionsBuilder);
                    });

                if (loggerFactory != null)
                    dbContextBuilder.UseLoggerFactory(loggerFactory);
            });

            return services;
        }
    }
}