﻿/**************************************************************
 * 
 * 唯一标识：1af2bbdd-3675-47e8-957e-4b3d5d5f34da
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2024/6/21 16:35:21
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.Data;
using Sgr.Data.DatabaseTranslators;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSgrSqlServerDbProvider(this IServiceCollection services)
        {
            services.TryAddTransient<IDbProviderFactoryBuilder, SqlServerDbProviderFactoryBuilder>();

            return services;
        }
    }
}
