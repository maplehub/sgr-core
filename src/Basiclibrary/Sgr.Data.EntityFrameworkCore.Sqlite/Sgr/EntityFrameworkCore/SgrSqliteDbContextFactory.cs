﻿/**************************************************************
 * 
 * 唯一标识：e63d0d57-41c3-4ed3-b822-cc884b4f0cde
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/19 11:24:01
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.EntityFrameworkCore
{
    public class SgrSqliteDbContextFactory
    {
        public virtual SgrDbContext CreateSqrDbContext(string connectionString, IMediator mediator, Action<SqliteDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrDbContext>(connectionString, optionsAction);

            var dbContext = new SgrDbContext(optionsBuilder.Options, mediator);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }

        public virtual SgrSlaveDbContext CreateSgrSlaveDbContext(string connectionString, Action<SqliteDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrSlaveDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrSlaveDbContext>(connectionString, optionsAction);

            var dbContext = new SgrSlaveDbContext(optionsBuilder.Options);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }


        protected virtual DbContextOptionsBuilder<TContext> getDbContextOptionsBuilder<TContext>(string connectionString, Action<SqliteDbContextOptionsBuilder> optionsAction) where TContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

#if DEBUG
            Console.WriteLine("DB Connection String :" + connectionString);
#endif
            optionsBuilder.UseSqlite(connectionString,
                builder => optionsAction?.Invoke(builder)); //b => b.MigrationsAssembly("Sgr.Admin.WebHost"));
            return optionsBuilder;
        }
    }
}
