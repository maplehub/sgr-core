﻿/**************************************************************
 * 
 * 唯一标识：d4b3c1d1-6137-4b9a-8713-8aaf53b8e817
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/21 16:31:37
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Data.DatabaseTranslators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data
{
    public class SqliteDbProviderFactoryBuilder : IDbProviderFactoryBuilder
    {
        public DatabaseType DatabaseType => DatabaseType.SQLite;

        public DbProviderFactory GetDbProviderFactory()
        {
            return Microsoft.Data.Sqlite.SqliteFactory.Instance;
        }
    }
}