﻿/**************************************************************
 *
 * 唯一标识：0068360b-b7ef-4980-802c-0ac9a4973466
 * 命名空间：Sgr.Caching.Services
 * 创建时间：2023/8/23 7:21:33
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Sgr.Exceptions;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Caching.Services
{
    public class MemoryCacheManager : ICacheManager
    {
        //private static readonly object _mutex = new();

        private bool _disposed;
        private CancellationTokenSource _clearToken = new();

        private readonly ConcurrentDictionary<string, byte> _keysList = new();

        private readonly CacheOptions _cacheOptions;
        private readonly IMemoryCache _memoryCache;

        public MemoryCacheManager(IOptions<CacheOptions> options, IMemoryCache memoryCache)
        {
            _cacheOptions = options.Value;
            _memoryCache = memoryCache;
        }

        #region ICacheManager

        public Task SetAsync(string key, object obj, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();

            Set(key, obj, cacheEntryOptions);

            return Task.CompletedTask;
        }

        public void Set(string key, object obj, CacheEntryOptions? cacheEntryOptions = null)
        {
            if (string.IsNullOrEmpty(key))
                return;

            if (obj == null)
                return;

            _memoryCache.Set(key, obj, prepareMemoryCacheEntryOptions(cacheEntryOptions));

            OnAddKey(key);
        }

        public async Task<TData> GetAsync<TData>(string key, Func<Task<TData>> acquire, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            if (string.IsNullOrEmpty(key))
                throw new BusinessException("ICacheManager.GetAsync Parameter Key Is Null Or Empty!");

            token.ThrowIfCancellationRequested();

            if (_memoryCache.TryGetValue(key, out TData? result) && result != null)
                return result!;

            result = await acquire();

            if (result != null)
                await SetAsync(key, result, cacheEntryOptions);

            return result;

            //bool isNew = false;

            //var result = await _memoryCache.GetOrCreateAsync(key, async entry =>
            //{
            //    entry.SetOptions(prepareMemoryCacheEntryOptions(cacheEntryOptions));
            //    isNew = true;
            //    return await acquire();
            //});

            ////do not cache null value
            //if (result == null)
            //    await RemoveAsync(key);
            //else
            //{
            //    if(isNew)
            //        OnAddKey(key);
            //}

            //return result;
        }

        public async Task<TData> GetAsync<TData>(string key, Func<TData> acquire, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            if (string.IsNullOrEmpty(key))
                throw new BusinessException("ICacheManager.GetAsync Parameter Key Is Null Or Empty!");

            token.ThrowIfCancellationRequested();

            if (_memoryCache.TryGetValue(key, out TData? result) && result != null)
                return result!;

            result = acquire();

            if (result != null)
                await SetAsync(key, result, cacheEntryOptions);

            return result;

            //bool isNew = false;

            //var result = _memoryCache.GetOrCreate(key, entry =>
            //{
            //    entry.SetOptions(prepareMemoryCacheEntryOptions(cacheEntryOptions));
            //    isNew = true;
            //    return acquire();
            //});

            ////do not cache null value
            //if (result == null)
            //    await RemoveAsync(key);
            //else
            //{
            //    if (isNew)
            //        OnAddKey(key);
            //}

            //return result;
        }

        public TData Get<TData>(string key, Func<TData> acquire, CacheEntryOptions? cacheEntryOptions = null)
        {
            if (string.IsNullOrEmpty(key))
                return acquire();

            if (_memoryCache.TryGetValue(key, out TData? result) && result != null)
                return result!;

            result = acquire();

            if (result != null)
                Set(key, result, cacheEntryOptions);

            return result;

            //bool isNew = false;

            //var result = _memoryCache.GetOrCreate(key, entry =>
            //{
            //    entry.SetOptions(prepareMemoryCacheEntryOptions(cacheEntryOptions));
            //    isNew = true;
            //    return acquire();
            //});

            ////do not cache null value
            //if (result == null)
            //     Remove(key);
            //else
            //{
            //    if (isNew)
            //        OnAddKey(key);
            //}
            //return result;
        }

        public Task RemoveAsync(string key, CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();
            Remove(key);
            return Task.CompletedTask;
        }

        public void Remove(string key)
        {
            _memoryCache.Remove(key);
            OnRemoveKey(key);
        }

        public Task RemoveByPrefixAsync(string prefix, CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();

            RemoveByPrefix(prefix);

            return Task.CompletedTask;
        }

        public void RemoveByPrefix(string prefix)
        {
            var keys = _keysList
                .Keys
                .Where(key => key.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            //var keys = _keysList.Where(kv => kv.Key.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
            //    .Select(kv => kv.Key)
            //    .ToList();

            foreach (var key in keys)
            {
                _memoryCache.Remove(key);
                _keysList.TryRemove(key, out _);
            }
        }

        public Task ClearAsync(CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();

            Clear();

            return Task.CompletedTask;
        }

        public void Clear()
        {
            _clearToken.Cancel();
            _clearToken.Dispose();

            _clearToken = new CancellationTokenSource();

            _keysList.Clear();
        }

        public CacheEntryOptions CreateCacheEntryOptions()
        {
            return _cacheOptions.CreateCacheEntryOptions();
        }

        #region Dispose

        /// <summary>
        /// Dispose cache manager
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _memoryCache.Dispose();

            _disposed = true;
        }

        #endregion Dispose

        #endregion ICacheManager

        protected virtual MemoryCacheEntryOptions prepareMemoryCacheEntryOptions(CacheEntryOptions? cacheEntryOptions)
        {
            //设置过期时间
            var options = new MemoryCacheEntryOptions();

            cacheEntryOptions ??= _cacheOptions.CreateCacheEntryOptions();

            options.SlidingExpiration = cacheEntryOptions!.SlidingExpiration;
            options.AbsoluteExpirationRelativeToNow = cacheEntryOptions!.AbsoluteExpirationRelativeToNow;
            options.AbsoluteExpiration = cacheEntryOptions!.AbsoluteExpiration;

            //设置清理缓存所需的令牌
            options.AddExpirationToken(new CancellationChangeToken(_clearToken.Token));

            return options;
        }

        protected virtual void OnAddKey(string key)
        {
            _keysList.TryAdd(key, default);
        }

        protected virtual void OnRemoveKey(string key)
        {
            _keysList.TryRemove(key, out _);
        }
    }
}