﻿/**************************************************************
 * 
 * 唯一标识：dcb12c46-434f-4e2b-8de9-f8f022a26596
 * 命名空间：Sgr.Data
 * 创建时间：2024/6/21 16:29:59
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Data.DatabaseTranslators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Data
{
    public class PostgreSqlDbProviderFactoryBuilder : IDbProviderFactoryBuilder
    {
        public DatabaseType DatabaseType => DatabaseType.PostgreSql;

        public DbProviderFactory GetDbProviderFactory()
        {
            return Npgsql.NpgsqlFactory.Instance;
        }
    }
}