﻿/**************************************************************
 * 
 * 唯一标识：b2cafde3-ddde-4f72-a884-908c3d7403fa
 * 命名空间：Sgr.EntityFrameworkCore
 * 创建时间：2024/6/19 11:22:42
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;
using System; 
namespace Sgr.EntityFrameworkCore
{
    public class SgrPostgreSqlDbContextFactory
    {
        public virtual SgrDbContext CreateSqrDbContext(string connectionString, IMediator mediator, Action<NpgsqlDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrDbContext>(connectionString, optionsAction);

            var dbContext = new SgrDbContext(optionsBuilder.Options, mediator);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }

        public virtual SgrSlaveDbContext CreateSgrSlaveDbContext(string connectionString, Action<NpgsqlDbContextOptionsBuilder> optionsAction)
        {
            DbContextOptionsBuilder<SgrSlaveDbContext> optionsBuilder = getDbContextOptionsBuilder<SgrSlaveDbContext>(connectionString, optionsAction);

            var dbContext = new SgrSlaveDbContext(optionsBuilder.Options);
            //dbContext.Database.EnsureCreated();
            return dbContext;
        }


        protected virtual DbContextOptionsBuilder<TContext> getDbContextOptionsBuilder<TContext>(string connectionString, Action<NpgsqlDbContextOptionsBuilder> optionsAction) where TContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();

#if DEBUG
            Console.WriteLine("DB Connection String :" + connectionString);
#endif
            optionsBuilder.UseNpgsql(connectionString, 
                builder => optionsAction?.Invoke(builder)); //b => b.MigrationsAssembly("Sgr.Admin.WebHost"));
            return optionsBuilder;
        }
    }
}
