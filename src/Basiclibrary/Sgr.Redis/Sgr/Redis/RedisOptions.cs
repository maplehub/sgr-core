﻿using Microsoft.Extensions.Options;
using StackExchange.Redis;
using StackExchange.Redis.Profiling;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Redis
{
    public class RedisOptions
    {
        /// <summary>
        /// 宿主程序名称
        /// </summary>
        public string? InstanceName { get; set; }
        /// <summary>
        /// Redis连接字符串
        /// </summary>
        public string? ConnectionString { get; set; }
        /// <summary>
        /// Redis连接配置
        /// </summary>
        public ConfigurationOptions? ConfigurationOptions { get; set; }
        /// <summary>
        /// 性能分析函数
        /// </summary>
        public Func<ProfilingSession>? ProfilingSession { get; set; }
        /// <summary>
        /// 网络或连接异常后是否强制执行重连
        /// </summary>
        public bool UseForceReconnectWhenConnectionException { get; set; }

        internal ConfigurationOptions GetConfiguredOptions()
        {
            var options = ConfigurationOptions ?? ConfigurationOptions.Parse(ConnectionString!);
            options.AbortOnConnectFail = false;
            return options;
        }
    }
}
