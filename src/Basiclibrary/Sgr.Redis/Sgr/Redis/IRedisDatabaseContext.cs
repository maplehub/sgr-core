﻿using StackExchange.Redis;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Threading;

namespace Sgr.Redis
{
    /// <summary>
    /// Redis数据库连接上下文
    /// </summary>
    public interface IRedisDatabaseContext : IDisposable
    {
        RedisKey InstancePrefix { get; }

        /// <summary>
        /// 同步获取 IDatabase
        /// </summary>
        /// <returns></returns>
        IDatabase Connect();

        /// <summary>
        /// 异步获取 IDatabase
        /// </summary>
        /// <returns></returns>
        ValueTask<IDatabase> ConnectAsync(CancellationToken token = default);
        /// <summary>
        /// 发生异常后的处理
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="cache"></param>
        void OnRedisError(Exception exception, IDatabase cache);

        ///// <summary>
        ///// 获取 IServer
        ///// </summary>
        ///// <param name="endPoint"></param>
        ///// <returns></returns>
        //IServer GetServer(EndPoint endPoint);
        ///// <summary>
        ///// 获取 EndPoint[]
        ///// </summary>
        ///// <returns></returns>
        //EndPoint[] GetEndPoints();

    }
}