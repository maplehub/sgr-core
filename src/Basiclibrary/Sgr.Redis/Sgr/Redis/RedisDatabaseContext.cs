﻿using Microsoft.Extensions.Options;
using Sgr.Exceptions;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Redis
{
    /// <summary>
    /// Redis数据库连接上下文
    /// </summary>
    public class RedisDatabaseContext : IRedisDatabaseContext
    {
        private readonly TimeSpan ReconnectMinInterval = TimeSpan.FromSeconds(60);      //两次连接至少间隔1分钟
        private readonly TimeSpan ReconnectErrorThreshold = TimeSpan.FromSeconds(30);   //只有在异常发生30秒后才执行重连

        private readonly SemaphoreSlim _connectionLock = new SemaphoreSlim(initialCount: 1, maxCount: 1);
        private readonly RedisOptions _options;
        private readonly RedisKey _instancePrefix;

        private long _lastConnectTicks = DateTimeOffset.UtcNow.Ticks;                   //最近一次连接的时间
        private long _firstErrorTimeTicks;                                              //第一次连接异常的时间
        private long _previousErrorTimeTicks;                                           //上一次连接异常的时间
        private bool _disposed;

        private volatile IDatabase? _cache;

        private static readonly Version ServerVersionWithExtendedSetCommand = new Version(4, 0, 0);

        /// <summary>
        /// Redis数据库连接上下文
        /// </summary>
        /// <param name="options"></param>
        public RedisDatabaseContext(IOptions<RedisOptions> options)
        {
            Check.NotNull(options, nameof(options));

            _options = options.Value;

            if (string.IsNullOrEmpty(_options.InstanceName))
                _instancePrefix = (RedisKey)Encoding.UTF8.GetBytes(Environment.MachineName + ".");
            else
                _instancePrefix = (RedisKey)Encoding.UTF8.GetBytes(_options.InstanceName);
        }

        /// <summary>
        /// Redis Key 前缀
        /// </summary>
        public RedisKey InstancePrefix => _instancePrefix;

        /// <summary>
        /// 获取 IDatabase
        /// </summary>
        /// <returns></returns>
        public IDatabase Connect()
        {
            //检查当前类未开始执行释放操作
            CheckDisposed();

            var cache = _cache;
            if (cache != null)
                return cache;

            _connectionLock.Wait();
            try
            {
                cache = _cache;
                if (cache is null)
                {
                    IConnectionMultiplexer connection = ConnectionMultiplexer.Connect(_options.GetConfiguredOptions());

                    if (!connection.IsConnected)
                        throw new BusinessException("redis cannot connect!");

                    PrepareConnection(connection);
                    cache = _cache = connection.GetDatabase();
                }
                return cache;
            }
            finally
            {
                _connectionLock.Release();
            }
        }

        public async ValueTask<IDatabase> ConnectAsync(CancellationToken token = default)
        {
            CheckDisposed();
            token.ThrowIfCancellationRequested();

            var cache = _cache;
            if (cache != null)
                return cache;

            await _connectionLock.WaitAsync(token).ConfigureAwait(false);

            try
            {
                cache = _cache;

                if (cache is null)
                {
                    IConnectionMultiplexer connection = await ConnectionMultiplexer.ConnectAsync(_options.GetConfiguredOptions()).ConfigureAwait(false);

                    if (!connection.IsConnected)
                        throw new BusinessException("redis cannot connect!");

                    PrepareConnection(connection);
                    cache = _cache = connection.GetDatabase();
                }
                return cache;
            }
            finally
            {
                _connectionLock.Release();
            }
        }

        public void OnRedisError(Exception exception, IDatabase cache)
        {
            if (_options.UseForceReconnectWhenConnectionException
                && (exception is RedisConnectionException or SocketException))
            {
                //获取距离上一次执行连接的时间间隔
                var utcNow = DateTimeOffset.UtcNow;
                var previousConnectTime = ReadTimeTicks(ref _lastConnectTicks);
                TimeSpan elapsedSinceLastReconnect = utcNow - previousConnectTime;

                //确保两次连接至少间隔1分钟
                if (elapsedSinceLastReconnect < ReconnectMinInterval)
                    return;

                //首次异常时间
                var firstErrorTime = ReadTimeTicks(ref _firstErrorTimeTicks);
                if (firstErrorTime == DateTimeOffset.MinValue)
                {
                    WriteTimeTicks(ref _firstErrorTimeTicks, utcNow);
                    WriteTimeTicks(ref _previousErrorTimeTicks, utcNow);
                    return;
                }

                TimeSpan elapsedSinceFirstError = utcNow - firstErrorTime;
                TimeSpan elapsedSinceMostRecentError = utcNow - ReadTimeTicks(ref _previousErrorTimeTicks);

                //确认发生异常后30秒才执行重连
                bool shouldReconnect =
                   elapsedSinceFirstError >= ReconnectErrorThreshold
                   && elapsedSinceMostRecentError <= ReconnectErrorThreshold;

                WriteTimeTicks(ref _previousErrorTimeTicks, utcNow);

                if (!shouldReconnect)
                {
                    return;
                }

                WriteTimeTicks(ref _firstErrorTimeTicks, DateTimeOffset.MinValue);
                WriteTimeTicks(ref _previousErrorTimeTicks, DateTimeOffset.MinValue);

                var tmp = Interlocked.CompareExchange(ref _cache, null, cache);
                if (ReferenceEquals(tmp, cache))
                {
                    ReleaseConnection(tmp);
                }
            }
        }

        ///// <summary>
        ///// 获取 IServer
        ///// </summary>
        ///// <param name="endPoint"></param>
        ///// <returns></returns>
        //public IServer GetServer(EndPoint endPoint)
        //{
        //    return GetConnection().GetServer(endPoint);
        //}

        ///// <summary>
        ///// 获取 EndPoint[]
        ///// </summary>
        ///// <returns></returns>
        //public EndPoint[] GetEndPoints()
        //{
        //    return GetConnection().GetEndPoints();
        //}

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;
            ReleaseConnection(Interlocked.Exchange(ref _cache, null));
        }

        private void PrepareConnection(IConnectionMultiplexer connection)
        {
            //更新最近一次更新的时间
            WriteTimeTicks(ref _lastConnectTicks, DateTimeOffset.UtcNow);

            ValidateServerFeatures(connection);
            TryRegisterProfiler(connection);
        }

        private void CheckDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(this.GetType().FullName);
        }

        private void ReleaseConnection(IDatabase? cache)
        {
            var connection = cache?.Multiplexer;
            if (connection is not null)
            {
                try
                {
                    connection.Close();
                    connection.Dispose();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void ValidateServerFeatures(IConnectionMultiplexer connection)
        {
            _ = connection ?? throw new InvalidOperationException($"{nameof(connection)} cannot be null.");

            foreach (var endPoint in connection.GetEndPoints())
            {
                if (connection.GetServer(endPoint).Version < ServerVersionWithExtendedSetCommand)
                    throw new NotSupportedException("redis must 4.0 and above versions!");
            }
        }

        private void TryRegisterProfiler(IConnectionMultiplexer connection)
        {
            _ = connection ?? throw new InvalidOperationException($"{nameof(connection)} cannot be null.");

            if (_options.ProfilingSession is not null)
            {
                connection.RegisterProfiler(_options.ProfilingSession);
            }
        }

        private static void WriteTimeTicks(ref long field, DateTimeOffset value)
        {
            var ticks = value == DateTimeOffset.MinValue ? 0L : value.UtcTicks;
            Volatile.Write(ref field, ticks); // avoid torn values
        }

        private static DateTimeOffset ReadTimeTicks(ref long field)
        {
            var ticks = Volatile.Read(ref field); // avoid torn values
            return ticks == 0 ? DateTimeOffset.MinValue : new DateTimeOffset(ticks, TimeSpan.Zero);
        }
    }
}