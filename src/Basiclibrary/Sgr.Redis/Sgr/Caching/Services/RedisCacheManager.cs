﻿using Microsoft.Extensions.Options;
using Sgr.Redis;
using Sgr.Utilities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Sgr.Caching.Services
{
    public class RedisCacheManager: ICacheManager
    {
        private const long NotPresent = -1;

        // KEYS[1] = = 缓存的 key
        // ARGV[1] = 绝对过期时间 absolute-expiration - ticks as long (-1 for none)
        // ARGV[2] = 滑动过期时间 sliding-expiration - ticks as long (-1 for none)
        // ARGV[3] = 相对有效期 relative-expiration (long, in seconds, -1 for none) - Min(absolute-expiration - Now, sliding-expiration)
        // ARGV[4] = 缓存的数据 data - byte[]
        // this order should not change LUA script depends on it
        private const string SetScript = (@"
                redis.call('HSET', KEYS[1], 'absexp', ARGV[1], 'sldexp', ARGV[2], 'data', ARGV[4])
                if ARGV[3] ~= '-1' then
                  redis.call('EXPIRE', KEYS[1], ARGV[3])
                end
                return 1");

        private const string AbsoluteExpirationKey = "absexp";
        private const string SlidingExpirationKey = "sldexp";
        private const string DataKey = "data";

        // combined keys - same hash keys fetched constantly; avoid allocating an array each time
        private static readonly RedisValue[] _hashMembersAbsoluteExpirationSlidingExpirationData = new RedisValue[] { AbsoluteExpirationKey, SlidingExpirationKey, DataKey };
        private static readonly RedisValue[] _hashMembersAbsoluteExpirationSlidingExpiration = new RedisValue[] { AbsoluteExpirationKey, SlidingExpirationKey };

        private static RedisValue[] GetHashFields(bool getData) => getData
            ? _hashMembersAbsoluteExpirationSlidingExpirationData
            : _hashMembersAbsoluteExpirationSlidingExpiration;


        private readonly IRedisDatabaseContext _redisDatabaseContext;
        private readonly CacheOptions _cacheOptions;

        public RedisCacheManager(IOptions<CacheOptions> options,
            IRedisDatabaseContext redisDatabaseContext)
        {
            _cacheOptions = options.Value;
            _redisDatabaseContext = redisDatabaseContext;
        }

        #region Serializer

        private RedisValue objectRerialize(object obj)
        {
            return JsonSerializer.Serialize(obj, JsonHelper.GetJsonSerializerOptions());
        }

        private T? objectDeserialize<T>(RedisValue value)
        {
            return JsonSerializer.Deserialize<T>(value.ToString(), JsonHelper.GetJsonSerializerOptions());
        }

        #endregion


        #region ICacheManager

        public void Set(string key, object obj, CacheEntryOptions? cacheEntryOptions = null)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));
            Check.NotNull(obj, nameof(obj));

            cacheEntryOptions ??= _cacheOptions.CreateCacheEntryOptions();

            var redisDatabase = _redisDatabaseContext.Connect();

            var creationTime = DateTimeOffset.UtcNow;
            var absoluteExpiration = getAbsoluteExpiration(creationTime, cacheEntryOptions);
             
            try
            {
                redisDatabase.ScriptEvaluate(SetScript, new RedisKey[] { _redisDatabaseContext.InstancePrefix.Append(key) },
                    new RedisValue[]
                    {
                        absoluteExpiration?.Ticks ?? NotPresent,                            //绝对过期时间
                        cacheEntryOptions!.SlidingExpiration?.Ticks ?? NotPresent,          //滑动过期时间
                        getExpirationInSeconds(creationTime, absoluteExpiration, cacheEntryOptions) ?? NotPresent,
                        objectRerialize(obj)
                    });
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }
        }

        public async Task SetAsync(string key, object obj, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));
            Check.NotNull(obj, nameof(obj));

            cacheEntryOptions ??= _cacheOptions.CreateCacheEntryOptions();

            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            var creationTime = DateTimeOffset.UtcNow;
            var absoluteExpiration = getAbsoluteExpiration(creationTime, cacheEntryOptions);

            try
            {
               await redisDatabase.ScriptEvaluateAsync(SetScript, new RedisKey[] { _redisDatabaseContext.InstancePrefix.Append(key) },
                    new RedisValue[]
                    {
                        absoluteExpiration?.Ticks ?? NotPresent,                            //绝对过期时间
                        cacheEntryOptions!.SlidingExpiration?.Ticks ?? NotPresent,          //滑动过期时间
                        getExpirationInSeconds(creationTime, absoluteExpiration, cacheEntryOptions) ?? NotPresent,
                        objectRerialize(obj)
                    }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }
        }

        public async Task<TData> GetAsync<TData>(string key, Func<Task<TData>> acquire, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            RedisValue[] results;
            try
            {
                results = await redisDatabase.HashGetAsync(_redisDatabaseContext.InstancePrefix.Append(key), GetHashFields(true)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }

            //刷新过期时间
            if (results.Length >= 2)
            {
                mapMetadata(results, out DateTimeOffset? absExpr, out TimeSpan? sldExpr);
                await refreshAsync(redisDatabase, key, absExpr, sldExpr);
            }

            //获取缓存的数据
            if (results.Length >= 3 && !results[2].IsNull)
            {
                TData? data = objectDeserialize<TData>(results[2]);
                if (data != null)
                    return data;
            }

            //如果缓存的数据获取失败，那么则创建
            TData newData = await acquire();
            if (newData != null)
              await SetAsync(key, newData, cacheEntryOptions);
            return newData;
        }

        public async Task<TData> GetAsync<TData>(string key, Func<TData> acquire, CacheEntryOptions? cacheEntryOptions = null, CancellationToken token = default)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            RedisValue[] results;
            try
            {
                results = await redisDatabase.HashGetAsync(_redisDatabaseContext.InstancePrefix.Append(key), GetHashFields(true)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }

            //刷新过期时间
            if (results.Length >= 2)
            {
                mapMetadata(results, out DateTimeOffset? absExpr, out TimeSpan? sldExpr);
                await refreshAsync(redisDatabase, key, absExpr, sldExpr);
            }

            //获取缓存的数据
            if (results.Length >= 3 && !results[2].IsNull)
            {
                TData? data = objectDeserialize<TData>(results[2]);
                if (data != null)
                    return data;
            }

            //如果缓存的数据获取失败，那么则创建
            TData newData = acquire();
            if (newData != null)
                await SetAsync(key, newData, cacheEntryOptions);
            return newData;
        }

        public TData Get<TData>(string key, Func<TData> acquire, CacheEntryOptions? cacheEntryOptions = null)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            var redisDatabase = _redisDatabaseContext.Connect();

            //获取数据
            RedisValue[] results;
            try
            {
                results = redisDatabase.HashGet(_redisDatabaseContext.InstancePrefix.Append(key), GetHashFields(true));
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }

            //刷新过期时间
            if (results.Length >= 2)
            {
                mapMetadata(results, out DateTimeOffset? absExpr, out TimeSpan? sldExpr);
                refresh(redisDatabase, key, absExpr, sldExpr);
            }

           //获取缓存的数据
            if (results.Length >= 3 && !results[2].IsNull)
            {
                TData? data = objectDeserialize<TData>(results[2]);
                if(data != null)
                    return data;
            }

            //如果缓存的数据获取失败，那么则创建
            TData newData = acquire();
            if (newData != null)
                Set(key, newData, cacheEntryOptions);
            return newData;
        }

        public async Task RemoveAsync(string key, CancellationToken token = default)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            try
            {
                await redisDatabase.KeyDeleteAsync(_redisDatabaseContext.InstancePrefix.Append(key)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }
        }

        public void Remove(string key)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            var redisDatabase = _redisDatabaseContext.Connect();

            try
            {
                redisDatabase.KeyDelete(_redisDatabaseContext.InstancePrefix.Append(key));
            }
            catch (Exception ex)
            {
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                throw;
            }
        }

        public async Task RemoveByPrefixAsync(string prefix, CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            string key = $"{_redisDatabaseContext.InstancePrefix}{prefix}*";

            foreach (var server in redisDatabase.Multiplexer.GetServers())
            {
                try
                {
                    IEnumerable<RedisKey> keys = server.Keys(redisDatabase.Database, key);
                    await redisDatabase.KeyDeleteAsync(keys.ToArray());
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                    throw;
                }
            }
        }

        public void RemoveByPrefix(string prefix)
        {
            var redisDatabase = _redisDatabaseContext.Connect();

            string key = $"{_redisDatabaseContext.InstancePrefix}{prefix}*";

            foreach (var server in redisDatabase.Multiplexer.GetServers())
            {
                try
                {
                    IEnumerable<RedisKey> keys = server.Keys(redisDatabase.Database, key);
                    redisDatabase.KeyDelete(keys.ToArray());
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                    throw;
                }
            }
        }

        public async Task ClearAsync(CancellationToken token = default)
        {
            token.ThrowIfCancellationRequested();

            var redisDatabase = await _redisDatabaseContext.ConnectAsync(token).ConfigureAwait(false);

            foreach (var server in redisDatabase.Multiplexer.GetServers())
            {
                try
                {
                    await server.FlushDatabaseAsync();
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                    throw;
                }
            }
        }

        public void Clear()
        {
            var redisDatabase = _redisDatabaseContext.Connect();

            foreach (var server in redisDatabase.Multiplexer.GetServers())
            {
                try
                {
                    server.FlushDatabase();
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, redisDatabase);
                    throw;
                }
            }
        }

        public CacheEntryOptions CreateCacheEntryOptions()
        {
            return _cacheOptions.CreateCacheEntryOptions();
        }


        public void Dispose()
        {
        }

        #endregion

        //private IEnumerable<RedisKey> getKeys(IServer cache, EndPoint endPoint, string key)
        //{

        //}

        private void refresh(IDatabase cache, string key, DateTimeOffset? absExpr, TimeSpan? sldExpr)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            if (sldExpr.HasValue)
            {
                TimeSpan? expr;
                if (absExpr.HasValue)
                {
                    var relExpr = absExpr.Value - DateTimeOffset.Now;
                    expr = relExpr <= sldExpr.Value ? relExpr : sldExpr;
                }
                else
                {
                    expr = sldExpr;
                }

                try
                {
                    cache.KeyExpire(_redisDatabaseContext.InstancePrefix.Append(key), expr);
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, cache);
                    throw;
                }
            }
        }

        private async Task refreshAsync(IDatabase cache, string key, DateTimeOffset? absExpr, TimeSpan? sldExpr, CancellationToken token = default)
        {
            Check.StringNotNullOrEmpty(key, nameof(key));

            token.ThrowIfCancellationRequested();

            if (sldExpr.HasValue)
            {
                TimeSpan? expr;
                if (absExpr.HasValue)
                {
                    var relExpr = absExpr.Value - DateTimeOffset.Now;
                    expr = relExpr <= sldExpr.Value ? relExpr : sldExpr;
                }
                else
                {
                    expr = sldExpr;
                }
                try
                {
                    await cache.KeyExpireAsync(_redisDatabaseContext.InstancePrefix.Append(key), expr).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    _redisDatabaseContext.OnRedisError(ex, cache);
                    throw;
                }
            }
        }


        private void mapMetadata(RedisValue[] results, out DateTimeOffset? absoluteExpiration, out TimeSpan? slidingExpiration)
        {
            absoluteExpiration = null;
            slidingExpiration = null;
            var absoluteExpirationTicks = (long?)results[0];
            if (absoluteExpirationTicks.HasValue && absoluteExpirationTicks.Value != NotPresent)
            {
                absoluteExpiration = new DateTimeOffset(absoluteExpirationTicks.Value, TimeSpan.Zero);
            }
            var slidingExpirationTicks = (long?)results[1];
            if (slidingExpirationTicks.HasValue && slidingExpirationTicks.Value != NotPresent)
            {
                slidingExpiration = new TimeSpan(slidingExpirationTicks.Value);
            }
        }

        /// <summary>
        /// 获取绝对过期的时间
        /// </summary>
        /// <param name="creationTime"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private DateTimeOffset? getAbsoluteExpiration(DateTimeOffset creationTime, CacheEntryOptions options)
        {

            //优先根据绝对过期时间与当前时间的间隔来确定缓存的过期时间
            if (options.AbsoluteExpirationRelativeToNow.HasValue)
            {
                return creationTime + options.AbsoluteExpirationRelativeToNow;
            }

            //如果绝对过期时间早于当前时间则抛出异常，否则返回绝对过期时间
            if (options.AbsoluteExpiration.HasValue && options.AbsoluteExpiration <= creationTime)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(options.AbsoluteExpiration),
                    options.AbsoluteExpiration.Value,
                    "The absolute expiration value must be in the future.");
            }

            return options.AbsoluteExpiration;
        }

        /// <summary>
        /// 获取滑动过期的时间间隔（秒）
        /// </summary>
        /// <param name="creationTime"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static long? getExpirationInSeconds(DateTimeOffset creationTime, DateTimeOffset? absoluteExpiration, CacheEntryOptions options)
        {
            if (absoluteExpiration.HasValue)
            {
                if (options.SlidingExpiration.HasValue)
                    return (long)Math.Min(
                        (absoluteExpiration.Value - creationTime).TotalSeconds,
                        options.SlidingExpiration.Value.TotalSeconds);
                else
                    return (long)(absoluteExpiration.Value - creationTime).TotalSeconds;
            }
            else if (options.SlidingExpiration.HasValue)
            {
                return (long)options.SlidingExpiration.Value.TotalSeconds;
            }
            return null;
        }

    }
}
