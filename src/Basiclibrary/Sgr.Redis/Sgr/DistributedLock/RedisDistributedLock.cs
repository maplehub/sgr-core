﻿using Microsoft.Extensions.Logging;
using Sgr.Exceptions;
using Sgr.Redis;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.DistributedLock
{
    public class RedisDistributedLock : IDistributedLock
    {
        private readonly string _hostName;
        private readonly IRedisDatabaseContext _redisDatabaseContext;
        private readonly ILogger _logger;

        public RedisDistributedLock(IRedisDatabaseContext redisDatabaseContext, ILogger<RedisDistributedLock> logger)
        {
            _hostName = Dns.GetHostName() + ':' + Process.GetCurrentProcess().Id;
            _redisDatabaseContext = redisDatabaseContext;
            _logger = logger;
        }

        #region IDistributedLock

        public ILocker AcquireLock(string key, TimeSpan? expiration = null)
        {
            var locker = TryAcquireLock(key, TimeSpan.MaxValue, expiration);
            if (locker == null)
                throw new BusinessException("Acquire Lock Fail!");
            return locker;
        }

        public async Task<ILocker> AcquireLockAsync(string key, TimeSpan? expiration = null)
        {
            var locker = await TryAcquireLockAsync(key, TimeSpan.MaxValue, expiration);
            if (locker == null)
                throw new BusinessException("Acquire Lock Fail!");
            return locker;
        }

        public bool IsLockAcquired(string key)
        {
            var redisDatabase = _redisDatabaseContext.Connect();
            if (redisDatabase == null)
            {
                _logger.LogError("Fails to check whether the named lock '{LockName}' is already acquired.", getLockKey(key));
                return false;
            }

            try
            {
                return (redisDatabase.LockQuery(getLockKey(key))).HasValue;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Fails to check whether the named lock '{LockName}' is already acquired.", getLockKey(key));
            }

            return false;
        }

        private RedisKey getLockKey(string key)
        {
            return _redisDatabaseContext.InstancePrefix.Append(".lock.").Append(key);
        }

        public async Task<bool> IsLockAcquiredAsync(string key)
        {
            var redisDatabase = await _redisDatabaseContext.ConnectAsync();
            if (redisDatabase == null)
            {
                _logger.LogError("Fails to check whether the named lock '{LockName}' is already acquired.", getLockKey(key));
                return false;
            }

            try
            {
                return (await redisDatabase.LockQueryAsync(getLockKey(key))).HasValue;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Fails to check whether the named lock '{LockName}' is already acquired.", getLockKey(key));
            }

            return false;
        }

        public ILocker? TryAcquireLock(string key, TimeSpan timeout, TimeSpan? expiration = null)
        {
            using (var cts = new CancellationTokenSource(timeout != TimeSpan.MaxValue ? timeout : Timeout.InfiniteTimeSpan))
            {
                var retries = 0.0;

                while (!cts.IsCancellationRequested)
                {
                    var locked = Lock(key, expiration ?? TimeSpan.MaxValue);

                    if (locked)
                    {
                        return new Locker(this, key, expiration);
                    }

                    try
                    {
                        Task.Delay(GetDelay(++retries), cts.Token);
                    }
                    catch (TaskCanceledException)
                    {
                        if (_logger.IsEnabled(LogLevel.Debug))
                        {
                            _logger.LogDebug("Timeout elapsed before acquiring the named lock '{LockName}' after the given timeout of '{Timeout}'.",
                               getLockKey(key), timeout.ToString());
                        }
                    }
                }
            }

            return null;
        }

        public async Task<ILocker?> TryAcquireLockAsync(string key, TimeSpan timeout, TimeSpan? expiration = null)
        {
            using (var cts = new CancellationTokenSource(timeout != TimeSpan.MaxValue ? timeout : Timeout.InfiniteTimeSpan))
            {
                var retries = 0.0;

                while (!cts.IsCancellationRequested)
                {
                    var locked = await LockAsync(key, expiration ?? TimeSpan.MaxValue);

                    if (locked)
                        return new Locker(this, key, expiration);

                    try
                    {
                        await Task.Delay(GetDelay(++retries), cts.Token);
                    }
                    catch (TaskCanceledException)
                    {
                        if (_logger.IsEnabled(LogLevel.Debug))
                        {
                            _logger.LogDebug("Timeout elapsed before acquiring the named lock '{LockName}' after the given timeout of '{Timeout}'.",
                               getLockKey(key), timeout.ToString());
                        }
                    }
                }
            }

            return null;
        }

        #endregion IDistributedLock

        #region Locker

        //锁续期
        //https://www.5axxw.com/questions/simple/s961px
        //https://zhuanlan.zhihu.com/p/635007911?utm_id=0

        private class Locker : ILocker
        {
            private readonly RedisDistributedLock _lock;
            private readonly string _key;
            private bool _disposed;
            private readonly CancellationTokenSource _cts;

            private readonly TimeSpan _expiration;

            public Locker(RedisDistributedLock redislock, string key, TimeSpan? expiration = null)
            {
                _lock = redislock;
                _key = key;

                //用于锁续期
                _expiration = expiration ?? TimeSpan.MaxValue;
                _cts = new CancellationTokenSource(TimeSpan.FromDays(1));

                if (_expiration != TimeSpan.MaxValue && _expiration > TimeSpan.MinValue)
                    startRenewTask(TimeSpan.FromMilliseconds(_expiration.TotalMilliseconds / 2));
            }

            private void startRenewTask(TimeSpan renewInterval)
            {
                Task.Run(async () =>
                {
                    while (!_cts.IsCancellationRequested)
                    {
                        await Task.Delay(renewInterval, _cts.Token);

                        if (_disposed)
                            break;

                        await _lock.LockExtendAsync(_key, _expiration);
                    }
                }, _cts.Token);
                //.ContinueWith((task) =>
                //{
                //});
            }

            public ValueTask DisposeAsync()
            {
                if (_disposed)
                {
                    return default;
                }

                _disposed = true;

                _cts?.Dispose();

                return _lock.ReleaseAsync(_key);
            }

            public void Dispose()
            {
                if (_disposed)
                {
                    return;
                }

                _disposed = true;

                _cts?.Dispose();

                _lock.Release(_key);
            }
        }

        #endregion Locker

        private bool Lock(string key, TimeSpan expiry)
        {
            var redisDatabase = _redisDatabaseContext.Connect();
            if (redisDatabase == null)
            {
                _logger.LogError("Fails to acquire the named lock '{LockName}'.", getLockKey(key));
                return false;
            }

            try
            {
                return redisDatabase.LockTake(getLockKey(key), _hostName, expiry);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fails to acquire the named lock '{LockName}'.", getLockKey(key));
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
            }

            return false;
        }

        private async Task<bool> LockAsync(string key, TimeSpan expiry)
        {
            var redisDatabase = await _redisDatabaseContext.ConnectAsync();
            if (redisDatabase == null)
            {
                _logger.LogError("Fails to acquire the named lock '{LockName}'.", getLockKey(key));
                return false;
            }

            try
            {
                return await redisDatabase.LockTakeAsync(getLockKey(key), _hostName, expiry);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fails to acquire the named lock '{LockName}'.", getLockKey(key));
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
            }

            return false;
        }

        private async ValueTask ReleaseAsync(string key)
        {
            var redisDatabase = await _redisDatabaseContext.ConnectAsync();
            try
            {
                await redisDatabase.LockReleaseAsync(getLockKey(key), _hostName);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fails to release the named lock '{LockName}'.", getLockKey(key));
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
            }
        }

        private void Release(string key)
        {
            var redisDatabase = _redisDatabaseContext.Connect();
            try
            {
                redisDatabase.LockRelease(getLockKey(key), _hostName, CommandFlags.FireAndForget);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fails to release the named lock '{LockName}'.", getLockKey(key));
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
            }
        }

        private async Task LockExtendAsync(string key, TimeSpan expiry)
        {
            var redisDatabase = await _redisDatabaseContext.ConnectAsync();
            try
            {
                await redisDatabase.LockExtendAsync(getLockKey(key), _hostName, expiry);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fails to extend the named lock '{LockName}'.", getLockKey(key));
                _redisDatabaseContext.OnRedisError(ex, redisDatabase);
            }
        }

        private static readonly double _baseDelay = 100;
        private static readonly double _maxDelay = 10000;

        private static TimeSpan GetDelay(double retries)
        {
            var delay = _baseDelay
                * (1.0 + ((Math.Pow(1.8, retries - 1.0) - 1.0)
                    * (0.6 + new Random().NextDouble() * 0.4)));

            return TimeSpan.FromMilliseconds(Math.Min(delay, _maxDelay));

            // 2 examples with 10 retries
            // --------------------------
            // 100     100 (start from base)
            // 164     171
            // 256     312
            // 401     519
            // 754     766
            // 1327    1562
            // 2950    3257
            // 4596    4966
            // 7215    8667
            // 10000   10000 (max reached)
        }
    }
}