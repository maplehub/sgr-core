﻿/**************************************************************
 *
 * 唯一标识：869751a3-2108-4423-80df-eaa2e6d3be7c
 * 命名空间：Sgr.Trackers
 * 创建时间：2024/7/20 15:54:15
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers
{
    /// <summary>
    /// 基于缓存实现消息标记，解决消息幂等性问题
    /// </summary>
    public interface IMessageMark
    {
        /// <summary>
        /// 将消息其标记为已执行（注意此处应该是原子操作）
        /// </summary>
        /// <param name="msgId">消息标识</param>
        /// <param name="effectiveSecond">有效期，单位秒（默认7天）</param>
        /// <param name="token"></param>
        /// <returns>消息在此之前已被标记 或者 标记失败 返回False</returns>
        Task<bool> MarkAsProcessedAsync(string msgId, int effectiveSecond = 604800, CancellationToken token = default);

        /// <summary>
        /// 删除消息已执行的标记
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task RemoveAsync(string msgId, CancellationToken token = default);
    }
}