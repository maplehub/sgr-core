﻿/**************************************************************
 *
 * 唯一标识：d197341e-8352-4b0a-b19a-0fae5d31c84c
 * 命名空间：Sgr.Trackers
 * 创建时间：2024/7/20 15:47:57
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Trackers
{
    /// <summary>
    /// 基于令牌实现消息幂等处理
    /// </summary>
    public interface IMessageToken
    {
        /// <summary>
        /// 颁发令牌
        /// </summary>
        /// <param name="effectiveSecond">有效期，单位秒（默认1天）</param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<string> IssuingTokenAsync(int effectiveSecond = 86400, CancellationToken token = default);

        /// <summary>
        /// 验证令牌(原子操作)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> VerifyTokenAsync(string key, CancellationToken token = default);
    }
}