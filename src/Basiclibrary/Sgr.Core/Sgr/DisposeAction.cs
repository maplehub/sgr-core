﻿/**************************************************************
 *
 * 唯一标识：e66aaa01-de42-45ae-b6d1-3368a7653bf8
 * 命名空间：Sgr
 * 创建时间：2024/7/14 21:26:41
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 * COPY FROM ABP
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr
{
    public class DisposeAction : IDisposable
    {
        private readonly Action _action;

        public DisposeAction(Action action)
        {
            Check.NotNull(action, nameof(action));

            _action = action;
        }

        public void Dispose()
        {
            _action();
        }
    }

    public class DisposeAction<T> : IDisposable
    {
        private readonly Action<T> _action;
        private readonly T? _parameter;

        public DisposeAction(Action<T> action, T parameter)
        {
            Check.NotNull(action, nameof(action));

            _action = action;
            _parameter = parameter;
        }

        public void Dispose()
        {
            if (_parameter != null)
            {
                _action(_parameter);
            }
        }
    }
}