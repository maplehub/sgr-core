﻿/**************************************************************
 * 
 * 唯一标识：8392fe20-1174-491c-9d6f-1d2821952dfb
 * 命名空间：Sgr.Caching.Services
 * 创建时间：2023/9/6 11:19:13
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Sgr.DistributedLock
{
    public interface IDistributedLock
    {
        /// <summary>
        /// 获取锁。一直等待，直到获取成功
        /// </summary>
        /// <param name="key">锁对应的Key</param>
        /// <param name="expiration">锁的过期时间</param>
        /// <returns></returns>
        ILocker AcquireLock(string key, TimeSpan? expiration = null);

        /// <summary>
        /// 获取锁。一直等待，直到获取成功
        /// </summary>
        /// <param name="key">锁对应的Key</param>
        /// <param name="expiration">锁的过期时间</param>
        /// <returns></returns>
        Task<ILocker> AcquireLockAsync(string key, TimeSpan? expiration = null);

        /// <summary>
        /// 获取锁。如果指定指定时间 timeout 内未获取锁，则返回NULL
        /// </summary>
        /// <param name="key">锁对应的Key</param>
        /// <param name="timeout">获取锁的等待时间</param>
        /// <param name="expiration">锁的过期时间</param>
        /// <returns></returns>
        ILocker? TryAcquireLock(string key,
            TimeSpan timeout,
            TimeSpan? expiration = null);

        /// <summary>
        /// 获取锁。如果指定指定时间 timeout 内未获取锁，则返回NULL
        /// </summary>
        /// <param name="key">锁对应的Key</param>
        /// <param name="timeout">获取锁的等待时间</param>
        /// <param name="expiration">锁的过期时间</param>
        /// <returns></returns>
        Task<ILocker?> TryAcquireLockAsync(string key, 
            TimeSpan timeout, 
            TimeSpan? expiration = null);

        /// <summary>
        /// 检查锁是否已经被占用
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IsLockAcquired(string key);

        /// <summary>
        /// 检查锁是否已经被占用
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<bool> IsLockAcquiredAsync(string key);
    }
}
