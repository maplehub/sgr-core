﻿/**************************************************************
 * 
 * 唯一标识：416f78bd-1500-4562-bffd-71ebb46d2f2b
 * 命名空间：Sgr.DistributedLock
 * 创建时间：2024/4/5 21:04:38
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.DistributedLock
{
    public interface ILocker : IDisposable, IAsyncDisposable
    {
    }
}
