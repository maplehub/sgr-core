﻿/**************************************************************
 * 
 * 唯一标识：163372cd-28a9-4221-80c1-8892d4f810c9
 * 命名空间：Sgr.DistributedLock
 * 创建时间：2024/4/5 21:06:58
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.DistributedLock
{
    public class LocalDistributedLock : IDistributedLock, IDisposable
    {
        private readonly ILogger _logger;
        private readonly Dictionary<string, Semaphore> _semaphores = new();

        public LocalDistributedLock(ILogger<LocalDistributedLock> logger)
        {
            _logger = logger;
        }


        #region IDistributedLock

        public ILocker AcquireLock(string key, TimeSpan? expiration = null)
        {
            var semaphore = GetOrCreateSemaphore(key);
            semaphore.Value.Wait();

            return new Locker(this, semaphore, expiration);
        }


        public async Task<ILocker> AcquireLockAsync(string key, TimeSpan? expiration = null)
        {
            var semaphore = GetOrCreateSemaphore(key);
            await semaphore.Value.WaitAsync();

            return new Locker(this, semaphore, expiration);
        }

        public ILocker? TryAcquireLock(string key,
            TimeSpan timeout,
            TimeSpan? expiration = null)
        {
            var semaphore = GetOrCreateSemaphore(key);

            if (semaphore.Value.Wait(timeout != TimeSpan.MaxValue ? timeout : Timeout.InfiniteTimeSpan))
            {
                return new Locker(this, semaphore, expiration);
            }

            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.LogDebug("Timeout elapsed before acquiring the named lock '{LockName}' after the given timeout of '{Timeout}'.",
                    key, timeout.ToString());
            }

            return null;
        }

        public async Task<ILocker?> TryAcquireLockAsync(string key,
            TimeSpan timeout,
            TimeSpan? expiration = null)
        {
            var semaphore = GetOrCreateSemaphore(key);

            if (await semaphore.Value.WaitAsync(timeout != TimeSpan.MaxValue ? timeout : Timeout.InfiniteTimeSpan))
            {
                return new Locker(this, semaphore, expiration);
            }

            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.LogDebug("Timeout elapsed before acquiring the named lock '{LockName}' after the given timeout of '{Timeout}'.",
                    key, timeout.ToString());
            }

            return null;
        }

        public bool IsLockAcquired(string key)
        {
            lock (_semaphores)
            {
                if (_semaphores.TryGetValue(key, out var semaphore))
                {
                    return semaphore.Value.CurrentCount == 0;
                }
                return false;
            }
        }

        public Task<bool> IsLockAcquiredAsync(string key)
        {
            lock (_semaphores)
            {
                if (_semaphores.TryGetValue(key, out var semaphore))
                {
                    return Task.FromResult(semaphore.Value.CurrentCount == 0);
                }

                return Task.FromResult(false);
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            var semaphores = _semaphores.Values.ToArray();

            foreach (var semaphore in semaphores)
            {
                semaphore.Value.Dispose();
            }
        }

        #endregion


        #region IDistributedLock

        private class Semaphore
        {
            public Semaphore(string key, SemaphoreSlim value)
            {
                Key = key;
                Value = value;
                RefCount = 1;
            }

            internal string Key { get; }
            internal SemaphoreSlim Value { get; }
            internal int RefCount { get; set; }
        }

        #endregion

        #region Locker

        private class Locker : ILocker
        {

            private readonly LocalDistributedLock _localLock;
            private readonly Semaphore _semaphore;
            private readonly CancellationTokenSource? _cts;
            private volatile int _released;
            private bool _disposed;

            public Locker(LocalDistributedLock localLock, Semaphore semaphore, TimeSpan? expiration)
            {
                _localLock = localLock;
                _semaphore = semaphore;

                if (expiration.HasValue && expiration.Value != TimeSpan.MaxValue)
                {
                    _cts = new CancellationTokenSource(expiration.Value);
                    _cts.Token.Register(Release);
                }
            }

            private void Release()
            {
                if (Interlocked.Exchange(ref _released, 1) == 0)
                {
                    lock (_localLock._semaphores)
                    {
                        if (_localLock._semaphores.TryGetValue(_semaphore.Key, out var semaphore))
                        {
                            semaphore.RefCount--;

                            if (semaphore.RefCount == 0)
                            {
                                _localLock._semaphores.Remove(_semaphore.Key);
                            }
                        }
                    }

                    _semaphore.Value.Release();
                }
            }

            public ValueTask DisposeAsync()
            {
                Dispose();
                return default;
            }

            public void Dispose()
            {
                if (_disposed)
                {
                    return;
                }

                _disposed = true;

                _cts?.Dispose();

                Release();
            }
        }


        #endregion


        private Semaphore GetOrCreateSemaphore(string key)
        {
            lock (_semaphores)
            {
                if (_semaphores.TryGetValue(key, out var semaphore))
                {
                    semaphore.RefCount++;
                }
                else
                {
                    semaphore = new Semaphore(key, new SemaphoreSlim(1));
                    _semaphores[key] = semaphore;
                }

                return semaphore;
            }
        }
    }
}
