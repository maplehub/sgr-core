﻿/**************************************************************
 * 
 * 唯一标识：568d73d9-6e29-464e-bbf1-28e690ca31d6
 * 命名空间：Sgr.Algorithm
 * 创建时间：2024/7/12 20:45:11
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Algorithm
{
    public static class ConsistentHashExtensions
    {
        public static string SearchNodeName(this IConsistentHash ring,string key)
        {
            return ring.SearchNodeName(Encoding.UTF8.GetBytes(key));
        }
    }
}
