﻿/**************************************************************
 * 
 * 唯一标识：40ab504a-a561-4e89-b04c-40106c77d701
 * 命名空间：Sgr.Algorithm
 * 创建时间：2024/7/12 14:41:09
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Algorithm
{
    /// <summary>
    /// 一致性Hash算法
    /// </summary>
    public interface IConsistentHash : IDisposable
    {
        /// <summary>
        /// 获取所有节点名称
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetAllNodeNames();
        /// <summary>
        /// 获取所有Hash Key
        /// </summary>
        /// <returns></returns>
        IEnumerable<uint> GetAllHashKeys();

        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="virtualNodeCount"></param>
        void AddNode(string nodeName,uint virtualNodeCount = 200);

        /// <summary>
        /// 移除节点
        /// </summary>
        /// <param name="nodeName"></param>
        void RemoveNode(string nodeName);

        /// <summary>
        /// 查找节点
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        string SearchNodeName(ReadOnlySpan<byte> source);
    }
}
