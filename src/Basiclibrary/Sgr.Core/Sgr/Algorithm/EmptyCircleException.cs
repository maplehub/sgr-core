﻿/**************************************************************
 * 
 * 唯一标识：b7c18550-7d40-4690-b2b1-17b37aadacdb
 * 命名空间：Sgr.Algorithm
 * 创建时间：2024/7/12 16:23:47
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Sgr.Exceptions;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Sgr.Algorithm
{
    public class EmptyCircleException : BusinessException
    {
        public EmptyCircleException(string message) : base(message)
        {
        }

        public EmptyCircleException(string messageFormat, params object[] args) : base(messageFormat, args)
        {
        }

        public EmptyCircleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmptyCircleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
