﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Identity.Services
{
    /// <summary>
    /// 令牌黑名单管理
    /// </summary>
    public interface ITokenBlacklistService
    {
        /// <summary>
        /// 将令牌加入黑名单
        /// </summary>
        /// <param name="token"></param>
        /// <param name="expiration"></param>
        /// <returns></returns>
        Task AddToBlacklistAsync(string token, TimeSpan? expiration = null);

        /// <summary>
        /// 检查令牌是否在黑名单中
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> IsBlacklistedAsync(string token);

        /// <summary>
        /// 将令牌从黑名单中移除
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> RemoveFromBlacklistAsync(string token);

        /// <summary>
        /// 清空黑名单
        /// </summary>
        /// <returns></returns>
        Task ClearBlacklistAsync();
    }
}