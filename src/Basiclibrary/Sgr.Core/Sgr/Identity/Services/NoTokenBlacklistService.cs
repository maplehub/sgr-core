﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Identity.Services
{
    public class NoTokenBlacklistService : ITokenBlacklistService
    {
        public Task AddToBlacklistAsync(string token, TimeSpan? expiration = null)
        {
            return Task.CompletedTask;
        }

        public Task ClearBlacklistAsync()
        {
            return Task.CompletedTask;
        }

        public Task<bool> IsBlacklistedAsync(string token)
        {
            return Task.FromResult(false);
        }

        public Task<bool> RemoveFromBlacklistAsync(string token)
        {
            return Task.FromResult(true);
        }
    }
}