﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Identity.Services
{
    public interface IPasswordHashService
    {
        /// <summary>
        /// 密码Hash处理
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        string HashPassword(string password);

        /// <summary>
        /// 验证密码
        /// </summary>
        /// <param name="password"></param>
        /// <param name="hashedPassword"></param>
        /// <returns></returns>
        bool VerifyPassword(string password, string hashedPassword);
    }
}