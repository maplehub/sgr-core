﻿using Sgr.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Identity.Services
{
    public class Md5PasswordHashService : IPasswordHashService
    {
        public string HashPassword(string password)
        {
            return HashHelper.CreateMd5(password);
        }

        public bool VerifyPassword(string password, string hashedPassword)
        {
            return hashedPassword == HashHelper.CreateMd5(password);
        }
    }
}