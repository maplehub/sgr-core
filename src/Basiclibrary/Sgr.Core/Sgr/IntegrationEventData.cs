﻿/**************************************************************
 *
 * 唯一标识：13aad1b6-857d-495d-88b3-c774878ff863
 * 命名空间：Sgr.Cap
 * 创建时间：2024/7/22 19:46:13
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Generator;
using System;

namespace Sgr
{
    public class IntegrationEventData<TData> where TData : class
    {
        public string Id { get; set; }
        public TData MessageBody { get; set; }
        public DateTimeOffset CreatedDate { get; set; }

        private IntegrationEventData(string id, TData messageBody)
            : this(id, messageBody, DateTimeOffset.Now)
        {
        }

        public IntegrationEventData(string id, TData messageBody, DateTimeOffset createdDate)
        {
            Id = id;
            MessageBody = messageBody;
            CreatedDate = createdDate;
        }

        public static IntegrationEventData<TData> CreateNew(INumberIdGenerator numberIdGenerator, TData messageBody)
        {
            string id = numberIdGenerator.GenerateUniqueId().ToString();

            return new IntegrationEventData<TData>(id, messageBody);
        }

        public static IntegrationEventData<TData> CreateNew(IStringIdGenerator stringIdGenerator, TData messageBody)
        {
            string id = stringIdGenerator.GenerateUniqueId();

            return new IntegrationEventData<TData>(id, messageBody);
        }
    }
}