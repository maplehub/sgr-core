﻿using Sgr.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Application.Queries
{
    public class NoReadDbContext : IReadDbContext
    {
        public Task<bool> AnyAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<decimal?> AverageAsync<T>(IQueryable<T> query, Expression<Func<T, decimal?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<int> CountAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public ValueTask DisposeAsync()
        {
            throw new NotImplementedException();
        }

        public Task<T?> FirstOrDefaultAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<long> LongCountAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<TResult?> MaxAsync<T, TResult>(IQueryable<T> query, Expression<Func<T, TResult?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<TResult?> MinAsync<T, TResult>(IQueryable<T> query, Expression<Func<T, TResult?>> selector, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<PagedResponse<T>> PagedListByPageSizeAsync<T>(IQueryable<T> query, int pageIndex, int pageSize, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<PagedResponse<T>> PaginateAsync<T>(IQueryable<T> query, int skip, int take, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> QueryWithNavigation<T>(params string[] navigationPropertyPath) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<T?> SingleOrDefaultAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> ToListAsync<T>(IQueryable<T> query, CancellationToken cancellationToken = default) where T : class
        {
            throw new NotImplementedException();
        }
    }
}