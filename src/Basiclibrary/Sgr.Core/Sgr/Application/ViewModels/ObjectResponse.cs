﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.Application.ViewModels
{
    [Serializable]
    public class ObjectResponse<TViewModel>
    {
        public ObjectResponse()
        {
        }

        public ObjectResponse(bool success, string message, TViewModel data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 执行消息
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// 具体数据
        /// </summary>
        public TViewModel? Data { get; set; }

        /// <summary>
        /// 执行成功
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ObjectResponse<TViewModel> SuccessResponse(TViewModel data)
        {
            return new ObjectResponse<TViewModel>(true, "", data);
        }

        /// <summary>
        /// 执行失败
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ObjectResponse<TViewModel> FailResponse(string message)
        {
            return new ObjectResponse<TViewModel>() { Success = false, Message = message };
        }
    }
}