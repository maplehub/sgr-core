﻿/**************************************************************
 * 
 * 唯一标识：1fe88032-faec-43be-a942-8181f8092fec
 * 命名空间：Microsoft.Extensions.Logging
 * 创建时间：2023/8/10 21:05:05
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.Logging
{
    /// <summary>
    ///  扩展
    /// </summary>
    public static class LoggerExtensions
    {
        /// <summary>
        /// 记录异常
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        /// <param name="level"></param>
        public static void LogException(this ILogger logger, Exception exception, LogLevel level = LogLevel.Error)
        {
            var strBuilder = new StringBuilder();
            if (exception != null)
            {
                strBuilder.AppendLine($"Exception Message : {exception.Message}");

                strBuilder.AppendLine("---------- Exception StackTrace ----------");
                strBuilder.AppendLine(exception.StackTrace);

                if (exception.Data != null && exception.Data.Count > 0)
                {
                    strBuilder.AppendLine("---------- Exception Data ----------");
                    foreach (var key in exception.Data.Keys)
                    {
                        strBuilder.AppendLine($"{key} = {exception.Data[key]}");
                    }
                }
            }

            switch (level)
            {
                case LogLevel.Error:
                    logger.LogError(strBuilder.ToString());
                    break;
                case LogLevel.Warning:
                    logger.LogWarning(strBuilder.ToString());
                    break;
                case LogLevel.Information:
                    logger.LogInformation(strBuilder.ToString());
                    break;
                case LogLevel.Trace:
                    logger.LogTrace(strBuilder.ToString());
                    break;
                case LogLevel.Critical:
                    logger.LogCritical(strBuilder.ToString());
                    break;
                default: // LogLevel.Debug || LogLevel.None
                    logger.LogDebug(strBuilder.ToString());
                    break;
            }
        }
    }
}
