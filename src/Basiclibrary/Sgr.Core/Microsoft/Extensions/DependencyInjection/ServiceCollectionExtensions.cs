﻿/**************************************************************
 *
 * 唯一标识：e30e4e87-77b7-46c3-8c3f-bd193c51df81
 * 命名空间：Microsoft.Extensions.DependencyInjection
 * 创建时间：2023/7/24 10:54:21
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Sgr.Algorithm;
using Sgr.Application;
using Sgr.Application.Queries;
using Sgr.AuditLogs.Services;
using Sgr.Caching;
using Sgr.Caching.Services;
using Sgr.Data;
using Sgr.DataCategories.Services;
using Sgr.DistributedLock;
using Sgr.Domain.Entities.Auditing;
using Sgr.Generator;
using Sgr.Identity.Services;
using Sgr.Trackers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 扩展
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddSgrCore(this IServiceCollection services)
        {
            //配置Id生成器
            //services.Configure<SnowflakeOption>(configuration.GetSection("Sgr:Snowflake"));
            services.AddOptions<SnowflakeOption>()
                .BindConfiguration("Sgr:Snowflake")
                .Configure(opt =>
                {
                    if (int.TryParse(Environment.GetEnvironmentVariable("SGR_SNOWFLAKE_DATACENTERID"), out int datacenterId))
                        opt.DatacenterId = datacenterId;

                    if (int.TryParse(Environment.GetEnvironmentVariable("SGR_SNOWFLAKE_MACHINEID"), out int machineid))
                        opt.MachineId = machineid;

                    if (opt.DatacenterId < 0)
                        opt.DatacenterId = 0;

                    if (opt.DatacenterId > 31)
                        opt.DatacenterId = 31;

                    if (opt.MachineId < 0)
                        opt.MachineId = 0;

                    if (opt.MachineId > 31)
                        opt.MachineId = 31;
                });

            services.TryAddSingleton<IStringIdGenerator, DefaultStringIdGenerator>();
            services.TryAddSingleton<INumberIdGenerator, DefaultNumberIdGenerator>();

            //数据字典
            services.TryAddSingleton<ICategoryTypeService, DefaultCategoryTypeService>();

            //签名验证工具
            services.TryAddSingleton<ISignatureChecker, DefaultSignatureChecker>();

            //配置审计接口
            services.TryAddSingleton<IAuditedOperator, DefaultAuditedOperator>();
            services.TryAddTransient<IAuditLogService, DefaultAuditLogService>();

            //认证相关
            services.TryAddTransient<IPasswordHashService, Md5PasswordHashService>();
            services.TryAddScoped<IAccountAuthService, NoAccountAuthService>();
            services.TryAddScoped<ITokenBlacklistService, NoTokenBlacklistService>();
            //services.AddTransient<IPermissionChecker, PermissionChecker>();

            //缓存
            services.TryAddSingleton<ICacheManager, NoCacheManager>();

            //Trackers
            services.TryAddTransient<IMessageMark, MessageMarkByMemory>();
            services.TryAddTransient<IMessageToken, MessageTokenByMemory>();

            //本地使用的锁(限单机使用)
            services.TryAddSingleton<IDistributedLock, LocalDistributedLock>();

            //算法
            services.AddTransient<IConsistentHash, DefaultConsistentHash>(); //一致性Hash算法的默认实现

            //查询服务
            services.TryAddScoped<IReadDbContext, NoReadDbContext>();

            return services;
        }
    }
}