﻿using Sgr.Security.Permissions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.AuditLogs
{
    public class Permissions : IPermissionProvider
    {
        public static readonly FunctionPermission QueryAuditLogPermission = new("Sgr.QueryAuditLog", "审计日志", "浏览审计日志");

        public Task<IEnumerable<FunctionPermission>> GetFunctionPermissionsAsync()
        {
            return Task.FromResult(new FunctionPermission[]
            {
                QueryAuditLogPermission
            }.AsEnumerable());
        }
    }
}