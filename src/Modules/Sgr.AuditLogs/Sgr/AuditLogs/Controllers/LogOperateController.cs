﻿/**************************************************************
 *
 * 唯一标识：ab46d708-7df5-4fbc-80c4-aaf86719f6d7
 * 命名空间：Sgr.AuditLogs.Controllers
 * 创建时间：2023/8/20 15:58:24
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Sgr.Application.ViewModels;
using Sgr.AspNetCore.ActionFilters;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.AuditLogs.Queries;
using Sgr.AuditLogs.ViewModels;
using Sgr.ExceptionHandling;
using System.Threading.Tasks;

namespace Sgr.AuditLogs.Controllers
{
    /// <summary>
    /// 审计日志
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [ApiController]
    public class LogOperateController : ControllerBase
    {
        private readonly ILogOperateQueries _logOperateQueries;
        private readonly IAuthorizationService _authorizationService;

        public LogOperateController(ILogOperateQueries logOperateQueries,
            IAuthorizationService authorizationService)
        {
            _logOperateQueries = logOperateQueries;
            _authorizationService = authorizationService;
        }

        #region Queries

        /// <summary>
        /// 查询审计日志列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("查询审计日志列表")]
        public async Task<ActionResult<PagedResponse<LogOperateListModel>>> GetListAsync(LogOperateSearchModel request)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.QueryAuditLogPermission))
                return this.Unauthorized();

            return await _logOperateQueries.GetListAsync(request);
        }

        /// <summary>
        /// 查询审计日志详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[AllowAnonymous]
        [HttpGet("details/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        //[ResponseCache(Duration = 300, VaryByQueryKeys = new[] { "*" })]
        [AuditLogActionFilter("查询审计日志详情")]
        public async Task<ActionResult<LogOperateModel?>> GetAsync(long id)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.QueryAuditLogPermission))
                return this.Unauthorized();

            var item = await _logOperateQueries.GetAsync(id);
            if (item == null)
                return NotFound();

            return item;
        }

        #endregion Queries
    }
}