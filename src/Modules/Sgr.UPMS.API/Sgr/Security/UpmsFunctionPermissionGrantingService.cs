﻿/**************************************************************
 *
 * 唯一标识：1ffcac2b-4d2f-41cc-8707-1c4baeb8653c
 * 命名空间：Sgr.Security
 * 创建时间：2023/8/28 11:46:24
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Caching;
using Sgr.Caching.Services;
using Sgr.UPMS;
using Sgr.UPMS.Domain.Organizations;
using Sgr.UPMS.Domain.Roles;
using Sgr.UPMS.Domain.Users;
using Sgr.UPMS.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Sgr.Security
{
    public class UpmsFunctionPermissionGrantingService : IFunctionPermissionGrantingService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ICacheManager _cacheManager;

        public UpmsFunctionPermissionGrantingService(
            IServiceScopeFactory serviceScopeFactory,
            ICacheManager cacheManager)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _cacheManager = cacheManager;
        }

        private async Task<UserPermissionCacheItem?> LoadUserPermissionsAsync(long userId)
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var provider = scope.ServiceProvider;

            //加载用户
            var userRepo = provider.GetRequiredService<IUserRepository>();
            var user = await userRepo.GetAsync(userId, new[] { "Roles" });
            if (user == null || user.IsDeleted || user.State == Domain.Entities.EntityStates.Deactivate)
                return null;

            var result = new UserPermissionCacheItem
            {
                IsSuperAdmin = user.IsSuperAdmin,
                OrgId = user.OrgId,
                Permissions = new HashSet<string>()
            };

            if (!result.IsSuperAdmin)
            {
                //加载角色
                if (user.Roles.Any())
                {
                    var roleRepo = provider.GetRequiredService<IRoleRepository>();
                    var resources = await roleRepo.GetResourcesByRoleIdsAsync(user.Roles.Select(s => s.RoleId).ToArray());
                    foreach (string resource in resources)
                    {
                        result.Permissions.Add(resource);
                    }
                }
            }

            return result;
        }

        private CacheEntryOptions CreateCacheOptions()
        {
            // 绝对过期时间与当前时间的间隔为60分钟
            // 滑动过期时间为5分钟
            return new CacheEntryOptions()
                .SetAbsoluteExpirationRelativeToNowSecond(3600)
                .SetSlidingExpirationSecond(300);
        }

        public virtual async Task<bool> IsGrantedAsync(string orgId, string userId, string functionPermissionName)
        {
            if (string.IsNullOrEmpty(functionPermissionName))
                return false;

            if (!long.TryParse(orgId, out long longOrgId))
                return false;

            if (!long.TryParse(userId, out long longUserId))
                return false;

            //从缓存中获取用户权限
            UserPermissionCacheItem? permissionCache = await _cacheManager.GetAsync(
                string.Format(CacheKeys.AUTH_ORG_USER_KEY, longOrgId, longUserId, functionPermissionName),
                async () =>
                {
                    return await LoadUserPermissionsAsync(longUserId);
                },
                CreateCacheOptions());

            // 无法查询到，则返回false
            if (permissionCache == null)
                return false;

            if (permissionCache.IsSuperAdmin)
                return true;

            return permissionCache.Permissions.Contains(functionPermissionName);
        }

        public Task<bool> IsGrantedAsync(string orgId, string userId, FunctionPermissionRequirement requirement)
        {
            return IsGrantedAsync(orgId, userId, requirement.Permission.Name);
        }

        public Task<bool> IsGrantedAsync(ICurrentUser currentUser, FunctionPermissionRequirement requirement)
        {
            return IsGrantedAsync(currentUser.OrgId, currentUser.Id, requirement);
        }

        public async Task<bool> IsGrantedAsync(ClaimsPrincipal claimsPrincipal, FunctionPermissionRequirement requirement)
        {
            var claimUserId = claimsPrincipal.Claims.FirstOrDefault(f => f.Type == Constant.CLAIM_USER_ID);
            if (claimUserId == null)
                return false;

            var claimOrgId = claimsPrincipal.Claims.FirstOrDefault(f => f.Type == Constant.CLAIM_USER_ORGID);
            if (claimOrgId == null)
                return false;

            return await IsGrantedAsync(claimOrgId.Value, claimUserId.Value, requirement);
        }
    }
}