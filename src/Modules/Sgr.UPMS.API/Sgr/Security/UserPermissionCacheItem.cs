﻿using System.Collections.Generic;

namespace Sgr.Security
{
    /// <summary>
    /// 用于在缓存系统中保存用户权限信息
    /// </summary>
    public class UserPermissionCacheItem
    {
        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public bool IsSuperAdmin { get; set; }

        /// <summary>
        /// 所属组织标识
        /// </summary>
        public long OrgId { get; set; }

        /// <summary>
        /// 拥有的资源集合
        /// </summary>
        public HashSet<string> Permissions { get; set; } = new();
    }
}