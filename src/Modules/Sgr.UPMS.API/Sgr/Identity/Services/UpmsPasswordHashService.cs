﻿namespace Sgr.Identity.Services
{
    public class UpmsPasswordHashService : IPasswordHashService
    {
        private const int WorkFactor = 12; // 可配置的工作因子

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, WorkFactor);
        }

        public bool VerifyPassword(string password, string hashedPassword)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hashedPassword))
                return false;
            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
        }
    }
}