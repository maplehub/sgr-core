﻿/**************************************************************
 *
 * 唯一标识：772e1481-accf-462e-abce-7dd64b1955c4
 * 命名空间：Sgr.Identity.Services
 * 创建时间：2023/8/28 9:17:42
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Options;
using Sgr.Caching;
using Sgr.Exceptions;
using Sgr.UPMS;
using Sgr.UPMS.Domain.LogLogins;
using Sgr.UPMS.Domain.Organizations;
using Sgr.UPMS.Domain.Users;
using Sgr.UPMS.Domain.UserTokens;
using Sgr.UPMS.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.Identity.Services
{
    public class UpmsAccountAuthService : IAccountAuthService
    {
        private readonly ILogLoginRepository _logLoginRepository;
        private readonly IUserRefreshTokenRepository _userRefreshTokenRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHashService _passwordHashService;
        private readonly UpmsOptions _upmsOptions;

        public UpmsAccountAuthService(IUserRepository userRepository,
            ILogLoginRepository logLoginRepository,
            IUserRefreshTokenRepository userRefreshTokenRepository,
            IPasswordHashService passwordHashService,
            IOptions<UpmsOptions> options)
        {
            _userRefreshTokenRepository = userRefreshTokenRepository;
            _userRepository = userRepository;
            _passwordHashService = passwordHashService;
            _upmsOptions = options.Value;
            _logLoginRepository = logLoginRepository;
        }

        /// <summary>
        /// 创建刷新Token
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newRefreshToken">新令牌值</param>
        /// <param name="oldRefreshToken">旧令牌值（如果不存在则为空字符串）</param>
        /// <param name="minutes">刷新令牌有效时长（分钟）,默认12小时</param>
        /// <param name="bindingMark">与令牌的绑定用户的设备信息或会话标识符</param>
        public async Task CreateRefreshTokenAsync(string userId, string newRefreshToken, string oldRefreshToken = "", int minutes = 720, string bindingMark = "")
        {
            if (!long.TryParse(userId, out long id))
                throw new BusinessException($"userId {userId} 格式错误");

            //创建新的刷新令牌
            UserRefreshToken userRefreshToken = new UserRefreshToken(newRefreshToken, DateTimeOffset.UtcNow.AddMinutes(minutes), id, bindingMark);
            await _userRefreshTokenRepository.InsertAsync(userRefreshToken);

            //删除之前旧的刷新令牌
            if (!string.IsNullOrEmpty(oldRefreshToken))
            {
                UserRefreshToken? oldUserRefreshToken = await _userRefreshTokenRepository.GetByRefreshTokenAsync(oldRefreshToken);
                if (oldUserRefreshToken != null)
                    await _userRefreshTokenRepository.DeleteAsync(oldUserRefreshToken);
            }

            await _userRefreshTokenRepository.UnitOfWork.SaveEntitiesAsync();
        }

        public async Task<Tuple<AccountLoginResults, Account?>> ValidateAccountAsync(string loginName, string password)
        {
            User? user = await _userRepository.GetByLoginNameAsync(loginName);

            if (user == null)
                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.NotExist, null);

            int statu = user.CheckUserState();

            if (statu == 1)
                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.IsDelete, null);

            if (statu == 2)
                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.IsDeactivate, null);

            if (statu == 3)
                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.IsLock, null);

            if (user.CheckPassWord(_passwordHashService, password))
            {
                //显式加载用户的权限列表
                await _userRepository.CollectionAsync(user, f => f.Roles);

                //登录成功
                user.LoginSuccess();

                await _userRepository.UpdateAsync(user);
                await _userRepository.UnitOfWork.SaveEntitiesAsync();

                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.Success,
                    new Account($"{user.Id}", $"{user.OrgId}", user.LoginName, user.Roles.Select(f => f.RoleId.ToString()).ToArray()));
            }
            else
            {
                //登录失败
                user.LoginFail();
                //连续登录失败次数超过上限则锁定账号
                if (_upmsOptions.FailedPasswordAllowedAttempts > 0
                    && user.FailedLoginAttempts > _upmsOptions.FailedPasswordAllowedAttempts)
                {
                    user.LoginLock(DateTimeOffset.UtcNow.AddMinutes(_upmsOptions.FailedPasswordLockoutMinutes));
                }

                await _userRepository.UpdateAsync(user);
                await _userRepository.UnitOfWork.SaveEntitiesAsync();
                return new Tuple<AccountLoginResults, Account?>(AccountLoginResults.WrongPassword, null);
            }
        }

        public async Task<Tuple<ValidateRefreshTokenResults, Account?>> ValidateRefreshTokenAsync(string userId, string refreshToken)
        {
            if (string.IsNullOrEmpty(refreshToken))
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.NotExist, null);

            UserRefreshToken? userRefreshToken = await _userRefreshTokenRepository.GetByRefreshTokenAsync(refreshToken);

            if (userRefreshToken == null)
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.NotExist, null);

            if (userRefreshToken.IsExpire())
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.Expire, null);

            if (!long.TryParse(userId, out long id))
                throw new BusinessException($"userId {userId} 格式错误");

            if (userRefreshToken.UserId != id)
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.NotExist, null);

#if DEBUG
            if (userRefreshToken.GetRefreshInterval() < 1)
#else
            if (userRefreshToken.GetRefreshInterval() < 60)
#endif
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.Frequently, null);

            User? user = await _userRepository.GetAsync(id);

            if (user == null
                || user.CheckUserState() != 0)
                return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.AccountAbnormal, null);

            return new Tuple<ValidateRefreshTokenResults, Account?>(ValidateRefreshTokenResults.Success,
                 new Account($"{user.Id}", $"{user.OrgId}", user.LoginName));
        }

        /// <summary>
        /// 移除刷新令牌
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public async Task RemoveRefreshTokenAsync(string userId, string refreshToken)
        {
            if (!long.TryParse(userId, out long id))
                return;

            UserRefreshToken? userRefreshToken = await _userRefreshTokenRepository.GetByRefreshTokenAsync(refreshToken);
            if (userRefreshToken != null && userRefreshToken.UserId == id)
                await _userRefreshTokenRepository.DeleteAsync(userRefreshToken);
        }

        public async Task CreateLoginLogAsync(string loginName, string ipAddress, string loginWay, string clientBrowser, string clientOs, bool status, string remark, string orgId)
        {
            if (!long.TryParse(orgId, out long oid))
                oid = 0;

            LogLogin logLogin = new LogLogin(loginName, oid)
            {
                ClientOs = clientOs,
                IpAddress = ipAddress,
                LoginWay = loginWay,
                LoginTime = DateTimeOffset.UtcNow,
                UserName = loginName,
                Status = status,
                Remark = remark,
                ClientBrowser = clientBrowser
            };
            await _logLoginRepository.InsertAsync(logLogin);
            await _logLoginRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}