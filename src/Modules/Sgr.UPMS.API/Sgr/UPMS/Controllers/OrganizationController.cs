﻿/**************************************************************
 *
 * 唯一标识：99b060da-1e92-40c8-a149-eda1679394bf
 * 命名空间：Sgr.UPMS.Controllers
 * 创建时间：2023/8/25 15:43:42
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.Application.ViewModels;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.ExceptionHandling;
using Sgr.Oss.Services;
using Sgr.UPMS.Application.Commands.Organizations;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.ViewModels;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Controllers
{
    /// <summary>
    /// 组织机构管理
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [ApiController]
    public class OrganizationController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IAuthorizationService _authorizationService;
        private readonly IOrganizationQueries _organizationQueries;

        public OrganizationController(IMediator mediator,
            IAuthorizationService authorizationService,
            IOrganizationQueries organizationQueries)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _organizationQueries = organizationQueries ?? throw new ArgumentNullException(nameof(organizationQueries));
        }

        #region Commond

        /// <summary>
        /// 创建组织机构
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建组织机构")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateOrgCommand command)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateOrgPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 修改组织机构
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改组织机构")]
        public async Task<ActionResult<bool>> UpdateOrgAsync([FromBody] UpdateOrgCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.UpdateOrgPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 注销组织机构
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("cancellation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("注销组织机构")]
        public async Task<ActionResult<bool>> CancellationOrgAsync([FromBody] CancellationOrgCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.CancellationOrgPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 注册组织机构
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("注册组织机构")]
        public async Task<ActionResult<bool>> RegisterOrgAsync([FromBody] RegisterOrgCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 绑定组织机构
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("associated_parent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("绑定组织机构")]
        public async Task<ActionResult<bool>> AssociatedParentOrgAsync([FromBody] AssociatedParentOrgCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.AssociatedParentOrgPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        #endregion Commond

        #region Queries

        /// <summary>
        /// 根据Id获取组织机构信息
        /// </summary>
        /// <param name="id">组织Id</param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutOrganizationViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutOrganizationViewModel?>> GetByIdAsync(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewUserPermission))
                return Unauthorized();

            var result = await _organizationQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取组织机构分页列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(PagedResponse<OutOrganizationPagedModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PagedResponse<OutOrganizationPagedModel>>> GetPagedListAsync([FromBody] InOrganizationSearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewUserPermission))
                return Unauthorized();

            var result = await _organizationQueries.GetPagedListAsync(request);
            return Ok(result);
        }

        #endregion Queries
    }
}