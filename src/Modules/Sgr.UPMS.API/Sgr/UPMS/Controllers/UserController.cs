﻿/**************************************************************
 *
 * 唯一标识：1dad0b24-9f2f-406a-bedf-0ba5e8a24642
 * 命名空间：Sgr.UPMS.API.Controllers
 * 创建时间：2024/1/1
 * 描述：用户控制器
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.Domain.Entities;
using Sgr.UPMS.Application.Commands.Roles;
using Sgr.UPMS.Application.Commands.Users;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.Queries.Impl;
using Sgr.UPMS.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.API.Controllers
{
    /// <summary>
    /// 用户控制器
    /// </summary>
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserQueries _userQueries;

        public UserController(IMediator mediator,
            IAuthorizationService authorizationService,
            IUserQueries userQueries)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        #region Command

        /// <summary>
        /// 创建账号
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建账号")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateUserCommand command)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateUserPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 修改账号
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改账号")]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateUserCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.UpdateUserPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 删除账号
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("删除账号")]
        public async Task<ActionResult<bool>> DeleteAsync([FromBody] DeleteUserCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.DeleteUserPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 调整账号状态
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("change-status")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("调整账号状态")]
        public async Task<ActionResult<bool>> ModifyUserStatusAsync([FromBody] ModifyUserStatusCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ModifyUserStatusPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("reset-password")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("重置密码")]
        public async Task<ActionResult<bool>> ResetPasswordAsync([FromBody] ResetPasswordCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ResetPasswordPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 用户中心-修改个人信息
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("change-personal")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("用户中心-修改个人信息")]
        public async Task<ActionResult<bool>> ModifyUserAsync([FromBody] ModifyUserCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ModifyUserPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 用户中心-修改密码
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("change-password")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("用户中心-修改密码")]
        public async Task<ActionResult<bool>> ModifyPasswordAsync([FromBody] ModifyPasswordCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ModifyPasswordPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        #endregion Command

        #region Query

        /// <summary>
        /// 根据Id获取用户信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutUserViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutDepartmentViewModel>> GetByIdAsync(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewUserPermission))
                return Unauthorized();

            var result = await _userQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<OutUserViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<OutUserViewModel>>> GetPagedListAsync([FromBody] InUserSearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewUserPermission))
                return Unauthorized();

            var result = await _userQueries.GetPagedListAsync(request);
            return Ok(result);
        }

        #endregion Query
    }
}