﻿/**************************************************************
 *
 * 唯一标识：cba256ae-0755-4fa8-8cdb-338009b0a8e7
 * 命名空间：Sgr.UPMS.API.Controllers
 * 创建时间：2024/1/1
 * 描述：角色控制器
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.Application.ViewModels;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.ExceptionHandling;
using Sgr.UPMS.Application.Commands.Roles;
using Sgr.UPMS.Application.Commands.Users;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.Queries.Impl;
using Sgr.UPMS.Application.ViewModels;
using System;
using System.Threading.Tasks;

namespace Sgr.UPMS.API.Controllers
{
    /// <summary>
    /// 角色控制器
    /// </summary>
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IAuthorizationService _authorizationService;
        private readonly IRoleQueries _roleQueries;

        public RoleController(IMediator mediator,
            IAuthorizationService authorizationService,
            IRoleQueries roleQueries)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _roleQueries = roleQueries ?? throw new ArgumentNullException(nameof(roleQueries));
        }

        #region Command

        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建角色")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateRoleCommand command)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateRolePermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改角色")]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateRoleCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.UpdateRolePermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("删除角色")]
        public async Task<ActionResult<bool>> DeleteAsync([FromBody] DeleteRoleCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.DeleteRolePermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 调整角色状态
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("change-status")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("调整角色状态")]
        public async Task<ActionResult<bool>> ModifyUserStatusAsync([FromBody] ModifyRoleStatusCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ModifyRoleStatusPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 授予功能权限
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("allocate-functionpermission")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("授予功能权限")]
        public async Task<ActionResult<bool>> ResetPasswordAsync([FromBody] AllocateFunctionPermissionCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.AllocateFunctionPermissionPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        #endregion Command

        #region Query

        /// <summary>
        /// 根据Id获取角色信息
        /// </summary>
        /// <param name="id">角色Id</param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutRoleViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutRoleViewModel>> GetById(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewLogLoginPermission))
                return Unauthorized();

            var result = await _roleQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取角色分页列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(PagedResponse<OutRoleViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PagedResponse<OutRoleViewModel>>> GetPagedList([FromBody] InRoleSearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewLogLoginPermission))
                return Unauthorized();

            var result = await _roleQueries.GetPagedListAsync(request);
            return Ok(result);
        }

        #endregion Query
    }
}