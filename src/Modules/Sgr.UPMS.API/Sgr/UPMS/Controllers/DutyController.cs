﻿/**************************************************************
 *
 * 唯一标识：56967dd2-3e85-456d-b0e8-5986d4747289
 * 命名空间：Sgr.UPMS.Controllers
 * 创建时间：2023/8/27 19:32:14
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.UPMS.Application.Commands.Departments;
using Sgr.UPMS.Application.Commands.Duties;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Controllers
{
    /// <summary>
    /// 职务管理
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [ApiController]
    public class DutyController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IAuthorizationService _authorizationService;
        private readonly IDutyQueries _dutyQueries;

        public DutyController(IMediator mediator,
            IAuthorizationService authorizationService,
            IDutyQueries dutyQueries)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _dutyQueries = dutyQueries ?? throw new ArgumentNullException(nameof(dutyQueries));
        }

        #region Command

        /// <summary>
        /// 创建职务
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建职务")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateDutyCommand command)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateDutyPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 修改职务
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改职务")]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateDutyCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.UpdateDutyPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 删除职务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:long:min(1)}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("删除职务")]
        public async Task<ActionResult<bool>> DeleteAsync(long id)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.DeleteDutyPermission))
                return this.Unauthorized();

            return await _mediator.Send(new DeleteDutyCommand() { DutyId = id });
        }

        /// <summary>
        /// 调整职务状态
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("change-status")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("调整职务状态")]
        public async Task<ActionResult<bool>> ModifyUserStatusAsync([FromBody] ModifyDutyStatusCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.ModifyDutyStatusPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        #endregion Command

        #region Query

        /// <summary>
        /// 根据Id获取职务信息
        /// </summary>
        /// <param name="id">职务Id</param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutDutyViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutDutyViewModel?>> GetById(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewDutyPermission))
                return Unauthorized();

            var result = await _dutyQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取职务列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<OutDutyViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<OutDutyViewModel>>> GetList([FromBody] InDutySearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewDutyPermission))
                return Unauthorized();

            var result = await _dutyQueries.GetListByOrgIdAsync(request);
            return Ok(result);
        }

        #endregion Query
    }
}