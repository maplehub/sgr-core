﻿/**************************************************************
 *
 * 唯一标识：be28a09b-52d3-4202-9f45-c4fc25eb41ab
 * 命名空间：Sgr.UPMS.Controllers
 * 创建时间：2023/8/27 20:32:39
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.UPMS.Application.Commands.Departments;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Controllers
{
    /// <summary>
    /// 部门管理
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IDepartmentQueries _departmentQueries;
        private readonly IAuthorizationService _authorizationService;

        public DepartmentController(IMediator mediator,
            IDepartmentQueries departmentQueries,
            IAuthorizationService authorizationService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _departmentQueries = departmentQueries ?? throw new ArgumentNullException(nameof(departmentQueries));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
        }

        #region Command

        /// <summary>
        /// 创建部门
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建部门")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateDepartmentCommand command)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateDepartmentPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 修改部门
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改部门")]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateDepartmentCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.UpdateDepartmentPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isCascade"></param>
        /// <returns></returns>
        [HttpDelete("{id:long:min(1)}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("删除部门")]
        public async Task<ActionResult<bool>> DeleteAsync(long id, [FromQuery] bool? isCascade)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(this.User, Permissions.DeleteDepartmentPermission))
                return this.Unauthorized();

            return await _mediator.Send(new DeleteDepartmentCommand() { DepartmentId = id, IsCascade = isCascade.HasValue ? isCascade.Value : true });
        }

        #endregion Command

        #region Queries

        /// <summary>
        /// 根据Id获取部门信息
        /// </summary>
        /// <param name="id">部门Id</param>
        /// <returns>部门视图模型</returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutDepartmentViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutDepartmentViewModel>> GetByIdAsync(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewDepartmentPermission))
                return Unauthorized();

            var result = await _departmentQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>部门列表</returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<OutDepartmentViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<OutDepartmentViewModel>>> GetListAsync([FromBody] InDepartmentSearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewDepartmentPermission))
                return Unauthorized();

            var result = await _departmentQueries.GetListAsync(request);
            return Ok(result);
        }

        #endregion Queries
    }
}