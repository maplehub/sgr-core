/**************************************************************
 *
 * 命名空间：Sgr.UPMS.API.Controllers
 * 创建时间：2024/1/1
 * 描述：登录日志控制器
 *
 **************************************************************/

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.Queries;
using Sgr.UPMS.Application.Queries.Impl;
using Sgr.UPMS.Application.ViewModels;
using System.Threading.Tasks;

namespace Sgr.UPMS.API.Controllers
{
    /// <summary>
    /// 登录日志控制器
    /// </summary>
    [Route("api/loglogin")]
    [ApiController]
    public class LogLoginController : ControllerBase
    {
        private readonly ILogLoginQueries _logLoginQueries;
        private readonly IAuthorizationService _authorizationService;

        public LogLoginController(ILogLoginQueries logLoginQueries,
            IAuthorizationService authorizationService)
        {
            _logLoginQueries = logLoginQueries;
            _authorizationService = authorizationService;
        }

        /// <summary>
        /// 根据Id获取登录日志
        /// </summary>
        /// <param name="id">日志Id</param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(typeof(OutLogLoginViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OutLogLoginViewModel?>> GetByIdAsync(long id)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewLogLoginPermission))
                return Unauthorized();

            var result = await _logLoginQueries.GetByIdAsync(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        /// <summary>
        /// 获取登录日志分页列表
        /// </summary>
        /// <param name="request">查询参数</param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(typeof(PagedResponse<OutLogLoginViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PagedResponse<OutLogLoginViewModel>>>
            GetPagedListAsync([FromBody] InLogLoginSearchModel request)
        {
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.ViewLogLoginPermission))
                return Unauthorized();

            var result = await _logLoginQueries.GetPagedListAsync(request);
            return Ok(result);
        }
    }
}