﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sgr.Identity;
using Sgr.Indentity.Utilities;
using Sgr.Utilities;
using System;

namespace Sgr.Indentity.Controllers
{
    /// <summary>
    /// 验证码服务
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class CaptchaController : ControllerBase
    {
        private readonly CaptchaOptions _captchaOptions;

        public CaptchaController(CaptchaOptions captchaOptions)
        {
            _captchaOptions = captchaOptions;
        }

        [HttpGet]
        public FileResult GetCaptcha()
        {
            Tuple<string, byte[]> captchaCode = _captchaOptions.CaptchaIsArithmetic ? CaptchaHelper.CreateArithmeticCaptcha() : CaptchaHelper.CreateCaptcha();
            this.HttpContext.Response.Headers["sgr-captcha"] = CaptchaHelper.BuildCaptchaCode(captchaCode.Item1);
            return File(captchaCode.Item2, @"image/jpeg");
        }
    }
}