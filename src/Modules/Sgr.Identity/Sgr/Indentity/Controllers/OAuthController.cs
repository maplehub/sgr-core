﻿/**************************************************************
 *
 * 唯一标识：23d7abe5-f26d-4e5d-862c-5581824b195c
 * 命名空间：Sgr.Indentity.Controllers
 * 创建时间：2023/8/21 11:34:49
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Sgr.ExceptionHandling;
using Sgr.Exceptions;
using Sgr.Indentity.Application.Commands;
using Sgr.Indentity.Application.ViewModels;
using System;
using System.Threading.Tasks;

namespace Sgr.Indentity.Controllers
{
    /// <summary>
    /// 基于JWT的认证服务
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class OAuthController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OAuthController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        /// <summary>
        /// 基于JWT令牌认证登录
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        //[DisableCors]
        [Route("token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TokenViewModel>> TokenAsync([FromBody] UserLoginCommand command)
        {
            var result = await _mediator.Send(command);

            if (!result.Success || result.Data == null)
                return this.CustomBadRequest(result.Message ?? "");

            return result.Data;
        }

        /// <summary>
        /// 刷新JWT令牌
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("refresh-token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TokenViewModel>> RefreshTokenAsync([FromBody] RefreshTokenCommand command)
        {
            var result = await _mediator.Send(command);

            if (!result.Success || result.Data == null)
                return this.CustomBadRequest(result.Message ?? "");

            return result.Data;
        }

        /// <summary>
        /// 注销
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("logout")]
        public async Task<bool> LogoutAsync([FromBody] LogoutCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}