﻿//using Sgr.Indentity.Application.ViewModels;
//using Sgr.Utilities;
//using System.Net.Http;
//using System.Text.Json;
//using System.Threading.Tasks;
//using System;
//using System.Net.Http.Json;
//using Sgr.Indentity.Utilities;
//using System.Xml.Linq;

//namespace Sgr.Indentity
//{
//#if DEBUG

//    public static class DevAuthHelper
//    {
//        private static readonly JsonSerializerOptions _jsonSerializerOptions;
//        private const string BaseUrl = "api/v1/sgr/oauth";

//        static DevAuthHelper()
//        {
//            _jsonSerializerOptions = new JsonSerializerOptions();
//            JsonHelper.UpdateJsonSerializerOptions(_jsonSerializerOptions);
//        }

//        public static async Task<string> GetDevTokenAsync()
//        {
//            var name = "SGR Default";// 开发环境默认用户名
//            string password = "qwert@123"; // 开发环境默认密码
//            var nonce = Guid.NewGuid().ToString("N");
//            var verificationCode = "1234";
//            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
//            var url = "https://localhost:7046";

//            using var httpClient = new HttpClient();
//            // 设置基础地址，根据实际情况修改
//            httpClient.BaseAddress = new Uri(url);

//            // 创建登录命令
//            var loginCommand = new
//            {
//                Name = name,
//                Password = HashHelper.CreateMd5(password),
//                Timestamp = timestamp,
//                Nonce = nonce,
//                Signature = HashHelper.CreateMd5($"sgr-{timestamp}-{nonce}-{name}-{password}-sgr"), // 如果需要签名，在这里添加
//                VerificationCode = verificationCode, // 如果需要验证码，在这里添加
//                VerificationHash = CaptchaHelper.BuildCaptchaCode(verificationCode) // 如果需要验证码哈希，在这里添加
//            };

//            try
//            {
//                // 发送登录请求
//                var response = await httpClient.PostAsJsonAsync($"{BaseUrl}/token", loginCommand, _jsonSerializerOptions);
//                response.EnsureSuccessStatusCode();

//                // 解析响应
//                var result = await response.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
//                return result?.AccessToken ?? string.Empty;
//            }
//            catch (Exception ex)
//            {
//                // 在开发环境中记录异常
//                Console.WriteLine($"获取开发环境 Token 失败: {ex.Message}");
//                return string.Empty;
//            }
//        }
//    }

//#endif
//}