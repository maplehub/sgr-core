﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.JsonWebTokens;
using Sgr.Application;
using Sgr.Identity;
using Sgr.Identity.Services;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Indentity.Application.Commands
{
    public class LogoutCommandHandle :
        IdentityCommandHandleBase,
        IRequestHandler<LogoutCommand, bool>
    {
        private readonly JwtOptions _jwtOptions;
        private readonly ISignatureChecker _signatureChecker;
        private readonly ITokenBlacklistService _tokenBlacklistService;

        public LogoutCommandHandle(
            IAccountAuthService accountService,
            IHttpContextAccessor httpContextAccessor,
            ISignatureChecker signatureChecker,
            ITokenBlacklistService tokenBlacklistService,
            JwtOptions jwtOptions
            )
            : base(accountService, httpContextAccessor)
        {
            _jwtOptions = jwtOptions;
            _signatureChecker = signatureChecker;
            _tokenBlacklistService = tokenBlacklistService;
        }

        public async Task<bool> Handle(LogoutCommand request, CancellationToken cancellationToken)
        {
            Check.NotNull(request, nameof(request));
            Check.StringNotNullOrEmpty(request.AccessToken, nameof(request.AccessToken));
            if (_jwtOptions.UseSignature)
            {
                if (!_signatureChecker.VerifySignature(request.Signature, request.Timestamp, request.Nonce, $"{request.AccessToken}"))
                    return false;
            }

            ClaimsPrincipal? claimsPrincipal = ValidateAccessToken(request.AccessToken!, _jwtOptions);
            if (claimsPrincipal == null)
                return false;

            //令牌添加至黑名单
            var jti = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Jti)?.Value;

            if (!string.IsNullOrEmpty(jti))
            {
                await _tokenBlacklistService.AddToBlacklistAsync(jti, TimeSpan.FromSeconds(_jwtOptions.ExpireSeconds));
            }

            //刷新令牌移除
            var userId = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == Constant.CLAIM_USER_ID)?.Value;
            HttpRequest? httpRequest = _httpContextAccessor?.HttpContext?.Request;

            if (!string.IsNullOrEmpty(userId)
                && httpRequest != null
                && httpRequest.Cookies.TryGetValue("refreshToken", out var oldRefreshToken))
            {
                await this._accountService.RemoveRefreshTokenAsync(userId, oldRefreshToken);
            }

            return true;
        }
    }
}