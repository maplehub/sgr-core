﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Sgr.Application;
using Sgr.Application.ViewModels;
using Sgr.Generator;
using Sgr.Identity;
using Sgr.Identity.Services;
using Sgr.Indentity.Application.ViewModels;
using Sgr.Indentity.Utilities;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Indentity.Application.Commands
{
    public class UserLoginCommandHandle :
        IdentityCommandHandleBase,
        IRequestHandler<UserLoginCommand, ObjectResponse<TokenViewModel>>
    {
        private readonly JwtOptions _jwtOptions;
        private readonly ISignatureChecker _signatureChecker;
        private readonly IStringIdGenerator _stringIdGenerator;

        public UserLoginCommandHandle(
            IAccountAuthService accountService,
            IHttpContextAccessor httpContextAccessor,
            ISignatureChecker signatureChecker,
            IStringIdGenerator stringIdGenerator,
            JwtOptions jwtOptions
            )
            : base(accountService, httpContextAccessor)
        {
            _jwtOptions = jwtOptions;
            _signatureChecker = signatureChecker;
            _stringIdGenerator = stringIdGenerator;
        }

        public async Task<ObjectResponse<TokenViewModel>> Handle(UserLoginCommand request, CancellationToken cancellationToken)
        {
            Check.NotNull(request, nameof(request));

            Check.StringNotNullOrEmpty(request.Name, nameof(request.Name));
            Check.StringNotNullOrEmpty(request.Password, nameof(request.Password));

            if (_jwtOptions.UseSignature)
            {
                if (!_signatureChecker.VerifySignature(request.Signature, request.Timestamp, request.Nonce, $"{request.Name}-{request.Password}"))
                    return ObjectResponse<TokenViewModel>.FailResponse("获取令牌时，参数中的签名Signature验证失败!");
            }

            if (_jwtOptions.UseCaptcha)
            {
                if (request.VerificationHash != CaptchaHelper.BuildCaptchaCode(request.VerificationCode))
                    return ObjectResponse<TokenViewModel>.FailResponse("验证码错误!");
            }

            //验证账号
            var loginResult = await _accountService.ValidateAccountAsync(request.Name, request.Password);

            //账号验证失败
            if (loginResult.Item1 != AccountLoginResults.Success)
            {
                string msg = getMessageFromAccountLoginResults(loginResult.Item1);
                await CreateLoginLogAsync(request.Name, "账号密码登录", false, msg, loginResult.Item2?.OrgId ?? "");
                return ObjectResponse<TokenViewModel>.FailResponse(msg);
            }

            if (loginResult.Item2 == null)
            {
                string msg = "创建令牌时的账号信息为空!";
                await CreateLoginLogAsync(request.Name, "账号密码登录", false, msg, loginResult.Item2?.OrgId ?? "");
                return ObjectResponse<TokenViewModel>.FailResponse(msg);
            }

            //创建JWT令牌
            string jti = _stringIdGenerator.GenerateUniqueId();
            var claims = GetClaims(jti, loginResult.Item2, _jwtOptions);
            string accessToken = CreateAccessToken(claims, _jwtOptions);

            //创建刷新令牌并将其写入库中
            string newRefreshToken = _stringIdGenerator.GenerateUniqueId();
            await _accountService.CreateRefreshTokenAsync(loginResult.Item2.Id,
                newRefreshToken,
                "",
                _jwtOptions.RefreshTokenExpireMinutes,
                _httpContextAccessor?.HttpContext?.GetClientIpAddress() ?? "");

            //刷新令牌写入Cookie
            WriteRefreshTokenToCookie(_httpContextAccessor?.HttpContext?.Response, newRefreshToken, _jwtOptions);

            //创建登录日志
            await CreateLoginLogAsync(request.Name, "账号密码登录", true, "", loginResult.Item2?.OrgId ?? "");

            return ObjectResponse<TokenViewModel>.SuccessResponse(new TokenViewModel()
            {
                AccessToken = accessToken
            });
        }

        private static string getMessageFromAccountLoginResults(AccountLoginResults accountLoginResults)
        {
            return accountLoginResults switch
            {
                AccountLoginResults.IsDeactivate => "账号被禁用!",
                AccountLoginResults.IsDelete => "账号被删除!",
                AccountLoginResults.IsLock => "账号被锁定!",
                AccountLoginResults.WrongPassword or AccountLoginResults.NotExist => "账号或密码错误!",
                AccountLoginResults.Success => "成功",
                _ => "未知的",
            };
        }
    }
}