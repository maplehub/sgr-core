﻿using MediatR;
using Sgr.Application.ViewModels;
using Sgr.Indentity.Application.ViewModels;

namespace Sgr.Indentity.Application.Commands
{
    public class RefreshTokenCommand : IRequest<ObjectResponse<TokenViewModel>>
    {
        /// <summary>
        /// 访问令牌文本内容。
        /// </summary>
        public string? AccessToken { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; } = string.Empty;

        /// <summary>
        /// 随机数
        /// </summary>
        public string Nonce { get; set; } = string.Empty;

        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; } = string.Empty;
    }
}