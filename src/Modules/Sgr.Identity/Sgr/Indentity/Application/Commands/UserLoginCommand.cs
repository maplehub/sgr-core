﻿using MediatR;
using Sgr.Application.ViewModels;
using Sgr.Indentity.Application.ViewModels;

namespace Sgr.Indentity.Application.Commands
{
    public class UserLoginCommand : IRequest<ObjectResponse<TokenViewModel>>
    {
        /// <summary>
        /// 用户名称（可以是登录账号、手机号或邮箱地址）
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; } = string.Empty;

        /// <summary>
        /// 随机数
        /// </summary>
        public string Nonce { get; set; } = string.Empty;

        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; } = string.Empty;

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerificationCode { get; set; } = string.Empty;

        /// <summary>
        /// 验证码Hash值
        /// </summary>
        public string VerificationHash { get; set; } = string.Empty;
    }
}