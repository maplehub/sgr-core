﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Sgr.AuditLogs;
using Sgr.Generator;
using Sgr.Identity;
using Sgr.Identity.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Indentity.Application.Commands
{
    public abstract class IdentityCommandHandleBase
    {
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly IAccountAuthService _accountService;

        public IdentityCommandHandleBase(IAccountAuthService accountService, IHttpContextAccessor httpContextAccessor)
        {
            _accountService = accountService;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 创建登录日志
        /// </summary>
        /// <param name="loginName">登录用户名称</param>
        /// <param name="loginWay">登录方式</param>
        /// <param name="status">登录状态</param>
        /// <param name="remarks">备注</param>
        /// <param name="orgId">所属组织</param>
        /// <returns></returns>
        protected async Task CreateLoginLogAsync(
            string loginName,
            string loginWay,
            bool status,
            string remarks,
            string orgId)
        {
            var httpUserAgentProvider = _httpContextAccessor.HttpContext?.RequestServices?.GetService<IHttpUserAgentProvider>();
            if (httpUserAgentProvider != null && _httpContextAccessor.HttpContext != null)
            {
                var httpUserAgent = httpUserAgentProvider.Analysis(_httpContextAccessor.HttpContext);

                await _accountService.CreateLoginLogAsync(loginName,
                                 _httpContextAccessor.HttpContext!.GetClientIpAddress(),
                                 loginWay,
                                 httpUserAgent?.BrowserInfo ?? "",
                                 httpUserAgent?.Os ?? "",
                                 status,
                                 remarks,
                                 orgId);
            }
        }

        protected Claim[] GetClaims(string jti, Account account, JwtOptions jwtOptions)
        {
            return new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,jti),//JwtRegisteredClaimNames.Jti JWT的唯一标识符，可用于后续JWT黑名单处理逻辑
                new Claim(ClaimTypes.Name, account.LoginName),//JwtRegisteredClaimNames.Name
                //new Claim(ClaimTypes.Role,string.Join("," ,account.Roles)),   //出于安全考虑，令牌中不再包含角色列表
                new Claim(Constant.CLAIM_USER_ID, account.Id.ToString()),
                new Claim(Constant.CLAIM_USER_ORGID, account.OrgId.ToString())
            };
        }

        /// <summary>
        /// 刷新令牌写入Cookie
        /// </summary>
        /// <param name="httpResponse"></param>
        /// <param name="refreshToken"></param>
        /// <param name="jwtOptions"></param>
        protected void WriteRefreshTokenToCookie(HttpResponse? httpResponse, string refreshToken, JwtOptions jwtOptions)
        {
            if (httpResponse == null)
                return;

            httpResponse!.Cookies.Append("refreshToken", refreshToken, new CookieOptions
            {
                HttpOnly = true,
                Secure = jwtOptions.RefreshTokenSecure,                                           // 生产环境中应启用 HTTPS
                SameSite = HttpCookieOptions.ToSameSiteMode(jwtOptions.RefreshTokenSameSite),     // 根据需求设置
                Expires = DateTime.UtcNow.AddMinutes(jwtOptions.RefreshTokenExpireMinutes)        // 设置适当的过期时间
            });
        }

        /// <summary>
        /// 创建令牌
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        protected string CreateAccessToken(IEnumerable<Claim> claims, JwtOptions options)
        {
            //过期时间
            TimeSpan timeSpan = TimeSpan.FromSeconds(options.ExpireSeconds);
            //加密的token密钥
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Key));
            //签名证书，其值为securityKey和HmacSha256Signature算法
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            //表示jwt token的描述信息，其值包括Issuer签发方，Audience接收方，Claims载荷，过期时间和签名证书
            var tokenDescriptor = new JwtSecurityToken(options.Issuer,
                options.Audience,
                claims,
                expires: DateTime.Now.Add(timeSpan),
                signingCredentials: credentials);
            //使用该方法转换为字符串形式的jwt token返回
            return new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
        }

        /// <summary>
        /// 验证令牌
        /// </summary>
        /// <param name="token"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        protected ClaimsPrincipal? ValidateAccessToken(string token, JwtOptions options)
        {
            if (string.IsNullOrWhiteSpace(token))
                return default;

            token = token.Replace($"{JwtBearerDefaults.AuthenticationScheme} ", "");
            TokenValidationParameters validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,                                                  //是否验证发行商
                ValidateAudience = true,                                                //是否验证受众者
                ValidateLifetime = true,                                                //是否验证失效时间
                ValidateIssuerSigningKey = true,                                        //是否验证签名键
                ValidIssuer = options.Issuer,
                ValidAudience = options.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Key)),
                ClockSkew = TimeSpan.FromSeconds(options.ClockSkewSeconds),             //过期时间容错值，解决服务器端时间不同步问题（秒）
                RequireExpirationTime = true,
            };

            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();

            ClaimsPrincipal? claimsPrincipal;

            try
            {
                claimsPrincipal = jwtSecurityTokenHandler.ValidateToken(token, validationParameters, out _);
            }
            catch (SecurityTokenExpiredException)
            {
                //表示过期
                return default;
            }
            catch (SecurityTokenException)
            {
                //表示token错误
                return default;
            }
            catch
            {
                return default;
            }
            return claimsPrincipal;
        }
    }
}