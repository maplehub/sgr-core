﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Sgr.Application;
using Sgr.Application.ViewModels;
using Sgr.Generator;
using Sgr.Identity;
using Sgr.Identity.Services;
using Sgr.Indentity.Application.ViewModels;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.Indentity.Application.Commands
{
    public class RefreshTokenCommandHandle :
        IdentityCommandHandleBase,
        IRequestHandler<RefreshTokenCommand, ObjectResponse<TokenViewModel>>
    {
        private readonly JwtOptions _jwtOptions;
        private readonly ISignatureChecker _signatureChecker;
        private readonly IStringIdGenerator _stringIdGenerator;

        public RefreshTokenCommandHandle(
            IAccountAuthService accountService,
            IHttpContextAccessor httpContextAccessor,
            ISignatureChecker signatureChecker,
            IStringIdGenerator stringIdGenerator,
            JwtOptions jwtOptions
            )
            : base(accountService, httpContextAccessor)
        {
            _jwtOptions = jwtOptions;
            _signatureChecker = signatureChecker;
            _stringIdGenerator = stringIdGenerator;
        }

        public async Task<ObjectResponse<TokenViewModel>> Handle(RefreshTokenCommand request, CancellationToken cancellationToken)
        {
            Check.NotNull(request, nameof(request));
            Check.StringNotNullOrEmpty(request.AccessToken, nameof(request.AccessToken));
            if (_jwtOptions.UseSignature)
            {
                if (!_signatureChecker.VerifySignature(request.Signature, request.Timestamp, request.Nonce, $"{request.AccessToken}"))
                    return ObjectResponse<TokenViewModel>.FailResponse("刷新令牌时，参数中的签名Signature验证失败!");
            }

            HttpRequest? httpRequest = _httpContextAccessor?.HttpContext?.Request;

            if (httpRequest == null || !httpRequest.Cookies.TryGetValue("refreshToken", out var oldRefreshToken) || string.IsNullOrEmpty(oldRefreshToken))
                return ObjectResponse<TokenViewModel>.FailResponse("RefreshToken无法解析!");

            ClaimsPrincipal? claimsPrincipal = ValidateAccessToken(request.AccessToken!, _jwtOptions);
            if (claimsPrincipal == null)
                return ObjectResponse<TokenViewModel>.FailResponse("AccessToken验证失败!");

            var userId = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == Constant.CLAIM_USER_ID)?.Value;
            var userName = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value ?? "";
            var orgId = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == Constant.CLAIM_USER_ORGID)?.Value ?? "";

            if (userId == null)
                return ObjectResponse<TokenViewModel>.FailResponse("AccessToken中无法获取用户标识!");

            if (string.IsNullOrEmpty(userName))
                return ObjectResponse<TokenViewModel>.FailResponse("AccessToken中无法获取用户名称!");

            //验证刷新Token
            var validateResult = await _accountService.ValidateRefreshTokenAsync(userId, oldRefreshToken);

            if (validateResult.Item1 != ValidateRefreshTokenResults.Success)
            {
                string msg = getMessageFromValidateRefreshTokenResults(validateResult.Item1);
                await CreateLoginLogAsync(userName, "刷新令牌", false, msg, orgId);
                return ObjectResponse<TokenViewModel>.FailResponse(msg);
            }

            if (validateResult.Item2 == null)
            {
                string msg = "刷新令牌时的账号信息为空!";
                await CreateLoginLogAsync(userName, "刷新令牌", false, msg, orgId);
                return ObjectResponse<TokenViewModel>.FailResponse(msg);
            }

            //创建JWT令牌
            string jti = _stringIdGenerator.GenerateUniqueId();
            var claims = GetClaims(jti, validateResult.Item2, _jwtOptions);
            string accessToken = CreateAccessToken(claims, _jwtOptions);

            //创建刷新令牌并将其写入库中
            string newRefreshToken = _stringIdGenerator.GenerateUniqueId();
            await _accountService.CreateRefreshTokenAsync(userId,
                newRefreshToken,
                oldRefreshToken,
                _jwtOptions.RefreshTokenExpireMinutes,
                _httpContextAccessor?.HttpContext?.GetClientIpAddress() ?? "");

            //刷新令牌写入Cookie
            WriteRefreshTokenToCookie(_httpContextAccessor?.HttpContext?.Response, newRefreshToken, _jwtOptions);

            //创建登录日志
            await CreateLoginLogAsync(userName, "刷新令牌", true, "", orgId);

            return ObjectResponse<TokenViewModel>.SuccessResponse(new TokenViewModel()
            {
                AccessToken = accessToken
            });
        }

        private static string getMessageFromValidateRefreshTokenResults(ValidateRefreshTokenResults validateRefreshTokenResults)
        {
            return validateRefreshTokenResults switch
            {
                ValidateRefreshTokenResults.Expire => "RefreshToken已过期!",
                ValidateRefreshTokenResults.Frequently => "RefreshToken刷新过于频繁!",
                ValidateRefreshTokenResults.AccountAbnormal => "账号异常!",
                ValidateRefreshTokenResults.NotExist => "RefreshToken不存在!",
                ValidateRefreshTokenResults.Success => "成功",
                _ => "未知的",
            };
        }
    }
}