﻿/**************************************************************
 *
 * 唯一标识：9b5ffe62-1b15-42ce-9f36-539511816351
 * 命名空间：Sgr.DataCategories.Controllers
 * 创建时间：2023/8/20 16:57:30
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sgr.Application.ViewModels;
using Sgr.AspNetCore.ActionFilters;
using Sgr.AspNetCore.ActionFilters.AuditLogs;
using Sgr.DataCategories.Application.Commands;
using Sgr.DataCategories.Application.Queries;
using Sgr.DataCategories.Application.ViewModels;
using Sgr.ExceptionHandling;
using System;
using System.Threading.Tasks;

namespace Sgr.DataCategories.Controllers
{
    /// <summary>
    /// 数据字典
    /// </summary>
    [Route("api/v1/sgr/[controller]")]
    [ApiController]
    public class DataCategoryController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IDataCategoryQueries _dataCategoryQueries;
        private readonly IAuthorizationService _authorizationService;

        public DataCategoryController(IMediator mediator,
            IDataCategoryQueries dataCategoryQueries,
            IAuthorizationService authorizationService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _dataCategoryQueries = dataCategoryQueries ?? throw new ArgumentNullException(nameof(dataCategoryQueries));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
        }

        #region Commond

        /// <summary>
        /// 创建数据字典项
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("创建数据字典项")]
        public async Task<ActionResult<bool>> CreateAsync([FromBody] CreateCategpryItemCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateAndUpdateCategpryItemPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 创建数据字典项
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("修改数据字典项")]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateCategpryItemCommand command)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.CreateAndUpdateCategpryItemPermission))
                return this.Unauthorized();

            return await _mediator.Send(command);
        }

        /// <summary>
        /// 删除数据字典项
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:long:min(1)}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("删除数据字典项")]
        public async Task<ActionResult<bool>> DeleteAsync(long id)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.DeleteCategpryItemPermission))
                return this.Unauthorized();

            return await _mediator.Send(new DeleteCategpryItemCommand() { Id = id });
        }

        #endregion Commond

        #region Queries

        /// <summary>
        /// 查询字典类型列表
        /// </summary>
        /// <returns></returns>
        [Route("types")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("查询字典类型列表")]
        public async Task<ActionResult<NameValue[]>> GetCategoryTypesAsync()
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.QueryCategpryItemPermission))
                return this.Unauthorized();

            return await _dataCategoryQueries.GetCategoryTypesAsync();
        }

        /// <summary>
        /// 查询符合条件的数据字典项集合
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AuditLogActionFilter("查询符合条件的数据字典项集合")]
        public async Task<ActionResult<PagedResponse<DataDictionaryItemModel>>> GetCategoryItemsAsync(CategpryItemSearchModel request)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.QueryCategpryItemPermission))
                return this.Unauthorized();

            return await _dataCategoryQueries.GetItemsByCategoryTypeAsync(request);
        }

        /// <summary>
        /// 查询数据字典项详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("items/{id:long:min(1)}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AuditLogActionFilter("查询数据字典项详情")]
        public async Task<ActionResult<DataDictionaryItemModel?>> GetAsync(long id)
        {
            //权限认证
            if (!await _authorizationService.AuthorizeAsync(User, Permissions.QueryCategpryItemPermission))
                return Unauthorized();

            var item = await _dataCategoryQueries.GetItemByIdAsync(id);
            if (item == null)
                return NotFound();

            return item;
        }

        #endregion Queries
    }
}