﻿using Microsoft.EntityFrameworkCore;
using Sgr.DataCategories.Domain;
using Sgr.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.DataCategories.Infrastructure.Checkers
{
    public class DataCategoryItemChecker : IDataCategoryItemChecker
    {
        private readonly SgrDbContext _context;

        public DataCategoryItemChecker(SgrDbContext context)
        {
            _context = context;
        }

        public async Task<bool> ValueIsUniqueAsync(string value,
            string categoryTypeCode,
            long id = 0,
            CancellationToken cancellationToken = default)
        {
            return !await _context.Set<DataCategoryItem>()
                .TagWith("Check_DataCategoryItem_Unique")
                .Where(f => f.CategoryTypeCode == categoryTypeCode)
                .Where(f => f.DcItemValue == value)
                .Where(f => f.Id != id)
                .AnyAsync(cancellationToken);
        }
    }
}