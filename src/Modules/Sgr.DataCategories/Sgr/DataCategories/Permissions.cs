﻿using Sgr.Security.Permissions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.DataCategories
{
    public class Permissions : IPermissionProvider
    {
        public static readonly FunctionPermission CreateAndUpdateCategpryItemPermission = new("Sgr.CreateAndUpdateCategpryItem", "数据字典", "创建并维护数据字典项");
        public static readonly FunctionPermission DeleteCategpryItemPermission = new("Sgr.DeleteCategpryItem", "数据字典", "生成数据字典项");
        public static readonly FunctionPermission QueryCategpryItemPermission = new("Sgr.QueryCategpryItem", "数据字典", "浏览数据字典项");

        public Task<IEnumerable<FunctionPermission>> GetFunctionPermissionsAsync()
        {
            return Task.FromResult(new FunctionPermission[]
            {
                CreateAndUpdateCategpryItemPermission,
                DeleteCategpryItemPermission,
                QueryCategpryItemPermission
            }.AsEnumerable());
        }
    }
}