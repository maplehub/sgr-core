﻿using Microsoft.EntityFrameworkCore;
using Sgr.Domain.Entities.Auditing;
using Sgr.Domain.Repositories;
using Sgr.Domain.Uow;
using Sgr.EntityFrameworkCore;
using Sgr.Exceptions;
using Sgr.Generator;
using Sgr.UPMS.Domain.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Infrastructure.Repositories
{
    /// <summary>
    ///
    /// </summary>
    public class RoleRepository
        : EfCoreRepositoryOfTEntityAndTPrimaryKey<Role, long>, IRoleRepository
    {
        private readonly SgrDbContext _context;
        private readonly IAuditedOperator _auditedOperator;
        private readonly INumberIdGenerator _numberIdGenerator;

        /// <summary>
        ///
        /// </summary>
        public override IUnitOfWork UnitOfWork => _context;

        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <param name="auditedOperator"></param>
        /// <param name="numberIdGenerator"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public RoleRepository(SgrDbContext context, IAuditedOperator auditedOperator, INumberIdGenerator numberIdGenerator)
        {
            _context = context ?? throw new BusinessException("SgrDbContext Is Null");
            _auditedOperator = auditedOperator ?? throw new BusinessException("IAuditedOperator Is Null");
            _numberIdGenerator = numberIdGenerator ?? throw new BusinessException("INumberIdGenerator Is Null");
        }

        /// <summary>
        /// 设置主键Id
        /// </summary>
        /// <param name="entity"></param>
        protected override void CheckAndSetId(Role entity)
        {
            if (entity.Id == 0)
                entity.Id = _numberIdGenerator.GenerateUniqueId();
        }

        /// <summary>
        /// 设置审计接口
        /// </summary>
        /// <returns></returns>
        protected override IAuditedOperator? GetAuditedOperator()
        {
            return _auditedOperator;
        }

        /// <summary>
        /// 获取数据库上下文
        /// </summary>
        /// <returns></returns>
        protected override DbContext GetDbContext()
        {
            return _context;
        }

        /// <summary>
        /// 根据角色ID获取资源列表
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetResourcesByRoleId(long roleId)
        {
            var role = await _context.Set<Role>()
                                     .Include(r => r.Resources)
                                     .FirstOrDefaultAsync(r => r.Id == roleId);

            if (role == null)
                throw new BusinessException($"Role with ID {roleId} not found");

            return role.Resources.Select(r => r.ResourceCode);
        }

        /// <summary>
        /// 根据角色ID集合获取对应的资源列表
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        /// <exception cref="BusinessException"></exception>
        public async Task<IEnumerable<string>> GetResourcesByRoleIdsAsync(long[] roleIds)
        {
            if (roleIds == null || !roleIds.Any())
                throw new BusinessException("Role IDs cannot be empty");

            // 使用Select + SelectMany进行扁平化查询,避免Include带来的数据重复
            var resources = await _context.Set<Role>()
                .Where(r => roleIds.Contains(r.Id))
                .SelectMany(r => r.Resources.Select(res => res.ResourceCode))
                .Distinct()
                .ToListAsync();

            return resources;
        }
    }
}