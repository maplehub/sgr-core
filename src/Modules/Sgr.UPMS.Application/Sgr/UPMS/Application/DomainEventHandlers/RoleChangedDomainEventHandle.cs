﻿/**************************************************************
 *
 * 唯一标识：ad597c0f-3250-4067-890c-51858d5aa2e8
 * 命名空间：Sgr.UPMS.Application.DomainEventHandlers
 * 创建时间：2023/9/6 7:41:48
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Sgr.Caching.Services;
using Sgr.Domain.Uow;
using System.Threading.Tasks;
using System.Threading;
using Sgr.UPMS.Events;
using Sgr.UPMS.Domain.Users;

namespace Sgr.UPMS.Application.DomainEventHandlers
{
    public class RoleChangedDomainEventHandle : INotificationHandler<RoleChangedDomainEvent>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly ICacheManager _cacheManager;

        public RoleChangedDomainEventHandle(
            IUnitOfWork unitOfWork,
            IUserRepository userRepository,
            ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(RoleChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            async Task clearCacheAsync()
            {
                //清理所有角色下的用户认证缓存
                var users = await _userRepository.GetByRoleIdAsync(notification.RoleId, cancellationToken);
                foreach (var item in users)
                {
                    await _cacheManager.RemoveAsync(string.Format(CacheKeys.AUTH_ORG_USER_KEY, item.OrgId, item.Id));
                }
            }
            ;

            if (_unitOfWork.HasActiveTransaction)
                _unitOfWork.AddTransactionCompletedHandler(clearCacheAsync);
            else
                await clearCacheAsync();
        }
    }
}