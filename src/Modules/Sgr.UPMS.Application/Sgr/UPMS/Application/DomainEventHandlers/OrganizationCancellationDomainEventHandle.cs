﻿using MediatR;
using Sgr.Caching.Services;
using Sgr.Domain.Uow;
using Sgr.UPMS.Domain.Users;
using Sgr.UPMS.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.DomainEventHandlers
{
    public class OrganizationCancellationDomainEventHandle : INotificationHandler<OrganizationCancellationDomainEvent>
    {
        private readonly IUserRepository _userRepository;

        public OrganizationCancellationDomainEventHandle(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Handle(OrganizationCancellationDomainEvent notification, CancellationToken cancellationToken)
        {
            var users = await _userRepository.GetByOrgIdAsync(notification.OrgId, cancellationToken);
            if (users != null && users.Count() > 0)
            {
                foreach (var user in users)
                {
                    user.LoginLockForever();
                    await _userRepository.UpdateAsync(user);
                }
            }

            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}