/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：组织机构查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Application.ViewModels;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.Organizations;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 组织机构查询服务实现
    /// </summary>
    public class OrganizationQueries : IOrganizationQueries
    {
        private readonly IReadDbContext _context;

        public OrganizationQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutOrganizationViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<Organization>()
                .Where(f => f.Id == id)
                .Select(f => new OutOrganizationViewModel
                {
                    Id = f.Id,
                    Name = f.Name,
                    Code = f.Code,
                    OrgTypeCode = f.OrgTypeCode,
                    AreaCode = f.AreaCode,
                    StaffSizeCode = f.StaffSizeCode,
                    Leader = f.Leader,
                    Phone = f.Phone,
                    Email = f.Email,
                    Address = f.Address,
                    LogoUrl = f.LogoUrl,
                    BusinessLicensePath = f.BusinessLicensePath,
                    Remarks = f.Remarks,
                    OrderNumber = f.OrderNumber,
                    Confirmed = f.Confirmed,
                    State = f.State,
                    ParentId = f.ParentId
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<PagedResponse<OutOrganizationPagedModel>> GetPagedListAsync(InOrganizationSearchModel request)
        {
            Check.NotNull(request, nameof(request));

            var query = _context.Query<Organization>();

            if (!string.IsNullOrEmpty(request.Code))
                query = query.Where(f => f.Code == request.Code);

            if (!string.IsNullOrEmpty(request.OrgTypeCode))
                query = query.Where(f => f.OrgTypeCode == request.OrgTypeCode);

            if (request.ParentId.HasValue)
                query = query.Where(f => f.ParentId == request.ParentId.Value);

            var select = query
                .Select(f => new OutOrganizationPagedModel
                {
                    Id = f.Id,
                    ParentId = f.ParentId,
                    Name = f.Name,
                    Code = f.Code,
                    OrgTypeCode = f.OrgTypeCode,
                    AreaCode = f.AreaCode,
                    StaffSizeCode = f.StaffSizeCode,
                    OrderNumber = f.OrderNumber,
                    Confirmed = f.Confirmed,
                    State = f.State
                });

            return await _context.PagedListByPageSizeAsync(select, request.PageIndex, request.PageSize);
        }
    }
}