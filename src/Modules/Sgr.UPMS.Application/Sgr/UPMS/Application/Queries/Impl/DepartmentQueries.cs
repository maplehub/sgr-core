/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：部门查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.Departments;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 部门查询服务实现
    /// </summary>
    public class DepartmentQueries : IDepartmentQueries
    {
        private readonly IReadDbContext _context;

        public DepartmentQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutDepartmentViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<Department>()
                .Where(f => f.Id == id)
                .Select(f => new OutDepartmentViewModel
                {
                    Id = f.Id,
                    Code = f.Code,
                    Name = f.Name,
                    OrderNumber = f.OrderNumber,
                    Leader = f.Leader,
                    Phone = f.Phone,
                    Email = f.Email,
                    Remarks = f.Remarks,
                    State = f.State,
                    ParentId = f.ParentId,
                    OrgId = f.OrgId
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<IEnumerable<OutDepartmentViewModel>> GetListAsync(InDepartmentSearchModel request)
        {
            Check.NotNull(request, nameof(request));

            if (request.OrgId <= 0)
                throw new BusinessException($"组织Id({request.OrgId})需大于零");

            var query = _context.Query<Department>().Where(f => f.OrgId == request.OrgId);

            if (request.ParentId > 0)
                query = query.Where(f => f.ParentId == request.ParentId);

            var select = query
                .OrderBy(f => f.OrderNumber)
                .Select(f => new OutDepartmentViewModel
                {
                    Id = f.Id,
                    Code = f.Code,
                    Name = f.Name,
                    OrderNumber = f.OrderNumber,
                    Leader = f.Leader,
                    Phone = f.Phone,
                    Email = f.Email,
                    Remarks = f.Remarks,
                    State = f.State,
                    ParentId = f.ParentId,
                    OrgId = f.OrgId
                });

            return await _context.ToListAsync(select);
        }
    }
}