/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：职务查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.Duties;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 职务查询服务实现
    /// </summary>
    public class DutyQueries : IDutyQueries
    {
        private readonly IReadDbContext _context;

        public DutyQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutDutyViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<Duty>()
                .Where(f => f.Id == id)
                .Select(f => new OutDutyViewModel
                {
                    Id = f.Id,
                    Code = f.Code,
                    Name = f.Name,
                    OrderNumber = f.OrderNumber,
                    Remarks = f.Remarks,
                    State = f.State,
                    OrgId = f.OrgId
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<IEnumerable<OutDutyViewModel>> GetListByOrgIdAsync(InDutySearchModel request)
        {
            Check.NotNull(request, nameof(request));

            if (request.OrgId <= 0)
                throw new BusinessException($"组织Id({request.OrgId})需大于零");

            var query = _context.Query<Duty>()
                .Where(f => f.OrgId == request.OrgId);

            if (request.State.HasValue)
                query = query.Where(f => f.State == request.State.Value);

            var select = query
                .OrderBy(f => f.OrderNumber)
                .Select(f => new OutDutyViewModel
                {
                    Id = f.Id,
                    Code = f.Code,
                    Name = f.Name,
                    OrderNumber = f.OrderNumber,
                    Remarks = f.Remarks,
                    State = f.State,
                    OrgId = f.OrgId
                });

            return await _context.ToListAsync(select);
        }
    }
}