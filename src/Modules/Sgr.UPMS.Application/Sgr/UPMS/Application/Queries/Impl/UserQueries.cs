/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：用户查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Application.ViewModels;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 用户查询服务实现
    /// </summary>
    public class UserQueries : IUserQueries
    {
        private readonly IReadDbContext _context;

        public UserQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutUserViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<User>()
                .Where(f => f.Id == id)
                .Select(f => new OutUserViewModel
                {
                    Id = f.Id,
                    LoginName = f.LoginName,
                    FirstLoginTime = f.FirstLoginTime,
                    LastLoginTime = f.LastLoginTime,
                    LoginSuccessCount = f.LoginSuccessCount,
                    UserName = f.UserName,
                    UserPhone = f.UserPhone,
                    UserEmail = f.UserEmail,
                    QQ = f.QQ,
                    Wechat = f.Wechat,
                    IsSuperAdmin = f.IsSuperAdmin,
                    State = f.State,
                    OrgId = f.OrgId,
                    DepartmentId = f.DepartmentId,
                    DepartmentName = f.DepartmentName
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<PagedResponse<OutUserViewModel>> GetPagedListAsync(InUserSearchModel request)
        {
            Check.NotNull(request, nameof(request));

            if (request.OrgId <= 0)
                throw new BusinessException($"组织Id({request.OrgId})需大于零");

            var query = _context.Query<User>()
                .Where(f => f.OrgId == request.OrgId);

            if (request.DepartmentId.HasValue)
                query = query.Where(f => f.DepartmentId == request.DepartmentId.Value);

            if (!string.IsNullOrEmpty(request.LoginName))
                query = query.Where(f => f.LoginName == request.LoginName);

            if (request.State.HasValue)
                query = query.Where(f => f.State == request.State.Value);

            var select = query
                .Select(f => new OutUserViewModel
                {
                    Id = f.Id,
                    LoginName = f.LoginName,
                    FirstLoginTime = f.FirstLoginTime,
                    LastLoginTime = f.LastLoginTime,
                    LoginSuccessCount = f.LoginSuccessCount,
                    UserName = f.UserName,
                    UserPhone = f.UserPhone,
                    UserEmail = f.UserEmail,
                    QQ = f.QQ,
                    Wechat = f.Wechat,
                    IsSuperAdmin = f.IsSuperAdmin,
                    State = f.State,
                    OrgId = f.OrgId,
                    DepartmentId = f.DepartmentId,
                    DepartmentName = f.DepartmentName
                });

            return await _context.PagedListByPageSizeAsync(select, request.PageIndex, request.PageSize);
        }
    }
}