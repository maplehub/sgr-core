/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：登录日志查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Application.ViewModels;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.LogLogins;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 登录日志查询服务实现
    /// </summary>
    public class LogLoginQueries : ILogLoginQueries
    {
        private readonly IReadDbContext _context;

        public LogLoginQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutLogLoginViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<LogLogin>()
                .Where(f => f.Id == id)
                .Select(f => new OutLogLoginViewModel
                {
                    Id = f.Id,
                    LoginName = f.LoginName,
                    UserName = f.UserName,
                    LoginTime = f.LoginTime,
                    IpAddress = f.IpAddress,
                    Location = f.Location,
                    LoginWay = f.LoginWay,
                    LoginProvider = f.LoginProvider,
                    ProviderKey = f.ProviderKey,
                    ProviderDisplayName = f.ProviderDisplayName,
                    ClientBrowser = f.ClientBrowser,
                    ClientOs = f.ClientOs,
                    Status = f.Status,
                    Remark = f.Remark,
                    OrgId = f.OrgId
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<PagedResponse<OutLogLoginViewModel>> GetPagedListAsync(InLogLoginSearchModel request)
        {
            Check.NotNull(request, nameof(request));

            if (request.OrgId <= 0)
                throw new BusinessException($"组织Id({request.OrgId})需大于零");

            var query = _context.Query<LogLogin>()
                .Where(f => f.OrgId == request.OrgId);

            if (!string.IsNullOrEmpty(request.LoginName))
                query = query.Where(f => f.LoginName == request.LoginName);

            if (request.StartTime.HasValue)
                query = query.Where(f => f.LoginTime >= request.StartTime.Value);

            if (request.EndTime.HasValue)
                query = query.Where(f => f.LoginTime <= request.EndTime.Value);

            var select = query
                .OrderByDescending(f => f.CreationTime)
                .Select(f => new OutLogLoginViewModel
                {
                    Id = f.Id,
                    LoginName = f.LoginName,
                    UserName = f.UserName,
                    LoginTime = f.LoginTime,
                    IpAddress = f.IpAddress,
                    Location = f.Location,
                    LoginWay = f.LoginWay,
                    LoginProvider = f.LoginProvider,
                    ProviderKey = f.ProviderKey,
                    ProviderDisplayName = f.ProviderDisplayName,
                    ClientBrowser = f.ClientBrowser,
                    ClientOs = f.ClientOs,
                    Status = f.Status,
                    Remark = f.Remark,
                    OrgId = f.OrgId
                });

            return await _context.PagedListByPageSizeAsync(select, request.PageIndex, request.PageSize);
        }
    }
}