/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries.Impl
 * 创建时间：2024/1/1
 * 描述：角色查询服务实现
 *
 **************************************************************/

using Sgr.Application.Queries;
using Sgr.Application.ViewModels;
using Sgr.Exceptions;
using Sgr.UPMS.Application.ViewModels;
using Sgr.UPMS.Domain.Roles;
using System.Linq;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries.Impl
{
    /// <summary>
    /// 角色查询服务实现
    /// </summary>
    public class RoleQueries : IRoleQueries
    {
        private readonly IReadDbContext _context;

        public RoleQueries(IReadDbContext context)
        {
            _context = context;
        }

        public async Task<OutRoleViewModel?> GetByIdAsync(long id)
        {
            if (id <= 0)
                throw new BusinessException($"Id({id})需大于零");

            var query = _context.Query<Role>()
                .Where(f => f.Id == id)
                .Select(f => new OutRoleViewModel
                {
                    Id = f.Id,
                    Code = f.Code,
                    RoleName = f.RoleName,
                    OrderNumber = f.OrderNumber,
                    Remarks = f.Remarks,
                    State = f.State,
                    OrgId = f.OrgId
                });

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<PagedResponse<OutRoleViewModel>> GetPagedListAsync(InRoleSearchModel request)
        {
            Check.NotNull(request, nameof(request));

            var query = _context.Query<Role>();

            if (!string.IsNullOrEmpty(request.RoleName))
                query = query.Where(f => f.RoleName.Contains(request.RoleName));

            var select = query.Select(f => new OutRoleViewModel
            {
                Id = f.Id,
                Code = f.Code,
                RoleName = f.RoleName,
                OrderNumber = f.OrderNumber,
                Remarks = f.Remarks,
                State = f.State,
                OrgId = f.OrgId
            });

            return await _context.PagedListByPageSizeAsync(select, request.PageIndex, request.PageSize);
        }
    }
}