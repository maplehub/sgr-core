/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：职务查询服务接口
 *
 **************************************************************/

using Sgr.UPMS.Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 职务查询服务接口
    /// </summary>
    public interface IDutyQueries
    {
        /// <summary>
        /// 根据Id获取职务信息
        /// </summary>
        /// <param name="id">职务Id</param>
        /// <returns>职务视图模型</returns>
        Task<OutDutyViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 获取职务列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>职务列表</returns>
        Task<IEnumerable<OutDutyViewModel>> GetListByOrgIdAsync(InDutySearchModel request);
    }
}