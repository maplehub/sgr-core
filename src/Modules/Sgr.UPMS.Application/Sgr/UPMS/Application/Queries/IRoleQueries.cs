/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：角色查询服务接口
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 角色查询服务接口
    /// </summary>
    public interface IRoleQueries
    {
        /// <summary>
        /// 根据Id获取角色信息
        /// </summary>
        /// <param name="id">角色Id</param>
        /// <returns>角色视图模型</returns>
        Task<OutRoleViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 获取角色分页列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>分页结果</returns>
        Task<PagedResponse<OutRoleViewModel>> GetPagedListAsync(InRoleSearchModel request);
    }
}