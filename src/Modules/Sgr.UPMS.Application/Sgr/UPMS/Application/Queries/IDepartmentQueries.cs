/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：部门查询服务接口
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 部门查询服务接口
    /// </summary>
    public interface IDepartmentQueries
    {
        /// <summary>
        /// 根据Id获取部门信息
        /// </summary>
        /// <param name="id">部门Id</param>
        /// <returns>部门视图模型</returns>
        Task<OutDepartmentViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 根据组织Id获取部门列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>部门列表</returns>
        Task<IEnumerable<OutDepartmentViewModel>> GetListAsync(InDepartmentSearchModel request);
    }
}