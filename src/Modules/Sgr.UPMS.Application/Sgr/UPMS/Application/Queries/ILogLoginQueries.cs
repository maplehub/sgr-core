/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：登录日志查询服务接口
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 登录日志查询服务接口
    /// </summary>
    public interface ILogLoginQueries
    {
        /// <summary>
        /// 根据Id获取登录日志
        /// </summary>
        /// <param name="id">日志Id</param>
        /// <returns>登录日志视图模型</returns>
        Task<OutLogLoginViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 获取登录日志分页列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>分页结果</returns>
        Task<PagedResponse<OutLogLoginViewModel>> GetPagedListAsync(InLogLoginSearchModel request);
    }
}