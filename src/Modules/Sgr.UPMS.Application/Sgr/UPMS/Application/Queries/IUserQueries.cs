/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：用户查询服务接口
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 用户查询服务接口
    /// </summary>
    public interface IUserQueries
    {
        /// <summary>
        /// 根据Id获取用户信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns>用户视图模型</returns>
        Task<OutUserViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>用户列表</returns>
        Task<PagedResponse<OutUserViewModel>> GetPagedListAsync(InUserSearchModel request);
    }
}