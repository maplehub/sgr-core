/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.Queries
 * 创建时间：2024/1/1
 * 描述：组织机构查询服务接口
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Queries
{
    /// <summary>
    /// 组织机构查询服务接口
    /// </summary>
    public interface IOrganizationQueries
    {
        /// <summary>
        /// 根据Id获取组织机构信息
        /// </summary>
        /// <param name="id">组织Id</param>
        /// <returns>组织机构视图模型</returns>
        Task<OutOrganizationViewModel?> GetByIdAsync(long id);

        /// <summary>
        /// 获取组织机构分页列表
        /// </summary>
        /// <param name="request">查询条件</param>
        /// <returns>分页结果</returns>
        Task<PagedResponse<OutOrganizationPagedModel>> GetPagedListAsync(InOrganizationSearchModel request);
    }
}