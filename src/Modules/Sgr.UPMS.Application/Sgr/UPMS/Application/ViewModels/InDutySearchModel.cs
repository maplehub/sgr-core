/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：职务查询模型
 *
 **************************************************************/

using Sgr.Domain.Entities;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 职务查询模型
    /// </summary>
    public class InDutySearchModel
    {
        /// <summary>
        /// 组织Id
        /// </summary>
        public long OrgId { get; set; }

        /// <summary>
        /// 职务状态（可选）
        /// </summary>
        public EntityStates? State { get; set; }
    }
}