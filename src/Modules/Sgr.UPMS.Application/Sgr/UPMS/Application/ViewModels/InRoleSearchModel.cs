/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：角色查询模型
 *
 **************************************************************/

using Sgr.Application.ViewModels;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 角色查询模型
    /// </summary>
    public class InRoleSearchModel : PagedRequest
    {
        /// <summary>
        /// 角色名称（可选）
        /// </summary>
        public string? RoleName { get; set; }
    }
}