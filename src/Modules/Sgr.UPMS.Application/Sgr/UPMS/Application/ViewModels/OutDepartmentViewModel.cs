/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：部门视图模型
 *
 **************************************************************/

using Sgr.Domain.Entities;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 部门视图模型
    /// </summary>
    public class OutDepartmentViewModel
    {
        /// <summary>
        /// 部门Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 部门编码
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 排序号
        /// </summary>
        public int OrderNumber { get; set; }

        /// <summary>
        /// 负责人/联系人
        /// </summary>
        public string? Leader { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string? Phone { get; set; }

        /// <summary>
        /// 联系邮箱
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string? Remarks { get; set; }

        /// <summary>
        /// 部门状态
        /// </summary>
        public EntityStates State { get; set; }

        /// <summary>
        /// 上级部门Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 所属组织Id
        /// </summary>
        public long OrgId { get; set; }
    }
}