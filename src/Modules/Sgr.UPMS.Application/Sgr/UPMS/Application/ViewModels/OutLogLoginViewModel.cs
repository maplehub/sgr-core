/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：登录日志视图模型
 *
 **************************************************************/

using System;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 登录日志视图模型
    /// </summary>
    public class OutLogLoginViewModel
    {
        /// <summary>
        /// 日志Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 登录账号
        /// </summary>
        public string LoginName { get; set; } = string.Empty;

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string? UserName { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTimeOffset LoginTime { get; set; }

        /// <summary>
        /// 登录Ip地址
        /// </summary>
        public string? IpAddress { get; set; }

        /// <summary>
        /// 登录地点
        /// </summary>
        public string? Location { get; set; }

        /// <summary>
        /// 登录途径
        /// </summary>
        public string? LoginWay { get; set; }

        /// <summary>
        /// LoginProvider
        /// </summary>
        public string? LoginProvider { get; set; }

        /// <summary>
        /// ProviderKey
        /// </summary>
        public string? ProviderKey { get; set; }

        /// <summary>
        /// ProviderDisplayName
        /// </summary>
        public string? ProviderDisplayName { get; set; }

        /// <summary>
        /// 客户端浏览器
        /// </summary>
        public string? ClientBrowser { get; set; }

        /// <summary>
        /// 客户端系统
        /// </summary>
        public string? ClientOs { get; set; }

        /// <summary>
        /// 登录状态
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// 登录描述
        /// </summary>
        public string? Remark { get; set; }

        /// <summary>
        /// 所属组织Id
        /// </summary>
        public long OrgId { get; set; }
    }
}