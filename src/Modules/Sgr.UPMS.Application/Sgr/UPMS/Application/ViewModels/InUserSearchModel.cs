/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：用户查询模型
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using Sgr.Domain.Entities;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 用户查询模型
    /// </summary>
    public class InUserSearchModel : PagedRequest
    {
        /// <summary>
        /// 组织Id
        /// </summary>
        public long OrgId { get; set; }

        /// <summary>
        /// 部门Id（可选）
        /// </summary>
        public long? DepartmentId { get; set; }

        /// <summary>
        /// 登录名称（可选）
        /// </summary>
        public string? LoginName { get; set; }

        /// <summary>
        /// 账号状态（可选）
        /// </summary>
        public EntityStates? State { get; set; }
    }
}