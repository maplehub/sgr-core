/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：组织机构查询模型
 *
 **************************************************************/

using Sgr.Application.ViewModels;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 组织机构查询模型
    /// </summary>
    public class InOrganizationSearchModel : PagedRequest
    {
        /// <summary>
        /// 组织机构编码（可选）
        /// </summary>
        public string? Code { get; set; }

        /// <summary>
        /// 组织机构类型编码（可选）
        /// </summary>
        public string? OrgTypeCode { get; set; }

        /// <summary>
        /// 上级组织Id（可选）
        /// </summary>
        public long? ParentId { get; set; }
    }
}