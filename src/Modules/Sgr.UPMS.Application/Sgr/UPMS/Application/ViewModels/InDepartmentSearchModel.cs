/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：部门查询模型
 *
 **************************************************************/

using Sgr.Domain.Entities;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 部门查询模型
    /// </summary>
    public class InDepartmentSearchModel
    {
        /// <summary>
        /// 上级部门Id
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// 所属组织Id
        /// </summary>
        public long OrgId { get; set; }
    }
}