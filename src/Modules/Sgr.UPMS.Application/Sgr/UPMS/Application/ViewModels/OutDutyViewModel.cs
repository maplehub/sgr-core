/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：职务视图模型
 *
 **************************************************************/

using Sgr.Domain.Entities;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 职务视图模型
    /// </summary>
    public class OutDutyViewModel
    {
        /// <summary>
        /// 职务Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 职务编码
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 职务名称
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 排序号
        /// </summary>
        public int OrderNumber { get; set; }

        /// <summary>
        /// 职务备注
        /// </summary>
        public string? Remarks { get; set; }

        /// <summary>
        /// 职务状态
        /// </summary>
        public EntityStates State { get; set; }

        /// <summary>
        /// 所属组织Id
        /// </summary>
        public long OrgId { get; set; }
    }
}