/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：组织机构视图模型
 *
 **************************************************************/

using Sgr.Domain.Entities;
using Sgr.UPMS.Domain.Organizations;
using System;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 组织机构视图模型
    /// </summary>
    public class OutOrganizationViewModel
    {
        /// <summary>
        /// 组织Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 组织机构名称
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 组织机构编码
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// 组织机构类型编码
        /// </summary>
        public string OrgTypeCode { get; set; } = string.Empty;

        /// <summary>
        /// 所属行政区划编码
        /// </summary>
        public string AreaCode { get; set; } = string.Empty;

        /// <summary>
        /// 人员规模编码
        /// </summary>
        public string StaffSizeCode { get; set; } = string.Empty;

        /// <summary>
        /// 机构负责人
        /// </summary>
        public string? Leader { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string? Phone { get; set; }

        /// <summary>
        /// 联系邮箱
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// 所在地址
        /// </summary>
        public string? Address { get; set; }

        /// <summary>
        /// Logo地址
        /// </summary>
        public string? LogoUrl { get; set; }

        /// <summary>
        /// 营业执照路径
        /// </summary>
        public string? BusinessLicensePath { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string? Remarks { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int OrderNumber { get; set; }

        /// <summary>
        /// 认证状态
        /// </summary>
        public ConfirmedStates Confirmed { get; set; }

        /// <summary>
        /// 组织状态
        /// </summary>
        public EntityStates State { get; set; }

        /// <summary>
        /// 上级组织Id
        /// </summary>
        public long ParentId { get; set; }
    }
}