/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：登录日志查询模型
 *
 **************************************************************/

using Sgr.Application.ViewModels;
using System;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 登录日志查询模型
    /// </summary>
    public class InLogLoginSearchModel : PagedRequest
    {
        /// <summary>
        /// 组织Id
        /// </summary>
        public long OrgId { get; set; }

        /// <summary>
        /// 登录账号（可选）
        /// </summary>
        public string? LoginName { get; set; }

        /// <summary>
        /// 开始时间（可选）
        /// </summary>
        public DateTimeOffset? StartTime { get; set; }

        /// <summary>
        /// 结束时间（可选）
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }
    }
}