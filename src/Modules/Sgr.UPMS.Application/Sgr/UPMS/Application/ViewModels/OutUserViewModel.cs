/**************************************************************
 *
 * 命名空间：Sgr.UPMS.Application.ViewModels
 * 创建时间：2024/1/1
 * 描述：用户视图模型
 *
 **************************************************************/

using Sgr.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Sgr.UPMS.Application.ViewModels
{
    /// <summary>
    /// 用户视图模型
    /// </summary>
    public class OutUserViewModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 登录名称
        /// </summary>
        public string LoginName { get; set; } = string.Empty;

        /// <summary>
        /// 首次登录时间
        /// </summary>
        public DateTimeOffset? FirstLoginTime { get; set; }

        /// <summary>
        /// 最近一次登录时间
        /// </summary>
        public DateTimeOffset? LastLoginTime { get; set; }

        /// <summary>
        /// 登录成功次数
        /// </summary>
        public int LoginSuccessCount { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string? UserName { get; set; }

        /// <summary>
        /// 用户绑定的手机号码
        /// </summary>
        public string? UserPhone { get; set; }

        /// <summary>
        /// 用户绑定的邮箱地址
        /// </summary>
        public string? UserEmail { get; set; }

        /// <summary>
        /// 用户QQ号码
        /// </summary>
        public string? QQ { get; set; }

        /// <summary>
        /// 用户微信号码
        /// </summary>
        public string? Wechat { get; set; }

        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public bool IsSuperAdmin { get; set; }

        /// <summary>
        /// 账号状态
        /// </summary>
        public EntityStates State { get; set; }

        /// <summary>
        /// 所属组织Id
        /// </summary>
        public long OrgId { get; set; }

        /// <summary>
        /// 所属部门Id
        /// </summary>
        public long? DepartmentId { get; set; }

        /// <summary>
        /// 所属部门名称
        /// </summary>
        public string DepartmentName { get; set; } = string.Empty;
    }
}