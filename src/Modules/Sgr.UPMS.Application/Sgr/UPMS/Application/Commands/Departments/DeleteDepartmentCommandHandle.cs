﻿/**************************************************************
 *
 * 唯一标识：02ba8fb0-777d-481f-af91-72656c4d6f46
 * 命名空间：Sgr.UPMS.Application.Commands.Departments
 * 创建时间：2023/8/27 20:05:34
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Sgr.Domain.Repositories;
using Sgr.UPMS.Domain.Departments;
using Sgr.UPMS.Events;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.UPMS.Application.Commands.Departments
{
    public class DeleteDepartmentCommandHandle : IRequestHandler<DeleteDepartmentCommand, bool>
    {
        private readonly IDepartmentManage _departmentManage;
        private readonly IDepartmentRepository _departmentRepository;

        public DeleteDepartmentCommandHandle(IDepartmentRepository departmentRepository,
          IDepartmentManage departmentManage)
        {
            _departmentRepository = departmentRepository;
            _departmentManage = departmentManage;
        }

        public async Task<bool> Handle(DeleteDepartmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await _departmentRepository.GetAsync(request.DepartmentId, cancellationToken);
            if (entity == null)
                return false;

            return await _departmentManage.DeleteTreeAsync(entity, request.IsCascade, cancellationToken);
        }
    }
}