﻿/**************************************************************
 *
 * 唯一标识：66e4619c-617c-4f8c-80f5-fa9691a823a9
 * 命名空间：Sgr.UPMS
 * 创建时间：2023/8/28 13:47:46
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

namespace Sgr.UPMS
{
    public static class CacheKeys
    {
        public const string AUTH_ALL_PREFIX = "Sgr.UPMS.AUTH";
        public const string AUTH_ORG_KEY = "Sgr.UPMS.AUTH.{0}";
        public const string AUTH_ORG_USER_KEY = "Sgr.UPMS.AUTH.{0}.{1}";

        public const string ORG_ALL_PREFIX = "Sgr.UPMS.ORG";
        public const string ORG_KEY = "Sgr.UPMS.ORG.{0}";
    }
}