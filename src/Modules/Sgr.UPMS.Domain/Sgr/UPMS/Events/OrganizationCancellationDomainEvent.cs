﻿using MediatR;

namespace Sgr.UPMS.Events
{
    /// <summary>
    ///  组织机构注销或恢复事件
    /// </summary>
    public class OrganizationCancellationDomainEvent : INotification
    {
        public long OrgId { get; }

        /// <summary>
        /// True = 注销，False = 恢复
        /// </summary>
        public bool IsCancellation { get; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="orgId">组织Id</param>
        /// <param name="isCancellation">True = 注销，False = 恢复</param>
        public OrganizationCancellationDomainEvent(long orgId, bool isCancellation)
        {
            this.OrgId = orgId;
        }
    }
}