﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sgr.UPMS.Events
{
    public class DutyDeleteDomainEvent : INotification
    {
        public long DutyId { get; }

        public DutyDeleteDomainEvent(long dutyId)
        {
            this.DutyId = dutyId;
        }
    }
}