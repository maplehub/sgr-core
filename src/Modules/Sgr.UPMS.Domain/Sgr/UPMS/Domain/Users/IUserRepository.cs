﻿using Sgr.Domain.Repositories;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;

namespace Sgr.UPMS.Domain.Users
{
    /// <summary>
    ///
    /// </summary>
    public interface IUserRepository : IBaseRepositoryOfTEntityAndTPrimaryKey<User, long>
    {
        Task<IEnumerable<User>> GetByOrgIdAsync(long orgId,
            CancellationToken cancellationToken = default);

        Task<IEnumerable<User>> GetByDepartmentIdAsync(long departmentId,
            CancellationToken cancellationToken = default);

        Task<IEnumerable<User>> GetByDutyIdAsync(long dutyId,
            CancellationToken cancellationToken = default);

        Task<IEnumerable<User>> GetByRoleIdAsync(long roleId,
            CancellationToken cancellationToken = default);

        Task<User?> GetByLoginNameAsync(string loginName,
            CancellationToken cancellationToken = default);

        Task<User?> GetByLoginNameAsync(string loginName,
            string[] propertiesToInclude,
            CancellationToken cancellationToken = default);
    }
}