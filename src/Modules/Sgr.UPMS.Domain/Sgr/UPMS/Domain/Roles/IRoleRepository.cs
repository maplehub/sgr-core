﻿using Sgr.Domain.Repositories;
using Sgr.Exceptions;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sgr.UPMS.Domain.Roles
{
    /// <summary>
    ///
    /// </summary>
    public interface IRoleRepository : IBaseRepositoryOfTEntityAndTPrimaryKey<Role, long>
    {
        /// <summary>
        /// 根据角色ID集合获取对应的资源列表
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        /// <exception cref="BusinessException"></exception>
        Task<IEnumerable<string>> GetResourcesByRoleIdsAsync(long[] roleIds);
    }
}