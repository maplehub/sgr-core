﻿/**************************************************************
 *
 * 唯一标识：c70e01a0-b152-47ed-be07-35b6ea5a6c17
 * 命名空间：Sgr.UPMS.Domain.Departments
 * 创建时间：2023/8/27 19:42:32
 * 机器名称：DESKTOP-S0D075D
 * 创建者：antho
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Domain.Entities;
using Sgr.Domain.Managers;
using Sgr.Domain.Repositories;
using Sgr.Generator;
using Sgr.UPMS.Domain.Organizations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sgr.UPMS.Domain.Departments
{
    public class DepartmentManage : TreeNodeManageBase<Department, long>, IDepartmentManage
    {
        private readonly INumberIdGenerator _numberIdGenerator;
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentManage(IDepartmentRepository repository,
            INumberIdGenerator numberIdGenerator) : base(repository)
        {
            _numberIdGenerator = numberIdGenerator;
            _departmentRepository = repository;
        }

        public async Task<Department> CreateNewAsync(string name, int orderNumber, string? remarks, string? leader, string? phone, string? email, long orgId, long? parentId)
        {
            Check.StringNotNullOrWhiteSpace(name, nameof(name));

            //获取Id及Id-Path
            long id = _numberIdGenerator.GenerateUniqueId();

            parentId = parentId ?? 0;
            string nodePath = await GetNodePathAsync(parentId.Value, id, 5);

            return new Department(Guid.NewGuid().ToString("N"), name, orderNumber, remarks, orgId)
            {
                Id = id,
                ParentId = parentId.Value,
                NodePath = nodePath
            };
        }

        /// <summary>
        /// 删除部门树
        /// </summary>
        /// <param name="department"></param>
        /// <param name="isCascade"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> DeleteTreeAsync(Department department, bool isCascade = true, CancellationToken cancellationToken = default)
        {
            Check.NotNull(department, nameof(department));

            //先删除子部门
            if (isCascade)
            {
                var sons = await _departmentRepository.GetChildNodesRecursionAsync(department!, cancellationToken);
                foreach (var son in sons)
                {
                    son.MarkForDeletion();
                    await _departmentRepository.DeleteAsync(son!, cancellationToken);
                }
            }

            //再删除当前部门
            department.MarkForDeletion();
            await _departmentRepository.DeleteAsync(department, cancellationToken);

            //执行保存
            return await _departmentRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }

        protected override void ChangeTreeNodePath(Department entity, long parentId, string nodePath)
        {
            entity.ParentId = parentId;
            entity.NodePath = nodePath;
        }
    }
}