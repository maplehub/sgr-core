﻿/**************************************************************
 *
 * 唯一标识：2a125c54-c0e9-4822-a42a-6532cff86387
 * 命名空间：Xunit.Sgr.Cap.HelperClass
 * 创建时间：2024/7/22 17:27:32
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Sgr.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Cap.HelperClass
{
    public class TestDataConnectionStringProvider : IDataConnectionStringProvider
    {
        public IEnumerable<DataConnectionString> LoadDataConnectionStrings()
        {
            return new DataConnectionString[]
            {
                new DataConnectionString(
                    "TEST",
                    DatabaseType.MySql,
                    "Server=localhost;Database=sgr;User Id=root;Password=1qaz@WSX;Pooling=true;Min Pool Size=0;Max Pool Size=100;",
                    new string[]{ })
            };
        }
    }
}