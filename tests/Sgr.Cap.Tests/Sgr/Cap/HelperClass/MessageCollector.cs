﻿/**************************************************************
 *
 * 唯一标识：c7cadc2d-78ae-4c78-8399-41a36ce81460
 * 命名空间：Xunit.Sgr.Cap
 * 创建时间：2024/7/22 16:29:13
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Cap.HelperClass
{
    public class MessageCollector
    {
        private readonly ObservableMessage _observable;

        public MessageCollector(ObservableMessage observable)
        {
            _observable = observable;
        }

        public void Add(object data)
        {
            _observable.GetMessage($"{data}");
        }
    }
}