﻿/**************************************************************
 *
 * 唯一标识：dc7ea3ad-17fe-41ed-9d38-51e402efbb09
 * 命名空间：Xunit.Sgr.Cap.HelperClass
 * 创建时间：2024/7/23 15:09:13
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Cap.HelperClass
{
    public delegate void NotifyMessageChangedEventHandler(object? sender, string message);

    public class ObservableMessage
    {
        public event NotifyMessageChangedEventHandler? NotifyMessageChanged;

        public void GetMessage(string message)
        {
            NotifyMessageChanged?.Invoke(this, message);
        }
    }
}