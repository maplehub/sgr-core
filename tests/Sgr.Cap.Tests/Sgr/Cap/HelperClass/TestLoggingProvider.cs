﻿/**************************************************************
 *
 * 唯一标识：f3c2fedd-4969-414e-b036-6e9f9e761ede
 * 命名空间：Xunit.Sgr.Cap.HelperClass
 * 创建时间：2024/7/22 16:44:19
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Logging;
using Xunit.Abstractions;

namespace Xunit.Sgr.Cap.HelperClass
{
    public class TestLoggingProvider : ILoggerProvider
    {
        private readonly ITestOutputHelper _outputHelper;

        public TestLoggingProvider(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new TestOutputLogger(_outputHelper, categoryName);
        }
    }
}