﻿/**************************************************************
 *
 * 唯一标识：9b967021-3555-4300-a433-0b5feaf2d4f7
 * 命名空间：Xunit.Sgr.Cap
 * 创建时间：2024/7/22 17:17:12
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using DotNetCore.CAP;
using DotNetCore.CAP.Messages;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sgr.EntityFrameworkCore;
using Sgr.Redis;
using Sgr.Utilities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit.Sgr.Cap.HelperClass;

namespace Xunit.Sgr.Cap
{
    public class CapMessageingTests : CapTestBase
    {
        private readonly ITestOutputHelper _output;

        public CapMessageingTests(ITestOutputHelper testOutput)
            : base(testOutput)
        {
            _output = testOutput;
        }

        protected override void AddCap(IServiceCollection services, IConfiguration configuration)
        {
            //var rabbitMQOptions = ServiceProviderServiceExtensions.GetRequiredService<IConfiguration>(services.BuildServiceProvider())
            //    .GetSection(rabbitMQSection).Get<RabbitMQOptions>();

            var rabbitMQOptions = configuration.GetSection("ThirdParty:cap_rabbitmq").Get<RabbitMQOptions>();
            var redisOptions = ServiceProviderServiceExtensions.GetRequiredService<IOptions<RedisOptions>>(services.BuildServiceProvider());

            services.AddCap(x =>
            {
                x.DefaultGroupName = TestGroupName;
                x.UseRabbitMQ(opt => { opt = rabbitMQOptions!; });
                //x.UseRedis(opt => { opt.Configuration = ConfigurationOptions.Parse(redisOptions?.Value?.ConnectionString!); });
                x.UseEntityFramework<SgrDbContext>();
                x.FailedThresholdCallback = (e) =>
                {
                    if (e.MessageType == MessageType.Publish)
                        _output.WriteLine("Cap发送消息失败;" + JsonHelper.SerializeObject(e.Message));
                    else if (e.MessageType == MessageType.Subscribe)
                        _output.WriteLine("Cap接收消息失败;" + JsonHelper.SerializeObject(e.Message));
                };
            });
        }

        /// <summary>
        /// 简单的消息收发
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SendMessageTest()
        {
            await _publisher.PublishAsync("sgr.custom.message", "Test Message");

            var waitMessage = base._observable.WaitOneMessage("Test Message");
            if (waitMessage != await Task.WhenAny(waitMessage, Task.Delay(5000)))
                Assert.Fail("获取并处理消息超时");
        }

        [Fact]
        public async Task SendDelayMessageTest()
        {
            //发送一个消息延迟2秒的消息
            await _publisher.PublishDelayAsync(TimeSpan.FromSeconds(2000), "sgr.custom.message", "Test Delay Message");

            //等待一秒即超时，如果WhenAny任务未超时则失败
            var waitMessage = base._observable.WaitOneMessage("Test Delay Message");
            if (waitMessage == await Task.WhenAny(waitMessage, Task.Delay(5000)))
                Assert.Fail("延迟消息异常");
        }

        [Fact]
        public async Task SendHeaderMessageTest()
        {
            //发送一个消息延迟2秒的消息
            var header = new Dictionary<string, string?>()
            {
                ["my.header.first"] = "first",
                ["my.header.second"] = "second"
            };
            await _publisher.PublishAsync("sgr.header.message", "Test Message", header);

            var waitMessage = base._observable.WaitOneMessage("Test Message-first-second");
            if (waitMessage != await Task.WhenAny(waitMessage, Task.Delay(5000)))
                Assert.Fail("获取并处理消息超时");
        }

        [Fact]
        public async Task SendMessageMarkTest()
        {
            await _publisher.PublishAsync("sgr.mark.message", "Test Message Mark");

            var waitMessage = base._observable.WaitOneMessage("Test Message Mark");
            if (waitMessage != await Task.WhenAny(waitMessage, Task.Delay(5000)))
                Assert.Fail("获取并处理消息超时");
        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<CustomTestSubscriber>();
        }
    }
}