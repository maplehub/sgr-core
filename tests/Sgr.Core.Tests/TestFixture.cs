﻿/**************************************************************
 *
 * 唯一标识：6758e8f5-24b2-494c-8d67-794a2011e3fc
 * 命名空间：Xunit
 * 创建时间：2024/7/12 16:34:45
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sgr.Data;
using Sgr.Generator;
using Sgr.Threading;
using Sgr.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sgr.Generator;

namespace Xunit
{
    public class TestFixture : IDisposable
    {
        private readonly IHost _host;

        public TestFixture()
        {
            _host = new HostBuilder().ConfigureAppConfiguration((hostContext, config) =>
            {
                config.AddJsonFile(@"appsettings.json");
            })
            .ConfigureServices((hostContext, serviceCollection) =>
            {
                serviceCollection.AddLogging();
                serviceCollection.AddSgrCore();
                serviceCollection.AddSgrCaching();
                serviceCollection.AddSgrMysqlDbProvider();
                serviceCollection.AddSgrGenerator();

                serviceCollection.AddTransient<ISegmentProvider, TestSegmentProvider>();
            })
            .Build();

            using (var scope = _host.Services.CreateScope())
            {
                IDatabaseSeed databaseSeed = scope.ServiceProvider.GetRequiredService<IDatabaseSeed>();
                AsyncHelper.RunSync(() => { return databaseSeed.SeedAsync(); });
                //databaseSeed.SeedAsync().ConfigureAwait(false).GetAwaiter().GetResult();
            }
        }

        public IServiceProvider ServiceProvider => _host.Services;

        public void Dispose()
        { _host.Dispose(); }
    }
}