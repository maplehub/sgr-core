﻿using Microsoft.Extensions.DependencyInjection;
using Sgr.Redis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Redis
{
    public class RedisDatabaseContextTests : IClassFixture<RedisTestFixture>
    {
        //private readonly ITestOutputHelper _output;
        private readonly RedisTestFixture _redisFixture;

        public RedisDatabaseContextTests(/*ITestOutputHelper output,*/ RedisTestFixture redisFixture)
        {
            //this._output = output;
            this._redisFixture = redisFixture;
        }

        [Fact]
        public async Task XXXXX()
        {
            IRedisDatabaseContext redisDatabaseContext = this._redisFixture.ServiceProvider.GetRequiredService<IRedisDatabaseContext>();

            var redisDatabase = redisDatabaseContext.Connect();
            string key = "TEST";
            string value = "EMPTY";

            //锁不存在时，释放锁将返回False
            bool x1 = await redisDatabase.LockReleaseAsync(redisDatabaseContext.InstancePrefix.Append(key), value);

            //分布式锁过期后再去释放，返回False
            await redisDatabase.LockTakeAsync(redisDatabaseContext.InstancePrefix.Append(key), value, TimeSpan.FromSeconds(5));
            await Task.Delay(6000);
            bool x2 = await redisDatabase.LockReleaseAsync(redisDatabaseContext.InstancePrefix.Append(key), value);

            //分布式锁未过期后再去释放，返回True
            await redisDatabase.LockTakeAsync(redisDatabaseContext.InstancePrefix.Append(key), value, TimeSpan.FromSeconds(5));
            await Task.Delay(2000);
            bool x3 = await redisDatabase.LockReleaseAsync(redisDatabaseContext.InstancePrefix.Append(key), value);

            //锁释放后，再次执行释放锁操作返回False
            bool x4 = await redisDatabase.LockReleaseAsync(redisDatabaseContext.InstancePrefix.Append(key), value);
        }

        [Fact(DisplayName = "GetDefaultInstancePrefix")]
        public void GetDefaultInstancePrefix()
        {
            IRedisDatabaseContext redisDatabaseContext = this._redisFixture.ServiceProvider.GetRequiredService<IRedisDatabaseContext>();
            Assert.Equal(Environment.MachineName + ".", redisDatabaseContext.InstancePrefix);
            //this._output.WriteLine("just for tests ");
        }

        [Fact]
        public void RedisConnect()
        {
            // Arrange
            IRedisDatabaseContext redisDatabaseContext = this._redisFixture.ServiceProvider.GetRequiredService<IRedisDatabaseContext>();

            // Act
            var database = redisDatabaseContext.Connect();

            // Assert
            Assert.NotNull(database);
            Assert.True(database?.Multiplexer?.IsConnected);

            //this._output.WriteLine("just for tests ");
        }

        [Fact]
        public async Task RedisConnectAsync()
        {
            // Arrange
            IRedisDatabaseContext redisDatabaseContext = this._redisFixture.ServiceProvider.GetRequiredService<IRedisDatabaseContext>();

            // Act
            var database = await redisDatabaseContext.ConnectAsync();

            // Assert
            Assert.NotNull(database);
            Assert.True(database?.Multiplexer?.IsConnected);

            //this._output.WriteLine("just for tests ");
        }
    }
}

// Arrange

// Act

// Assert