﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Redis
{
    public class Book
    {
        public string Name { get; set; } = "";
        public float Price { get; set; }
        public int PageNumber { get; set; }
    }
}