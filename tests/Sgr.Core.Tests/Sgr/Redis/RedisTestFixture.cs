﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Redis
{
    public class RedisTestFixture : IDisposable
    {
        private readonly IHost _host;

        public RedisTestFixture()
        {
            _host = new HostBuilder().ConfigureAppConfiguration((hostContext, config) =>
            {
                config.AddJsonFile(@"appsettings.json");
            })
            .ConfigureServices((hostContext, serviceCollection) =>
            {
                serviceCollection.AddLogging();
                serviceCollection.AddSgrCore();
                serviceCollection.UseRedis();
            })
            .Build();

            //_configuration = new ConfigurationBuilder()
            //   .SetBasePath(Directory.GetCurrentDirectory())
            //   .AddJsonFile(@"appsettings.json", false, false)
            //   .Build();

            //_serviceCollection = new ServiceCollection();
            //_serviceCollection.AddSingleton(_configuration);
            //_serviceCollection.AddSgrCore();
            //_serviceCollection.UseRedis();
            //_serviceProvider = _serviceCollection.BuildServiceProvider();
        }

        public IServiceProvider ServiceProvider => _host.Services;

        public void Dispose()
        { _host.Dispose(); }
    }
}