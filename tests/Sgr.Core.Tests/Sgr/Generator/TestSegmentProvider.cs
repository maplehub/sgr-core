﻿using Sgr.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Generator
{
    public class TestSegmentProvider : ISegmentProvider
    {
        public static string SegmentKey = "TestSegment";

        public Dictionary<string, string> GetSegments()
        {
            var dic = new Dictionary<string, string>();
            dic.Add(SegmentKey, "用于测试的标识");

            return dic;
        }
    }
}