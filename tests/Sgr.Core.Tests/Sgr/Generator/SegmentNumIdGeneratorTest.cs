﻿/**************************************************************
 *
 * 唯一标识：988cee24-fbff-4164-837c-81ad7323fdf7
 * 命名空间：Sgr.Generator.Tests
 * 创建时间：2024/6/21 19:38:28
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Generator;
using System.Collections.Concurrent;
using Xunit.Abstractions;

namespace Xunit.Sgr.Generator
{
    public class SegmentNumIdGeneratorTest : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _redisFixture;

        public SegmentNumIdGeneratorTest(ITestOutputHelper output, TestFixture redisFixture)
        {
            _output = output;
            _redisFixture = redisFixture;
        }

        [Fact]
        public async Task GetUniqueIdTest()
        {
            ConcurrentDictionary<long, byte> data = new ConcurrentDictionary<long, byte>();

            var idGenerator = _redisFixture.ServiceProvider.GetRequiredService<ISegmentNumIdGenerator>();

            int threadCount = 10;
            int step = 100;

            await Parallel.ForAsync(0, threadCount, async (index, token) =>
            {
                for (int i = 0; i < step; i++)
                {
                    Assert.True(data.TryAdd(await idGenerator.GetUniqueIdAsync(TestSegmentProvider.SegmentKey), 0));
                }
            });

            Assert.True(data.Count() == threadCount * step);
        }
    }
}