﻿/**************************************************************
 *
 * 唯一标识：dab07958-62f0-4eb8-b5cc-157a3609c047
 * 命名空间：Xunit.Sgr.Trackers
 * 创建时间：2024/7/20 20:30:38
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Generator;
using Sgr.Trackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Trackers
{
    public class MessageMarkByMemoryTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _fixture;

        public MessageMarkByMemoryTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._fixture = redisFixture;
        }

        [Fact]
        public async Task MessageMarkTest()
        {
            var messageMark = this._fixture.ServiceProvider.GetRequiredService<IMessageMark>();

            Assert.True(messageMark.GetType().FullName?.Contains("MessageMarkByMemory"));

            string msgId = Guid.NewGuid().ToString("N");

            //执行测试前，先确保msgId不存在缓存中
            await messageMark.RemoveAsync(msgId);

            //标记为已执行，有效期1秒，标记成功返回True
            Assert.True(await messageMark.MarkAsProcessedAsync(msgId, 1));

            //有效期内再次标记，将返回False
            Assert.False(await messageMark.MarkAsProcessedAsync(msgId, 1));

            //等待有效期过后，再次执行标记将返回True
            await Task.Delay(1500);
            Assert.True(await messageMark.MarkAsProcessedAsync(msgId, 1));

            //有效期内先删除标记，然后再次标记，将返回True
            await messageMark.RemoveAsync(msgId);
            Assert.True(await messageMark.MarkAsProcessedAsync(msgId, 1));
        }

        [Fact]
        public async Task MessageMarkConcurrencyTest()
        {
            var messageMark = this._fixture.ServiceProvider.GetRequiredService<IMessageMark>();

            int value = 0;
            string msgId = Guid.NewGuid().ToString("N");

            int count = 20;
            Task[] tasks = new Task[count];

            for (int i = 0; i < count; i++)
            {
                tasks[i] = Task.Run(async () =>
                {
                    _output.WriteLine($"{i},Thread Id = {Thread.CurrentThread.ManagedThreadId}");
                    if (await messageMark.MarkAsProcessedAsync(msgId, 2))
                        value++;
                });
            }

            await Task.WhenAll(tasks);

            Assert.True(value == 1);
        }
    }
}