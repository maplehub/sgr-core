﻿/**************************************************************
 *
 * 唯一标识：276ae408-5357-43fc-b273-1882b26f0ba7
 * 命名空间：Xunit.Sgr.Trackers
 * 创建时间：2024/7/20 22:04:51
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Caching.Services;
using Sgr.Trackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Trackers
{
    public class MessageTokenByMemoryTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _fixture;

        public MessageTokenByMemoryTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._fixture = redisFixture;
        }

        [Fact]
        public async Task MessageTokenTest()
        {
            var messageToken = this._fixture.ServiceProvider.GetRequiredService<IMessageToken>();

            Assert.True(messageToken.GetType().FullName?.Contains("MessageTokenByMemory"));

            string msgId = Guid.NewGuid().ToString("N");

            //验证不存在的Token，返回False
            //Assert.False(await messageToken.VerifyTokenAsync(msgId));

            //获取Token，再验证Token，返回True
            string token = await messageToken.IssuingTokenAsync(1);
            Assert.True(await messageToken.VerifyTokenAsync(token));

            //Token多次验证，返回False
            Assert.False(await messageToken.VerifyTokenAsync(token));

            //获取Token，等待有效期过后，再验证Token，返回True
            token = await messageToken.IssuingTokenAsync(1);
            await Task.Delay(1500);
            Assert.False(await messageToken.VerifyTokenAsync(token));
        }

        [Fact]
        public async Task MessageTokenConcurrencyTest()
        {
            var messageToken = this._fixture.ServiceProvider.GetRequiredService<IMessageToken>();

            int value = 0;
            string msgId = await messageToken.IssuingTokenAsync(2);

            int count = 20;
            Task[] tasks = new Task[count];

            for (int i = 0; i < count; i++)
            {
                tasks[i] = Task.Run(async () =>
                {
                    _output.WriteLine($"{i},Thread Id = {Thread.CurrentThread.ManagedThreadId}");
                    if (await messageToken.VerifyTokenAsync(msgId))
                        value++;
                });
            }

            await Task.WhenAll(tasks);

            Assert.True(value == 1);
        }
    }
}