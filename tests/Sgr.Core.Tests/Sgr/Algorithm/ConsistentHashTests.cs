﻿/**************************************************************
 *
 * 唯一标识：0edf7f71-a06c-4ba6-b664-d0ccc6e1100b
 * 命名空间：Tests.Sgr.Algorithm
 * 创建时间：2024/7/12 16:33:25
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Algorithm;
using Sgr.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Algorithm
{
    public class ConsistentHashTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _fixture;

        public ConsistentHashTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._fixture = redisFixture;
        }

        [Fact]
        public void ConsistentHash_AddNodeTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();

            consistentHash!.AddNode("ABC");

            Assert.Equal(1, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(200, consistentHash!.GetAllHashKeys()?.Count());

            consistentHash!.AddNode("EFG", 100);

            Assert.Equal(2, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(300, consistentHash!.GetAllHashKeys()?.Count());
        }

        [Fact]
        public void ConsistentHash_AddNodeConcurrencyTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();

            var numbers = Enumerable.Range(0, 10).ToList();

            Parallel.ForEach(numbers, number =>
            {
                consistentHash!.AddNode("ABC");
            });

            Assert.Equal(1, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(200, consistentHash!.GetAllHashKeys()?.Count());
        }

        [Fact]
        public void ConsistentHash_RemoveNodeTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();

            consistentHash!.AddNode("ABC");

            Assert.Equal(1, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(200, consistentHash!.GetAllHashKeys()?.Count());

            consistentHash!.RemoveNode("XYZ");

            Assert.Equal(1, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(200, consistentHash!.GetAllHashKeys()?.Count());

            consistentHash!.RemoveNode("ABC");
            Assert.Equal(0, consistentHash!.GetAllNodeNames()?.Count());
            Assert.Equal(0, consistentHash!.GetAllHashKeys()?.Count());
        }

        [Fact]
        public void ConsistentHash_RemoveNodeConcurrencyTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();

            consistentHash!.AddNode("ABC");

            Parallel.For(0, 100, (idx) =>
            {
                consistentHash!.AddNode("ABC");
                consistentHash!.AddNode("EFG");
            });

            Parallel.For(0, 100, (idx) =>
            {
                consistentHash!.RemoveNode("ABC");
            });

            Assert.Equal(1, consistentHash!.GetAllNodeNames()?.Count());

            Assert.Equal(200, consistentHash!.GetAllHashKeys()?.Count());
        }

        [Fact]
        public void ConsistentHash_SearchNodeNameTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();
            consistentHash!.AddNode("ABC");

            Assert.Equal("ABC", consistentHash.SearchNodeName("123"));
        }

        [Fact]
        public void ConsistentHash_SearchNodeNameTest2()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();
            consistentHash!.AddNode("ABC");
            consistentHash!.AddNode("EFG");
            consistentHash!.AddNode("GIJ");

            Parallel.For(0, 1000, (idx) =>
            {
                Assert.True("ABC" == consistentHash.SearchNodeName(idx.ToString())
                    || "EFG" == consistentHash.SearchNodeName(idx.ToString())
                    || "GIJ" == consistentHash.SearchNodeName(idx.ToString()));
            });
        }

        [Fact]
        public void ConsistentHash_SearchNodeNameEmptyTest()
        {
            IConsistentHash consistentHash = _fixture.ServiceProvider.GetRequiredService<IConsistentHash>();

            try
            {
                consistentHash.SearchNodeName("123");
            }
            catch (EmptyCircleException)
            {
                return;
            }

            Assert.Fail("expected error");
        }
    }
}