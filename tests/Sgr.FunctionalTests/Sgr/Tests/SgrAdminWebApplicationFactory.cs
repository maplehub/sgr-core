﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sgr.Tests;
using Sgr.Utilities;
using Sgr.Admin.WebHost;
using System.Net.Http.Headers;
using System.Text;
using Sgr.Indentity.Application.ViewModels;
using System.Net;
using Xunit.Sgr.Indentity.Helpers;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http.Json;
using System.Text.Json;
using Sgr.Data;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Xunit.Sgr.Tests
{
    public class SgrAdminWebApplicationFactory : SgrWebApplicationFactory<Program>
    {
        /// <summary>
        /// 创建Admin HttpClient
        /// </summary>
        /// <param name="withToken"></param>
        /// <returns></returns>
        public virtual HttpClient CreateAdminHttpClient(bool withToken = true)
        {
            return CreateNewClient(withToken, true);
        }

        public virtual HttpClient CreateUnauthorizedClient(bool withToken = true)
        {
            return CreateNewClient(withToken, false);
        }

        /// <summary>
        /// 创建一个新的HttpClient
        /// </summary>
        /// <param name="withToken"></param>
        /// <param name="authorized"></param>
        /// <returns></returns>
        protected virtual HttpClient CreateNewClient(bool withToken = true, bool authorized = true)
        {
            var client = CreateClient();

            if (withToken)
            {
                // 获取访问令牌
                string authToken = AsyncHelper.RunSync(() => { return GetAccessTokenAsync(authorized); });

                // 将令牌添加到默认请求头中
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", authToken);
            }

            return client;
        }

        public async Task ExecuteScopeAsync(Func<IServiceProvider, Task> action)
        {
            using var scope = Services.CreateScope();
            await action(scope.ServiceProvider);
        }

        public async Task<T> ExecuteScopeAsync<T>(Func<IServiceProvider, Task<T>> action)
        {
            using var scope = Services.CreateScope();
            return await action(scope.ServiceProvider);
        }

        /// <summary>
        /// 获取访问令牌
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetAccessTokenAsync(bool isAdmin = false)
        {
            var client = CreateClient();
            var command = isAdmin ? AuthenticationTestHelper.CreateAdminLoginCommand() : AuthenticationTestHelper.CreateCustomLoginCommand();
            var response = await client.PostAsJsonAsync($"api/v1/sgr/oauth/token", command);
            var result = await response.Content.ReadFromJsonAsync<TokenViewModel>();
            return result?.AccessToken ?? "";
        }

        protected override void PrepareHost(IHost host)
        {
            var serviceProvider = host.Services;
            using (var scope = serviceProvider.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                //初始化测试所需数据
                AsyncHelper.RunSync(() => { return SeedDataHelper.InitData(scopedServices); });
            }
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder
                .ConfigureServices(services =>
                {
                    // 清理数据库链接配置，改为使用测试数据库
                    services.RemoveAll<IDataConnectionStringProvider>();
                    services.AddSingleton<IDataConnectionStringProvider, TestDataConnectionStringProvider>();

                    //// Remove the app's ApplicationDbContext registration.
                    //var descriptor = services.SingleOrDefault(
                    //d => d.ServiceType ==
                    //    typeof(DbContextOptions<AppDbContext>));

                    //if (descriptor != null)
                    //{
                    //    services.Remove(descriptor);
                    //}

                    //// This should be set for each individual test run
                    //string inMemoryCollectionName = Guid.NewGuid().ToString();

                    //// Add ApplicationDbContext using an in-memory database for testing.
                    //services.AddDbContext<AppDbContext>(options =>
                    //{
                    //    options.UseInMemoryDatabase(inMemoryCollectionName);
                    //});
                });
        }
    }
}