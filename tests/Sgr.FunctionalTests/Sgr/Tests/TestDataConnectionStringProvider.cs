﻿using Sgr;
using Sgr.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xunit.Sgr.Tests
{
    public class TestDataConnectionStringProvider : IDataConnectionStringProvider
    {
        public IEnumerable<DataConnectionString> LoadDataConnectionStrings()
        {
            return new DataConnectionString[]
            {
                new DataConnectionString(
                    Constant.DEFAULT_DATABASE_SOURCE_NAME,
                    DatabaseType.MySql,
                    "Server=localhost;Database=sgr-test;User Id=root;Password=1qaz@WSX;Pooling=true;Min Pool Size=0;Max Pool Size=100;",
                    new string[]{ })
            };
        }
    }
}