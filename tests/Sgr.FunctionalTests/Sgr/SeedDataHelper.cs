﻿using Microsoft.Extensions.DependencyInjection;
using Sgr.Data;
using Sgr.Domain.Uow;
using Sgr.UPMS.Domain.Departments;
using Sgr.UPMS.Domain.Duties;
using Sgr.UPMS.Domain.Organizations;
using Sgr.UPMS.Domain.Roles;
using Sgr.UPMS.Domain.Users;
using Sgr.Utilities;
using Sgr;
using Sgr.Identity.Services;

namespace Xunit.Sgr
{
    public class SeedDataHelper
    {
        public static string ORG_CODE = "ORG_CODE";
        public static string USERNAME_ADMIN = "SGR Admin";
        public static string USERNAME_CUSTOM = "SGR Custom";
        public static string PASSWORD = "qwert@123";

        public static long USERID_CUSTOM = 132120263311;

        public static long DepartmentId01 = 6001;
        public static long DepartmentId02 = 6002;
        public static long DepartmentId03 = 6003;

        public static long DutyId01 = 7001;
        public static long DutyId02 = 7002;
        public static long DutyId03 = 7003;

        public static long RoleId01 = 8001;
        public static long RoleId02 = 8002;
        public static long RoleId03 = 8003;

        public static async Task InitData(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IDatabaseSeed databaseSeed = scope.ServiceProvider.GetRequiredService<IDatabaseSeed>();
                await databaseSeed.SeedAsync();

                //创建组织
                ICurrentUser currentUser = scope.ServiceProvider.GetRequiredService<ICurrentUser>();
                IUserChecker userChecker = scope.ServiceProvider.GetRequiredService<IUserChecker>();

                IUnitOfWork unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                IUserRepository userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                IOrganizationRepository organizationRepository = scope.ServiceProvider.GetRequiredService<IOrganizationRepository>();
                IDutyRepository dutyRepository = scope.ServiceProvider.GetRequiredService<IDutyRepository>();
                IDepartmentRepository departmentRepository = scope.ServiceProvider.GetRequiredService<IDepartmentRepository>();
                IRoleRepository roleRepository = scope.ServiceProvider.GetRequiredService<IRoleRepository>();
                IPasswordHashService passwordHashService = scope.ServiceProvider.GetRequiredService<IPasswordHashService>();

                long.TryParse(currentUser.Id, out long userId);
                long.TryParse(currentUser.OrgId, out long orgId);

                //创建组织
                var org = await organizationRepository.GetAsync(orgId);
                if (org == null)
                {
                    org = new Organization(ORG_CODE, "staffSizeCode", "测试专用");
                    org.Id = orgId;
                    await organizationRepository.InsertAsync(org);
                }

                //创建部门
                await createDept(departmentRepository, orgId, DepartmentId01);
                await createDept(departmentRepository, orgId, DepartmentId02);
                await createDept(departmentRepository, orgId, DepartmentId03);

                //创建职务
                await createDuty(dutyRepository, orgId, DutyId01);
                await createDuty(dutyRepository, orgId, DutyId02);
                await createDuty(dutyRepository, orgId, DutyId03);

                //创建角色
                await createRole(roleRepository, orgId, RoleId01);
                await createRole(roleRepository, orgId, RoleId02);
                await createRole(roleRepository, orgId, RoleId03);

                //创建具有超级管理员权限的用户
                var user = await userRepository.GetAsync(userId); ;
                if (user == null)
                {
                    user = await User.CreateNewAdminAsync(USERNAME_ADMIN, passwordHashService.HashPassword(PASSWORD), orgId, userChecker);
                    user.Id = userId;
                    await userRepository.InsertAsync(user);
                }

                //创建普通用户
                var user_custom = await userRepository.GetAsync(USERID_CUSTOM); ;
                if (user_custom == null)
                {
                    user_custom = await User.CreateNewAsync(USERNAME_CUSTOM, passwordHashService.HashPassword(PASSWORD), orgId, userChecker);
                    user_custom.Id = USERID_CUSTOM;
                    await userRepository.InsertAsync(user_custom);
                }

                await unitOfWork.SaveEntitiesAsync();
            }
        }

        private static async Task createDept(IDepartmentRepository repository, long orgId, long deptId)
        {
            var entity = await repository.GetAsync(deptId);
            if (entity == null)
            {
                entity = new Department($"{deptId}_Core", $"{deptId}_NAME", 0, $"{deptId}_REMARK", orgId);
                entity.Id = deptId;
                await repository.InsertAsync(entity);
            }
        }

        private static async Task createDuty(IDutyRepository repository, long orgId, long dutyId)
        {
            var entity = await repository.GetAsync(dutyId);
            if (entity == null)
            {
                entity = new Duty($"{dutyId}_CODE", $"{dutyId}_NAME", 0, $"{dutyId}_REMARK", orgId);
                entity.Id = dutyId;
                await repository.InsertAsync(entity);
            }
        }

        private static async Task createRole(IRoleRepository repository, long orgId, long roleId)
        {
            var entity = await repository.GetAsync(roleId);
            if (entity == null)
            {
                entity = new Role($"{roleId}_CODE", $"{roleId}_NAME", orgId, 0, $"{roleId}_REMARK");
                entity.Id = roleId;
                await repository.InsertAsync(entity);
            }
        }
    }
}