﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Sgr.Application.ViewModels;
using Sgr.AuditLogs.ViewModels;
using Sgr.Utilities;
using Xunit;
using Xunit.Sgr.Tests;
using Xunit.Sgr.UPMS;

namespace Xunit.Sgr.AuditLogs
{
    [Collection("Sgr.Admin")]
    public class LogOperateControllerTests
    {
        private readonly HttpClient _admin_client;
        private readonly HttpClient _unauthorizedClient;

        private readonly JsonSerializerOptions _jsonSerializerOptions;
        private const string BaseUrl = "api/v1/sgr/logoperate";

        public LogOperateControllerTests(SgrAdminWebApplicationFactory factory)
        {
            _admin_client = factory.CreateAdminHttpClient();
            _unauthorizedClient = factory.CreateUnauthorizedClient();

            _jsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(_jsonSerializerOptions);
        }

        /// <summary>
        /// 测试未授权访问 - 应返回401状态码
        /// </summary>
        [Theory]
        [InlineData("POST", "list")]                 // 获取日志列表
        [InlineData("GET", "details/1")]            // 获取日志详情
        public async Task AccessWithoutAuthorization_AllEndpoints_ShouldReturnUnauthorized(string method, string endpoint)
        {
            // Arrange
            var request = new HttpRequestMessage(new HttpMethod(method), $"{BaseUrl}/{endpoint}");

            // 对于 POST 请求，添加一个空的搜索模型作为内容体
            if (method == "POST")
            {
                var searchModel = new LogOperateSearchModel
                {
                    PageIndex = 1,
                    PageSize = 10
                };
                request.Content = JsonContent.Create(searchModel, options: _jsonSerializerOptions);
            }

            // Act
            var response = await _unauthorizedClient.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task GetList_ShouldReturnPagedResponse()
        {
            // Arrange
            var request = new LogOperateSearchModel
            {
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _admin_client.PostAsJsonAsync($"{BaseUrl}/list", request, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<LogOperateListModel>>(_jsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotNull(result.Items);
        }

        [Fact]
        public async Task GetList_WithFilters_ShouldReturnFilteredResults()
        {
            // Arrange
            var request = new LogOperateSearchModel
            {
                PageIndex = 1,
                PageSize = 10,
                RequestStart = DateTime.UtcNow.AddDays(-1),
                RequestEnd = DateTime.UtcNow
            };

            // Act
            var response = await _admin_client.PostAsJsonAsync($"{BaseUrl}/list", request, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<LogOperateListModel>>(_jsonSerializerOptions);
            Assert.NotNull(result);
            foreach (var item in result.Items)
            {
                Assert.True(item.RequestTime >= request.RequestStart);
                Assert.True(item.RequestTime <= request.RequestEnd);
            }
        }

        [Fact]
        public async Task GetDetails_WithValidId_ShouldReturnLogDetails()
        {
            // Arrange
            // First get a log entry from the list
            var listRequest = new LogOperateSearchModel { PageIndex = 1, PageSize = 1 };
            var listResponse = await _admin_client.PostAsJsonAsync($"{BaseUrl}/list", listRequest);
            var listResult = await listResponse.Content.ReadFromJsonAsync<PagedResponse<LogOperateListModel>>(_jsonSerializerOptions);

            if (listResult != null && listResult.Items.Count > 0)
            {
                var logId = listResult.Items.First().Id;

                // Act
                var response = await _admin_client.GetAsync($"{BaseUrl}/details/{logId}");

                // Assert
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                var result = await response.Content.ReadFromJsonAsync<LogOperateModel>(_jsonSerializerOptions);
                Assert.NotNull(result);
                Assert.Equal(logId, result.Id);
            }
        }

        [Fact]
        public async Task GetDetails_WithInvalidId_ShouldReturnNotFound()
        {
            // Arrange
            long invalidId = 99999999999999;

            // Act
            var response = await _admin_client.GetAsync($"{BaseUrl}/details/{invalidId}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}