﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sgr.Tests;
using System.Net;
using System.Net.Http.Json;
using Sgr.Application.ViewModels;
using Sgr.DataCategories.Application.Commands;
using Sgr.DataCategories.Application.ViewModels;
using Sgr;
using Sgr.Tests.Attributes;
using Sgr.Domain.Entities;
using System.Text.Json;
using Sgr.Utilities;
using Xunit.Abstractions;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Application.Queries;
using Sgr.DataCategories.Domain;
using Xunit.Sgr.UPMS;

namespace Xunit.Sgr.DataCategories
{
    [Collection("Sgr.Admin")]
    public class DataCategoryControllerTests : IClassFixture<DataCategoryFixture>
    {
        private readonly DataCategoryFixture _fixture;

        public DataCategoryControllerTests(
            DataCategoryFixture fixture)
        {
            _fixture = fixture;
        }

        #region 认证与授权测试

        /// <summary>
        /// 测试未授权访问 - 应返回401状态码
        /// </summary>
        [Theory]
        [InlineData("GET", "types")]                  // 获取分类类型
        [InlineData("POST", "list")]                  // 获取列表
        [InlineData("GET", "items/1")]               // 获取单个项
        [InlineData("POST", "")]                     // 创建项
        [InlineData("PUT", "")]                      // 更新项
        [InlineData("DELETE", "1")]                  // 删除项
        public async Task AccessWithoutAuthorization_AllEndpoints_ShouldReturnUnauthorized(string method, string endpoint)
        {
            // Arrange
            var request = new HttpRequestMessage(new HttpMethod(method), $"{DataCategoryFixture.BaseUrl}/{endpoint}");

            // 为 POST 和 PUT 请求添加请求体
            if (method == "POST" || method == "PUT")
            {
                var content = method == "POST" ?
                    (method == "POST" && endpoint == "list" ?
                        new CategpryItemSearchModel { CategoryTypeCode = DataCategoryFixture.CategoryTypeCode } :
                        new CreateCategpryItemCommand
                        {
                            CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                            DcItemName = "Test",
                            DcItemValue = "Test"
                        }) :
                    (object)new UpdateCategpryItemCommand { Id = 1 };

                request.Content = JsonContent.Create(content, options: _fixture.JsonSerializerOptions);
            }

            // Act
            var response = await _fixture.UnauthorizedClient.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #endregion 认证与授权测试

        #region 查询测试

        [Fact]
        public async Task GetCategoryTypes_ShouldReturnAllTypes()
        {
            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DataCategoryFixture.BaseUrl}/types");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<NameValue[]>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Contains(result, t => t.Value == DataCategoryFixture.CategoryTypeCode);
        }

        [Fact]
        public async Task GetItems_WithValidCategoryType_ShouldReturnItems()
        {
            // Arrange
            var request = new CategpryItemSearchModel
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{DataCategoryFixture.BaseUrl}/list", request, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<DataDictionaryItemModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.Contains(result.Items, item => item.CategoryTypeCode == DataCategoryFixture.CategoryTypeCode);
        }

        [Fact]
        public async Task GetItemById_WithValidId_ShouldReturnItem()
        {
            // Arrange
            var items = await _fixture.GetTestItemsAsync();
            var testItem = items.First();

            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DataCategoryFixture.BaseUrl}/items/{testItem.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<DataDictionaryItemModel>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(testItem.Id, result.Id);
            Assert.Equal(testItem.DcItemName, result.DcItemName);
        }

        [Fact]
        public async Task GetItemById_WithInvalidId_ShouldReturnNotFound()
        {
            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DataCategoryFixture.BaseUrl}/items/999999");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        #endregion 查询测试

        #region CRUD测试

        [Fact]
        public async Task CreateItem_WithValidData_ShouldSucceed()
        {
            // Arrange
            var command = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                DcItemName = $"Test_{Guid.NewGuid():N}",
                DcItemValue = $"TestValue_{Guid.NewGuid():N}",
                OrderNumber = 100,
                Remarks = "Test"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DataCategoryFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var items = await _fixture.GetTestItemsAsync();
            Assert.Contains(items, i => i.DcItemValue == command.DcItemValue);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("TestValue")]//已存在的KEY无法写入
        public async Task CreateItem_WithInvalidName_ShouldReturnBadRequest(string invalidValue)
        {
            // Arrange
            var command = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                DcItemName = "DcItemName",
                DcItemValue = invalidValue
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DataCategoryFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.True(response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.InternalServerError);
        }

        [Fact]
        public async Task UpdateItem_WithValidData_ShouldSucceed()
        {
            // Arrange
            var items = await _fixture.GetTestItemsAsync();
            var testItem = items.First(f => f.DcItemValue == "TestValue");
            var newName = $"Updated_{Guid.NewGuid():N}";

            var command = new UpdateCategpryItemCommand
            {
                Id = testItem.Id,
                DcItemName = newName,
                OrderNumber = 200,
                State = EntityStates.Deactivate,
                Remarks = "XXXXXX"
            };

            // Act
            var response = await _fixture.AdminClient.PutAsJsonAsync(DataCategoryFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedItem = await _fixture.GetItemByIdAsync(testItem.Id);
            Assert.NotNull(updatedItem);
            Assert.Equal(command.DcItemName, updatedItem.DcItemName);
            Assert.Equal(command.OrderNumber, updatedItem.OrderNumber);
            Assert.Equal(command.State, updatedItem.State);
            Assert.Equal(command.Remarks, updatedItem.Remarks);
        }

        [Fact]
        public async Task DeleteItem_WithValidId_ShouldSucceed()
        {
            // Arrange New
            var command = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                OrderNumber = 1,
                DcItemName = $"TestItem_{Guid.NewGuid():N}",
                DcItemValue = "TestValue_DeleteItem",
                Remarks = "Test"
            };
            await _fixture.CreateTestItemAsync(command);

            // Arrange Get
            var items = await _fixture.GetTestItemsAsync();
            var testItem = items.First(f => f.DcItemValue == "TestValue_DeleteItem");

            // Act
            var response = await _fixture.AdminClient.DeleteAsync($"{DataCategoryFixture.BaseUrl}/{testItem.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var deletedItem = await _fixture.GetItemByIdAsync(testItem.Id);
            Assert.Null(deletedItem);
        }

        #endregion CRUD测试

        #region 并发测试

        [Fact]
        public async Task ConcurrentCreateItems_WithSameValue_ShouldHandleDuplicates()
        {
            // Arrange
            var value = $"TestValue_{Guid.NewGuid():N}";
            var command1 = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                DcItemName = $"Test1_{Guid.NewGuid():N}",
                DcItemValue = value,
                OrderNumber = 100,
                Remarks = "Test"
            };

            var command2 = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                DcItemName = $"Test2_{Guid.NewGuid():N}",
                DcItemValue = value,
                OrderNumber = 101,
                Remarks = "Test"
            };

            // Act - 同时创建两个具有相同 DcItemValue 的项
            var task1 = _fixture.AdminClient.PostAsJsonAsync(DataCategoryFixture.BaseUrl, command1, _fixture.JsonSerializerOptions);
            var task2 = _fixture.AdminClient.PostAsJsonAsync(DataCategoryFixture.BaseUrl, command2, _fixture.JsonSerializerOptions);

            var results = await Task.WhenAll(task1, task2);

            // Assert - 确保只有一个创建成功
            var successCount = results.Count(r => r.StatusCode == HttpStatusCode.OK);
            Assert.Equal(1, successCount);

            var conflictCount = results.Count(r => r.StatusCode == HttpStatusCode.BadRequest
                || r.StatusCode == HttpStatusCode.Conflict
                || r.StatusCode == HttpStatusCode.InternalServerError);
            Assert.Equal(1, conflictCount);

            // Verify - 检查最终只创建了一个项
            var items = await _fixture.GetTestItemsAsync();
            var createdItems = items.Where(i => i.DcItemValue == value).ToList();
            Assert.Single(createdItems);
        }

        [Fact]
        public async Task ConcurrentDeleteAndUpdate_ShouldHandleRace()
        {
            // Arrange
            var command = new CreateCategpryItemCommand
            {
                CategoryTypeCode = DataCategoryFixture.CategoryTypeCode,
                DcItemName = $"Test_{Guid.NewGuid():N}",
                DcItemValue = $"TestValue_{Guid.NewGuid():N}",
                OrderNumber = 100,
                Remarks = "Test"
            };
            await _fixture.CreateTestItemAsync(command);

            var items = await _fixture.GetTestItemsAsync();
            var testItem = items.First(f => f.DcItemValue == command.DcItemValue);

            var updateCommand = new UpdateCategpryItemCommand
            {
                Id = testItem.Id,
                DcItemName = $"Updated_{Guid.NewGuid():N}",
                OrderNumber = 201,
                State = EntityStates.Normal,
                Remarks = "Update during delete"
            };

            // Act - 同时执行删除和更新操作
            var deleteTask = _fixture.AdminClient.DeleteAsync($"{DataCategoryFixture.BaseUrl}/{testItem.Id}");
            var updateTask = _fixture.AdminClient.PutAsJsonAsync(DataCategoryFixture.BaseUrl, updateCommand, _fixture.JsonSerializerOptions);

            await Task.WhenAll(deleteTask, updateTask);

            // Assert - 检查最终状态
            var finalItem = await _fixture.GetItemByIdAsync(testItem.Id);

            if (deleteTask.Result.StatusCode == HttpStatusCode.OK)
            {
                // 如果删除成功，项应该不存在
                Assert.Null(finalItem);
                // 更新应该失败
                Assert.NotEqual(HttpStatusCode.OK, updateTask.Result.StatusCode);
            }
            else if (updateTask.Result.StatusCode == HttpStatusCode.OK)
            {
                // 如果更新成功，项应该存在且已更新
                Assert.NotNull(finalItem);
                Assert.Equal(updateCommand.DcItemName, finalItem.DcItemName);
                // 删除应该失败
                Assert.NotEqual(HttpStatusCode.OK, deleteTask.Result.StatusCode);
            }
        }

        #endregion 并发测试
    }

    public class DataCategoryFixture : IAsyncLifetime
    {
        /// <summary>
        /// 未授权访问客户端
        /// </summary>
        public HttpClient UnauthorizedClient { get; private set; }

        /// <summary>
        /// 管理员[已授权]访问客户端
        /// </summary>
        public HttpClient AdminClient { get; private set; }

        public JsonSerializerOptions JsonSerializerOptions { get; }

        public const string BaseUrl = "api/v1/sgr/datacategory";
        public const string CategoryTypeCode = "F_SOFT_CLASSIFICATION";

        private readonly SgrAdminWebApplicationFactory _factory;

        public DataCategoryFixture(SgrAdminWebApplicationFactory factory)
        {
            _factory = factory;

            AdminClient = factory.CreateAdminHttpClient();
            UnauthorizedClient = factory.CreateUnauthorizedClient();
            JsonSerializerOptions = new JsonSerializerOptions();

            JsonHelper.UpdateJsonSerializerOptions(JsonSerializerOptions);
        }

        public async Task InitializeAsync()
        {
            try
            {
                await _factory.ExecuteScopeAsync(async serviceProvider =>
                {
                    var sender = serviceProvider.GetRequiredService<ISender>();
                    var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                    // 清理之前的数据
                    await ClearDataAsync(sender, readDbContext);

                    // 创建测试数据
                    await CreateTestDataAsync(sender);

                    Console.WriteLine($"Test environment setup completed. ");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to setup test environment: {ex.Message}");
            }
        }

        public async Task DisposeAsync()
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                // 额外清理所有测试数据
                await ClearDataAsync(sender, readDbContext);

                Console.WriteLine($"Test environment dispose completed.");
            });
        }

        private async Task CreateTestDataAsync(ISender sender)
        {
            var command = new CreateCategpryItemCommand()
            {
                CategoryTypeCode = CategoryTypeCode,
                OrderNumber = 1,
                DcItemName = $"TestItem_{Guid.NewGuid():N}",
                DcItemValue = "TestValue",//,
                Remarks = "Test"
            };
            await sender.Send(command);
        }

        private async Task ClearDataAsync(ISender _mediator, IReadDbContext readDbContext)
        {
            var olds = readDbContext.Query<DataCategoryItem>()
                .Where(i => i.CategoryTypeCode == CategoryTypeCode)
                .ToList();

            foreach (var item in olds)
            {
                await _mediator.Send(new DeleteCategpryItemCommand() { Id = item.Id });
            }
        }

        #region Helper Methods

        public async Task CreateTestItemAsync(CreateCategpryItemCommand command)
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                await sender.Send(command);
            });
        }

        public async Task<IEnumerable<DataCategoryItem>> GetTestItemsAsync()
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<DataCategoryItem>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<DataCategoryItem>()
                    .Where(i => i.CategoryTypeCode == CategoryTypeCode);
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<DataCategoryItem?> GetItemByIdAsync(long id)
        {
            return await _factory.ExecuteScopeAsync<DataCategoryItem?>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                var query = readDbContext.Query<DataCategoryItem>()
                    .Where(i => i.Id == id);
                return await readDbContext.FirstOrDefaultAsync(query);
            });
        }

        #endregion Helper Methods
    }
}