﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Xunit;
using Sgr.UPMS.Application.Commands.Duties;
using Sgr.UPMS.Application.ViewModels;
using Sgr.Domain.Entities;
using Sgr.Application.Queries;
using Sgr.UPMS.Domain.Duties;
using Sgr.Utilities;
using System.Text.Json;
using Xunit.Sgr.Tests;

namespace Xunit.Sgr.UPMS
{
    [Collection("Sgr.Admin")]
    public class DutyControllerTests : IClassFixture<DutyFixture>
    {
        private readonly DutyFixture _fixture;

        public DutyControllerTests(DutyFixture fixture)
        {
            _fixture = fixture;
        }

        #region 认证与授权测试

        /// <summary>
        /// 测试未授权访问 - 应返回401状态码
        /// </summary>
        [Theory]
        [InlineData("GET", "items/1")]           // 获取单个职务
        [InlineData("POST", "list")]             // 获取职务列表
        [InlineData("POST", "")]                 // 创建职务
        [InlineData("PUT", "")]                  // 更新职务
        [InlineData("DELETE", "99999")]               // 删除职务
        [InlineData("POST", "change-status")]    // 修改职务状态
        public async Task AccessWithoutAuthorization_AllEndpoints_ShouldReturnUnauthorized(string method, string endpoint)
        {
            // Arrange
            var client = _fixture.UnauthorizedClient;
            var request = new HttpRequestMessage(new HttpMethod(method), $"{DutyFixture.BaseUrl}/{endpoint}");

            // 对于 POST 和 PUT 请求，添加一个空的内容体
            if (method == "POST" || method == "PUT")
            {
                request.Content = JsonContent.Create(new { }, options: _fixture.JsonSerializerOptions);
            }

            // Act
            var response = await client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #endregion 认证与授权测试

        #region 查询测试

        /// <summary>
        /// 测试根据ID获取职务信息 - 有效ID场景
        /// </summary>
        [Fact]
        public async Task GetById_WithValidId_ShouldReturnDuty()
        {
            // Arrange
            var duties = await _fixture.GetTestDutiesAsync();
            var testDuty = duties.First();

            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DutyFixture.BaseUrl}/items/{testDuty.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<OutDutyViewModel>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(testDuty.Id, result.Id);
            Assert.Equal(testDuty.Name, result.Name);
        }

        /// <summary>
        /// 测试根据ID获取职务信息 - 无效ID场景
        /// </summary>
        [Theory]
        [InlineData(999999)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidId_ShouldReturnNotFound(long id)
        {
            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DutyFixture.BaseUrl}/items/{id}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        /// <summary>
        /// 测试获取职务列表
        /// </summary>
        [Fact]
        public async Task GetList_ShouldReturnDuties()
        {
            // Arrange
            var request = new InDutySearchModel
            {
                OrgId = _fixture.TestOrgId
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{DutyFixture.BaseUrl}/list", request, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<IEnumerable<OutDutyViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Contains(result, duty => duty.OrgId == _fixture.TestOrgId);
        }

        #endregion 查询测试

        #region CRUD测试

        /// <summary>
        /// 测试创建职务 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task CreateDuty_WithValidData_ShouldSucceed()
        {
            // Arrange
            var command = new CreateDutyCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDuty_{Guid.NewGuid():N}",
                OrderNumber = 1,
                Remarks = "Initial Test Duty"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DutyFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var duties = await _fixture.GetTestDutiesAsync();
            Assert.Contains(duties, d => d.Name == command.Name);
        }

        /// <summary>
        /// 测试创建职务 - 无效数据场景
        /// </summary>
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task CreateDuty_WithInvalidName_ShouldReturnBadRequest(string invalidName)
        {
            // Arrange
            var command = new CreateDutyCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = invalidName
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DutyFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// 测试更新职务 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task UpdateDuty_WithValidData_ShouldSucceed()
        {
            // Arrange
            var duties = await _fixture.GetTestDutiesAsync();
            var testDuty = duties.First();
            var newName = $"Updated_{Guid.NewGuid():N}";

            var command = new UpdateDutyCommand
            {
                DutyId = testDuty.Id,
                Name = newName,
                Remarks = "Updated Description"
            };

            // Act
            var response = await _fixture.AdminClient.PutAsJsonAsync(DutyFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedDuty = await _fixture.GetDutyByIdAsync(testDuty.Id);
            Assert.NotNull(updatedDuty);
            Assert.Equal(command.Name, updatedDuty.Name);
            Assert.Equal(command.Remarks, updatedDuty.Remarks);
        }

        /// <summary>
        /// 测试删除职务
        /// </summary>
        [Fact]
        public async Task DeleteDuty_WithValidId_ShouldSucceed()
        {
            // Arrange
            var command = new CreateDutyCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDuty_{Guid.NewGuid():N}"
            };
            await _fixture.CreateTestDutyAsync(command);

            var duties = await _fixture.GetTestDutiesAsync();
            var testDuty = duties.First(d => d.Name == command.Name);

            // Act
            var response = await _fixture.AdminClient.DeleteAsync($"{DutyFixture.BaseUrl}/{testDuty.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var deletedDuty = await _fixture.GetDutyByIdAsync(testDuty.Id);
            Assert.Null(deletedDuty);
        }

        /// <summary>
        /// 测试修改职务状态
        /// </summary>
        [Fact]
        public async Task ModifyDutyStatus_WithValidData_ShouldSucceed()
        {
            // Arrange
            var duties = await _fixture.GetTestDutiesAsync();
            var testDuty = duties.First();
            var newStatus = testDuty.State == EntityStates.Normal ? EntityStates.Deactivate : EntityStates.Normal;

            var command = new ModifyDutyStatusCommand
            {
                DutyId = testDuty.Id,
                State = newStatus
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{DutyFixture.BaseUrl}/change-status",
                command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedDuty = await _fixture.GetDutyByIdAsync(testDuty.Id);
            Assert.NotNull(updatedDuty);
            Assert.Equal(newStatus, updatedDuty.State);
        }

        #endregion CRUD测试

        #region 并发测试

        /// <summary>
        /// 测试并发删除和更新职务
        /// </summary>
        [Fact]
        public async Task ConcurrentDeleteAndUpdate_ShouldHandleRace()
        {
            // Arrange
            var command = new CreateDutyCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDuty_{Guid.NewGuid():N}"
            };
            await _fixture.CreateTestDutyAsync(command);

            var duties = await _fixture.GetTestDutiesAsync();
            var testDuty = duties.First(d => d.Name == command.Name);

            var updateCommand = new UpdateDutyCommand
            {
                DutyId = testDuty.Id,
                Name = $"Updated_{Guid.NewGuid():N}"
            };

            var deleteCommand = new DeleteDutyCommand { DutyId = testDuty.Id };

            // Act
            var deleteTask = _fixture.AdminClient.DeleteAsync($"{DutyFixture.BaseUrl}/{testDuty.Id}");
            var updateTask = _fixture.AdminClient.PutAsJsonAsync(DutyFixture.BaseUrl, updateCommand, _fixture.JsonSerializerOptions);

            await Task.WhenAll(deleteTask, updateTask);

            // Assert
            var finalDuty = await _fixture.GetDutyByIdAsync(testDuty.Id);
            if (deleteTask.Result.StatusCode == HttpStatusCode.OK)
            {
                Assert.Null(finalDuty);
            }
            else if (updateTask.Result.StatusCode == HttpStatusCode.OK)
            {
                Assert.NotNull(finalDuty);
                Assert.Equal(updateCommand.Name, finalDuty.Name);
            }
        }

        #endregion 并发测试

        #region 性能测试

        /// <summary>
        /// 测试大量数据查询性能
        /// </summary>
        [Fact]
        public async Task GetList_WithLargeDataset_ShouldPerformEfficiently()
        {
            // Arrange
            const int batchSize = 100;
            var tasks = new List<Task>();

            // 批量创建测试数据
            for (int i = 0; i < batchSize; i++)
            {
                var command = new CreateDutyCommand
                {
                    OrgId = _fixture.TestOrgId,
                    Name = $"PerformanceTest_Duty_{i}_{Guid.NewGuid():N}",
                    OrderNumber = i,
                    Remarks = $"Performance test duty {i}"
                };
                tasks.Add(_fixture.CreateTestDutyAsync(command));
            }
            await Task.WhenAll(tasks);

            var request = new InDutySearchModel
            {
                OrgId = _fixture.TestOrgId
            };

            // Act
            var startTime = DateTime.UtcNow;
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{DutyFixture.BaseUrl}/list",
                request,
                _fixture.JsonSerializerOptions);
            var endTime = DateTime.UtcNow;
            var duration = endTime - startTime;

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<IEnumerable<OutDutyViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.True(result.Count() >= batchSize);
            Assert.True(duration.TotalSeconds < 3, "查询时间超过3秒");
        }

        #endregion 性能测试
    }

    public class DutyFixture : IAsyncLifetime
    {
        public HttpClient UnauthorizedClient { get; private set; }
        public HttpClient AdminClient { get; private set; }
        public JsonSerializerOptions JsonSerializerOptions { get; }
        public const string BaseUrl = "api/v1/sgr/duty";
        public long TestOrgId { get; } = 1;

        private readonly SgrAdminWebApplicationFactory _factory;

        public DutyFixture(SgrAdminWebApplicationFactory factory)
        {
            _factory = factory;
            AdminClient = factory.CreateAdminHttpClient();
            UnauthorizedClient = factory.CreateUnauthorizedClient();
            JsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(JsonSerializerOptions);
        }

        public async Task InitializeAsync()
        {
            try
            {
                await _factory.ExecuteScopeAsync(async serviceProvider =>
                {
                    var sender = serviceProvider.GetRequiredService<ISender>();
                    var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                    // 清理现有数据
                    await ClearDataAsync(sender, readDbContext);

                    // 创建测试数据
                    await CreateTestDataAsync(sender);

                    Console.WriteLine("Duty test environment setup completed.");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to setup duty test environment: {ex.Message}");
            }
        }

        public async Task DisposeAsync()
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                await ClearDataAsync(sender, readDbContext);
                Console.WriteLine("Duty test environment cleanup completed.");
            });
        }

        #region Helper Methods

        public async Task<IEnumerable<Duty>> GetTestDutiesAsync()
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<Duty>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Duty>()
                    .Where(d => d.OrgId == TestOrgId);
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<Duty?> GetDutyByIdAsync(long id)
        {
            return await _factory.ExecuteScopeAsync<Duty?>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Duty>()
                    .Where(d => d.Id == id);
                return await readDbContext.FirstOrDefaultAsync(query);
            });
        }

        private async Task CreateTestDataAsync(ISender sender)
        {
            var command = new CreateDutyCommand
            {
                OrgId = TestOrgId,
                Name = $"TestDuty_{Guid.NewGuid():N}",
                OrderNumber = 1,
                Remarks = "Initial Test Duty"
            };
            await sender.Send(command);
        }

        private async Task ClearDataAsync(ISender sender, IReadDbContext readDbContext)
        {
            var duties = await GetTestDutiesAsync();
            foreach (var duty in duties)
            {
                await sender.Send(new DeleteDutyCommand { DutyId = duty.Id });
            }
        }

        public async Task CreateTestDutyAsync(CreateDutyCommand command)
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                await sender.Send(command);
            });
        }

        #endregion Helper Methods
    }
}