﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Xunit;
using Sgr.UPMS.Application.Commands.Departments;
using Sgr.UPMS.Application.ViewModels;
using Sgr.Domain.Entities;
using Sgr.Application.Queries;
using Sgr.UPMS.Domain.Departments;
using Sgr.Utilities;
using System.Text.Json;
using Xunit.Sgr.Tests;
using Sgr.UPMS.Application.Commands.Organizations;

namespace Xunit.Sgr.UPMS
{
    [Collection("Sgr.Admin")]
    public class DepartmentControllerTests : IClassFixture<DepartmentFixture>
    {
        private readonly DepartmentFixture _fixture;

        public DepartmentControllerTests(DepartmentFixture fixture)
        {
            _fixture = fixture;
        }

        #region 认证与授权测试

        /// <summary>
        /// 测试未授权访问 - 应返回401状态码
        /// </summary>
        [Theory]
        [InlineData("GET", "items/1")]           // 获取单个部门
        [InlineData("POST", "list")]             // 获取部门列表
        [InlineData("POST", "")]                 // 创建部门
        [InlineData("PUT", "")]                  // 更新部门
        [InlineData("DELETE", "1")]              // 删除部门
        public async Task AccessWithoutAuthorization_AllEndpoints_ShouldReturnUnauthorized(string method, string endpoint)
        {
            // Arrange
            var client = _fixture.UnauthorizedClient;
            var request = new HttpRequestMessage(new HttpMethod(method), $"{DepartmentFixture.BaseUrl}/{endpoint}");

            // 对于 POST 和 PUT 请求，添加一个空的内容体
            if (method == "POST" || method == "PUT")
            {
                request.Content = JsonContent.Create(new { }, options: _fixture.JsonSerializerOptions);
            }

            // Act
            var response = await client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #endregion 认证与授权测试

        #region 查询测试

        /// <summary>
        /// 测试根据ID获取部门信息 - 有效ID场景
        /// </summary>
        [Fact]
        public async Task GetById_WithValidId_ShouldReturnDepartment()
        {
            // Arrange
            var departments = await _fixture.GetTestDepartmentsAsync();
            var testDepartment = departments.First();

            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DepartmentFixture.BaseUrl}/items/{testDepartment.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<OutDepartmentViewModel>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(testDepartment.Id, result.Id);
            Assert.Equal(testDepartment.Name, result.Name);
        }

        /// <summary>
        /// 测试根据ID获取部门信息 - 无效ID场景
        /// </summary>
        [Theory]
        [InlineData(999999)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidId_ShouldReturnNotFound(long id)
        {
            // Act
            var response = await _fixture.AdminClient.GetAsync($"{DepartmentFixture.BaseUrl}/items/{id}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        /// <summary>
        /// 测试获取部门列表
        /// </summary>
        [Fact]
        public async Task GetList_ShouldReturnDepartments()
        {
            // Arrange
            var request = new InDepartmentSearchModel
            {
                OrgId = _fixture.TestOrgId,
                ParentId = null
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{DepartmentFixture.BaseUrl}/list", request, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<IEnumerable<OutDepartmentViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Contains(result, dept => dept.OrgId == _fixture.TestOrgId);
        }

        #endregion 查询测试

        #region CRUD测试

        /// <summary>
        /// 测试创建部门 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task CreateDepartment_WithValidData_ShouldSucceed()
        {
            // Arrange
            var command = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDept_{Guid.NewGuid():N}",
                OrderNumber = 100,
                Leader = "Test Leader",
                Phone = "13800138000",
                Email = "test@test.com",
                Remarks = "Test Department"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DepartmentFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var departments = await _fixture.GetTestDepartmentsAsync();
            Assert.Contains(departments, d => d.Name == command.Name);
        }

        /// <summary>
        /// 测试创建部门 - 无效数据场景
        /// </summary>
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task CreateDepartment_WithInvalidName_ShouldReturnBadRequest(string invalidName)
        {
            // Arrange
            var command = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = invalidName,
                OrderNumber = 100
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DepartmentFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// 测试更新部门 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task UpdateDepartment_WithValidData_ShouldSucceed()
        {
            // Arrange
            var departments = await _fixture.GetTestDepartmentsAsync();
            var testDepartment = departments.First();
            var newName = $"Updated_{Guid.NewGuid():N}";

            var command = new UpdateDepartmentCommand
            {
                DepartmentId = testDepartment.Id,
                Name = newName,
                OrderNumber = 200,
                Leader = "Updated Leader",
                Phone = "13900139000",
                Email = "updated@test.com",
                Remarks = "Updated Department"
            };

            // Act
            var response = await _fixture.AdminClient.PutAsJsonAsync(DepartmentFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedDepartment = await _fixture.GetDepartmentByIdAsync(testDepartment.Id);
            Assert.NotNull(updatedDepartment);
            Assert.Equal(command.Name, updatedDepartment.Name);
            Assert.Equal(command.Leader, updatedDepartment.Leader);
        }

        /// <summary>
        /// 测试删除部门
        /// </summary>
        [Fact]
        public async Task DeleteDepartment_WithValidId_ShouldSucceed()
        {
            // Arrange
            var command = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDept_{Guid.NewGuid():N}",
                OrderNumber = 100
            };
            await _fixture.CreateTestDepartmentAsync(command);

            var departments = await _fixture.GetTestDepartmentsAsync();
            var testDepartment = departments.First(d => d.Name == command.Name);

            // Act
            var response = await _fixture.AdminClient.DeleteAsync($"{DepartmentFixture.BaseUrl}/{testDepartment.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var deletedDepartment = await _fixture.GetDepartmentByIdAsync(testDepartment.Id);
            Assert.Null(deletedDepartment);
        }

        #endregion CRUD测试

        #region 树形结构测试

        /// <summary>
        /// 测试创建子部门
        /// </summary>
        [Fact]
        public async Task CreateChildDepartment_WithValidParent_ShouldSucceed()
        {
            // Arrange - 先创建父部门
            var parentCommand = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"ParentDept_{Guid.NewGuid():N}",
                OrderNumber = 100,
                ParentId = null
            };
            await _fixture.CreateTestDepartmentAsync(parentCommand);

            // 获取创建的父部门
            var departments = await _fixture.GetTestDepartmentsAsync();
            var parentDept = departments.First(d => d.Name == parentCommand.Name);

            // 创建子部门
            var childCommand = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"ChildDept_{Guid.NewGuid():N}",
                OrderNumber = 1,
                ParentId = parentDept.Id
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(DepartmentFixture.BaseUrl, childCommand, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var childDept = (await _fixture.GetTestDepartmentsAsync())
                .FirstOrDefault(d => d.Name == childCommand.Name);
            Assert.NotNull(childDept);
            Assert.Equal(parentDept.Id, childDept.ParentId);
            Assert.StartsWith(parentDept.NodePath, childDept.NodePath);
        }

        /// <summary>
        /// 测试查询子部门列表
        /// </summary>
        [Fact]
        public async Task GetList_WithParentId_ShouldReturnChildDepartments()
        {
            // Arrange - 创建父部门和多个子部门
            var parentCommand = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"ParentDept_{Guid.NewGuid():N}",
                OrderNumber = 100
            };
            await _fixture.CreateTestDepartmentAsync(parentCommand);

            var departments = await _fixture.GetTestDepartmentsAsync();
            var parentDept = departments.First(d => d.Name == parentCommand.Name);

            // 创建多个子部门
            for (int i = 1; i <= 3; i++)
            {
                var childCommand = new CreateDepartmentCommand
                {
                    OrgId = _fixture.TestOrgId,
                    Name = $"ChildDept_{i}_{Guid.NewGuid():N}",
                    OrderNumber = i,
                    ParentId = parentDept.Id
                };
                await _fixture.CreateTestDepartmentAsync(childCommand);
            }

            // Act - 查询子部门
            var request = new InDepartmentSearchModel
            {
                OrgId = _fixture.TestOrgId,
                ParentId = parentDept.Id
            };
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{DepartmentFixture.BaseUrl}/list", request, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<IEnumerable<OutDepartmentViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(3, result.Count());
            Assert.All(result, dept => Assert.Equal(parentDept.Id, dept.ParentId));
        }

        /// <summary>
        /// 测试级联删除部门
        /// </summary>
        [Fact]
        public async Task DeleteDepartment_WithCascade_ShouldDeleteAllChildren()
        {
            // Arrange - 创建父部门和子部门
            var parentCommand = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"ParentDept_{Guid.NewGuid():N}",
                OrderNumber = 100
            };
            await _fixture.CreateTestDepartmentAsync(parentCommand);

            var departments = await _fixture.GetTestDepartmentsAsync();
            var parentDept = departments.First(d => d.Name == parentCommand.Name);

            // 创建子部门
            var childCommand = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"ChildDept_{Guid.NewGuid():N}",
                OrderNumber = 1,
                ParentId = parentDept.Id
            };
            await _fixture.CreateTestDepartmentAsync(childCommand);

            // Act - 级联删除父部门
            var response = await _fixture.AdminClient.DeleteAsync($"{DepartmentFixture.BaseUrl}/{parentDept.Id}?IsCascade=true");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify - 验证父部门和子部门都已被删除
            departments = await _fixture.GetTestDepartmentsAsync();
            Assert.DoesNotContain(departments, d => d.Id == parentDept.Id);
            Assert.DoesNotContain(departments, d => d.ParentId == parentDept.Id);
        }

        #endregion 树形结构测试

        #region 并发测试

        /// <summary>
        /// 测试并发创建同名部门
        /// </summary>
        [Fact]
        public async Task ConcurrentCreateDepartments_WithSameName_ShouldHandleDuplicates()
        {
            // Arrange
            var name = $"TestDept_{Guid.NewGuid():N}";
            var command1 = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = name,
                OrderNumber = 100
            };

            var command2 = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = name,
                OrderNumber = 101
            };

            // Act
            var task1 = _fixture.AdminClient.PostAsJsonAsync(DepartmentFixture.BaseUrl, command1, _fixture.JsonSerializerOptions);
            var task2 = _fixture.AdminClient.PostAsJsonAsync(DepartmentFixture.BaseUrl, command2, _fixture.JsonSerializerOptions);

            var results = await Task.WhenAll(task1, task2);

            // Assert
            var successCount = results.Count(r => r.StatusCode == HttpStatusCode.OK);
            Assert.Equal(2, successCount);
        }

        /// <summary>
        /// 测试并发删除和更新部门
        /// </summary>
        [Fact]
        public async Task ConcurrentDeleteAndUpdate_ShouldHandleRace()
        {
            // Arrange
            var command = new CreateDepartmentCommand
            {
                OrgId = _fixture.TestOrgId,
                Name = $"TestDept_{Guid.NewGuid():N}",
                OrderNumber = 100
            };
            await _fixture.CreateTestDepartmentAsync(command);

            var departments = await _fixture.GetTestDepartmentsAsync();
            var testDepartment = departments.First(d => d.Name == command.Name);

            var updateCommand = new UpdateDepartmentCommand
            {
                DepartmentId = testDepartment.Id,
                Name = $"Updated_{Guid.NewGuid():N}",
                OrderNumber = 200
            };

            // Act
            var deleteTask = _fixture.AdminClient.DeleteAsync($"{DepartmentFixture.BaseUrl}/{testDepartment.Id}");
            var updateTask = _fixture.AdminClient.PutAsJsonAsync(DepartmentFixture.BaseUrl, updateCommand, _fixture.JsonSerializerOptions);

            await Task.WhenAll(deleteTask, updateTask);

            // Assert
            var finalDepartment = await _fixture.GetDepartmentByIdAsync(testDepartment.Id);
            if (deleteTask.Result.StatusCode == HttpStatusCode.OK)
            {
                Assert.Null(finalDepartment);
                Assert.NotEqual(HttpStatusCode.OK, updateTask.Result.StatusCode);
            }
            else if (updateTask.Result.StatusCode == HttpStatusCode.OK)
            {
                Assert.NotNull(finalDepartment);
                Assert.Equal(updateCommand.Name, finalDepartment.Name);
                Assert.NotEqual(HttpStatusCode.OK, deleteTask.Result.StatusCode);
            }
        }

        #endregion 并发测试
    }

    public class DepartmentFixture : IAsyncLifetime
    {
        /// <summary>
        /// 未授权访问客户端
        /// </summary>
        public HttpClient UnauthorizedClient { get; private set; }

        /// <summary>
        /// 管理员[已授权]访问客户端
        /// </summary>
        public HttpClient AdminClient { get; private set; }

        public JsonSerializerOptions JsonSerializerOptions { get; }
        public const string BaseUrl = "api/v1/sgr/department";
        public long TestOrgId { get; } = 1;

        private readonly SgrAdminWebApplicationFactory _factory;

        public DepartmentFixture(SgrAdminWebApplicationFactory factory)
        {
            _factory = factory;
            AdminClient = factory.CreateAdminHttpClient();
            UnauthorizedClient = factory.CreateUnauthorizedClient();
            JsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(JsonSerializerOptions);
        }

        public async Task InitializeAsync()
        {
            try
            {
                await _factory.ExecuteScopeAsync(async serviceProvider =>
                {
                    var sender = serviceProvider.GetRequiredService<ISender>();
                    var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                    // 清理现有数据
                    await ClearDataAsync(sender, readDbContext);

                    // 创建测试数据
                    await CreateTestDataAsync(sender);

                    Console.WriteLine("Department test environment setup completed.");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to setup department test environment: {ex.Message}");
            }
        }

        public async Task DisposeAsync()
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                await ClearDataAsync(sender, readDbContext);
                Console.WriteLine("Department test environment cleanup completed.");
            });
        }

        #region Helper Methods

        private async Task CreateTestDataAsync(ISender sender)
        {
            var command = new CreateDepartmentCommand
            {
                OrgId = TestOrgId,
                Name = $"TestDept_{Guid.NewGuid():N}",
                OrderNumber = 1,
                Leader = "Test Leader",
                Phone = "13800138000",
                Email = "test@test.com",
                Remarks = "Initial Test Department"
            };
            await sender.Send(command);
        }

        private async Task ClearDataAsync(ISender sender, IReadDbContext readDbContext)
        {
            var departments = await GetTestDepartmentsAsync();
            foreach (var dept in departments)
            {
                await sender.Send(new DeleteDepartmentCommand { DepartmentId = dept.Id, IsCascade = true });
            }
        }

        public async Task CreateTestDepartmentAsync(CreateDepartmentCommand command)
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                await sender.Send(command);
            });
        }

        public async Task<IEnumerable<Department>> GetTestDepartmentsAsync()
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<Department>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Department>()
                    .Where(d => d.OrgId == TestOrgId);
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<IEnumerable<Department>> GetChildDepartmentsAsync(long parentId)
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<Department>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Department>()
                    .Where(d => d.ParentId == parentId && d.OrgId == TestOrgId);
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<Department?> GetDepartmentByIdAsync(long id)
        {
            return await _factory.ExecuteScopeAsync<Department?>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Department>()
                    .Where(d => d.Id == id);
                return await readDbContext.FirstOrDefaultAsync(query);
            });
        }

        #endregion Helper Methods
    }
}