﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Xunit;
using Sgr.Application.ViewModels;
using Sgr.UPMS.Application.ViewModels;
using Sgr.Utilities;
using Xunit.Sgr.Tests;
using Sgr.UPMS.Domain.LogLogins;
using Sgr.Application.Queries;
using System.Text.Json;
using Sgr.EntityFrameworkCore;
using Sgr.Generator;

namespace Xunit.Sgr.UPMS
{
    [Collection("Sgr.Admin")]
    public class LogLoginControllerTests : IClassFixture<LogLoginFixture>
    {
        private readonly LogLoginFixture _fixture;

        public LogLoginControllerTests(LogLoginFixture fixture)
        {
            _fixture = fixture;
        }

        #region 认证与授权测试

        /// <summary>
        /// 测试未授权访问各个端点 - 应返回401状态码
        /// </summary>
        [Theory]
        [InlineData("GET", "items/1")]           // 获取单个登录日志
        [InlineData("POST", "list")]             // 获取登录日志分页列表
        public async Task AccessWithoutAuthorization_AllEndpoints_ShouldReturnUnauthorized(string method, string endpoint)
        {
            // Arrange
            var client = _fixture.UnauthorizedClient;
            var request = new HttpRequestMessage(new HttpMethod(method), $"{LogLoginFixture.BaseUrl}/{endpoint}");

            if (method == "POST")
            {
                request.Content = JsonContent.Create(new { }, options: _fixture.JsonSerializerOptions);
            }

            // Act
            var response = await client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #endregion 认证与授权测试

        #region 查询测试

        /// <summary>
        /// 测试根据ID获取登录日志 - 有效ID场景
        /// </summary>
        [Fact]
        public async Task GetById_WithValidId_ShouldReturnLogLogin()
        {
            // Arrange
            var logLogins = await _fixture.GetTestLogLoginsAsync();
            var testLog = logLogins.First();

            // Act
            var response = await _fixture.AdminClient.GetAsync($"{LogLoginFixture.BaseUrl}/items/{testLog.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<OutLogLoginViewModel>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(testLog.Id, result.Id);
            Assert.Equal(testLog.LoginName, result.LoginName);
            Assert.Equal(testLog.UserName, result.UserName);
            Assert.Equal(testLog.IpAddress, result.IpAddress);
            Assert.Equal(testLog.Status, result.Status);
        }

        /// <summary>
        /// 测试根据ID获取登录日志 - 无效ID场景
        /// </summary>
        [Theory]
        [InlineData(999999)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidId_ShouldReturnNotFound(long id)
        {
            // Act
            var response = await _fixture.AdminClient.GetAsync($"{LogLoginFixture.BaseUrl}/items/{id}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        /// <summary>
        /// 测试获取登录日志分页列表 - 基本分页场景
        /// </summary>
        [Fact]
        public async Task GetPagedList_BasicPaging_ShouldReturnCorrectItems()
        {
            // Arrange
            var request = new InLogLoginSearchModel
            {
                OrgId = _fixture.TestOrgId,
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{LogLoginFixture.BaseUrl}/list",
                request,
                _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutLogLoginViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.True(result.Total > 0);
            Assert.NotEmpty(result.Items);
            Assert.All(result.Items, item => Assert.Equal(_fixture.TestOrgId, item.OrgId));
        }

        /// <summary>
        /// 测试获取登录日志分页列表 - 按登录名筛选
        /// </summary>
        [Fact]
        public async Task GetPagedList_FilterByLoginName_ShouldReturnFilteredResults()
        {
            // Arrange
            var logLogins = await _fixture.GetTestLogLoginsAsync();
            var testLog = logLogins.First();
            var request = new InLogLoginSearchModel
            {
                OrgId = _fixture.TestOrgId,
                LoginName = testLog.LoginName,
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{LogLoginFixture.BaseUrl}/list",
                request,
                _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutLogLoginViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.All(result.Items, item => Assert.Equal(testLog.LoginName, item.LoginName));
        }

        /// <summary>
        /// 测试获取登录日志分页列表 - 按时间范围筛选
        /// </summary>
        [Fact]
        public async Task GetPagedList_FilterByTimeRange_ShouldReturnFilteredResults()
        {
            // Arrange
            var startTime = DateTimeOffset.UtcNow.AddHours(-3);
            var endTime = DateTimeOffset.UtcNow;
            var request = new InLogLoginSearchModel
            {
                OrgId = _fixture.TestOrgId,
                StartTime = startTime,
                EndTime = endTime,
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{LogLoginFixture.BaseUrl}/list",
                request,
                _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutLogLoginViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.All(result.Items, item =>
            {
                Assert.True(item.LoginTime >= startTime);
                Assert.True(item.LoginTime <= endTime);
            });
        }

        /// <summary>
        /// 测试获取登录日志分页列表 - 空结果场景
        /// </summary>
        [Fact]
        public async Task GetPagedList_WithNoResults_ShouldReturnEmptyList()
        {
            // Arrange
            var request = new InLogLoginSearchModel
            {
                OrgId = _fixture.TestOrgId,
                LoginName = "NonexistentUser",
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{LogLoginFixture.BaseUrl}/list",
                request,
                _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutLogLoginViewModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.Equal(0, result.Total);
        }

        #endregion 查询测试
    }

    public class LogLoginFixture : IAsyncLifetime
    {
        public HttpClient UnauthorizedClient { get; private set; }
        public HttpClient AdminClient { get; private set; }
        public JsonSerializerOptions JsonSerializerOptions { get; }
        public const string BaseUrl = "api/loglogin";
        public long TestOrgId { get; } = 1;

        private readonly SgrAdminWebApplicationFactory _factory;

        public LogLoginFixture(SgrAdminWebApplicationFactory factory)
        {
            _factory = factory;
            AdminClient = factory.CreateAdminHttpClient();
            UnauthorizedClient = factory.CreateUnauthorizedClient();
            JsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(JsonSerializerOptions);
        }

        public async Task InitializeAsync()
        {
            try
            {
                await _factory.ExecuteScopeAsync(async serviceProvider =>
                {
                    var dbContext = serviceProvider.GetRequiredService<SgrDbContext>();
                    var numberIdGenerator = serviceProvider.GetRequiredService<INumberIdGenerator>();

                    // 清理现有数据
                    await ClearDataAsync(dbContext);

                    // 创建测试数据
                    await CreateTestDataAsync(dbContext, numberIdGenerator);

                    Console.WriteLine("LogLogin test environment setup completed.");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to setup LogLogin test environment: {ex.Message}");
            }
        }

        public async Task DisposeAsync()
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var dbContext = serviceProvider.GetRequiredService<SgrDbContext>();
                await ClearDataAsync(dbContext);
                Console.WriteLine("LogLogin test environment cleanup completed.");
            });
        }

        #region Helper Methods

        public async Task<IEnumerable<LogLogin>> GetTestLogLoginsAsync()
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<LogLogin>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<LogLogin>()
                    .Where(l => l.OrgId == TestOrgId);
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<LogLogin?> GetLogLoginByIdAsync(long id)
        {
            return await _factory.ExecuteScopeAsync<LogLogin?>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<LogLogin>()
                    .Where(l => l.Id == id);
                return await readDbContext.FirstOrDefaultAsync(query);
            });
        }

        private async Task CreateTestDataAsync(SgrDbContext dbContext, INumberIdGenerator numberIdGenerator)
        {
            // 创建测试登录日志数据
            var testLogLogins = new List<LogLogin>
            {
                new LogLogin("testuser1",TestOrgId)
                {
                    Id = numberIdGenerator.GenerateUniqueId(),
                    UserName = "Test User 1",
                    LoginTime = DateTimeOffset.UtcNow.AddHours(-1),
                    IpAddress = "127.0.0.1",
                    Status = true,
                    Remark = "Test login 1"
                },
                new LogLogin("testuser2",TestOrgId)
                {
                    Id = numberIdGenerator.GenerateUniqueId(),
                    UserName = "Test User 2",
                    LoginTime = DateTimeOffset.UtcNow.AddHours(-2),
                    IpAddress = "127.0.0.2",
                    Status = false,
                    Remark = "Test login 2"
                }
            };

            await dbContext.AddRangeAsync(testLogLogins);
            await dbContext.SaveChangesAsync();
        }

        private async Task ClearDataAsync(SgrDbContext dbContext)
        {
            var logLogins = await GetTestLogLoginsAsync();
            dbContext.RemoveRange(logLogins);
            await dbContext.SaveChangesAsync();
        }

        #endregion Helper Methods
    }
}