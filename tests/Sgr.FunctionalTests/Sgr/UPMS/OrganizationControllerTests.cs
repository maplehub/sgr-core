﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Xunit;
using Sgr.UPMS.Application.Commands.Organizations;
using Sgr.UPMS.Application.ViewModels;
using Sgr.Domain.Entities;
using Sgr.Application.Queries;
using Sgr.UPMS.Domain.Organizations;
using Sgr.Utilities;
using System.Text.Json;
using Xunit.Sgr.Tests;
using Sgr.Application.ViewModels;
using Sgr.EntityFrameworkCore;

namespace Xunit.Sgr.UPMS
{
    [Collection("Sgr.Admin")]
    public class OrganizationControllerTests : IClassFixture<OrganizationFixture>
    {
        private readonly OrganizationFixture _fixture;

        public OrganizationControllerTests(OrganizationFixture fixture)
        {
            _fixture = fixture;
        }

        #region 认证与授权测试

        /// <summary>
        /// 测试未授权访问 - 应返回401状态码
        /// </summary>
        [Fact]
        public async Task AccessWithoutAuthorization_ShouldReturnUnauthorized()
        {
            // Act & Assert
            // 1. 查询接口
            var getResponse = await _fixture.UnauthorizedClient.GetAsync($"{OrganizationFixture.BaseUrl}/items/1");
            Assert.Equal(HttpStatusCode.Unauthorized, getResponse.StatusCode);

            var listResponse = await _fixture.UnauthorizedClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/list",
                new InOrganizationSearchModel(),
                _fixture.JsonSerializerOptions);
            Assert.Equal(HttpStatusCode.Unauthorized, listResponse.StatusCode);

            // 2. 创建接口
            var createResponse = await _fixture.UnauthorizedClient.PostAsJsonAsync(
                OrganizationFixture.BaseUrl,
                 new CreateOrgCommand(),
                _fixture.JsonSerializerOptions);
            Assert.Equal(HttpStatusCode.Unauthorized, createResponse.StatusCode);

            // 3. 修改接口
            var updateResponse = await _fixture.UnauthorizedClient.PutAsJsonAsync(
                OrganizationFixture.BaseUrl,
                new UpdateOrgCommand(),
                _fixture.JsonSerializerOptions);
            Assert.Equal(HttpStatusCode.Unauthorized, updateResponse.StatusCode);

            // 4. 注销接口
            var deleteResponse = await _fixture.UnauthorizedClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/cancellation",
                new CancellationOrgCommand(),
                _fixture.JsonSerializerOptions);
            Assert.Equal(HttpStatusCode.Unauthorized, deleteResponse.StatusCode);

            // 5. 绑定上级组织接口
            var associateResponse = await _fixture.UnauthorizedClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/associated_parent",
                new AssociatedParentOrgCommand(),
                _fixture.JsonSerializerOptions);
            Assert.Equal(HttpStatusCode.Unauthorized, associateResponse.StatusCode);
        }

        #endregion 认证与授权测试

        #region 查询测试

        /// <summary>
        /// 测试根据ID获取组织机构信息 - 有效ID场景
        /// </summary>
        [Fact]
        public async Task GetById_WithValidId_ShouldReturnOrganization()
        {
            // Arrange
            var organizations = await _fixture.GetTestOrganizationsAsync();
            var testOrg = organizations.First();

            // Act
            var response = await _fixture.AdminClient.GetAsync($"{OrganizationFixture.BaseUrl}/items/{testOrg.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<OutOrganizationViewModel>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.Equal(testOrg.Id, result.Id);
            Assert.Equal(testOrg.Name, result.Name);
        }

        /// <summary>
        /// 测试获取组织机构分页列表
        /// </summary>
        [Fact]
        public async Task GetPagedList_ShouldReturnOrganizations()
        {
            // Arrange
            var request = new InOrganizationSearchModel
            {
                PageIndex = 1,
                PageSize = 10
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync($"{OrganizationFixture.BaseUrl}/list", request, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutOrganizationPagedModel>>(_fixture.JsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
        }

        #endregion 查询测试

        #region CRUD测试

        /// <summary>
        /// 测试创建组织机构 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task CreateOrganization_WithValidData_ShouldSucceed()
        {
            // Arrange
            var command = new CreateOrgCommand
            {
                Name = $"TestOrg_{Guid.NewGuid():N}",
                OrderNumber = 100,
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                Leader = "Test Leader",
                Phone = "13800138000",
                Email = "test@test.com",
                Address = "Test Address",
                Remarks = "Test Organization",
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = $"password@1"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(OrganizationFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var organizations = await _fixture.GetTestOrganizationsAsync();
            Assert.Contains(organizations, o => o.Name == command.Name);
        }

        /// <summary>
        /// 测试创建组织机构 - 有效数据场景 [含有上级组织机构]
        /// </summary>
        [Fact]
        public async Task CreateOrganization_HasParentWithValidData_ShouldSucceed()
        {
            var organizations = await _fixture.GetTestOrganizationsAsync();
            var testOrg = organizations.First();

            // Arrange
            var command = new CreateOrgCommand
            {
                Name = $"TestOrg_{Guid.NewGuid():N}",
                OrderNumber = 100,
                StaffSizeCode = "",
                OrgTypeCode = "",
                AreaCode = "330100",
                Leader = "Test Leader",
                Phone = "13800138000",
                Email = "test@test.com",
                Address = "Test Address",
                Remarks = "Test Organization",
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = $"password@1",
                ParentId = testOrg.Id
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(OrganizationFixture.BaseUrl, command, _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            organizations = await _fixture.GetTestOrganizationsAsync();
            Assert.Contains(organizations, o => o.Name == command.Name);
        }

        /// <summary>
        /// 测试更新组织机构信息 - 有效数据场景
        /// </summary>
        [Fact]
        public async Task UpdateOrg_WithValidData_ShouldSucceed()
        {
            // Arrange
            var organizations = await _fixture.GetTestOrganizationsAsync();
            var testOrg = organizations.First();
            var command = new UpdateOrgCommand
            {
                Id = testOrg.Id,
                Name = $"Updated_{Guid.NewGuid():N}",
                StaffSizeCode = "StaffSizeCode",
                OrgTypeCode = "CORP",
                AreaCode = "AreaCode",
                Leader = "Updated Leader",
                Phone = "13900139000",
                Email = "updated@test.com",
                Address = "Updated Address"
            };

            // Act
            var response = await _fixture.AdminClient.PutAsJsonAsync(OrganizationFixture.BaseUrl, command);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedOrg = await _fixture.GetOrganizationByIdAsync(testOrg.Id);
            Assert.NotNull(updatedOrg);
            Assert.Equal(command.Name, updatedOrg.Name);
            Assert.Equal(command.Leader, updatedOrg.Leader);
        }

        /// <summary>
        /// 测试组织机构注册 - 匿名访问场景
        /// </summary>
        [Fact]
        public async Task RegisterOrganization_AnonymousAccess_ShouldSucceed()
        {
            // Arrange
            var command = new RegisterOrgCommand
            {
                Name = $"TestOrg_{Guid.NewGuid():N}",
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = $"password@1",
            };

            // Act
            var response = await _fixture.UnauthorizedClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/register",
                command,
                _fixture.JsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);
        }

        ///// <summary>
        ///// 测试注销组织机构 - 有效场景
        ///// </summary>
        //[Fact]
        //public async Task CancellationOrg_WithValidData_ShouldSucceed()
        //{
        //    // Arrange - 创建测试组织
        //    var createCommand = new CreateOrgCommand
        //    {
        //        Name = $"TestOrg_{Guid.NewGuid():N}",
        //        StaffSizeCode = "SIZE_100",
        //        OrgTypeCode = "CORP",
        //        AreaCode = "330100",
        //        OrderNumber = 100,
        //        AdminName = $"admin.{Guid.NewGuid():N}",
        //        AdminPassword = "password@1"
        //    };
        //    await _fixture.CreateTestOrganizationAsync(createCommand);

        //    var organizations = await _fixture.GetTestOrganizationsAsync();
        //    var testOrg = organizations.First(o => o.Name == createCommand.Name);

        //    var command = new CancellationOrgCommand
        //    {
        //        Id = testOrg.Id,
        //        Password = "password"
        //    };

        //    // Act
        //    var response = await _fixture.AdminClient.PostAsJsonAsync(
        //        $"{OrganizationFixture.BaseUrl}/cancellation",
        //        command,
        //        _fixture.JsonSerializerOptions);

        //    // Assert
        //    Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        //    var result = await response.Content.ReadFromJsonAsync<bool>();
        //    Assert.True(result);

        //    // Verify
        //    var cancelledOrg = await _fixture.GetOrganizationByIdAsync(testOrg.Id);
        //    Assert.Null(cancelledOrg);
        //}

        ///// <summary>
        ///// 测试注销组织机构 - 密码错误场景
        ///// </summary>
        //[Fact]
        //public async Task CancellationOrg_WithInvalidPassword_ShouldReturnBadRequest()
        //{
        //    // Arrange
        //    var organizations = await _fixture.GetTestOrganizationsAsync();
        //    var testOrg = organizations.First();

        //    var command = new CancellationOrgCommand
        //    {
        //        Id = testOrg.Id,
        //        Password = "wrongpassword"
        //    };

        //    // Act
        //    var response = await _fixture.AdminClient.DeleteAsync($"{OrganizationFixture.BaseUrl}");

        //    // Assert
        //    Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        //}

        #endregion CRUD测试

        #region 输入验证测试

        /// <summary>
        /// 测试创建组织机构 - 无效名称场景
        /// </summary>
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task CreateOrganization_WithInvalidName_ShouldReturnBadRequest(string invalidName)
        {
            // Arrange
            var command = new CreateOrgCommand
            {
                Name = invalidName,
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                OrderNumber = 100,
                AdminName = "admin",
                AdminPassword = "password@1"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(OrganizationFixture.BaseUrl, command);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// 测试更新组织机构 - 无效ID场景
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task UpdateOrg_WithInvalidId_ShouldReturnBadRequest(long invalidId)
        {
            // Arrange
            var command = new UpdateOrgCommand
            {
                Id = invalidId,
                Name = "TestOrg",
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "CORP",
                AreaCode = "330100"
            };

            // Act
            var response = await _fixture.AdminClient.PutAsJsonAsync(OrganizationFixture.BaseUrl, command);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.False(result);
        }

        #endregion 输入验证测试

        #region 树形结构测试

        /// <summary>
        /// 测试创建带有父组织的组织机构
        /// </summary>
        [Fact]
        public async Task CreateOrganization_WithParent_ShouldSucceed()
        {
            // Arrange - 先创建父组织
            var parentCommand = new CreateOrgCommand
            {
                Name = $"ParentOrg_{Guid.NewGuid():N}",
                StaffSizeCode = "SIZE_500",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                OrderNumber = 100,
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = "password@1"
            };
            await _fixture.CreateTestOrganizationAsync(parentCommand);

            var parentOrg = (await _fixture.GetTestOrganizationsAsync())
                .First(o => o.Name == parentCommand.Name);

            // 创建子组织
            var childCommand = new CreateOrgCommand
            {
                Name = $"ChildOrg_{Guid.NewGuid():N}",
                ParentId = parentOrg.Id,
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "DEPT",
                AreaCode = "330100",
                OrderNumber = 1,
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = "password@1"
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(OrganizationFixture.BaseUrl, childCommand);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var organizations = await _fixture.GetTestOrganizationsAsync();
            var childOrg = organizations.First(o => o.Name == childCommand.Name);
            Assert.Equal(parentOrg.Id, childOrg.ParentId);
        }

        /// <summary>
        /// 测试绑定上级组织机构
        /// </summary>
        [Fact]
        public async Task AssociateParentOrg_ShouldSucceed()
        {
            // Arrange
            var parentCommand = new CreateOrgCommand
            {
                Name = $"ParentOrg_{Guid.NewGuid():N}",
                StaffSizeCode = "SIZE_500",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                OrderNumber = 100,
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = "password@1"
            };
            await _fixture.CreateTestOrganizationAsync(parentCommand);

            var childCommand = new CreateOrgCommand
            {
                Name = $"ChildOrg_{Guid.NewGuid():N}",
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "DEPT",
                AreaCode = "330100",
                OrderNumber = 1,
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = "password@1"
            };
            await _fixture.CreateTestOrganizationAsync(childCommand);

            var organizations = await _fixture.GetTestOrganizationsAsync();
            var parentOrg = organizations.First(o => o.Name == parentCommand.Name);
            var childOrg = organizations.First(o => o.Name == childCommand.Name);

            var command = new AssociatedParentOrgCommand
            {
                Id = childOrg.Id,
                ParentId = parentOrg.Id
            };

            // Act
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/associated_parent",
                command);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);

            // Verify
            var updatedChild = await _fixture.GetOrganizationByIdAsync(childOrg.Id);
            Assert.NotNull(updatedChild);
            Assert.Equal(parentOrg.Id, updatedChild.ParentId);
        }

        /// <summary>
        /// 测试按父组织ID查询子组织
        /// </summary>
        [Fact]
        public async Task GetPagedList_FilterByParentId_ShouldReturnChildOrganizations()
        {
            // Arrange - 创建父组织和多个子组织
            var parentCommand = new CreateOrgCommand
            {
                Name = $"ParentOrg_{Guid.NewGuid():N}",
                StaffSizeCode = "SIZE_500",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                OrderNumber = 100,
                AdminName = $"admin.{Guid.NewGuid():N}",
                AdminPassword = "password@1"
            };
            await _fixture.CreateTestOrganizationAsync(parentCommand);

            var organizations = await _fixture.GetTestOrganizationsAsync();
            var parentOrg = organizations.First(o => o.Name == parentCommand.Name);

            // 创建多个子组织
            for (int i = 1; i <= 3; i++)
            {
                var childCommand = new CreateOrgCommand
                {
                    Name = $"ChildOrg_{i}_{Guid.NewGuid():N}",
                    ParentId = parentOrg.Id,
                    StaffSizeCode = "SIZE_100",
                    OrgTypeCode = "DEPT",
                    AreaCode = "330100",
                    OrderNumber = i,
                    AdminName = $"admin.{Guid.NewGuid():N}",
                    AdminPassword = "password@1"
                };
                await _fixture.CreateTestOrganizationAsync(childCommand);
            }

            // Act
            var request = new InOrganizationSearchModel
            {
                ParentId = parentOrg.Id,
                PageIndex = 1,
                PageSize = 10
            };
            var response = await _fixture.AdminClient.PostAsJsonAsync(
                $"{OrganizationFixture.BaseUrl}/list",
                request);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<PagedResponse<OutOrganizationPagedModel>>();
            Assert.NotNull(result);
            Assert.Equal(3, result.Total);
            Assert.All(result.Items, org => Assert.Equal(parentOrg.Id, org.ParentId));
        }

        #endregion 树形结构测试
    }

    public class OrganizationFixture : IAsyncLifetime
    {
        /// <summary>
        /// 未授权访问客户端
        /// </summary>
        public HttpClient UnauthorizedClient { get; private set; }

        /// <summary>
        /// 管理员[已授权]访问客户端
        /// </summary>
        public HttpClient AdminClient { get; private set; }

        public JsonSerializerOptions JsonSerializerOptions { get; }
        public const string BaseUrl = "api/v1/sgr/organization";

        private readonly SgrAdminWebApplicationFactory _factory;

        public OrganizationFixture(SgrAdminWebApplicationFactory factory)
        {
            _factory = factory;
            AdminClient = factory.CreateAdminHttpClient();
            UnauthorizedClient = factory.CreateUnauthorizedClient();
            JsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(JsonSerializerOptions);
        }

        public async Task InitializeAsync()
        {
            try
            {
                await _factory.ExecuteScopeAsync(async serviceProvider =>
                {
                    var sender = serviceProvider.GetRequiredService<ISender>();
                    var sgrDbContext = serviceProvider.GetRequiredService<SgrDbContext>();
                    var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();

                    // 清理现有数据
                    await ClearDataAsync(sgrDbContext, readDbContext);

                    // 创建测试数据
                    await CreateTestDataAsync(sender);

                    Console.WriteLine("Organization test environment setup completed.");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to setup organization test environment: {ex.Message}");
            }
        }

        public async Task DisposeAsync()
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var sgrDbContext = serviceProvider.GetRequiredService<SgrDbContext>();

                await ClearDataAsync(sgrDbContext, readDbContext);
                Console.WriteLine("Organization test environment cleanup completed.");
            });
        }

        #region Helper Methods

        private async Task CreateTestDataAsync(ISender sender)
        {
            var command = new CreateOrgCommand
            {
                Name = $"TestOrg_{Guid.NewGuid():N}",
                OrderNumber = 1,
                StaffSizeCode = "SIZE_100",
                OrgTypeCode = "CORP",
                AreaCode = "330100",
                Leader = "Test Leader",
                Phone = "13800138000",
                Email = "test@test.com",
                Address = "Test Address",
                Remarks = "Initial Test Organization"
            };
            await sender.Send(command);
        }

        private async Task ClearDataAsync(SgrDbContext dbContext, IReadDbContext readDbContext)
        {
            var organizations = await GetTestOrganizationsAsync();
            foreach (var org in organizations)
            {
                if (org.Name == SeedDataHelper.ORG_CODE)
                    continue;

                dbContext.Set<Organization>().Remove(org);
            }

            dbContext.SaveChanges();
        }

        public async Task CreateTestOrganizationAsync(CreateOrgCommand command)
        {
            await _factory.ExecuteScopeAsync(async serviceProvider =>
            {
                var sender = serviceProvider.GetRequiredService<ISender>();
                await sender.Send(command);
            });
        }

        public async Task<IEnumerable<Organization>> GetTestOrganizationsAsync()
        {
            return await _factory.ExecuteScopeAsync<IEnumerable<Organization>>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Organization>();
                return await readDbContext.ToListAsync(query);
            });
        }

        public async Task<Organization?> GetOrganizationByIdAsync(long id)
        {
            return await _factory.ExecuteScopeAsync<Organization?>(async serviceProvider =>
            {
                var readDbContext = serviceProvider.GetRequiredService<IReadDbContext>();
                var query = readDbContext.Query<Organization>()
                    .Where(o => o.Id == id);
                return await readDbContext.FirstOrDefaultAsync(query);
            });
        }

        #endregion Helper Methods
    }
}