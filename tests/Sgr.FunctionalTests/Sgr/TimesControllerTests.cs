﻿using Microsoft.AspNetCore.Mvc.Testing;
using NLog.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sgr.Tests;

namespace Xunit.Sgr
{
    [Collection("Sgr.Admin")]
    public class TimesControllerTests
    {
        private readonly HttpClient _client;

        public TimesControllerTests(SgrAdminWebApplicationFactory factory)
        {
            _client = factory.CreateUnauthorizedClient(false);
        }

        [Fact]
        public async Task GetUtcNow_ShouldReturnCorrectFormat()
        {
            // Act
            var response = await _client.GetAsync("/api/times/UtcNow");
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(result);
            Assert.Matches(@"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$", result);
        }

        [Fact]
        public async Task GetTimeZone_ShouldReturnLocalTimeZone()
        {
            // Act
            var response = await _client.GetAsync("/api/times/TimeZone");
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(TimeZoneInfo.Local.StandardName, result);
        }
    }
}