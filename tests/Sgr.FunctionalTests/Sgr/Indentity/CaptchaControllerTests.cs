﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using Xunit.Sgr.Tests;

namespace Xunit.Sgr.Indentity
{
    [Collection("Sgr.Admin")]
    public class CaptchaControllerTests
    {
        private readonly HttpClient _client;
        private const string BaseUrl = "api/v1/sgr/captcha";

        public CaptchaControllerTests(SgrAdminWebApplicationFactory factory)
        {
            _client = factory.CreateUnauthorizedClient(false);
        }

        [Fact]
        public async Task GetCaptcha_ShouldReturnValidImage()
        {
            // Act
            var response = await _client.GetAsync(BaseUrl);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("image/jpeg", response.Content.Headers.ContentType?.MediaType);
            Assert.True(response.Content.Headers.ContentLength > 0);
        }

        [Fact]
        public async Task GetCaptcha_ShouldIncludeValidCaptchaHeader()
        {
            // Act
            var response = await _client.GetAsync(BaseUrl);

            // Assert
            Assert.True(response.Headers.Contains("sgr-captcha"));
            var captchaHeader = response.Headers.GetValues("sgr-captcha").FirstOrDefault();
            Assert.NotNull(captchaHeader);
            Assert.True(captchaHeader.Length > 0);
        }

        [Fact]
        public async Task GetCaptcha_ShouldReturnDifferentCaptchasOnConsecutiveCalls()
        {
            // Act
            var response1 = await _client.GetAsync(BaseUrl);
            var response2 = await _client.GetAsync(BaseUrl);

            // Assert
            var captcha1 = response1.Headers.GetValues("sgr-captcha").FirstOrDefault();
            var captcha2 = response2.Headers.GetValues("sgr-captcha").FirstOrDefault();

            Assert.NotNull(captcha1);
            Assert.NotNull(captcha2);
            Assert.NotEqual(captcha1, captcha2);
        }
    }
}