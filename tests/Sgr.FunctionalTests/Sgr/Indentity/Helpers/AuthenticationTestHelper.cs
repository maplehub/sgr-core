using System;
using System.Security.Cryptography;
using System.Text;
using Sgr.Indentity.Application.Commands;
using Sgr.Indentity.Utilities;
using Sgr.Utilities;

namespace Xunit.Sgr.Indentity.Helpers
{
    internal static class AuthenticationTestHelper
    {
        public static string GenerateTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
        }

        public static string GenerateNonce()
        {
            return Guid.NewGuid().ToString("N");
        }

        public static string GenerateSignature(string timestamp, string nonce, string customParameters)
        {
            return HashHelper.CreateMd5($"sgr-{timestamp}-{nonce}-{customParameters}-sgr");
        }

        /// <summary>
        /// 创建管理员登录命令
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserLoginCommand CreateAdminLoginCommand(string password = "")
        {
            return CreateLoginCommand(SeedDataHelper.USERNAME_ADMIN, password);
        }

        /// <summary>
        ///  创建普通用户登录命令
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserLoginCommand CreateCustomLoginCommand(string password = "")
        {
            return CreateLoginCommand(SeedDataHelper.USERNAME_CUSTOM, password);
        }

        /// <summary>
        /// 创建用户登录命令
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserLoginCommand CreateLoginCommand(string userName, string password = "")
        {
            var timestamp = GenerateTimestamp();
            var nonce = GenerateNonce();
            var name = userName;
            var verificationCode = "1234";

            if (string.IsNullOrEmpty(password))
                password = SeedDataHelper.PASSWORD;

            return new UserLoginCommand
            {
                Name = name,  // 使用测试环境中的默认用户
                Password = password,  // 使用测试环境中的默认密码
                Timestamp = timestamp,
                Nonce = nonce,
                Signature = GenerateSignature(timestamp, nonce, $"{name}-{password}"),
                VerificationCode = verificationCode,
                VerificationHash = CaptchaHelper.BuildCaptchaCode(verificationCode)
            };
        }

        public static RefreshTokenCommand CreateRefreshTokenCommand(string accessToken)
        {
            var timestamp = GenerateTimestamp();
            var nonce = GenerateNonce();

            return new RefreshTokenCommand
            {
                AccessToken = accessToken,
                Timestamp = timestamp,
                Nonce = nonce,
                Signature = GenerateSignature(timestamp, nonce, accessToken)
            };
        }

        public static LogoutCommand CreateLogoutCommand(string accessToken)
        {
            var timestamp = GenerateTimestamp();
            var nonce = GenerateNonce();

            return new LogoutCommand
            {
                AccessToken = accessToken,
                Timestamp = timestamp,
                Nonce = nonce,
                Signature = GenerateSignature(timestamp, nonce, accessToken)
            };
        }
    }
}