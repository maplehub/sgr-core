﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Sgr.Application.ViewModels;
using Sgr.ExceptionHandling;
using Sgr.Indentity.Application.ViewModels;
using Sgr.Utilities;
using Xunit.Sgr.Indentity.Helpers;
using Xunit.Sgr.Tests;

namespace Xunit.Sgr.Indentity
{
    [Collection("Sgr.Admin")]
    public class OAuthControllerTests
    {
        private readonly HttpClient _client;
        private readonly JsonSerializerOptions _jsonSerializerOptions;
        private const string BaseUrl = "api/v1/sgr/oauth";

        public OAuthControllerTests(SgrAdminWebApplicationFactory factory)
        {
            _client = factory.CreateClient();

            _jsonSerializerOptions = new JsonSerializerOptions();
            JsonHelper.UpdateJsonSerializerOptions(_jsonSerializerOptions);
        }

        [Fact]
        public async Task Token_WithValidCredentials_ShouldSucceed()
        {
            // Arrange
            var command = AuthenticationTestHelper.CreateCustomLoginCommand();

            //string xx = JsonHelper.SerializeObject(command);
            // Act
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/token", command, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotNull(result!.AccessToken);
            Assert.Equal("Bearer", result.TokenType);
        }

        [Fact]
        public async Task Token_WithInvalidCredentials_ShouldFail()
        {
            // Arrange
            var command = AuthenticationTestHelper.CreateCustomLoginCommand("wrong_password");

            // Act
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/token", command, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<ServiceErrorResponse>(_jsonSerializerOptions);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task Token_WithInvalidSignature_ShouldFail()
        {
            // Arrange
            var command = AuthenticationTestHelper.CreateCustomLoginCommand();
            command.Signature = "invalid_signature";

            // Act
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/token", command, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<ServiceErrorResponse>(_jsonSerializerOptions);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task RefreshToken_WithValidToken_ShouldSucceed()
        {
            // Arrange
            // 获取令牌
            var loginCommand = AuthenticationTestHelper.CreateCustomLoginCommand();
            var loginResponse = await _client.PostAsJsonAsync($"{BaseUrl}/token", loginCommand, _jsonSerializerOptions);
            var tokenResult = await loginResponse.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(tokenResult?.AccessToken);

            string refrashToken = loginResponse.GetCookieValue("refreshToken");
            Assert.NotEmpty(refrashToken);

            var refreshCommand = AuthenticationTestHelper.CreateRefreshTokenCommand(tokenResult.AccessToken);
            Thread.Sleep(1000);

            // Act
            _client.DefaultRequestHeaders.Add("Cookie", $"refreshToken={refrashToken}");
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/refresh-token", refreshCommand, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(result);
            Assert.NotNull(result!.AccessToken);
            Assert.Equal("Bearer", result.TokenType);
        }

        [Fact]
        public async Task RefreshToken_WithInvalidRefreshToken_ShouldFail()
        {
            // Arrange
            // 获取令牌
            var loginCommand = AuthenticationTestHelper.CreateCustomLoginCommand();
            var loginResponse = await _client.PostAsJsonAsync($"{BaseUrl}/token", loginCommand, _jsonSerializerOptions);
            var tokenResult = await loginResponse.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(tokenResult?.AccessToken);

            string refrashToken = loginResponse.GetCookieValue("refreshToken");
            Assert.NotEmpty(refrashToken);

            var refreshCommand = AuthenticationTestHelper.CreateRefreshTokenCommand(tokenResult.AccessToken);
            Thread.Sleep(1000);

            // Act
            _client.DefaultRequestHeaders.Add("Cookie", $"refreshToken=InvalidRefreshToken");
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/refresh-token", refreshCommand, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<ServiceErrorResponse>();
            Assert.NotNull(result);
        }

        [Fact]
        public async Task RefreshToken_WithInvalidToken_ShouldFail()
        {
            // Arrange
            // 获取令牌
            var loginCommand = AuthenticationTestHelper.CreateCustomLoginCommand();
            var loginResponse = await _client.PostAsJsonAsync($"{BaseUrl}/token", loginCommand, _jsonSerializerOptions);
            var tokenResult = await loginResponse.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(tokenResult?.AccessToken);

            string refrashToken = loginResponse.GetCookieValue("refreshToken");
            Assert.NotEmpty(refrashToken);

            var refreshCommand = AuthenticationTestHelper.CreateRefreshTokenCommand("InvalidToken");
            Thread.Sleep(1000);

            // Act
            _client.DefaultRequestHeaders.Add("Cookie", $"refreshToken={refrashToken}");
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/refresh-token", refreshCommand, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<ServiceErrorResponse>(_jsonSerializerOptions);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task Logout_WithValidToken_ShouldSucceed()
        {
            // Arrange
            // 获取令牌
            var loginCommand = AuthenticationTestHelper.CreateCustomLoginCommand();
            var loginResponse = await _client.PostAsJsonAsync($"{BaseUrl}/token", loginCommand, _jsonSerializerOptions);
            var tokenResult = await loginResponse.Content.ReadFromJsonAsync<TokenViewModel>(_jsonSerializerOptions);
            Assert.NotNull(tokenResult?.AccessToken);

            string refrashToken = loginResponse.GetCookieValue("refreshToken");
            Assert.NotEmpty(refrashToken);

            var logoutCommand = AuthenticationTestHelper.CreateLogoutCommand(tokenResult.AccessToken);

            // Act
            _client.DefaultRequestHeaders.Add("Cookie", $"refreshToken={refrashToken}");
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/logout", logoutCommand, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.True(result);
        }

        [Fact]
        public async Task Logout_WithInvalidToken_ShouldFail()
        {
            // Arrange
            var command = AuthenticationTestHelper.CreateLogoutCommand("invalid_token");

            // Act
            var response = await _client.PostAsJsonAsync($"{BaseUrl}/logout", command, _jsonSerializerOptions);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var result = await response.Content.ReadFromJsonAsync<bool>();
            Assert.False(result);
        }
    }
}