﻿using Microsoft.Extensions.DependencyInjection;
using Sgr.Caching;
using Sgr.Caching.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Sgr.Redis.Tests
{
    public class RedisCacheManagerTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _redisFixture;

        private const string key1 = "sgr.tests.key1";
        private const string value1 = "sgr";
        private const string value4 = "this is new string";

        private const string key2 = "sgr.tests.key2";
        private const int value2 = 9527;

        private const string key3 = "sgr.tests.key3";
        private Book value3 = new Book() { Name = "设计模式", PageNumber = 637, Price =98.0f };

        public RedisCacheManagerTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._redisFixture = redisFixture;
        }




        [Fact]
        public void CanSetGetRemove()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();

            // Act

            clearKeys(cacheManager);
            //set
            cacheManager.Set(key1, value1);
            cacheManager.Set(key2, value2);
            cacheManager.Set(key3, value3);

            //get
            var cv1 = cacheManager.Get<string>(key1, () =>
            {
                throw new Exception("无法从缓存中获取数据");
            });

            var cv2 = cacheManager.Get<int>(key2, () =>
            {
                throw new Exception("无法从缓存中获取数据");
            });

            var cv3 = cacheManager.Get<Book>(key3, () =>
            {
                throw new Exception("无法从缓存中获取数据");
            });

            Assert.Equal(value1, cv1);
            Assert.Equal(value2, cv2);
            Assert.NotNull(cv3);
            Assert.Equal(value3.Name, cv3.Name);
            Assert.Equal(value3.Price, cv3.Price);
            Assert.Equal(value3.PageNumber, cv3.PageNumber);

            clearKeys(cacheManager);
        }


        [Fact]
        public async Task CanGetOrAddAsync()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();

            clearKeys(cacheManager);
            // Act

            //get
            var cv1 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value1;
            });

            var cv2 = await cacheManager.GetAsync<int>(key2, () =>
            {
                return Task.FromResult(value2);
            });

            var cv3 = await cacheManager.GetAsync<Book>(key3, () =>
            {
                return Task.FromResult(value3);
            });

            Assert.Equal(value1, cv1);
            Assert.Equal(value2, cv2);
            Assert.NotNull(cv3);
            Assert.Equal(value3.Name, cv3.Name);
            Assert.Equal(value3.Price, cv3.Price);
            Assert.Equal(value3.PageNumber, cv3.PageNumber);

            //remove
            clearKeys(cacheManager);
        }

        [Fact]
        public void CanGetOrAdd()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();

            clearKeys(cacheManager);

            // get
            var cv1 = cacheManager.Get<string>(key1, () =>
            {
                return value1;
            });

            var cv2 = cacheManager.Get<int>(key2, () =>
            {
                return value2;
            });

            var cv3 = cacheManager.Get<Book>(key3, () =>
            {
                return value3;
            });

            Assert.Equal(value1, cv1);
            Assert.Equal(value2, cv2);
            Assert.NotNull(cv3);
            Assert.Equal(value3.Name, cv3.Name);
            Assert.Equal(value3.Price, cv3.Price);
            Assert.Equal(value3.PageNumber, cv3.PageNumber);

            //remove
            clearKeys(cacheManager);
        }


        [Fact]
        public void CanRemoveByPrefix()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();

            // Act

            clearKeys(cacheManager);
            //set
            cacheManager.Set(key1, value1);
            cacheManager.Set(key2, value2);
            cacheManager.Set(key3, value3);

            //remove
            cacheManager.RemoveByPrefix("sgr.tests");

            //check
            var cv4 = cacheManager.Get(key1, () => { return value4; });
            Assert.Equal(value4, cv4);

            //clear
            clearKeys(cacheManager);
        }

        [Fact]
        public async Task CanRemoveByPrefixAsync()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();

            // Act

            clearKeys(cacheManager);
            //set
            await cacheManager.SetAsync(key1, value1);
            await cacheManager.SetAsync(key2, value2);
            await cacheManager.SetAsync(key3, value3);

            //remove
            await cacheManager.RemoveByPrefixAsync("sgr.tests");

            //check
            var cv4 = cacheManager.Get(key1, () => { return value4; });
            Assert.Equal(value4, cv4);

            //clear
            clearKeys(cacheManager);
        }

        /// <summary>
        /// 绝对过期（AbsoluteExpiration）
        /// </summary>
        [Fact]
        public async Task AbsoluteExpiration()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();
            // Act

            //先确保缓存不存在
            await cacheManager.RemoveAsync(key1);

 
            //再写入缓存，缓存的绝对过期时间为2秒
            await cacheManager.SetAsync(key1, value1, new Caching.CacheEntryOptions()
            {
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(2)
            });

            //立即读取缓存，此时缓存中应该还是之前存入的数据
            var cv1 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value4;
            });
            Assert.Equal(value1, cv1);

            //等待2秒后再此读取数据，此时缓存中应该就是新数据了
            Thread.Sleep(2000);
            var cv2 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value4;
            }, new Caching.CacheEntryOptions()
            {
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(2)
            });
            Assert.Equal(value4, cv2);

        }

        /// <summary>
        /// 绝对过期(AbsoluteExpirationRelativeToNow)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task AbsoluteExpirationRelativeToNow()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();
            CacheEntryOptions cacheEntryOptions = new Caching.CacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(2)
            };
            // Act

            //先确保缓存不存在
            await cacheManager.RemoveAsync(key1);


            //再写入缓存，缓存的绝对过期时间为2秒
            await cacheManager.SetAsync(key1, value1, cacheEntryOptions);

            //立即读取缓存，此时缓存中应该还是之前存入的数据
            var cv1 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value4;
            });
            Assert.Equal(value1, cv1);

            //等待2秒后再此读取数据，此时缓存中应该就是新数据了
            Thread.Sleep(2000);
            var cv2 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value4;
            }, cacheEntryOptions);
            Assert.Equal(value4, cv2);

        }

        /// <summary>
        /// 滑动过期
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SlidingExpiration()
        {
            // Arrange
            var cacheManager = _redisFixture.ServiceProvider.GetRequiredService<ICacheManager>();
            CacheEntryOptions cacheEntryOptions = new Caching.CacheEntryOptions()
            {
                 SlidingExpiration = TimeSpan.FromSeconds(2)
            };
            // Act

            //先确保缓存不存在
            await cacheManager.RemoveAsync(key1);


            //再写入缓存，缓存的绝对过期时间为2秒
            await cacheManager.SetAsync(key1, value1, cacheEntryOptions);

            //每隔200毫秒读取一次（共15次），此时缓存中应该还是之前存入的数据

            for(int i = 0; i < 15; i++)
            {
                var cv1 = await cacheManager.GetAsync<string>(key1, () =>
                {
                    return value4;
                });
                Assert.Equal(value1, cv1);

                Thread.Sleep(200);
            }

            //等待2秒后再此读取数据，此时缓存中应该就是新数据了
            Thread.Sleep(2000);
            var cv2 = await cacheManager.GetAsync<string>(key1, () =>
            {
                return value4;
            }, cacheEntryOptions);
            Assert.Equal(value4, cv2);

        }



        private void clearKeys(ICacheManager cacheManager)
        {
            //remove
            cacheManager.Remove(key1);
            cacheManager.Remove(key2);
            cacheManager.Remove(key3);
        }
    }
}

// Arrange

// Act

// Assert