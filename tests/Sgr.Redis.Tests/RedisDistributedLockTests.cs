﻿/**************************************************************
 * 
 * 唯一标识：d56da830-bc6c-4c56-9d5f-dd89c1c05f0a
 * 命名空间：Sgr.Redis.Tests
 * 创建时间：2024/4/5 22:39:43
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.DistributedLock;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Sgr.Redis.Tests
{
    public class RedisDistributedLockTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _redisFixture;


        public RedisDistributedLockTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._redisFixture = redisFixture;
        }

        /// <summary>
        /// 同时启动10线程获取锁，仅只能有一个线程获得到锁
        /// </summary>
        [Fact]
        public void CanTryAcquireLock()
        {
            var lockKey = "SGR.LOCK";
            var timeout = TimeSpan.FromSeconds(1);
            var expiration = TimeSpan.FromSeconds(5);

            var distributedLock = _redisFixture.ServiceProvider.GetRequiredService<IDistributedLock>();
            Assert.NotNull(distributedLock);

            var actionCount = 0; 
            Parallel.For(0, 5, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, (index) =>
            {
                var locker = distributedLock.TryAcquireLock(lockKey, timeout, expiration);

                _output.WriteLine($"{DateTime.Now} : 线程 {index} 获取锁{ (locker==null ? "失败" : "成功") }");
                 
                if (locker != null)
                {
                    using(var acquiredLock = locker)
                    {
                        Assert.True(distributedLock.IsLockAcquired(lockKey));
                        Thread.Sleep(3000);
                        actionCount++;
                    }
                }
            });

            Assert.Equal(1, actionCount);
        }

        /// <summary>
        /// 同时启动10线程获取锁，仅只能有一个线程获得到锁
        /// </summary>
        [Fact]
        public async Task CanTryAcquireLockAsync()
        {
            var lockKey = "SGR.LOCK";
            var timeout = TimeSpan.FromSeconds(1);
            var expiration = TimeSpan.FromSeconds(5);

            var distributedLock = _redisFixture.ServiceProvider.GetRequiredService<IDistributedLock>();
            Assert.NotNull(distributedLock);

            var actionCount = 0;
            await Parallel.ForAsync(0, 5, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, async (index, token) =>
            {
                var locker = await distributedLock.TryAcquireLockAsync(lockKey, timeout, expiration);
                _output.WriteLine($"{DateTime.Now} : 线程 {index} 获取锁{(locker == null ? "失败" : "成功")}");

                if (locker != null)
                {
                    using (var acquiredLock = locker)
                    {
                        Assert.True(distributedLock.IsLockAcquired(lockKey));
                        Thread.Sleep(3000);
                        actionCount++;
                    }
                }
            });

            Assert.Equal(1, actionCount);
        }

        /// <summary>
        /// 同时启动10线程获取锁，未获取到则等待，直至获取成功
        /// </summary>
        [Fact]
        public void CanAcquireLock()
        {
            var lockKey = "SGR.LOCK";
            var timeout = TimeSpan.FromSeconds(1);
            var expiration = TimeSpan.FromSeconds(5);

            var distributedLock = _redisFixture.ServiceProvider.GetRequiredService<IDistributedLock>();
            Assert.NotNull(distributedLock);

            var actionCount = 0;
            DateTimeOffset start = DateTimeOffset.Now;
            Parallel.For(0, 5, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, (index) =>
            {
                var locker = distributedLock.AcquireLock(lockKey, expiration);
                _output.WriteLine($"{DateTime.Now} : 线程 {index} 获取锁{(locker == null ? "失败" : "成功")}");

                if (locker != null)
                {
                    using (var acquiredLock = locker)
                    {
                        Assert.True(distributedLock.IsLockAcquired(lockKey));
                        Thread.Sleep(1000);
                        actionCount++;
                    }
                }
            });

            var seconds = (DateTimeOffset.Now - start).TotalSeconds;

            Assert.Equal(5, actionCount);
            Assert.True(seconds > 5);
        }

        /// <summary>
        /// 同时启动10线程获取锁，未获取到则等待，直至获取成功
        /// </summary>
        [Fact]
        public async Task CanAcquireLockAsync()
        {
            var lockKey = "SGR.LOCK";
            var timeout = TimeSpan.FromSeconds(1);
            var expiration = TimeSpan.FromSeconds(5);

            var distributedLock = _redisFixture.ServiceProvider.GetRequiredService<IDistributedLock>();
            Assert.NotNull(distributedLock);

            var actionCount = 0;
            DateTimeOffset start = DateTimeOffset.Now;
            await Parallel.ForAsync(0, 5, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, async (index, token) =>
            {
                var locker = await distributedLock.AcquireLockAsync(lockKey, expiration);
                _output.WriteLine($"{DateTime.Now} : 线程 {index} 获取锁{(locker == null ? "失败" : "成功")}");

                if (locker != null)
                {
                    using (var acquiredLock = locker)
                    {
                        Assert.True(distributedLock.IsLockAcquired(lockKey));
                        Thread.Sleep(1000);
                        actionCount++;
                    }
                }
            });

            var seconds = (DateTimeOffset.Now - start).TotalSeconds;

            Assert.Equal(5, actionCount);
            Assert.True(seconds > 5);
        }


        /// <summary>
        /// 锁续期
        /// </summary>
        [Fact]
        public void CanLockRenewal()
        {
            var distributedLock = _redisFixture.ServiceProvider.GetRequiredService<IDistributedLock>();
            Assert.NotNull(distributedLock);

            var lockKey = "SGR.LOCK";
            var timeout = TimeSpan.FromSeconds(1);
            var expiration = TimeSpan.FromSeconds(3);

            var locker = distributedLock.TryAcquireLock(lockKey, timeout, expiration);
            if (locker is null)
                Assert.Fail("无法获取分布式锁");
            using (var acquiredLock = locker)
            {
                Assert.True(distributedLock.IsLockAcquired(lockKey));

                //等待4s，超过锁定时间
                Thread.Sleep(4000);

                //尝试再次获取锁，看能够获得锁（如果续期成功，那么此刻应该是无法获取锁的）
                var newLocker = distributedLock.TryAcquireLock(lockKey, timeout, expiration);
                Assert.Null(newLocker);
            }

            Thread.Sleep(4000);
        }
    }
}
