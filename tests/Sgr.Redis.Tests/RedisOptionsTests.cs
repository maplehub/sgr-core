﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Sgr.Redis.Tests
{
    public class RedisOptionsTests
    {
        private readonly TestFixture _redisFixture;

        public RedisOptionsTests()
        {
            this._redisFixture = new TestFixture();
        }

        [Fact]
        public void RedisOptionsReadFromJosn()
        {
            IOptions<RedisOptions> redisOptions = this._redisFixture.ServiceProvider.GetRequiredService<IOptions<RedisOptions>>();

            Assert.NotNull(redisOptions);
            Assert.NotNull(redisOptions.Value);
            Assert.Equal("localhost", redisOptions.Value.ConnectionString);
            Assert.True(redisOptions.Value.UseForceReconnectWhenConnectionException);
        }

        [Fact]
        public void GetCustomRedisOptionsRead()
        {
            // Arrange
            string instanceName = "sgr";
            string connectionString = "connectionString";
            bool useForceReconnectWhenConnectionException = false;

            IConfiguration configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(@"appsettings.json", false, false)
               .Build();

            ServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton(configuration);
            serviceCollection.AddSgrCore();
            serviceCollection.UseRedis(opt =>
            {
                opt.InstanceName = instanceName;
                opt.UseForceReconnectWhenConnectionException = useForceReconnectWhenConnectionException;
                opt.ConnectionString = connectionString;
            });
            var sp = serviceCollection.BuildServiceProvider();

            // Act
            IOptions<RedisOptions> redisOptions = sp.GetRequiredService<IOptions<RedisOptions>>();

            Assert.NotNull(redisOptions);
            Assert.NotNull(redisOptions.Value);
            Assert.Equal(instanceName, redisOptions.Value.InstanceName);
            Assert.Equal(connectionString, redisOptions.Value.ConnectionString);
            Assert.Equal(useForceReconnectWhenConnectionException, redisOptions.Value.UseForceReconnectWhenConnectionException);

        }

    }
}

// Arrange

// Act

// Assert