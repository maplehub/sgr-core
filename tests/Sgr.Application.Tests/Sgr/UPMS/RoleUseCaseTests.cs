﻿/**************************************************************
 *
 * 唯一标识：f283fba7-8add-46f9-bbe7-58b8bc1ee196
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/19 16:25:41
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Domain.Entities;
using Sgr.Tests.Attributes;
using Sgr.UPMS.Application.Commands.Roles;
using Sgr.UPMS.Domain.Roles;
using Xunit.Abstractions;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class RoleUseCaseTests
    {
        private readonly string role_name = "role_name";

        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;

        public RoleUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.ServiceProvider.GetRequiredService<IMediator>();
        }

        [Fact, TestPriority(10)]
        public async Task CreateRoleCommandTest()
        {
            await DeleteRoleCommandTest();

            var command = new CreateRoleCommand()
            {
                RoleName = role_name,
                OrderNumber = 1,
                OrgId = 999,
                Remarks = ""
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(15)]
        public async Task AllocateFunctionPermissionCommandTest()
        {
            Role? entity = await getAsync();

            if (entity != null)
            {
                var command = new AllocateFunctionPermissionCommand()
                {
                    RoleId = entity.Id,
                    FunctionPermissions = new string[]
                    {
                        "ABC","EFG","XYZ"
                    }
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        [Fact, TestPriority(20)]
        public async Task ModifyRoleStatusCommandTest()
        {
            Role? entity = await getAsync();

            if (entity != null)
            {
                var command = new ModifyRoleStatusCommand()
                {
                    RoleId = entity.Id,
                    State = EntityStates.Normal
                };
                Assert.True(await _mediator.Send(command));

                entity = await getAsync();
                Assert.NotNull(entity);
                Assert.Equal(entity.State, command.State);
            }
        }

        [Fact, TestPriority(30)]
        public async Task UpdateRoleCommandTest()
        {
            Role? entity = await getAsync();

            if (entity != null)
            {
                var command = new UpdateRoleCommand()
                {
                    RoleId = entity.Id,
                    RoleName = role_name,
                    State = EntityStates.Normal,
                    OrderNumber = 99,
                    Remarks = "Remarks"
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        [Fact, TestPriority(40)]
        public async Task GetRoleTest()
        {
            Role? entity = await getAsync(true);
            Assert.NotNull(entity);

            Assert.Equal(entity.RoleName, role_name);
            Assert.True(entity.Remarks == "Remarks");
            Assert.True(entity.OrderNumber == 99);
            Assert.True(entity.State == EntityStates.Normal);

            Assert.True(entity.AnyPermission(ResourceType.FunctionalPermission, "ABC"));
            Assert.True(entity.AnyPermission(ResourceType.FunctionalPermission, "EFG"));
            Assert.True(entity.AnyPermission(ResourceType.FunctionalPermission, "XYZ"));
        }

        [Fact, TestPriority(99)]
        public async Task DeleteRoleCommandTest()
        {
            Role? entity = await getAsync();

            if (entity != null)
            {
                var command = new DeleteRoleCommand()
                {
                    RoleId = entity.Id
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        private async Task<Role?> getAsync(bool loadRelationship = false)
        {
            var repository = _fixture.ServiceProvider.GetRequiredService<IRoleRepository>();
            string[] items;
            if (loadRelationship)
                items = new string[] { "Resources" };
            else
                items = new string[] { };

            var list = await repository.GetAllAsync(items);
            return list.FirstOrDefault(f => f.RoleName == role_name);
        }
    }
}