﻿/**************************************************************
 *
 * 唯一标识：d07bbbe2-a1d8-4637-97f1-882951f85cca
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/19 15:48:00
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.UPMS.Application.Commands.Duties;
using Sgr.UPMS.Domain.Duties;
using Xunit.Abstractions;
using Sgr.Domain.Entities;
using Sgr.Tests.Attributes;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class DutyUseCaseTests
    {
        private readonly string duty_name = "duty_name";

        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;

        public DutyUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.ServiceProvider.GetRequiredService<IMediator>();
        }

        [Fact, TestPriority(10)]
        public async Task CreateDutyCommandTest()
        {
            await DeleteDutyCommandTest();

            var command = new CreateDutyCommand()
            {
                Name = duty_name,
                OrderNumber = 1,
                OrgId = 999,
                Remarks = ""
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(20)]
        public async Task ModifyDutyStatusCommandTest()
        {
            Duty? entity = await getAsync();

            if (entity != null)
            {
                var command = new ModifyDutyStatusCommand()
                {
                    DutyId = entity.Id,
                    State = EntityStates.Normal
                };
                Assert.True(await _mediator.Send(command));

                entity = await getAsync();
                Assert.NotNull(entity);
                Assert.Equal(entity.State, command.State);
            }
        }

        [Fact, TestPriority(30)]
        public async Task UpdateDutyCommandTest()
        {
            Duty? entity = await getAsync();

            if (entity != null)
            {
                var command = new UpdateDutyCommand()
                {
                    DutyId = entity.Id,
                    Name = duty_name,
                    OrderNumber = 99,
                    Remarks = "Remarks"
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        [Fact, TestPriority(40)]
        public async Task GetDutyCommandTest()
        {
            Duty? entity = await getAsync();

            Assert.NotNull(entity);

            Assert.True(entity.Name == duty_name);
            Assert.True(entity.OrderNumber == 99);
            Assert.True(entity.Remarks == "Remarks");
        }

        [Fact, TestPriority(99)]
        public async Task DeleteDutyCommandTest()
        {
            Duty? entity = await getAsync();

            if (entity != null)
            {
                var command = new DeleteDutyCommand()
                {
                    DutyId = entity.Id
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        private async Task<Duty?> getAsync()
        {
            var repository = _fixture.ServiceProvider.GetRequiredService<IDutyRepository>();
            var list = await repository.GetAllAsync();
            return list.FirstOrDefault(f => f.Name == duty_name);
        }
    }
}