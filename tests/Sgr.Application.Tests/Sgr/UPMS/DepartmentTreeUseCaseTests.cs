﻿/**************************************************************
 *
 * 唯一标识：065b39f5-6f04-49dc-8281-4d43a407fcbc
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/19 22:17:18
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Tests.Attributes;
using Sgr.UPMS.Domain.Departments;
using Xunit.Abstractions;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class DepartmentTreeUseCaseTests
    {
        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IDepartmentManage _departmentManage;
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentTreeUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _departmentManage = _fixture.ServiceProvider.GetRequiredService<IDepartmentManage>();
            _departmentRepository = _fixture.ServiceProvider.GetRequiredService<IDepartmentRepository>();
        }

        //技术部
        //   开发部
        //      项目开发部
        //      产品开发部
        //   测试部
        //   运维部
        //市场部
        //   销售部
        //   公关部
        //综合部

        [Fact, TestPriority(10)]
        public async Task CreateDepartmentTreeTest()
        {
            Department department = await _departmentManage.CreateNewAsync("技术部", 0, "", "", "", "", 9527, 0);
            await _departmentRepository.InsertAsync(department);

            long dept_id = department.Id;

            department = await _departmentManage.CreateNewAsync("测试部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("运维部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("开发部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            dept_id = department.Id;

            department = await _departmentManage.CreateNewAsync("项目开发部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("产品开发部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("市场部", 0, "", "", "", "", 9527, 0);
            await _departmentRepository.InsertAsync(department);

            dept_id = department.Id;
            department = await _departmentManage.CreateNewAsync("销售部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("公关部", 0, "", "", "", "", 9527, dept_id);
            await _departmentRepository.InsertAsync(department);

            department = await _departmentManage.CreateNewAsync("综合部", 0, "", "", "", "", 9527, 0);
            await _departmentRepository.InsertAsync(department);

            await _departmentRepository.UnitOfWork.SaveEntitiesAsync();
        }

        [Fact, TestPriority(20)]
        public async Task GetDepartmentRootNodesTest()
        {
            var nodes = await _departmentRepository.GetRootNodesAsync();

            Assert.True(nodes.Count(f => f.Name == "技术部" || f.Name == "市场部" || f.Name == "综合部") == 3);
        }

        [Fact, TestPriority(21)]
        public async Task HasDepartmentChildNodesAsyncTest()
        {
            var nodes = await _departmentRepository.GetRootNodesAsync();
            var node = nodes.FirstOrDefault(f => f.Name == "技术部");

            Assert.NotNull(node);
            Assert.True(await _departmentRepository.HasChildNodesAsync(node!));
        }

        [Fact, TestPriority(22)]
        public async Task GetDepartmentChildNodesAsyncTest()
        {
            var nodes = await _departmentRepository.GetRootNodesAsync();
            var node = nodes.FirstOrDefault(f => f.Name == "技术部");

            Assert.NotNull(node);

            var sons = await _departmentRepository.GetChildNodesAsync(node!);
            Assert.True(sons.Count() == 3);
        }

        [Fact, TestPriority(23)]
        public async Task GetDepartmentChildNodesRecursionAsync()
        {
            var nodes = await _departmentRepository.GetRootNodesAsync();
            var node = nodes.FirstOrDefault(f => f.Name == "技术部");

            Assert.NotNull(node);

            var sons = await _departmentRepository.GetChildNodesRecursionAsync(node!);
            Assert.True(sons.Count() == 5);
        }

        [Fact, TestPriority(99)]
        public async Task RemoveDepartmentAsync()
        {
            var nodes = await _departmentRepository.GetRootNodesAsync();

            foreach (var node in nodes)
            {
                if (node.Name == "技术部" || node.Name == "市场部" || node.Name == "综合部")
                {
                    var sons = await _departmentRepository.GetChildNodesRecursionAsync(node!);

                    await _departmentRepository.BulkDeleteAsync(sons);
                    await _departmentRepository.DeleteAsync(node);
                }
            }

            await _departmentRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}