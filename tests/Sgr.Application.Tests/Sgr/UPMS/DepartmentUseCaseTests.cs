﻿/**************************************************************
 *
 * 唯一标识：8cb5dfa7-a3c5-41fc-a9a0-54854c07a588
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/19 16:19:50
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Tests.Attributes;
using Sgr.UPMS.Application.Commands.Departments;
using Sgr.UPMS.Domain.Departments;
using Xunit.Abstractions;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class DepartmentUseCaseTests
    {
        private readonly string department_name = "department_name";

        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;

        public DepartmentUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.ServiceProvider.GetRequiredService<IMediator>();
        }

        [Fact, TestPriority(10)]
        public async Task CreateDepartmentCommandTest()
        {
            await DeleteDepartmentCommandTest();

            var command = new CreateDepartmentCommand()
            {
                Name = department_name,
                OrderNumber = 1,
                OrgId = 999,
                Remarks = ""
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(30)]
        public async Task UpdateDepartmentCommandTest()
        {
            Department? entity = await getAsync();

            if (entity != null)
            {
                var command = new UpdateDepartmentCommand()
                {
                    DepartmentId = entity.Id,
                    Name = department_name,
                    OrderNumber = 99,
                    Remarks = "Remarks",
                    Email = "Email@163.com",
                    Leader = "Leader",
                    Phone = "Phone"
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        [Fact, TestPriority(40)]
        public async Task GetDepartmentCommandTest()
        {
            Department? entity = await getAsync();

            Assert.NotNull(entity);
            Assert.Equal(entity.Name, department_name);

            Assert.True(entity.OrderNumber == 99);
            Assert.True(entity.Email == "Email@163.com");
            Assert.True(entity.Leader == "Leader");
            Assert.True(entity.Phone == "Phone");
            Assert.True(entity.Remarks == "Remarks");
        }

        [Fact, TestPriority(99)]
        public async Task DeleteDepartmentCommandTest()
        {
            Department? entity = await getAsync();

            if (entity != null)
            {
                var command = new DeleteDepartmentCommand()
                {
                    DepartmentId = entity.Id
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        private async Task<Department?> getAsync()
        {
            var repository = _fixture.ServiceProvider.GetRequiredService<IDepartmentRepository>();
            var list = await repository.GetAllAsync();
            return list.FirstOrDefault(f => f.Name == department_name);
        }
    }
}