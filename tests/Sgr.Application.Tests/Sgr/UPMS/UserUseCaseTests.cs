﻿/**************************************************************
 *
 * 唯一标识：11fbed85-1432-46e7-ba60-c5c4ee0ef281
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/17 22:13:13
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.BackGroundTasks;
using Sgr.Domain.Repositories;
using Sgr.Identity.Services;
using Sgr.Tests.Attributes;
using Sgr.UPMS.Application.Commands.Users;
using Sgr.UPMS.Domain.Users;
using Sgr.Utilities;
using Xunit.Abstractions;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class UserUseCaseTests
    {
        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;
        private readonly IPasswordHashService _passwordHashService;

        private readonly string loginName = "SGR";

        public UserUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.Server.Services.GetRequiredService<IMediator>();
            _passwordHashService = _fixture.Server.Services.GetRequiredService<IPasswordHashService>();
        }

        [Fact, TestPriority(10)]
        public async Task CreateUserCommandTest()
        {
            await physicalDeleteAsync();

            //Create
            var command = new CreateUserCommand()
            {
                LoginName = loginName,
                UserName = "Xunit",
                LoginPassword = "qwert@123",
                DepartmentId = TestHelper.DepartmentId01,
                DutyIds = new long[] { TestHelper.DutyId01, TestHelper.DutyId02 },
                RoleIds = new long[] { TestHelper.RoleId01 }
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(20)]
        public async Task GetUserTest()
        {
            User? user = await getUserAsync(true);
            Assert.NotNull(user);
            Assert.Equal(user.LoginName, loginName);
            Assert.Equal(user.DepartmentId, TestHelper.DepartmentId01);

            Assert.True(user.Duties.Count(
                f => f.DutyId == TestHelper.DutyId01
                || f.DutyId == TestHelper.DutyId02
                || f.DutyId == TestHelper.DutyId03) == 2);

            Assert.True(user.Roles.Count(
                f => f.RoleId == TestHelper.RoleId01
                || f.RoleId == TestHelper.RoleId02
                || f.RoleId == TestHelper.RoleId03) == 1);
        }

        [Fact, TestPriority(30)]
        public async Task ModifyPasswordCommandTest()
        {
            User? user = await getUserAsync();

            if (user != null)
            {
                string newPassWord = "1qaz@WSX";
                var command = new ModifyPasswordCommand()
                {
                    OldPassword = _passwordHashService.HashPassword(TestHelper.PASSWORD),
                    NewPassword = newPassWord,
                    UserId = user.Id
                };
                Assert.True(await _mediator.Send(command));

                user = await getUserAsync();

                Assert.NotNull(user);
                Assert.True(user.CheckPassWord(_passwordHashService, newPassWord));
            }
        }

        [Fact, TestPriority(35)]
        public async Task ResetPasswordCommandTest()
        {
            User? user = await getUserAsync();

            if (user != null)
            {
                string passWord = "asdfgh@1234";
                var command = new ResetPasswordCommand()
                {
                    Password = _passwordHashService.HashPassword(passWord),
                    UserId = user.Id
                };
                Assert.True(await _mediator.Send(command));

                user = await getUserAsync();

                Assert.NotNull(user);
                Assert.True(user.CheckPassWord(_passwordHashService, passWord));
            }
        }

        [Fact, TestPriority(40)]
        public async Task ModifyUserCommandTest()
        {
            User? user = await getUserAsync();

            if (user != null)
            {
                var command = new ModifyUserCommand()
                {
                    UserId = user.Id,
                    UserEmail = "UserEmail",
                    QQ = "QQ",
                    UserName = loginName,
                    UserPhone = "UserPhone",
                    Wechat = "Wechat"
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        [Fact, TestPriority(99)]
        public async Task DeleteUserCommandTest()
        {
            User? user = await getUserAsync();

            if (user != null)
            {
                var command = new DeleteUserCommand()
                {
                    UserId = user.Id,
                    Password = _passwordHashService.HashPassword(TestHelper.PASSWORD)
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        private async Task<User?> getUserAsync(bool loadRelationship = false)
        {
            //Get
            var userRepository = _fixture.ServiceProvider.GetRequiredService<IUserRepository>();
            var user = await userRepository.GetByLoginNameAsync(loginName);

            if (user != null && loadRelationship)
            {
                await userRepository.CollectionAsync(user, f => f.Duties);
                await userRepository.CollectionAsync(user, f => f.Roles);
            }

            return user;
        }

        private async Task physicalDeleteAsync()
        {
            User? user = await getUserAsync();

            if (user != null)
            {
                var userRepository = _fixture.ServiceProvider.GetRequiredService<IUserRepository>();
                await userRepository.PhysicalDeleteAsync(user);
                await userRepository.UnitOfWork.SaveEntitiesAsync();
            }
        }
    }
}