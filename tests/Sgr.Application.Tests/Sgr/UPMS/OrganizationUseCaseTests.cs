﻿/**************************************************************
 *
 * 唯一标识：5987ac6d-3dad-4768-bf21-2be0d8f2212c
 * 命名空间：Xunit.Sgr.UPMS
 * 创建时间：2024/7/19 16:46:14
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.UPMS.Application.Commands.Organizations;
using Sgr.UPMS.Domain.Organizations;
using Sgr.Utilities;
using Xunit.Abstractions;
using Sgr.Domain.Repositories;
using Sgr.UPMS.Domain.Users;
using Sgr.Domain.Uow;
using Sgr.Tests.Attributes;
using Sgr.Identity.Services;

namespace Xunit.Sgr.UPMS
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class OrganizationUseCaseTests
    {
        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;
        private readonly IPasswordHashService _passwordHashService;

        private readonly string orgName = "ST";
        private readonly string adminName = "adminName";

        public OrganizationUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.ServiceProvider.GetRequiredService<IMediator>();
            _passwordHashService = _fixture.ServiceProvider.GetRequiredService<IPasswordHashService>();
        }

        [Fact, TestPriority(5)]
        public async Task RegisterOrgCommandTest()
        {
            await physicalDeleteAsync();

            //Create
            var command = new RegisterOrgCommand()
            {
                Name = orgName,
                StaffSizeCode = "StaffSizeCode",
                Remarks = "Remarks",
                AdminName = adminName,
                AdminPassword = "qwert@123"
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(10)]
        public async Task CreateOrganizationCommandTest()
        {
            await physicalDeleteAsync();

            //Create
            var command = new CreateOrgCommand()
            {
                Name = orgName,
                StaffSizeCode = "StaffSizeCode",
                Remarks = "Remarks",
                OrgTypeCode = "OrgTypeCode",
                AreaCode = "AreaCode",
                AdminName = adminName,
                AdminPassword = "qwert@123"
            };
            Assert.True(await _mediator.Send(command));
        }

        [Fact, TestPriority(35)]
        public async Task UpdateOrgCommandTest()
        {
            Organization? org = await getOrganizationAsync();

            if (org != null)
            {
                var command = new UpdateOrgCommand()
                {
                    Id = org.Id,
                    Name = orgName,
                    StaffSizeCode = "StaffSizeCode",
                    Remarks = "Remarks",
                    OrgTypeCode = "OrgTypeCode",
                    AreaCode = "AreaCode",
                    Address = "Address",
                    Email = "Email",
                    Leader = "Leader",
                    Phone = "Phone"
                };
                Assert.True(await _mediator.Send(command));

                org = await getOrganizationAsync();

                Assert.NotNull(org);
                Assert.Equal(org.Name, command.Name);
                Assert.Equal(org.StaffSizeCode, command.StaffSizeCode);
                Assert.Equal(org.Remarks, command.Remarks);
                Assert.Equal(org.OrgTypeCode, command.OrgTypeCode);
                Assert.Equal(org.AreaCode, command.AreaCode);
                Assert.Equal(org.Address, command.Address);
                Assert.Equal(org.Email, command.Email);
                Assert.Equal(org.Leader, command.Leader);
                Assert.Equal(org.Phone, command.Phone);
            }
        }

        [Fact, TestPriority(40)]
        public async Task AssociatedParentOrgCommandTest()
        {
            Organization? org = await getOrganizationAsync();

            if (org != null)
            {
                var command = new AssociatedParentOrgCommand()
                {
                    Id = org.Id,
                    ParentId = 1
                };
                Assert.True(await _mediator.Send(command));

                org = await getOrganizationAsync();

                Assert.NotNull(org);
                Assert.Equal(org.ParentId, command.ParentId);
            }
        }

        [Fact, TestPriority(60)]
        public async Task GetOrganizationTest()
        {
            Organization? org = await getOrganizationAsync();
            Assert.NotNull(org);
            Assert.Equal(org.Name, orgName);
            Assert.True(org.LogoUrl == "LogoObjectName");
            Assert.True(org.StaffSizeCode == "StaffSizeCode");
            Assert.True(org.Remarks == "Remarks");
            Assert.True(org.OrgTypeCode == "OrgTypeCode");
            Assert.True(org.AreaCode == "AreaCode");
            Assert.True(org.Address == "Address");
            Assert.True(org.Email == "Email");
            Assert.True(org.Leader == "Leader");
            Assert.True(org.Phone == "Phone");
            Assert.True(org.BusinessLicensePath == "BusinessLicenseObjectName");
            Assert.True(org.Code == "123456789");
            Assert.True(org.ParentId == 1);
        }

        [Fact, TestPriority(99)]
        public async Task CancellationOrgCommandTest()
        {
            Organization? org = await getOrganizationAsync();

            if (org != null)
            {
                var command = new CancellationOrgCommand()
                {
                    Id = org.Id,
                    Password = _passwordHashService.HashPassword(TestHelper.PASSWORD)
                };
                Assert.True(await _mediator.Send(command));
            }
        }

        private async Task<Organization?> getOrganizationAsync()
        {
            var repository = _fixture.ServiceProvider.GetRequiredService<IOrganizationRepository>();
            var list = await repository.GetAllAsync();
            return list.FirstOrDefault(f => f.Name == orgName);
        }

        private async Task physicalDeleteAsync()
        {
            var orgRepository = _fixture.ServiceProvider.GetRequiredService<IOrganizationRepository>();
            var userRepository = _fixture.ServiceProvider.GetRequiredService<IUserRepository>();
            var unitOfWork = _fixture.ServiceProvider.GetRequiredService<IUnitOfWork>();

            Organization? org = (await orgRepository.GetAllAsync()).FirstOrDefault(f => f.Name == orgName);
            User? user = (await userRepository.GetAllAsync()).FirstOrDefault(f => f.LoginName == adminName);

            if (org != null)
                await orgRepository.PhysicalDeleteAsync(org);

            if (user != null)
                await userRepository.PhysicalDeleteAsync(user);

            await unitOfWork.SaveEntitiesAsync();
        }
    }
}