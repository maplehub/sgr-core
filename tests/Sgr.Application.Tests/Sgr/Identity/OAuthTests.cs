﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Indentity.Application.Commands;
using Sgr.Tests.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Identity
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class OAuthTests
    {
        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;
        private readonly IMediator _mediator;

        public OAuthTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _mediator = _fixture.ServiceProvider.GetRequiredService<IMediator>();
        }

        [Fact, TestPriority(10)]
        public async Task UserLoginTest()
        {
            var command = new UserLoginCommand()
            {
                Name = "admin",
                Password = "123456"
            };

            var result = await _mediator.Send(command);
            Assert.True(result.Success);
        }
    }
}