﻿/**************************************************************
 *
 * 唯一标识：9f4e8c38-89c4-4462-9b4f-d31e43398526
 * 命名空间：Xunit
 * 创建时间：2024/7/21 13:51:39
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.Extensions.Hosting;
using Sgr.Data;
using Sgr.Domain.Uow;
using Sgr;
using Sgr.UPMS.Domain.Departments;
using Sgr.UPMS.Domain.Duties;
using Sgr.UPMS.Domain.Organizations;
using Sgr.UPMS.Domain.Roles;
using Sgr.UPMS.Domain.Users;
using Sgr.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Sgr.Identity.Services;

namespace Xunit.Sgr
{
    public static class TestHelper
    {
        public static string PASSWORD = "qwert@123";

        public static long DepartmentId01 = 6001;
        public static long DepartmentId02 = 6002;
        public static long DepartmentId03 = 6003;

        public static long DutyId01 = 7001;
        public static long DutyId02 = 7002;
        public static long DutyId03 = 7003;

        public static long RoleId01 = 8001;
        public static long RoleId02 = 8002;
        public static long RoleId03 = 8003;

        public static async Task InitData(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IDatabaseSeed databaseSeed = scope.ServiceProvider.GetRequiredService<IDatabaseSeed>();
                await databaseSeed.SeedAsync();

                //创建组织
                ICurrentUser currentUser = scope.ServiceProvider.GetRequiredService<ICurrentUser>();
                IUserChecker userChecker = scope.ServiceProvider.GetRequiredService<IUserChecker>();

                IUnitOfWork unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                IUserRepository userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                IOrganizationRepository organizationRepository = scope.ServiceProvider.GetRequiredService<IOrganizationRepository>();
                IDutyRepository dutyRepository = scope.ServiceProvider.GetRequiredService<IDutyRepository>();
                IDepartmentRepository departmentRepository = scope.ServiceProvider.GetRequiredService<IDepartmentRepository>();
                IRoleRepository roleRepository = scope.ServiceProvider.GetRequiredService<IRoleRepository>();
                IPasswordHashService passwordHashService = scope.ServiceProvider.GetRequiredService<IPasswordHashService>();

                long.TryParse(currentUser.Id, out long userId);
                long.TryParse(currentUser.OrgId, out long orgId);

                //创建组织
                var org = await organizationRepository.GetAsync(orgId);
                if (org == null)
                {
                    org = new Organization("TEST", "CODE", "测试专用");
                    org.Id = orgId;
                    await organizationRepository.InsertAsync(org);
                }

                //创建部门
                await createDept(departmentRepository, orgId, DepartmentId01);
                await createDept(departmentRepository, orgId, DepartmentId02);
                await createDept(departmentRepository, orgId, DepartmentId03);

                //创建职务
                await createDuty(dutyRepository, orgId, DutyId01);
                await createDuty(dutyRepository, orgId, DutyId02);
                await createDuty(dutyRepository, orgId, DutyId03);

                //创建角色
                await createRole(roleRepository, orgId, RoleId01);
                await createRole(roleRepository, orgId, RoleId02);
                await createRole(roleRepository, orgId, RoleId03);

                //创建用户
                var user = await userRepository.GetAsync(userId); ;
                if (user == null)
                {
                    string loginName = string.IsNullOrEmpty(currentUser.LoginName) ? "ADMIN" : currentUser.LoginName;
                    user = await User.CreateNewAdminAsync(loginName, passwordHashService.HashPassword(PASSWORD), orgId, userChecker);
                    user.Id = userId;
                    await userRepository.InsertAsync(user);
                }

                await unitOfWork.SaveEntitiesAsync();
            }
        }

        private static async Task createDept(IDepartmentRepository repository, long orgId, long deptId)
        {
            var entity = await repository.GetAsync(deptId);
            if (entity == null)
            {
                entity = new Department($"{deptId}_Core", $"{deptId}_NAME", 0, $"{deptId}_REMARK", orgId);
                entity.Id = deptId;
                await repository.InsertAsync(entity);
            }
        }

        private static async Task createDuty(IDutyRepository repository, long orgId, long dutyId)
        {
            var entity = await repository.GetAsync(dutyId);
            if (entity == null)
            {
                entity = new Duty($"{dutyId}_CODE", $"{dutyId}_NAME", 0, $"{dutyId}_REMARK", orgId);
                entity.Id = dutyId;
                await repository.InsertAsync(entity);
            }
        }

        private static async Task createRole(IRoleRepository repository, long orgId, long roleId)
        {
            var entity = await repository.GetAsync(roleId);
            if (entity == null)
            {
                entity = new Role($"{roleId}_CODE", $"{roleId}_NAME", orgId, 0, $"{roleId}_REMARK");
                entity.Id = roleId;
                await repository.InsertAsync(entity);
            }
        }
    }
}