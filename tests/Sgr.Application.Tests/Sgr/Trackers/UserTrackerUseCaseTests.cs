﻿/**************************************************************
 *
 * 唯一标识：f462130f-2add-4b38-919d-496eb328f9d6
 * 命名空间：Xunit.Sgr.Trackers
 * 创建时间：2024/7/21 22:19:09
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Azure.Core;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Sgr.EntityFrameworkCore;
using Sgr.Identity.Services;
using Sgr.MediatR;
using Sgr.Tests.Attributes;
using Sgr.Trackers;
using Sgr.Trackers.Domain;
using Sgr.Trackers.Services;
using Sgr.UPMS.Application.Commands.Users;
using Sgr.UPMS.Domain.Departments;
using Sgr.UPMS.Domain.Users;
using Sgr.UPMS.Events;
using Sgr.UPMS.Infrastructure.Checkers;
using Sgr.UPMS.Infrastructure.Repositories;
using Sgr.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Xunit.Sgr.Trackers
{
    [Collection("SgrApp")]
    [TestCaseOrderer(ordererTypeName: "Sgr.Tests.Orderers.PriorityOrderer", ordererAssemblyName: "Sgr.Tests.Infrastructure")]
    public class UserTrackerUseCaseTests : IDisposable
    {
        private readonly ITestOutputHelper _output;
        private readonly SgrAppTestFixture _fixture;

        private readonly IUserChecker _userChecker;
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHashService _passwordHashService;
        private readonly IMessageTracker _messageTracker;
        private readonly IMessageTrackerLogRepository _messageTrackerLogRepository;

        public UserTrackerUseCaseTests(ITestOutputHelper output, SgrAppTestFixture fixture)
        {
            this._output = output;
            this._fixture = fixture;

            _userChecker = _fixture.Server.Services.GetRequiredService<IUserChecker>();
            _userRepository = _fixture.Server.Services.GetRequiredService<IUserRepository>();
            _passwordHashService = _fixture.Server.Services.GetRequiredService<IPasswordHashService>();
            _messageTracker = _fixture.Server.Services.GetRequiredService<IMessageTracker>();
            _messageTrackerLogRepository = _fixture.Server.Services.GetRequiredService<IMessageTrackerLogRepository>();
        }

        [Fact, TestPriority(10)]
        public async Task UserMessageTrackerTest()
        {
            string msgId = Guid.NewGuid().ToString("N");

            //首次执行，将返回True
            Assert.True(await CreateUserWithTracker(msgId));

            //再次执行，将返回False
            Assert.False(await CreateUserWithTracker(msgId));

            //更好一个新的msgId后再次执行，将返回True
            msgId = Guid.NewGuid().ToString("N");
            Assert.True(await CreateUserWithTracker(msgId, 2));

            //等待标识失效后，清理标识，再次执行，将返回True
            await Task.Delay(2500);

            await _messageTrackerLogRepository.CleanupIneffectiveAsync();
            await _messageTrackerLogRepository.UnitOfWork.SaveEntitiesAsync();

            Assert.True(await CreateUserWithTracker(msgId));
        }

        [Fact]
        public async Task UserMessageTrackerConcurrencyTest()
        {
            int value = 0;
            string msgId = Guid.NewGuid().ToString("N");

            int count = 20;
            Task[] tasks = new Task[count];

            for (int i = 0; i < count; i++)
            {
                tasks[i] = Task.Run(async () =>
                {
                    _output.WriteLine($"{i},Thread Id = {Thread.CurrentThread.ManagedThreadId}");

                    if (await CreateUserWithTracker(msgId))
                        value++;
                });
            }

            await Task.WhenAll(tasks);

            Assert.True(value == 1);
        }

        private async Task<bool> CreateUserWithTracker(string msgId, int effectiveSecond = 604800)
        {
            try
            {
                if (await _messageTracker.HasProcessedAsync(msgId))
                    return false;

                User user = await User.CreateNewAsync(Guid.NewGuid().ToString(), _passwordHashService.HashPassword("qwert@123"), 9527, _userChecker);
                user.UserName = "Tracker";

                await _userRepository.InsertAsync(user);
                await _messageTracker.MarkAsProcessedAsync(msgId, effectiveSecond);
                await _userRepository.UnitOfWork.SaveEntitiesAsync();

                return true;
            }
            catch (Exception ex)
            {
                this._output.WriteLine(ex.ToString());
            }

            return false;
        }

        public void Dispose()
        {
            //清理测试数据
            SgrDbContext dbContext = _fixture.Server.Services.GetRequiredService<SgrDbContext>();
            dbContext.Set<User>().Where(f => f.UserName == "Tracker").ExecuteDelete();
        }
    }
}