﻿/**************************************************************
 *
 * 唯一标识：1a70c63a-374c-4eff-8395-12a6088d0e49
 * 命名空间：Xunit
 * 创建时间：2024/7/21 13:55:34
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using Microsoft.AspNetCore.Mvc.Testing;

namespace Xunit.Sgr
{
    [CollectionDefinition("SgrApp")]
    public class SgrAppCollectionFixture : ICollectionFixture<SgrAppTestFixture>
    {
    }
}