﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sgr.Tests
{
    public class SgrWebApplicationFactory<TProgram> : WebApplicationFactory<TProgram> where TProgram : class
    {
        protected override IHost CreateHost(IHostBuilder builder)
        {
            //https://github.com/dotnet-architecture/eShopOnWeb/issues/465

            var host = builder.Build();
            host.Start();

            PrepareHost(host);

            return host;
        }

        /// <summary>
        /// 完成测试前所需的准备工作
        /// </summary>
        /// <param name="host"></param>
        protected virtual void PrepareHost(IHost host)
        {
        }
    }
}