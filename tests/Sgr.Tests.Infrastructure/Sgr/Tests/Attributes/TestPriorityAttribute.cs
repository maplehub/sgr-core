﻿/**************************************************************
 *
 * 唯一标识：d2edc0a1-a291-4177-b088-e254797ccf76
 * 命名空间：Sgr.Tests.Attributes
 * 创建时间：2024/7/21 19:56:24
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 *
 **************************************************************/

using System;

namespace Sgr.Tests.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class TestPriorityAttribute : Attribute
    {
        public int Priority { get; private set; }

        public TestPriorityAttribute(int priority) => Priority = priority;
    }
}