﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Sgr.Generator.Tests.Sgr.Generator
{
    public class YitterIdGeneratorTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _redisFixture;

        public YitterIdGeneratorTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            this._output = output;
            this._redisFixture = redisFixture;
        }

        [Fact]
        public void YitterIdGeneratorGetIdTest()
        {
            ConcurrentDictionary<long, byte> data = new ConcurrentDictionary<long, byte>();

            var idGenerator = _redisFixture.ServiceProvider.GetRequiredService<INumberIdGenerator>();

            int threadCount = 10;
            int step = 1000;

            Parallel.For(0, threadCount, x =>
            {
                for (int i = 0; i < step; i++)
                {
                    Assert.True(data.TryAdd(idGenerator.GenerateUniqueId(), 0));
                }
            });

            Assert.True(data.Count() == threadCount * step);
        }

        [Fact]
        public void YitterIdGeneratorGetIdTimeConsumingTest()
        {
            var idGenerator = _redisFixture.ServiceProvider.GetRequiredService<INumberIdGenerator>();

            Stopwatch stopwatch = Stopwatch.StartNew();

            for (int i = 0; i < 40000; i++)
            {
                idGenerator.GenerateUniqueId();
            }

            stopwatch.Stop();

            _output.WriteLine($"获取4万个Id，耗时 {stopwatch.ElapsedMilliseconds} 毫秒");
        }
    }
}