﻿/**************************************************************
 * 
 * 唯一标识：904fe650-c28b-4743-bfe8-86483bd6fe44
 * 命名空间：Sgr.Generator.Tests
 * 创建时间：2024/6/21 17:24:31
 * 机器名称：DESKTOP-HJ4OAG9
 * 创建者：CocoYuan
 * 电子邮箱：fengqinhua2016@163.com
 * 描述：
 * 
 **************************************************************/

using Microsoft.Extensions.DependencyInjection;
using Sgr.Generator.Segment.Entities;
using Sgr.Generator.Segment.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Sgr.Generator.Tests.Sgr.Generator
{
    public class SegmentEntityDataStorageTests : IClassFixture<TestFixture>
    {
        private readonly ITestOutputHelper _output;
        private readonly TestFixture _redisFixture;

        public SegmentEntityDataStorageTests(ITestOutputHelper output, TestFixture redisFixture)
        {
            _output = output;
            _redisFixture = redisFixture;
        }

        [Fact]
        public async Task GetAndInsertSegmentsTest()
        {
            var segmentEntityDataStorage = _redisFixture.ServiceProvider.GetRequiredService<ISegmentEntityDataStorage>();

            IEnumerable<SegmentEntity> segments = await segmentEntityDataStorage.GetAllAsync();
            IEnumerable<string> keys = await segmentEntityDataStorage.GetIdsAsync();

            Assert.True(segments.Count() == keys.Count());

            foreach(var segment in segments )
            {
                Assert.Contains(segment.Id, keys);
            }

            string key = Guid.NewGuid().ToString();
            await segmentEntityDataStorage.InsertAsync(key, 2000, "just for test");

            keys = await segmentEntityDataStorage.GetIdsAsync();
            Assert.Contains(key, keys);
        }

        [Fact]
        public async Task UpdateMaxIdAndGetGenSegmentAsyncTest()
        {
            var segmentEntityDataStorage = _redisFixture.ServiceProvider.GetRequiredService<ISegmentEntityDataStorage>();

            IEnumerable<SegmentEntity> segments = await segmentEntityDataStorage.GetAllAsync();

            if(segments.Count() > 0)
            {
                SegmentEntity oldSegmentEntity = segments.First();

                SegmentEntity newSegmentEntity = await segmentEntityDataStorage.UpdateMaxIdAndGetGenSegmentAsync(oldSegmentEntity.Id);

                Assert.True(newSegmentEntity.MaxId - oldSegmentEntity.MaxId == oldSegmentEntity.Step);
            }
        }

        [Fact]
        public async Task UpdateMaxIdAndGetGenSegmentAsyncTest2()
        {
            var segmentEntityDataStorage = _redisFixture.ServiceProvider.GetRequiredService<ISegmentEntityDataStorage>();

            IEnumerable<SegmentEntity> segments = await segmentEntityDataStorage.GetAllAsync();

            if (segments.Count() > 0)
            {
                SegmentEntity oldSegmentEntity = segments.First();
                int step = 1000;
                SegmentEntity newSegmentEntity = await segmentEntityDataStorage.UpdateMaxIdAndGetGenSegmentAsync(oldSegmentEntity.Id, step);

                Assert.True(newSegmentEntity.MaxId - oldSegmentEntity.MaxId == step);
            }
        }
    }
}
